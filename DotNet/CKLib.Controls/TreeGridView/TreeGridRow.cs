﻿#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Controls.TreeGridView
{
    #region Using List
    using CKLib.Utilities.Diagnostics;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Linq;
    using System.Windows;
    #endregion

    #region Public Helper Classes
    /// <summary>
    /// A class which encapsulates the changes made to a TreeGridRow.
    /// </summary>
    public class TreeGridRowEventArgs : EventArgs
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the TreeGridRowEventArgs class.
        /// </summary>
        /// <param name="row">The row that changed.</param>
        public TreeGridRowEventArgs(TreeGridRow row)
        {
            Row = row;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the row that changed.
        /// </summary>
        public TreeGridRow Row
        {
            get;
            private set;
        }
        #endregion
    }
    #endregion

    /// <summary>
    /// This class serves as the binding point for a tree row node in a TreeGridView.
    /// </summary>
    public class TreeGridRow : INotifyPropertyChanged, IEnumerable<TreeGridRow>
    {
        #region Variables
        /// <summary>
        /// The row that is serving as the parent to this row.
        /// </summary>
        private TreeGridRow m_Parent;

        /// <summary>
        /// A collection of children to this row.
        /// </summary>
        private ObservableCollection<TreeGridRow> m_Children;

        /// <summary>
        /// The object that this row is representing.
        /// </summary>
        private object m_SelectedObject;

        /// <summary>
        /// Indicates whether this row is expanded or collapsed.
        /// </summary>
        private bool m_IsExpanded;

        /// <summary>
        /// A function used to retrieve child nodes.
        /// </summary>
        private Func<object, IEnumerable<object>> m_ChildAccessor;

        /// <summary>
        /// A reference to the parent tree grid.
        /// </summary>
        private TreeGrid m_TreeGrid;

        /// <summary>
        /// A value indicating whether this row has any child nodes.
        /// </summary>
        private bool m_HasChildren;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the TreeGridRow class.
        /// </summary>
        public TreeGridRow()
        {
            m_Children = new ObservableCollection<TreeGridRow>();
        }

        /// <summary>
        /// Initializes a new instance of the TreeGridRow class.
        /// </summary>
        /// <param name="parent">A row that serves as the parent for this row.</param>
        public TreeGridRow(TreeGridRow parent)
            : this()
        {
            Parent = parent;
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets the root tree grid that this row belongs to.
        /// </summary>
        public TreeGrid TreeGrid
        {
            get
            {
                if (m_TreeGrid == null && Parent != null)
                {
                    return Parent.TreeGrid;
                }

                return m_TreeGrid;
            }

            set
            {
                m_TreeGrid = value;
                OnPropertyChanged("TreeGrid");
            }
        }

        /// <summary>
        /// Gets the index of this row in the parent TreeGrid.
        /// If no TreeGrid could be located, -1 is returned.
        /// </summary>
        public int Index
        {
            get
            {
                if (TreeGrid != null)
                {
                    return TreeGrid.IndexOfRow(this);
                }

                return -1;
            }
        }

        /// <summary>
        /// Gets the immediate parent to this grid row.
        /// </summary>
        public TreeGridRow Parent
        {
            get
            {
                return m_Parent;
            }

            set
            {
                if (m_Parent != null)
                {
                    m_Parent.RemoveChild(this);
                }

                m_Parent = value;
                if (m_Parent != null)
                {
                    Level = m_Parent.Level + 1;
                    m_Parent.AddChild(this);
                }
                else
                {
                    Level = 0;
                }

                OnPropertyChanged("Parent");
                OnPropertyChanged("Level");
            }
        }

        /// <summary>
        /// Gets or sets the object that this row is representing.
        /// </summary>
        public object SelectedObject
        {
            get
            {
                return m_SelectedObject;
            }

            set
            {
                if (m_SelectedObject != value)
                {
                    m_SelectedObject = value;
                    CheckForChildren();
                    OnPropertyChanged("SelectedObject");
                    OnPropertyChanged("Children");
                }
            }
        }

        /// <summary>
        /// This property is used as a design time aide to convert the level to an indentation for the row.
        /// </summary>
        public Thickness Indent
        {
            get
            {
                return new Thickness(Level * 20, 1, 1, 1);
            }
        }

        /// <summary>
        /// Gets the "Level" or "Depth" in the tree hierarchy that this row resides.
        /// (How many generations from the root parent).
        /// </summary>
        public int Level
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this row is "expanded".
        /// When expanded, the Children property of this row will be filled with the child elements.
        /// When NOT expanded, the Children property of this row should be empty.
        /// </summary>
        public bool IsExpanded
        {
            get
            {
                if (Parent == null || Parent.IsExpanded)
                {
                    return m_IsExpanded;
                }

                return false;
            }

            set
            {
                if (m_IsExpanded != value)
                {
                    if (ExpandedChanging != null)
                    {
                        ExpandedChanging(this, new TreeGridRowEventArgs(this));
                    }

                    if (m_IsExpanded)
                    {
                        m_Children.Clear();
                    }

                    m_IsExpanded = value;

                    if (m_IsExpanded)
                    {
                        foreach (var property in ChildAccessor(m_SelectedObject))
                        {
                            CreateChildRow(property);
                        }
                    }

                    if (ExpandedChanged != null)
                    {
                        ExpandedChanged(this, new TreeGridRowEventArgs(this));
                    }
                }

                OnPropertyChanged("IsExpanded");
            }
        }

        /// <summary>
        /// Gets a value indicating whether this row produces any child rows.
        /// </summary>
        public bool HasChildren
        {
            get
            {
                return m_HasChildren;
            }
        }

        public Func<object, IEnumerable<object>> ChildAccessor
        {
            get
            {
                if (m_ChildAccessor == null)
                {
                    if (Parent != null)
                    {
                        return Parent.ChildAccessor;
                    }
                }

                return m_ChildAccessor;
            }

            set
            {
                m_ChildAccessor = value;
                CheckForChildren();
            }
        }

        /// <summary>
        /// Gets the collection of child rows for this row.
        /// </summary>
        public IEnumerable<TreeGridRow> Children
        {
            get
            {
                return m_Children;
            }
        }

        /// <summary>
        /// Gets the maximum depth level that the rows can be generated for.
        /// </summary>
        public bool IsMaxLevel
        {
            get
            {
                var grid = TreeGrid;
                if (grid != null)
                {
                    return Level >= grid.MaxLevel;
                }

                return true;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Creates a row that is a child of this row using the specified object.
        /// </summary>
        /// <param name="selectedObject">The object that the new row should display.</param>
        /// <returns>The new row.</returns>
        protected virtual TreeGridRow CreateChildRow(object selectedObject)
        {
            return new TreeGridRow(this)
            {
                SelectedObject = selectedObject
            };
        }

        /// <summary>
        /// Checks to see if this row has any potential child nodes.
        /// </summary>
        private void CheckForChildren()
        {
            try
            {
                bool children = !IsMaxLevel && (m_Children.Count > 0 ||
                        (ChildAccessor != null && SelectedObject != null &&
                        ChildAccessor(SelectedObject).FirstOrDefault() != null));
                if (m_HasChildren != children)
                {
                    m_HasChildren = children;
                    OnPropertyChanged("HasChildren");
                }
            }
            catch (Exception e)
            {
                Error.WriteEntry("Failed to retrieve child nodes.", e);
            }
        }

        /// <summary>
        /// Adds the specified child to the children collection of this row.
        /// This method is intended to be called by the child being added.
        /// </summary>
        /// <param name="child">The child to add.</param>
        private void AddChild(TreeGridRow child)
        {
            m_Children.Add(child);
            TreeGrid.HookRow(child);
        }

        /// <summary>
        /// Removed the specified child from the collection of children.
        /// This method is intended to be called by the child being removed.
        /// </summary>
        /// <param name="child">The child to remove.</param>
        private void RemoveChild(TreeGridRow child)
        {
            TreeGrid.UnhookRow(child);
            m_Children.Remove(child);
        }

        /// <summary>
        /// Called when the value of a property has changed.
        /// </summary>
        /// <param name="propertyName">The name of the property that changed.</param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Gets an object which can be used to iterate the child rows of this row.
        /// </summary>
        /// <returns>An object which can be used to iterate the child rows of this row.</returns>
        public IEnumerator<TreeGridRow> GetEnumerator()
        {
            return Children.GetEnumerator();
        }

        /// <summary>
        /// Gets an object which can be used to iterate the child rows of this row.
        /// </summary>
        /// <returns>An object which can be used to iterate the child rows of this row.</returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        #endregion

        #region Events
        /// <summary>
        /// Event raised when the value of one of the properties changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Event raised before the expanded property is changed.
        /// </summary>
        public event EventHandler<TreeGridRowEventArgs> ExpandedChanging;
        
        /// <summary>
        /// Event raised after the expanded property has been changed.
        /// </summary>
        public event EventHandler<TreeGridRowEventArgs> ExpandedChanged;
        #endregion
    }
}
