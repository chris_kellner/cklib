﻿#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Controls.ObjectViewer
{
    #region Using List
    using System;
    using System.ComponentModel;
    #endregion

    /// <summary>
    /// This class serves as arguments to the filter property event.
    /// </summary>
    public class PropertyFilterEventArgs : EventArgs
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the PropertyFilterEventArgs class.
        /// </summary>
        /// <param name="property">The property being filtered.</param>
        public PropertyFilterEventArgs(PropertyDescriptor property)
        {
            Property = property;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the property being filtered.
        /// </summary>
        public PropertyDescriptor Property
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not to reject the property.
        /// </summary>
        public bool RejectProperty
        {
            get;
            set;
        }
        #endregion
    }
}
