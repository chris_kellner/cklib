﻿#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Controls.Configuration
{
    #region Using List
    using CKLib.Utilities.DataStructures.Configuration;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.Windows;
    #endregion

    /// <summary>
    /// Interaction logic for ConfigurationWindow.xaml
    /// </summary>
    public partial class ConfigurationWindow : Window
    {
        #region Variables
        /// <summary>
        /// The set of configurations on display in this window.
        /// </summary>
        private ObservableCollection<ComponentConfiguration> m_Configurations;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ConfigurationWindow class.
        /// </summary>
        public ConfigurationWindow()
        {
            InitializeComponent();
            var maxSize = SystemParameters.WorkArea;
            m_Configurations = new ObservableCollection<ComponentConfiguration>();
            m_Configurations.CollectionChanged += m_Configurations_CollectionChanged;
            MaxHeight = maxSize.Height;
            MaxWidth = maxSize.Width;
            SizeToContent = System.Windows.SizeToContent.WidthAndHeight;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the set of configurations being shows on this window.
        /// </summary>
        public ObservableCollection<ComponentConfiguration> ConfigurationItems
        {
            get
            {
                return m_Configurations;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Called when the configuration collection has changed.
        /// </summary>
        /// <param name="sender">The configuration collection.</param>
        /// <param name="e">The arguments describing the change.</param>
        private void m_Configurations_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    AddCore(e.NewItems[0] as ComponentConfiguration);
                    break;
                case NotifyCollectionChangedAction.Remove:
                    RemoveCore(e.NewItems[0] as ComponentConfiguration);
                    break;
            }
        }

        /// <summary>
        /// Adds a configuration item to the editor.
        /// </summary>
        /// <param name="config">The item to add.</param>
        protected virtual void AddCore(ComponentConfiguration config)
        {
            if (config != null)
            {
                edit_Main.AddConfiguration(config);
            }
        }

        /// <summary>
        /// Removes a configuration item from the editor.
        /// </summary>
        /// <param name="config">The item to remove.</param>
        protected virtual void RemoveCore(ComponentConfiguration config)
        {
            if (config != null)
            {
                edit_Main.RemoveConfiguration(config);
            }
        }

        /// <summary>
        /// Called when the Save button is clicked.
        /// </summary>
        /// <param name="sender">The save button.</param>
        /// <param name="e">The event args.</param>
        private void btn_Save_Click(object sender, RoutedEventArgs e)
        {
            foreach (var config in m_Configurations)
            {
                config.Save();
            }

            Close();
        }
        #endregion
    }
}
