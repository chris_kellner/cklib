﻿#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Controls.Configuration
{
    #region Using List
    using CKLib.Utilities.DataStructures;
    using CKLib.Utilities.DataStructures.Configuration;
    using CKLib.Utilities.DataStructures.Configuration.View;
    using System;
    using System.Linq;
    using System.Windows.Controls;
    #endregion

    /// <summary>
    /// Interaction logic for ConfigurationEditor.xaml
    /// </summary>
    public partial class ConfigurationEditor : UserControl
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ConfigurationEditor class.
        /// </summary>
        public ConfigurationEditor()
        {
            InitializeComponent();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Adds the specified Component Configuration to the editor.
        /// </summary>
        /// <param name="config"></param>
        /// <exception cref="System.ArgumentNullException">Throws an exception if config is null.</exception>
        public void AddConfiguration(ComponentConfiguration config)
        {
            if (config == null)
            {
                throw new ArgumentNullException("config");
            }

            var grid = grid_Main.TreeGrid;
            var root = ReflectionNode.Create(config,
                System.Reflection.BindingFlags.DeclaredOnly,
                new Attribute[] { new ConfigurationEditorBrowsableAttribute(true) });
            int row = grid.AddRoot(root, r => ((ReflectionNode)r).Children);
            grid.SetRowExpandedState(row, true);
        }

        /// <summary>
        /// Removes the specified ComponentConfiguration from the editor.
        /// </summary>
        /// <param name="config"></param>
        public void RemoveConfiguration(ComponentConfiguration config)
        {
            if (config != null)
            {
                var grid = grid_Main.TreeGrid;
                var row = grid.Roots.FirstOrDefault(r =>
                {
                    var node = r.SelectedObject as ReflectionNode;
                    if (node != null)
                    {
                        return node.GetCachedValue() == config;
                    }

                    return false;
                });

                if (row != null)
                {
                    grid.RemoveRoot(row.SelectedObject);
                }
            }
        }
        #endregion
    }
}
