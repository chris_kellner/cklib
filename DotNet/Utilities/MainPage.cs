﻿
#region Usage Rights

#endregion
namespace CKLib.Utilities
{
    /// <summary>
    /// \mainpage CKLib Utilities Assembly
    /// \section copyright_sec Copyright and License
    /// Copyright (c) 2012, Christopher Kellner
    /// All rights reserved.
    ///
    /// Redistribution and use in source and binary forms, with or without
    /// modification, are permitted provided that the following conditions are met: 
    /// 
    /// 1. Redistributions of source code must retain the above copyright notice, this
    ///    list of conditions and the following disclaimer. 
    /// 2. Redistributions in binary form must reproduce the above copyright notice,
    ///    this list of conditions and the following disclaimer in the documentation
    ///    and/or other materials provided with the distribution. 
    /// 
    /// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    /// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    /// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    /// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    /// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    /// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    /// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    /// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    /// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    /// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    /// 
    /// \section getstarted_sec Getting Started
    /// The best place to start with the library would depend upon your needs, if you
    /// are looking for Inter-Process or Network Communication then start with the 
    /// <see cref="CKLib.Utilities.Connection.Server.Server"/> and <see cref="CKLib.Utilities.Connection.Client.Client"/>
    /// classes.
    /// 
    /// If you are looking for configuration setting management then head over to the
    /// <see cref="CKLib.Utilities.DataStructures.Configuration.ConfigurationManager"/> class.
    /// 
    /// If you are looking for some threadsafe data structures, check out the <see cref="CKLib.Utilities.DataStructures"/> namespace.
    /// 
    /// If you want to use the library for "Duck-Typing" or other types of proxy creation
    /// look for the <see cref="CKLib.Utilities.Proxy.ProxyFactory"/> class.
    /// 
    /// For a listing of the messages that are natively supported by this library look
    /// at the <see cref="CKLib.Utilities.Messages"/> namespace.
    /// </summary>
    public class MainPage
    {
    }
}