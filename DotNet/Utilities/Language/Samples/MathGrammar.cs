﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using CKLib.Utilities.Language;
using CKLib.Utilities.Language.Evaluator;
using CKLib.Utilities.Language.Parse;
using CKLib.Utilities.Language.Samples;
using CKLib.Utilities.Language.Lex;
using CKLib.Utilities.Language.Evaluator.Nodes;
using CKLib.Utilities.Language.Validation;

namespace CKLib.Utilities.Language
{
    public class MathGrammar : Grammar
    {
        public const string TN_OPERATION = "Operation",
            TN_OP_PLUS = "Op_Plus",
            TN_OP_MINUS = "Op_Minus",
            TN_OP_MULTIPLY = "Op_Multiply",
            TN_OP_DIVIDE = "Op_Divide",
            TN_OP_EXPONENT = "Op_Exponent";

        public MathGrammar()
        {
            Name = "CMath";
            CanEvaluate = true;
        }

        protected override void DefineTokens()
        {
            Terminal comment = CTokens.Comment(this);
            Terminal @string = CTokens.Text(this)
                .AddCodeSample("\"This is a string\"")
                .AddCodeSample("'C'")
                .AddCodeSample("\"This should \\r\\n be one string\"");
            #region Keywords
            var keywords = Terminal.MakeKeywords(this, new Dictionary<string, string>(){
                {"KW_IF", "if"},
                {"KW_ELSE", "else"},
                {"KW_WHILE", "while"},
                {"KW_DO","do"},
                {"KW_RETURN", "return"},
                {"KW_FOR", "for"},
                {"KW_FOREACH", "foreach"},
                {"KW_BREAK", "break"},
                {"KW_CONTINUE", "continue"},
                {"KW_TRY", "try"},
                {"KW_CATCH", "catch"},
                {"KW_FINALLY", "finally"},
                {"KW_USING", "using"},
                {"KW_GOTO", "goto"},
                {"KW_SWITCH", "switch"},
                {"KW_CASE", "case"},
                {"KW_DEFAULT", "default"},
                {"KW_NAMESPACE", "namespace"},
                {"KW_IMPORT", "import"},
                {"KW_TYPEOF", "typeof"},
                {"KW_THIS", "this"},
                {"KW_IN", "in"},
                {"KW_IS", "is"},
                {"KW_AS", "as"},
                {"KW_NEW", "new"}
            });
            #endregion
            Terminal @var = new Terminal(this, "KW_VAR", MatchType.Regex, @"\bvar\b");
            Terminal number = CTokens.Number(this);
            Terminal boolean = CTokens.Boolean(this);
            var literal = CTokens.Literal(this, @string, boolean, number);
            Terminal identifier = CTokens.Identifier(this);

            Terminal comparison = new Terminal(this)
            {
                Name = "Comparison",
                MatchType = Lex.MatchType.Group,
                SubTokens = new List<Terminal>(){
                    new Terminal(this, "Cmp_Equals", MatchType.Literal, "=="),
                    new Terminal(this, "Cmp_NotEquals", MatchType.Literal, "!="),
                    new Terminal(this, "Cmp_GreaterOrEquals", MatchType.Literal, ">="),
                    new Terminal(this, "Cmp_LessOrEquals", MatchType.Literal, "<="),
                    new Terminal(this, "Cmp_Greater", MatchType.Literal, ">"),
                    new Terminal(this, "Cmp_Less", MatchType.Literal, "<")
                }
            };

            Terminal lambda = new Terminal(this, "Op_Lambda", MatchType.Literal, "=>");
            Terminal assignment = new Terminal(this)
            {
                Name = "Op_Assignment",
                MatchType = MatchType.Group,
                SubTokens = new List<Terminal>()
                {
                    new Terminal(this)
                    {
                        Name = "Op_Assignment_Binary",
                        MatchType = MatchType.Group,
                        SubTokens = new List<Terminal>()
                        {
                            new Terminal(this, "Op_Assign", MatchType.Literal, "="),
                            new Terminal(this, "Op_Sub_Assign", MatchType.Literal, "-="),
                            new Terminal(this, "Op_Add_Assign", MatchType.Literal, "+="),
                            new Terminal(this, "Op_Mul_Assign", MatchType.Literal, "*="),
                            new Terminal(this, "Op_Or_Assign", MatchType.Literal, "|="),
                            new Terminal(this, "Op_And_Assign", MatchType.Literal, "&="),
                            new Terminal(this, "Op_Neg_Assign", MatchType.Literal, "~="),
                        }
                    },
                    new Terminal(this)
                    {
                        Name = "Op_Assignment_Unary",
                        MatchType = MatchType.Group,
                        SubTokens = new List<Terminal>()
                        {
                            new Terminal(this, "Op_Decrement", MatchType.Literal, "--"),
                            new Terminal(this, "Op_Increment", MatchType.Literal, "++"),
                        }
                    }
                }
            };

            #region Operation
            Terminal operation = new Terminal(this)
            {
                Name = TN_OPERATION,
                MatchType = MatchType.Group,
                SubTokens = new List<Terminal>()
                {
                    new Terminal(this){
                        Name = "Op_Boolean",
                        MatchType = MatchType.Group,
                        SubTokens = new List<Terminal>(){
                            new Terminal(this)
                            {
                                Name = "Op_Binary",
                                MatchType = MatchType.Group,
                                SubTokens = new List<Terminal>()
                                {
                                    new Terminal(this, "Op_And", MatchType.Literal, "&&"),
                                    new Terminal(this, "Op_Or", MatchType.Literal, "||")
                                }
                            },
                            new Terminal(this)
                            {
                                Name= "Op_Unary",
                                MatchType = MatchType.Group,
                                SubTokens = new List<Terminal>()
                                {
                                    new Terminal(this)
                                    {
                                        Name= "Op_Unary_Left",
                                        MatchType = MatchType.Group,
                                        SubTokens = new List<Terminal>()
                                        {
                                            new Terminal(this, "Op_Not", MatchType.Literal, "!"),
                                            new Terminal(this, "Op_Invert", MatchType.Literal, "~"),
                                        }
                                    }
                                }
                            }
                        }
                    },
                    new Terminal(this){
                        Name = "Op_Numeric",
                        MatchType = MatchType.Group,
                        SubTokens = new List<Terminal>(){
                            //new Terminal(this, TN_OP_EXPONENT, MatchType.Literal, "**"),
                            new Terminal(this, TN_OP_PLUS, MatchType.Literal, "+"),
                            new Terminal(this, TN_OP_MINUS, MatchType.Literal, "-"),
                            new Terminal(this, TN_OP_MULTIPLY, MatchType.Literal, "*"),
                            new Terminal(this, TN_OP_DIVIDE, MatchType.Literal, "/"),
                            new Terminal(this)
                            {
                                Name = "Op_Bitwise",
                                MatchType = MatchType.Group,
                                SubTokens = new List<Terminal>()
                                {
                                    new Terminal(this, "Op_Bitwise_And", MatchType.Literal, "&"),
                                    new Terminal(this, "Op_Bitwise_Or", MatchType.Literal, "|"),
                                    new Terminal(this, "Op_Bitwise_Xor", MatchType.Literal, "^"),
                                }
                            }
                        }
                    },
                    new Terminal(this){
                        Name="Op_Flow",
                        MatchType = MatchType.Group,
                        SubTokens = new List<Terminal>(){
                            new Terminal(this, "coalesce", MatchType.Literal, "??"),
                            new Terminal(this, "dot", MatchType.Literal, "."),
                            new Terminal(this, "comma", MatchType.Literal, ","),
                            new Terminal(this, "colon", MatchType.Literal, ":"),
                            new Terminal(this, "question", MatchType.Literal, "?")
                        }
                    }
                }
            };
            #endregion
            Terminal eos = new Terminal(this, "EOS", MatchType.Literal, ";");
            Terminal parens = new Terminal(this)
            {
                Name = "Parentheses",
                MatchType = MatchType.Group,
                NonExecutable = true,
                SubTokens = new List<Terminal>()
                {
                    new Terminal(this, "Paren_Open", MatchType.Literal,"("),
                    new Terminal(this, "Paren_Close", MatchType.Literal,")")
                }
            };

            Terminal braces = new Terminal(this)
            {
                Name = "Braces",
                MatchType = MatchType.Group,
                NonExecutable = true,
                SubTokens = new List<Terminal>()
                {
                    new Terminal(this, "Brace_Open", MatchType.Literal,"{"),
                    new Terminal(this, "Brace_Close", MatchType.Literal,"}")
                }
            };

            Terminal brackets = new Terminal(this)
            {
                Name = "Brackets",
                MatchType = MatchType.Group,
                NonExecutable = true,
                SubTokens = new List<Terminal>()
                {
                    new Terminal(this, "Bracket_Open", MatchType.Literal,"["),
                    new Terminal(this, "Bracket_Close", MatchType.Literal,"]")
                }
            };

            //Terminal dot = new Terminal(this, "dot", MatchType.Literal, ".") { NonExecutable = true };
            //Terminal comma = new Terminal(this, "comma", MatchType.Literal, ",") { NonExecutable = true };
            //Terminal colon = new Terminal(this, "colon", MatchType.Literal, ":") { NonExecutable = true };

            RootTokens.Add(comment);
            RootTokens.Add(@string);
            RootTokens.Add(keywords);
            RootTokens.Add(@var);
            RootTokens.Add(literal);
            RootTokens.Add(identifier);
            RootTokens.Add(comparison);
            RootTokens.Add(lambda);
            RootTokens.Add(assignment);
            RootTokens.Add(operation);
            RootTokens.Add(eos);
            RootTokens.Add(parens);
            RootTokens.Add(braces);
            RootTokens.Add(brackets);
            //RootTokens.Add(dot);
            //RootTokens.Add(comma);
            //RootTokens.Add(colon);

            DefineTerminalGroup("Paren_Open", "Paren_Close");
            DefineTerminalGroup("Brace_Open", "Brace_Close");
            DefineTerminalGroup("Bracket_Open", "Bracket_Close");
        }

        protected override void DefinePatterns()
        {
            var genericParameters = new NonTerminal(this, "generic_parameters", "Cmp_Less ('type'type_name|Identifier) (comma ('type'type_name|Identifier))* Cmp_Greater")
            {
                Evaluator = new GenericArgsNodeEvaluator()
            };

            var typeName = new NonTerminal(this, "type_name")
            {
                Evaluator = new TypeNameNodeEvaluator(),
                SubPatterns = new List<NonTerminal>()
                {
                    //new NonTerminal(this, "type_name_array_multi", "('type'Identifier|type_name) Bracket_Open comma* Bracket_Close"),
                    new NonTerminal(this, "type_name_array_jagged", "('type'Identifier|type_name) (Bracket_Open Bracket_Close)"){
                        Evaluator = new TypeNameNodeEvaluator() { IsArray = true }
                    },
                    new NonTerminal(this, "type_name_generic"){
                        SubPatterns = new List<NonTerminal>(){
                               new NonTerminal(this, "type_name_generic_1", "('type'Identifier) ('generics'generic_parameters)(?!Paren_Open|parameters)"),
                               new NonTerminal(this, "type_name_generic_2", "('type'(?<=KW_NEW)Identifier) ('generics'generic_parameters)")
                        }
                    }
                }
            }.AddCodeSample("int[]")
            .AddCodeSample("int[][]")
            //.AddCodeSample("int[,]")
            .AddCodeSample("List<string>")
            .AddCodeSample("Dictionary<string, object>")
            .AddCodeSample("Dictionary<string, List<object>>");

            var type_groups = new NonTerminal(this, "type_groups")
            {
                SubPatterns = new List<NonTerminal>()
                {
                    genericParameters, typeName
                }
            };

            var @namespace = new NonTerminal(this, "namespace")
            {
                SubPatterns = new List<NonTerminal>()
                {
                    new NonTerminal(this, "namespace_elements", @"('element'(?<=KW_NAMESPACE|KW_USING)Identifier) (dot ('element'Identifier))*"),
                    new NonTerminal(this, "namespace_declaration", @"KW_NAMESPACE ('namespace'namespace_elements)"),
                    new NonTerminal(this, "namespace_include", @"KW_USING ('namespace'namespace_elements)")
                }
            };

            var expr = new NonTerminal(this, "expr");
            expr.Evaluator = new ExpressionNodeEvaluator();
            expr.SubPatterns = new List<NonTerminal>()
            {
                new NonTerminal(this, "expr_priority", "(?<!(Identifier|Keyword))Paren_Open expr Paren_Close") { Evaluator = new DelegatingNodeEvaluator() },
                new NonTerminal(this, "expr_unary_assignment")
                {
                    SubPatterns = new List<NonTerminal>()
                    {
                        new NonTerminal(this, "assign_unary_left", @"(Op_Assignment_Unary) (expr|(?<!dot)Identifier|accessChain)") { Evaluator = new ExpressionNodeEvaluator(-1, 1, 0) },
                        new NonTerminal(this, "assign_unary_right", @"(expr|(?<!dot)Identifier|accessChain) Op_Assignment_Unary") { Evaluator = new ExpressionNodeEvaluator(0, -1, 1) }
                    }
                },
                new NonTerminal(this, "expr_numeric")
                {
                    SubPatterns = new List<NonTerminal>()
                    {
                        new NonTerminal(this, "expr_3", @"(expr|Literal|accessChain|(?<!dot)Identifier) (Op_Multiply|Op_Divide) (expr|Literal|accessChain|Identifier)(?!Paren_Open|Bracket_Open|dot)"),
                        new NonTerminal(this, "expr_4", @"(expr|Literal|accessChain|(?<!dot)Identifier) (Op_Plus|Op_Minus) (expr|Literal|accessChain|Identifier)(?!Paren_Open|Bracket_Open|dot)"),
                    }
                },
                new NonTerminal(this, "expr_unary")
                {
                    SubPatterns = new List<NonTerminal>()
                    {
                        new NonTerminal(this, "expr_unary_left", @"(Op_Unary|Op_Minus) (expr|Literal|(?<!dot)Identifier|accessChain)") { Evaluator = new ExpressionNodeEvaluator(-1, 1, 0) },
                    }
                },
                new NonTerminal(this, "expr_bool")
                {
                    SubPatterns = new List<NonTerminal>()
                    {
                        new NonTerminal(this, "expr_bool_cmp", @"(expr|Literal|(?<!dot)Identifier|accessChain|assignment) Comparison (expr|Literal|Identifier|accessChain|assignment)(?!Paren_Open|Bracket_Open|dot)"),
                        new NonTerminal(this, "expr_bool_and", @"(expr_bool|Boolean|(?<!dot)Identifier|assignment) Op_And (expr_bool|Boolean|Identifier|accessChain|assignment)(?!Paren_Open|Bracket_Open|dot)"),
                        new NonTerminal(this, "expr_bool_or", @"(expr_bool|Boolean|(?<!dot)Identifier|assignment) Op_Or (expr_bool|Boolean|Identifier|accessChain|assignment)(?!Paren_Open|Bracket_Open|dot)")
                    }
                },
                new NonTerminal(this, "expr_coalesce", "('left'Identifier|Literal|accessChain|expr|KW_THIS) coalesce ('right'Identifier|Literal|expr|accessChain|KW_THIS)"),
                new NonTerminal(this, "expr_ternaryGate", "('s2'expr_bool|Identifier|Boolean|accessChain) question ('body'Identifier|Literal|accessChain|expr|KW_THIS)"),
                new NonTerminal(this, "expr_ternary", "('elif'expr_ternaryGate) colon ('else'Identifier|Literal|accessChain|expr|KW_THIS)(?!Paren_Open|Bracket_Open|Operation)"){
                    Evaluator = new IfNodeEvaluator()
                }
            };

            var declaration = new NonTerminal(this, "declaration", @"('type'KW_VAR|Identifier|type_name) ('name'Identifier)")
            {
                Evaluator = new DeclarationNodeEvaluator()
            }
            .AddCodeSample("int x")
            .AddCodeSample("byte[] data")
            .AddCodeSample("List<string> names");

            var assignment = new NonTerminal(this, "assignment")
            {
                Evaluator = new AssignmentNodeEvaluator(),
                SubPatterns = new List<NonTerminal>()
                {
                    new NonTerminal(this, "assign_equals", @"('lvalue'(?<!dot)Identifier|declaration|accessChain) Op_Assignment_Binary ('rvalue'expr|assignment|Literal|Identifier|accessChain|lambda_expr)(?!Operation|Comparison|dot)")
                }
            };

            var statement = new NonTerminal(this, "statement")
            {
                SubPatterns = new List<NonTerminal>()
                {
                    new NonTerminal(this, "statement_jump")
                    {
                        SubPatterns = new List<NonTerminal>()
                        {
                            new NonTerminal(this, "jump_break", @"KW_BREAK EOS") { Evaluator = new JumpNodeEvaluator(DirectionTypes.Break) },
                            new NonTerminal(this, "jump_continue", @"KW_CONTINUE EOS") { Evaluator = new JumpNodeEvaluator(DirectionTypes.Top) },
                            new NonTerminal(this, "jump_return", @"KW_RETURN ('jump_val'accessChain|Literal|Identifier|expr) EOS") { Evaluator = new JumpNodeEvaluator(DirectionTypes.Return) },
                            new NonTerminal(this, "jump_goto", @"KW_GOTO ('jump_val'Identifier) EOS") { Evaluator = new JumpNodeEvaluator(DirectionTypes.Jump) },
                        }
                    },
                    new NonTerminal(this, "statement_0", @"(declaration|assignment|accessChain|expr_unary_assignment) EOS")
                }
            };

            var block = new NonTerminal(this, "block", "Brace_Open (statement|conditional)* Brace_Close")
            {
                Evaluator = new RootNodeEvaluator()
                {
                    CreateScope = true,
                    BreakOnJumps = true
                }
            };

            var condition = new NonTerminal(this, "condition", @"(?<=(KW_IF|KW_WHILE))Paren_Open (expr_bool|Identifier|Boolean|accessChain) Paren_Close")
            {
                Evaluator = new DelegatingNodeEvaluator()
            };

            var switchTarget = new NonTerminal(this, "switch_target", @"(?<=KW_SWITCH)Paren_Open (accessChain|expr|Literal|Identifier) Paren_Close")
            {
                Evaluator = new DelegatingNodeEvaluator()
            };

            var caseStatement = new NonTerminal(this, "case_statement", @"(KW_CASE ('condition'Literal)|KW_DEFAULT) colon ('content'statement|block)*")
            {
                Evaluator = new RootNodeEvaluator() { BreakOnJumps = true, CreateScope = false, Filter = "content" }
            };

            var conditional = new NonTerminal(this, "conditional")
            {
                SubPatterns = new List<NonTerminal>()
                {
                    new NonTerminal(this, "conditional_if")
                    {
                        Evaluator = new IfNodeEvaluator(),
                        SubPatterns = new List<NonTerminal>()
                        {
                            new NonTerminal(this, "conditional_if_single", @"KW_IF ('s2'condition) ('body'block|statement)"),
                            new NonTerminal(this, "conditional_if_chain", @"('elif'conditional_if_single) ('elif'KW_ELSE conditional_if_single)* (KW_ELSE ('else'block|statement))?"),
                        }
                    },
                    new NonTerminal(this, "loop")
                    {
                        Evaluator = new LoopNodeEvaluator(),
                        SubPatterns = new List<NonTerminal>()
                        {
                            new NonTerminal(this, "loop_do", "KW_DO ('contents'block|statement) KW_WHILE ('s2'condition) EOS")
                            {
                                Evaluator = new LoopNodeEvaluator() { LoopType = LoopNodeEvaluator.LoopTypes.Do }
                            },
                            new NonTerminal(this, "loop_while", "KW_WHILE ('s2'condition) ('contents'block|statement)"),
                            new NonTerminal(this, "loop_for", "KW_FOR Paren_Open (('s1'statement)|EOS) ('s2'expr_bool)? EOS ('s3'(expr|assignment))? Paren_Close ('contents'block|statement)"),
                            new NonTerminal(this, "loop_foreach", "KW_FOREACH Paren_Open ('s1'declaration) KW_IN ('collection'Identifier|accessChain|expr_ternary|expr_coalesce) Paren_Close ('contents'block|statement)"){
                                Evaluator = new ForeachNodeEvaluator()
                            }
                        }
                    },
                    new NonTerminal(this, "switch", "KW_SWITCH ('target'switch_target) Brace_Open ('case'case_statement)* Brace_Close")
                    {
                        Evaluator = new SwitchNodeEvaluator()
                    }.AddCodeSample("switch(x) { case 3: case 4: default: break; }", true)
                }
            };

            var parameters = new NonTerminal(this, "parameters")
            {
                SubPatterns = new List<NonTerminal>()
                {
                    new NonTerminal(this, "invocation_parameters"){
                        SubPatterns = new List<NonTerminal>(){
                            new NonTerminal(this, "parameters_multi", @"(?<!Keyword)Paren_Open ('param'expr|Literal|accessChain|Identifier|lambda_expr|assignment) (comma ('param'expr|Literal|accessChain|Identifier|lambda_expr|assignment))* Paren_Close(?!Op_Lambda)"),
                            new NonTerminal(this, "parameters_void", @"(?<!Keyword)Paren_Open Paren_Close(?!Op_Lambda)")
                        }
                    },
                    new NonTerminal(this, "indexer_parameters", @"Bracket_Open ('param'expr|KW_THIS|Literal|accessChain|Identifier|assignment) (comma ('param'expr|KW_THIS|Literal|accessChain|Identifier|assignment))* Bracket_Close")
                }
            };
            var memberAccess = new NonTerminal(this, "memberAccess")
            {
                SubPatterns = new List<NonTerminal>()
                {
                    new NonTerminal(this, "methodAccessor", @"dot ('member'Identifier) ('type_params'generic_parameters)? ('parameters'invocation_parameters)"),
                    new NonTerminal(this, "fieldAccessor", @"dot ('member'Identifier)"),
                    new NonTerminal(this, "constructor_call"){
                        SubPatterns = new List<NonTerminal>(){
                            new NonTerminal(this, "object_Constructor", @"KW_NEW ('type'Identifier|type_name_generic) ('parameters'invocation_parameters)"){
                                     Evaluator = new ObjectConstructionNodeEvaluator()
                                 }.AddCodeSample("new List()", true)
                                 .AddCodeSample("new object()", true)
                                 .AddCodeSample("new Item(1, \"steve\")", true)
                                 .AddCodeSample("new List<Candy>()", true),
                            new NonTerminal(this, "array_constructor", @"KW_NEW ('type'Identifier|type_name_generic) ('parameters'indexer_parameters)"){
                                Evaluator = new ObjectConstructionNodeEvaluator() { IsArray = true }
                            }.AddCodeSample("new byte[1]", true)
                            .AddCodeSample("new byte[1, 4]", true)
                        }
                    },
                    new NonTerminal(this, "memberAccess_Root", @"('member'Identifier) ('type_params'generic_parameters)? ('parameters'invocation_parameters)"){
                        Evaluator = new MemberAccessorNodeEvaluator() { 
                            CanAssign = true,
                            AccessType = AccessTypes.This | AccessTypes.Class | AccessTypes.Variable,
                            MemberType = MemberTypes.Both
                        }
                    }
                }
            };
            var accessChain = new NonTerminal(this, "accessChain")
            {
                Evaluator = new MemberAccessorNodeEvaluator()
                {
                    CanAssign = true,
                    AccessType = AccessTypes.All,
                    MemberType = MemberTypes.Both
                },
                SubPatterns = new List<NonTerminal>()
                {
                    new NonTerminal(this, "accessChain_root", "('accessor'memberAccess_Root)")
                        .AddCodeSample("Run(1,2,3)"),
                    new NonTerminal(this, "accessChain_constructor", "constructor_call"){ Evaluator = new DelegatingNodeEvaluator(0) },
                    new NonTerminal(this, "accessChain_0", @"('site'Identifier|Literal|accessChain|KW_THIS) ('accessor'methodAccessor)"){
                         Evaluator = new MemberAccessorNodeEvaluator() { 
                            CanAssign = false,
                            AccessType = AccessTypes.Sited,
                            MemberType = MemberTypes.Method
                        }
                    }.AddCodeSample("this.Foo()"),
                    new NonTerminal(this, "accessChain_1", @"('site'Identifier|Literal|accessChain|KW_THIS) ('accessor'fieldAccessor)"){
                         Evaluator = new MemberAccessorNodeEvaluator() { 
                            CanAssign = true,
                            AccessType = AccessTypes.Sited,
                            MemberType = MemberTypes.Member
                        }
                    }   .AddCodeSample("this.x")
                        .AddCodeSample("this.x.y"),
                    new NonTerminal(this, "accessChain_Indexer", @"('site'Identifier|Literal|accessChain|KW_THIS) ('parameters'indexer_parameters)"){
                         Evaluator = new MemberAccessorNodeEvaluator() { 
                            CanAssign = true,
                            AccessType = AccessTypes.Sited,
                            MemberType = MemberTypes.Member,
                            ImpliedMemberName = "Item"
                        }
                    }.AddCodeSample("this.x[4]")
                    .AddCodeSample("x.go()[3]")
                    .AddCodeSample("x[4].get(x).y.x[3+i]")
                    .AddCodeSample("this[\"tuesday\"]")
                    .AddCodeSample("data[\"key\"]")
                }
            };

            string LAMBDA_TYPED_PARAMS = "('param'declaration)";
            string LAMBDA_PARAMS = "('param'('name'Identifier))";
            string LAMBDA_PARAMETERS = "Paren_Open ({0} (comma {0})*) Paren_Close(?=Op_Lambda)";
            var methodParameters = new NonTerminal(this, "method_parameters")
            {
                SubPatterns = new List<NonTerminal>()
                {
                    new NonTerminal(this, "method_parameters_multi_typed", string.Format(LAMBDA_PARAMETERS, LAMBDA_TYPED_PARAMS)),
                    new NonTerminal(this, "method_parameters_multi", string.Format(LAMBDA_PARAMETERS, LAMBDA_PARAMS)),
                    new NonTerminal(this, "method_parameters_void", "Paren_Open Paren_Close(?=Op_Lambda)"),
                    new NonTerminal(this, "method_parameters_single", "('param|name'Identifier)(?=Op_Lambda)")
                }
            };

            var lambda = new NonTerminal(this, "lambda_expr")
            {
                Evaluator = new MethodDeclarationNodeEvaluator(),
                SubPatterns = new List<NonTerminal>()
                {
                    new NonTerminal(this, "lambda_func", "('p-group'method_parameters) Op_Lambda ('method-body'(block|assignment|accessChain|expr_unary_assignment|expr))"),
                    //new NonTerminal(this, "lambda_single", @"('p-group'('param'('name'Identifier))) Op_Lambda ('method-body'(block|expr EOS)|(statement))"),
                }
            };

            RootPatterns.Add(type_groups);
            RootPatterns.Add(@namespace);
            RootPatterns.Add(expr);
            RootPatterns.Add(declaration);
            RootPatterns.Add(statement);
            RootPatterns.Add(block);
            RootPatterns.Add(condition);
            RootPatterns.Add(switchTarget);
            RootPatterns.Add(caseStatement);
            RootPatterns.Add(conditional);
            RootPatterns.Add(parameters);
            RootPatterns.Add(memberAccess);
            RootPatterns.Add(accessChain);
            RootPatterns.Add(methodParameters);
            RootPatterns.Add(lambda);
            RootPatterns.Add(assignment);
        }
    }
}
