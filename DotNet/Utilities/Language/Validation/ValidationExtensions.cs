﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Validation
{
    public static class ValidationExtensions
    {

        public static TElement AddCodeSample<TElement>(this TElement element, string sampleCode) where TElement : IGrammarElement
        {
            return AddCodeSample(element, sampleCode, false, -1);
        }

        public static TElement AddCodeSample<TElement>(this TElement element, string sampleCode, bool acceptSubnodes) where TElement : IGrammarElement
        {
            return AddCodeSample(element, sampleCode, acceptSubnodes, -1);
        }

        public static TElement AddCodeSample<TElement>(this TElement element, string sampleCode, int targetTokenIndex) where TElement : IGrammarElement
        {
            return AddCodeSample(element, sampleCode, false, targetTokenIndex);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="element"></param>
        /// <param name="sampleCode"></param>
        /// <param name="acceptSubnodes">Indicates whether or not to accept the element type as a direct child-node</param>
        /// <param name="targetTokenIndex"></param>
        /// <returns></returns>
        public static TElement AddCodeSample<TElement>(this TElement element, string sampleCode, bool acceptSubnodes, int targetTokenIndex) where TElement : IGrammarElement
        {
            if (element == null) { throw new ArgumentNullException("element"); }
            if (element.Grammar.EnableGrammarValidation)
            {
                if (string.IsNullOrEmpty(sampleCode)) { throw new ArgumentNullException("sampleCode"); }
                if (targetTokenIndex < 0) { targetTokenIndex = -1; }

                var validator = new CodeSampleGrammarValidator(element, new CodeSample(sampleCode, targetTokenIndex));
                validator.AcceptSubnodes = acceptSubnodes;
                element.Grammar.Validators.Add(validator);
            }

            return element;
        }
    }
}
