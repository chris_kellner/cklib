﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using CKLib.Utilities.Language.Parse;
using CKLib.Utilities.Language.Samples;
using CKLib.Utilities.Language.Evaluator;
using CKLib.Utilities.Language.Lex;
using CKLib.Utilities.Language.Validation;

namespace CKLib.Utilities.Language
{
    public abstract class Grammar : IDisposable
    {
        #region Constants
        public const string TN_EOF = "<EOF>";

        public static readonly Terminal T_EOF = new Terminal(null)
        {
            Name = TN_EOF,
            MatchType = MatchType.Null,
            NonParsed = true
        };
        #endregion

        #region Variables
        private bool m_IsInitialized;
        private Terminal m_EOL;
        private readonly CompilationUnit m_CompilationUnit;
        private Dictionary<IGrammarElement, IGrammarElement> m_GroupTerminals = new Dictionary<IGrammarElement, IGrammarElement>();
        #endregion

        #region Constructors
        public Grammar()
        {
            Version = "1.0";
            RootTokens = new List<Terminal>();
            RootPatterns = new List<NonTerminal>();
            Validators = new List<IGrammarValidator>();
            m_CompilationUnit = new CompilationUnit(this);
            m_EOL = CTokens.EndOfLine(this);
            RootTokens.Add(T_EOF);
            RootTokens.Add(m_EOL);
        }
        #endregion

        #region Properties
        public List<Terminal> RootTokens { get; set; }
        public List<NonTerminal> RootPatterns { get; set; }
        public List<IGrammarValidator> Validators { get; set; }

        public string Name { get; set; }
        public string Version { get; set; }
        public bool CanEvaluate { get; set; }
        public bool CanCompile { get; set; }
        public bool EnableParserDebugging { get; set; }
        public bool EnableGrammarValidation { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Pre-compiles all patterns in the language to improve evaluation speed instead of compiling them on demand.
        /// </summary>
        public void AotCompile()
        {
            if (!m_IsInitialized)
            {
                m_IsInitialized = true;
                DefineTokens();
                DefinePatterns();

                foreach (var token in RootTokens)
                {
                    token.Initialize();
                }

                foreach (var token in RootPatterns)
                {
                    token.Initialize();
                }
            }
        }

        public ICollection<ValidationResult> ValidateGrammar()
        {
            List<ValidationResult> results = new List<ValidationResult>();
            foreach (var validator in Validators)
            {
                results.Add(validator.Validate());
            }

            return results;
        }

        /// <summary>
        /// Adds a pairing of Terminal group identifiers which serve as delimiters for a collection of tokens.
        /// </summary>
        /// <param name="openTerminal">The Terminal which identifies the start of a group.</param>
        /// <param name="closeTerminal">The Terminal which identifies the end of a group.</param>
        public void DefineTerminalGroup(IGrammarElement openTerminal, IGrammarElement closeTerminal)
        {
            if (openTerminal == null)
            {
                throw new ArgumentNullException("openTerminal");
            }

            if (closeTerminal == null)
            {
                throw new ArgumentNullException("closeTerminal");
            }

            m_GroupTerminals[openTerminal] = closeTerminal;
        }

         /// <summary>
        /// Adds a pairing of Terminal group identifiers which serve as delimiters for a collection of tokens.
        /// </summary>
        /// <param name="openTerminalName">The name of the Terminal which identifies the start of a group.</param>
        /// <param name="closeTerminalName">The name of the Terminal which identifies the end of a group.</param>
        public void DefineTerminalGroup(string openTerminalName, string closeTerminalName)
        {
            var open = FindGrammarElement(openTerminalName);
            var close = FindGrammarElement(closeTerminalName);
            DefineTerminalGroup(open, close);
        }

        public IDictionary<IGrammarElement, IGrammarElement> GetTerminalGroups()
        {
            return m_GroupTerminals;
        }

        public Lexer CreateLexer()
        {
            return new Lexer(this);
        }

        public Parser CreateParser()
        {
            return new Parser(this);
        }

        public ASTNode Parse(string script)
        {
            var tokens = CreateLexer().LexTokens(script).ToList();
            var parsed = CreateParser().Parse(tokens);
            return m_CompilationUnit.MakeRoot(parsed);
        }

        public virtual TResult Evaluate<TResult>(string script)
        {
            return Evaluate<TResult>(new ScriptContext(), script);
        }

        public virtual object Evaluate(Type tResult, string script)
        {
            return Evaluate(new ScriptContext(), tResult, script);
        }

        public virtual TResult Evaluate<TResult>(ScriptContext context, string script)
        {
            var node = Parse(script);
            var eval = CreateEvaluator(context);
            return eval.Evaluate<TResult>(node);
        }

        public virtual object Evaluate(ScriptContext context, Type tResult, string script)
        {
            var node = Parse(script);
            var eval = CreateEvaluator(context);
            return eval.Evaluate(tResult, node);
        }

        public virtual ScriptEvaluator CreateEvaluator(ScriptContext context)
        {
            return new ScriptEvaluator(context);
        }

        public IGrammarElement FindGrammarElement(string name)
        {
            foreach (var cDef in RootTokens)
            {
                IGrammarElement def = FindToken(cDef, name);
                if (def != null)
                {
                    return def;
                }
            }

            foreach (var cPattern in RootPatterns)
            {
                IGrammarElement def = FindPattern(cPattern, name);
                if (def != null)
                {
                    return def;
                }
            }

            throw new GrammarDefinitionException("Grammar does not define an element named: " + name);
        }

        public Func<TResult> CreateDelegate<TResult>(ScriptContext context, string script)
        {
            var eval = CreateEvaluator(context);
            var tree = Parse(script);
            return () =>
            {
                TResult o = eval.Evaluate<TResult>(tree);
                return o;
            };
        }

        private NonTerminal FindPattern(NonTerminal cDef, string name)
        {
            if (cDef.Name == name)
            {
                return cDef;
            }
            else if (cDef.SubPatterns != null)
            {
                foreach (var sDef in cDef.SubPatterns)
                {
                    var def = FindPattern(sDef, name);
                    if (def != null)
                    {
                        return def;
                    }
                }
            }

            return null;
        }

        private Terminal FindToken(Terminal cDef, string name)
        {
            if (cDef.Name == name)
            {
                return cDef;
            }
            else if (cDef.MatchType == MatchType.Group)
            {
                foreach (var sDef in cDef.SubTokens)
                {
                    Terminal def = FindToken(sDef, name);
                    if (def != null)
                    {
                        return def;
                    }
                }
            }

            return null;
        }

        protected abstract void DefineTokens();
        protected abstract void DefinePatterns();
        #endregion

        #region IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~Grammar()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            // TODO: Orphan all items
        }
        #endregion
    }
}
