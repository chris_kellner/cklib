﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace CKLib.Utilities.Language.Evaluator
{
    public class EvaluationEventArgs : EventArgs
    {
        private AutoResetEvent m_Handle;

        public EvaluationEventArgs(ScriptEvaluator eval, ASTNode node, AutoResetEvent handle)
        {
            Evaluator = eval;
            CurrentNode = node;
            m_Handle = handle;
        }

        public ScriptEvaluator Evaluator { get; private set; }
        public ASTNode CurrentNode { get; private set; }

        public void Step()
        {
            m_Handle.Set();
        }

        public void Continue()
        {
            m_Handle.Set();
            Evaluator.DebugEnabled = false;
        }
    }
}
