﻿using CKLib.Utilities.Language.Evaluator.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace CKLib.Utilities.Language.Evaluator
{
    public class ScriptEvaluator
    {
        private Stack<ASTNode> m_NodeStack = new Stack<ASTNode>();
        private AutoResetEvent m_WaitHandle = new AutoResetEvent(false);

        public ScriptEvaluator(ScriptContext context)
        {
            Context = context;
            Stack = new EvaluationStack();
        }

        public ScriptContext Context { get; private set; }

        public EvaluationStack Stack { get; private set; }

        public bool DebugEnabled { get; set; }

        public Type ExpectedType { get; private set; }

        public TResult Evaluate<TResult>(ASTNode tree)
        {
            ExpectedType = typeof(TResult);
            EvaluationResult rslt = EvaluateInternal(tree);
            if (!rslt.IsValue)
            {
                throw new ExecutionException(
                string.Format(
                "Expected result value of type: {0}, but received control flow",
                typeof(TResult)),
                tree
                ); 
            }

            var obj = rslt.Value;
            if (obj is TResult)
            {
                return rslt.GetValue<TResult>();
            }
            else if (obj == null || obj.GetType() == typeof(void))
            {
                return default(TResult);
            }

            var cvt = DataConverter.ConvertData<TResult>(obj);
            if (cvt != null)
            {
                return cvt;
            }

            throw new ExecutionException(
                string.Format(
                "Expected result type: {0}, but was: {1}",
                typeof(TResult), obj != null ? obj.GetType() : typeof(void)
                ),
                tree
                );
        }

        public object Evaluate(Type tResult, ASTNode tree)
        {
            ExpectedType = tResult;
            EvaluationResult rslt = EvaluateInternal(tree);
            if (!rslt.IsValue)
            {
                throw new ExecutionException(
                string.Format(
                "Expected result value of type: {0}, but received control flow",
                tResult),
                tree
                );
            }

            var obj = rslt.Value;
            if (tResult == null || tResult.IsInstanceOfType(obj))
            {
                return rslt.Value;
            }
            else if (obj == null || obj.GetType() == typeof(void))
            {
                return null;
            }

            obj = DataConverter.ConvertData(obj, tResult);
            if (obj != null)
            {
                return obj;
            }

            throw new ExecutionException(
                string.Format(
                "Expected result type: {0}, but was: {1}",
                tResult, obj != null ? obj.GetType() : typeof(void)
                ),
                tree
                );
        }

        public EvaluationResult Evaluate(ASTNode tree)
        {
            ExpectedType = null;
            return EvaluateInternal(tree);
        }

        private EvaluationResult EvaluateInternal(ASTNode tree)
        {
            m_NodeStack.Push(tree);
            if (DebugEnabled)
            {
                OnEvaluateNode(tree);
                m_WaitHandle.WaitOne();
            }

            EvaluationResult returnValue = EvaluationResult.Next;
            var eval = tree.Evaluator;
            if (eval != null)
            {
                returnValue = eval.Evaluate(this, tree);
            }
            else if (tree.IsType("statement"))
            {
                returnValue = Evaluate(tree.Nodes[0]);
            }
            else
            {
                throw new ExecutionException(string.Format("Unexpected token '{0}'", tree.TokenType.Name), tree);
            }

            m_NodeStack.Pop();
            return returnValue;
        }



        public void MakeAssignment(ASTNode tree, object value)
        {
            var eval = tree.Evaluator;
            if (eval != null && eval.CanAssign)
            {
                eval.MakeAssignment(this, tree, value);
            }
            else
            {
                throw new ExecutionException("Cannot assign to node: " + tree, tree);
            }
        }

        protected virtual void OnEvaluateNode(ASTNode node)
        {
            if (DebugEnabled)
            {
                EventHandler<EvaluationEventArgs> handler = EvaluateNode;
                if (handler != null)
                {
                    handler(this, new EvaluationEventArgs(this, node, m_WaitHandle));
                }
            }
        }

        public event EventHandler<EvaluationEventArgs> EvaluateNode;
    }
}
