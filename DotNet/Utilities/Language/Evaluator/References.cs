﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator
{
    public class References
    {
        private Dictionary<string, Type> m_ImportedTypes = new Dictionary<string, Type>();

        private Dictionary<string, List<MethodInfo>> m_ImportedExtensionMethods = new Dictionary<string, List<MethodInfo>>();

        public void ImportNamespace(string @namespace)
        {
            foreach (var asm in AppDomain.CurrentDomain.GetAssemblies())
            {
                ImportNamespace(asm, @namespace);
            }
        }

        public void ImportNamespace(Assembly assembly, string @namespace)
        {
            foreach (var type in GetTypes(assembly, @namespace))
            {
                ImportType(type);
            }
        }

        public IEnumerable<Type> GetTypes(string @namespace)
        {
            return AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(asm => GetTypes(asm, @namespace));
        }

        public IEnumerable<Type> GetTypes(Assembly assembly, string @namespace)
        {
            if (!IsDynamicAssembly(assembly))
            {
                Type[] types = assembly.GetExportedTypes();
                foreach (var type in types)
                {
                    if (type.Namespace == @namespace)
                    {
                        yield return type;
                    }
                }
            }
        }

        private Type FindType(string typeName)
        {
            foreach (var asm in AppDomain.CurrentDomain.GetAssemblies())
            {
                Type t = asm.GetType(typeName, false);
                if (t != null)
                {
                    return t;
                }
            }

            return null;
        }

        public void ImportType(Type type)
        {
            m_ImportedTypes[type.Name] = type;
            if (type.IsAbstract && type.IsSealed)
            {
                foreach (var method in type.GetMethods(BindingFlags.Static | BindingFlags.DeclaredOnly | BindingFlags.Public))
                {
                    ImportExtensionMethod(method);
                }
            }
        }

        public void ImportExtensionMethod(MethodInfo info)
        {
            if (info.IsDefined(typeof(System.Runtime.CompilerServices.ExtensionAttribute), true))
            {
                List<MethodInfo> methods;
                if (!m_ImportedExtensionMethods.TryGetValue(info.Name, out methods))
                {
                    methods = new List<MethodInfo>();
                    m_ImportedExtensionMethods[info.Name] = methods;
                }

                methods.Add(info);
            }
        }

        public IEnumerable<MethodInfo> GetExtensionMethods(string name)
        {
             List<MethodInfo> methods;
             if (m_ImportedExtensionMethods.TryGetValue(name, out methods))
             {
                 return methods;
             }

             return Enumerable.Empty<MethodInfo>();
        }

        public MethodInfo BindExtensionMethod(string name, Type firstArgumentType, Binder binder, Type[] additionalParameterTypes)
        {
            List<MethodInfo> methods;
            if (m_ImportedExtensionMethods.TryGetValue(name, out methods))
            {
                var types = new Type[additionalParameterTypes.Length + 1];
                types[0] = firstArgumentType;
                additionalParameterTypes.CopyTo(types, 1);
                var method = binder.SelectMethod(BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly,
                    methods.ToArray(), types, null);
                return (MethodInfo)method;
            }

            return null;
        }

        public virtual Type BindToType(string typeName)
        {
            Type typ;
            m_ImportedTypes.TryGetValue(typeName, out typ);
            if (typ == null)
            {
                typ = Type.GetType(typeName);
                if (typ == null)
                {
                    typ = FindType(typeName);
                }

                if (typ != null)
                {
                    ImportType(typ);
                }
            }

            return typ;
        }

        private static bool IsDynamicAssembly(Assembly asm)
        {
            if (asm is System.Reflection.Emit.AssemblyBuilder)
            {
                return true;
            }

            var module = asm.ManifestModule as System.Reflection.Emit.ModuleBuilder;
            if (module != null)
            {
                return module.IsTransient();
            }

            return false;
        }
    }
}
