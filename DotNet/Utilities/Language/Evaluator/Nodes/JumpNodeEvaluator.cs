﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator
{
    public class JumpNodeEvaluator : INodeEvaluator
    {
        public const string ROLE_JUMP_VALUE = "jump_val";

        public JumpNodeEvaluator(DirectionTypes direction)
        {
            Direction = direction;
        }

        public EvaluationResult Evaluate(ScriptEvaluator evaluator, ASTNode tree)
        {
            object val = null;
            var target = tree.FindNodeByRole(ROLE_JUMP_VALUE, 0);
            if (target != null)
            {
                val = evaluator.Evaluate<object>(target);
            }

            return new EvaluationResult(Direction, val);
        }

        public void MakeAssignment(ScriptEvaluator evaluator, ASTNode tree, object value)
        {
            throw new NotSupportedException();
        }

        public DirectionTypes Direction { get; set; }

        public bool CanAssign
        {
            get { return false; }
        }

        public bool HasValue
        {
            get { return true; }
        }
    }
}
