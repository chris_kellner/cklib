﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator
{
    public class DelegatingNodeEvaluator : INodeEvaluator
    {
        public DelegatingNodeEvaluator()
            :this(0)
        {
        }

        public DelegatingNodeEvaluator(int delegateChildIndex)
            :this(n=>n.Nodes[delegateChildIndex])
        {
        }

        public DelegatingNodeEvaluator(Func<ASTNode, ASTNode> getDelegateFunction)
        {
            GetDelegateFunction = getDelegateFunction;
        }

        public EvaluationResult Evaluate(ScriptEvaluator evaluator, ASTNode tree)
        {
            var node = GetDelegateFunction(tree);
            return evaluator.Evaluate(node);
        }

        public void MakeAssignment(ScriptEvaluator evaluator, ASTNode tree, object value)
        {
            var node = GetDelegateFunction(tree);
            evaluator.MakeAssignment(node, value);
        }

        public bool CanAssign { get { return true; } }

        public bool HasValue { get { return true; } }

        public Func<ASTNode, ASTNode> GetDelegateFunction { get; set; }
    }
}
