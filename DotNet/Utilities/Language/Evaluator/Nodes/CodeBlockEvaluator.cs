﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator.Nodes
{
    public class CodeBlockEvaluator : ANodeEvaluator
    {
        public override EvaluationResult Evaluate(ScriptEvaluator evaluator, Type expectedType, ASTNode tree)
        {
            var value = tree.Value;
            value = StringUtils.FromCodeBlock(value, OpenMark, CloseMark, EscapeChar);
            return new EvaluationResult(value);
        }

        protected override string[] GetRequiredRoleNames()
        {
            return EmptyRoles;
        }

        public string OpenMark { get; set; }
        public string CloseMark { get; set; }
        public char EscapeChar { get; set; }
    }
}
