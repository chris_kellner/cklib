﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator
{
    public class LoopNodeEvaluator : INodeEvaluator
    {
        public enum LoopTypes
        {
            For,
            Foreach,
            While,
            Do
        }

        public const string ROLE_VAR_INIT = "s1";
        public const string ROLE_TERMINATION_CONDITION = "s2";
        public const string ROLE_ITERATE = "s3";
        public const string ROLE_CONTENTS = "contents";

        public EvaluationResult Evaluate(ScriptEvaluator evaluator, ASTNode tree)
        {
            using (var key = evaluator.Context.NewScope())
            {
                var s1 = tree.FindNodeByRole(ROLE_VAR_INIT, 0);
                var s2 = tree.FindNodeByRole(ROLE_TERMINATION_CONDITION, 0);
                var s3 = tree.FindNodeByRole(ROLE_ITERATE, 0);
                var contents = tree.FindNodeByRole(ROLE_CONTENTS, 0);

                if (s1 != null) evaluator.Evaluate(s1);
                if (LoopType != LoopTypes.Do)
                {
                    while (s2 == null || evaluator.Evaluate<bool>(s2))
                    {
                        if (contents != null)
                        {
                            using (var iKey = evaluator.Context.NewScope())
                            {
                                var rslt = evaluator.Evaluate(contents);
                                if (rslt.IsLoopExit)
                                {
                                    if (rslt.Direction == DirectionTypes.Break)
                                    {
                                        break;
                                    }

                                    return rslt;
                                }
                            }
                        }
                        if (s3 != null) evaluator.Evaluate(s3);
                    }
                }
                else
                {
                    do
                    {
                        if (contents != null)
                        {
                            using (var iKey = evaluator.Context.NewScope())
                            {
                                var rslt = evaluator.Evaluate(contents);
                                if (rslt.IsLoopExit)
                                {
                                    if (rslt.Direction == DirectionTypes.Break)
                                    {
                                        rslt = new EvaluationResult(rslt.Value);
                                        break;
                                    }

                                    return rslt;
                                }
                            }
                        }

                        if (s3 != null) evaluator.Evaluate(s3);
                    } while (s2 == null || evaluator.Evaluate<bool>(s2));
                }
            }
            return EvaluationResult.Next;
        }

        public void MakeAssignment(ScriptEvaluator evaluator, ASTNode tree, object value)
        {
            throw new NotSupportedException();
        }

        public bool CanAssign
        {
            get { return false; }
        }

        public bool HasValue
        {
            get { return false; }
        }

        public LoopTypes LoopType { get; set; }
    }
}
