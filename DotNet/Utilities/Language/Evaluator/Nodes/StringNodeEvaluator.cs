﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator.Nodes
{
    public class StringNodeEvaluator : ANodeEvaluator
    {
        public override EvaluationResult Evaluate(ScriptEvaluator evaluator, Type expectedType, ASTNode tree)
        {
            var value = tree.Value;
            value = StringUtils.FromStringLiteral(value, QuoteChar);
            return new EvaluationResult(value);
        }

        protected override string[] GetRequiredRoleNames()
        {
            return EmptyRoles;
        }

        public char QuoteChar { get; set; }
    }
}
