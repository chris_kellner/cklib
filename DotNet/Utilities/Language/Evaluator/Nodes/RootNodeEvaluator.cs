﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator
{
    public class RootNodeEvaluator : INodeEvaluator
    {
        public EvaluationResult Evaluate(ScriptEvaluator evaluator, ASTNode tree)
        {
            Type expectedType = evaluator.ExpectedType;
            EvaluationResult rslt = EvaluationResult.Next;
            IEnumerable<ASTNode> nodes = null;
            if (Filter != null)
            {
                nodes = tree.FindNodesByRole(Filter);
            }
            else
            {
                nodes = tree.Nodes;
            }

            IDisposable scope = null;
            if (CreateScope)
            {
                scope = evaluator.Context.NewScope();
            }

            using (scope)
            {
                var nodeArray = nodes.ToArray();
                if (nodeArray.Length > 0)
                {
                    for (int i = 0; i < nodeArray.Length - 1; i++)
                    {
                        var node = nodeArray[i];
                        rslt = evaluator.Evaluate(node);
                        if (BreakOnJumps && rslt.IsJump)
                        {
                            break;
                        }
                    }

                    if (!rslt.IsJump)
                    {
                        var n = nodeArray[nodeArray.Length - 1];
                        if (expectedType != null)
                        {
                            var obj = evaluator.Evaluate(expectedType, n);
                            rslt = new EvaluationResult(obj);
                        }
                        else
                        {
                            rslt = evaluator.Evaluate(n);
                        }
                    }
                }
            }

            return rslt;
        }

        public void MakeAssignment(ScriptEvaluator evaluator, ASTNode tree, object value)
        {
            throw new NotSupportedException();
        }

        public string Filter { get; set; }

        public bool BreakOnJumps { get; set; }
        public bool CreateScope { get; set; }

        public bool CanAssign
        {
            get { return false; }
        }

        public bool HasValue
        {
            get { return true; }
        }
    }
}
