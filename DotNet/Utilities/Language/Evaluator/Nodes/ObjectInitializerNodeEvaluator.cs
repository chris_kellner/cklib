﻿using CKLib.Utilities.Language.Evaluator.Runtime;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator.Nodes
{
    public enum InitializerTypes
    {
        Object,
        List,
        Array
    }

    public class ObjectInitializerNodeEvaluator : ANodeEvaluator
    {
        public string ROLE_ASSIGNMENT = "assignment";
        public string ROLE_ITEM = "item";

        public override EvaluationResult Evaluate(ScriptEvaluator evaluator, Type expectedType, ASTNode tree)
        {
            if (InitializerType == InitializerTypes.Object)
            {
                using (var scope = evaluator.Context.NewScope())
                {
                    object obj = null;
                    if (HasValue)
                    {
                        if (!ShouldUseScriptObject(expectedType))
                        {
                            obj = Activator.CreateInstance(expectedType);
                        }
                        else
                        {
                            obj = new ScriptObject(false);
                        }

                        evaluator.Context.SetLocal(ScriptContext.RESERVED_NAME_THIS, obj);
                        evaluator.Context.SetLocal(ScriptContext.RESERVED_NAME_CLASS, MemberInvoker.GetCallSiteType(obj));
                    }
                    else
                    {
                        obj = evaluator.Context.GetVariable(ScriptContext.RESERVED_NAME_THIS);
                    }

                    foreach (var node in tree.FindNodesByRole(ROLE_ASSIGNMENT))
                    {
                        evaluator.Evaluate(node);
                    }

                    return new EvaluationResult(obj);
                }
            }
            else if (InitializerType == InitializerTypes.List)
            {
                var list = evaluator.Context.GetVariable(ScriptContext.RESERVED_NAME_THIS);
                Action<object> adder = null;
                if (list is ICollection)
                {
                    adder = (o) => ((IList)list).Add(o);
                }

                foreach (var node in tree.FindNodesByRole(ROLE_ITEM))
                {
                    var obj = evaluator.Evaluate<object>(node);
                    adder(obj);
                }
            }
            else if (InitializerType == InitializerTypes.Array)
            {
                Type arrayType = typeof(object);
                if (expectedType != null && expectedType.IsArray)
                {
                    arrayType = expectedType.GetElementType();
                }

                var items = tree.FindNodesByRole(ROLE_ITEM)
                    .Select(n => evaluator.Evaluate(arrayType, n))
                    .ToArray<object>();
                if (arrayType != typeof(object))
                {
                    Array arr = Array.CreateInstance(arrayType, items.Length);
                    items.CopyTo(arr, 0);
                    return new EvaluationResult(arr);
                }
                else
                {
                    return new EvaluationResult(items);
                }
            }

            return EvaluationResult.Next;
        }

        private bool ShouldUseScriptObject(Type expectedType)
        {
            if (expectedType == null)
            {
                return true;
            }

            if (expectedType == typeof(object))
            {
                return true;
            }

            if (typeof(IMetadataAccessible).IsAssignableFrom(expectedType))
            {
                return true;
            }

            if (typeof(IDictionary<string, object>) == expectedType)
            {
                return true;
            }

            return false;
        }

        protected override string[] GetRequiredRoleNames()
        {
            return EmptyRoles;
        }

        public InitializerTypes InitializerType
        {
            get;
            set;
        }
    }
}
