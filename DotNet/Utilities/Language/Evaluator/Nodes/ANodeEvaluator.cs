﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator.Nodes
{
    public abstract class ANodeEvaluator : INodeEvaluator
    {
        private Dictionary<string, INodeEvaluator> m_RoleOverrides = new Dictionary<string, INodeEvaluator>();
        protected static readonly string[] EmptyRoles = new string[0];

        public EvaluationResult Evaluate(ScriptEvaluator evaluator, ASTNode tree)
        {
            ValidateRequiredRoles(tree);
            return Evaluate(evaluator, evaluator.ExpectedType, tree);
        }

        public abstract EvaluationResult Evaluate(ScriptEvaluator evaluator, Type expectedType, ASTNode tree);

        public virtual void MakeAssignment(ScriptEvaluator evaluator, ASTNode tree, object value)
        {
            throw new NotSupportedException();
        }

        protected void ValidateRequiredRoles(ASTNode tree)
        {
            bool evaluated;
            tree.TryGetCachedValue("ANODE_CACHED", out evaluated);
            if (!evaluated)
            {
                string[] names = GetRequiredRoleNames();
                for (int i = 0; i < names.Length; i++)
                {
                    string roleName = names[i];
                    var nodes = tree.FindNodesByRole(roleName);
                    if (!nodes.Any())
                    {
                        throw new ExecutionException("Missing required role: " + roleName, tree);
                    }

                    foreach (var node in nodes)
                    {
                        INodeEvaluator eval;
                        if (m_RoleOverrides.TryGetValue(roleName, out eval))
                        {
                            node.Evaluator = eval;
                        }
                    }
                }

                tree.CacheValue("ANODE_CACHED", true);
            }
        }

        protected virtual string[] GetRequiredRoleNames()
        {
            return EmptyRoles;
        }

        public virtual bool CanAssign
        {
            get;
            set;
        }

        public virtual bool HasValue
        {
            get;
            set;
        }

        public Dictionary<string, INodeEvaluator> RoleOverrides
        {
            get
            {
                return m_RoleOverrides;
            }
            set
            {
                m_RoleOverrides = value;
            }
        }
    }
}
