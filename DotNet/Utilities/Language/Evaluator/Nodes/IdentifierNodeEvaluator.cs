﻿using CKLib.Utilities.Language.Evaluator.Nodes;
using CKLib.Utilities.Language.Evaluator.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator
{
    [Flags]
    public enum IdentifierTypes
    {
        Unknown,
        Variable = 1,
        Member = 2,
        Type = 4,
        All = 7
    }

    public class IdentifierNodeEvaluator : ANodeEvaluator
    {
        //private MemberInvoker m_Invoker = new MemberInvoker(null);

        public IdentifierNodeEvaluator()
        {
            RequiresDeclaration = true;
        }

        public override EvaluationResult Evaluate(ScriptEvaluator evaluator, Type expectedType, ASTNode tree)
        {
            object value = null;
            bool hasValue = false;
            if (IdentifierType == IdentifierTypes.Unknown) IdentifierType = IdentifierTypes.All;
            if ((IdentifierType & IdentifierTypes.Variable) == IdentifierTypes.Variable)
            {
                ScriptVariable var;
                if (evaluator.Context.TryGetVariable(tree.Value, out var))
                {
                    hasValue = true;
                    if (!var.IsDefined)
                    {
                        throw new ExecutionException("Variable '" + tree.Value + "' is not defined", tree);
                    }

                    value = var.Value;
                }
            }

            if (!hasValue && (IdentifierType & IdentifierTypes.Member) == IdentifierTypes.Member)
            {
                ScriptVariable var;
                if(evaluator.Context.TryGetVariable(ScriptContext.RESERVED_NAME_THIS, out var))
                {
                    if (var.Value != null)
                    {
                        hasValue = evaluator.Context.MemberInvoker.TryGetMember(var.Value, tree.Value, null, out value);
                    }
                }
                else if (evaluator.Context.TryGetVariable(ScriptContext.RESERVED_NAME_CLASS, out var))
                {
                    if (var.Value != null)
                    {
                        hasValue = evaluator.Context.MemberInvoker.TryGetMember(var.Value, tree.Value, null, out value);
                    }
                }
            }

            if (!hasValue && (IdentifierType & IdentifierTypes.Type) == IdentifierTypes.Type)
            {
                var type = evaluator.Context.BindToType(tree.Value);
                if (type != null)
                {
                    hasValue = true;
                    value = new ClassName(type);
                }
            }

            if (!hasValue)
            {
                throw new ExecutionException("Variable '" + tree.Value + "' is not declared", tree);
            }

            return new EvaluationResult(value);
        }

        public override void MakeAssignment(ScriptEvaluator evaluator, ASTNode tree, object value)
        {
            bool hasHolder = false;
            ScriptVariable var = null;
            if (IdentifierType == IdentifierTypes.Unknown) IdentifierType = IdentifierTypes.All;
            if ((IdentifierType & IdentifierTypes.Variable) == IdentifierTypes.Variable)
            {
                hasHolder = evaluator.Context.TryGetVariable(tree.Value, out var);
            }

            if (!hasHolder && (IdentifierType & IdentifierTypes.Member) == IdentifierTypes.Member)
            {
                if (evaluator.Context.TryGetVariable(ScriptContext.RESERVED_NAME_THIS, out var))
                {
                    hasHolder = evaluator.Context.MemberInvoker.TrySetMember(var.Value, tree.Value, null, value);
                }
                else if (evaluator.Context.TryGetVariable(ScriptContext.RESERVED_NAME_THIS, out var))
                {
                    hasHolder = evaluator.Context.MemberInvoker.TrySetMember(var.Value, tree.Value, null, value);
                }

                if (hasHolder)
                {
                    return;
                }
            }

            if ((IdentifierType & IdentifierTypes.Variable) == IdentifierTypes.Variable)
            {
                if (!hasHolder && RequiresDeclaration)
                {
                    throw new ExecutionException("Variable '" + tree.Value + "' is not declared", tree);
                }
                else if (var == null)
                {
                    // var not declared, but language supports that by auto declaring it
                    var = evaluator.Context.DeclareLocal(tree.Value, null);
                }
                else
                {
                    if (!var.TrySet(value))
                    {
                        throw new ExecutionException(string.Format("Cannot implicitly convert type '{0}' to type'{1}'.", value.GetType().FullName, var.Type.FullName), tree);
                    }
                }
            }
        }

        public override bool CanAssign { get { return IdentifierType != IdentifierTypes.Type; } }

        public override bool HasValue { get { return true; } }

        public bool RequiresDeclaration { get; set; }

        public IdentifierTypes IdentifierType { get; set; }
    }
}
