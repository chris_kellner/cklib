﻿

namespace CKLib.Utilities.Language.Evaluator
{
    #region Using List
    using CKLib.Utilities.Language.Evaluator.Runtime;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    #endregion

    public class ScriptContext
    {
        #region Inner Classes
        private class ScopeKey : IDisposable
        {
            private ScriptContext m_Context;
            public ScopeKey(ScriptContext ctx)
            {
                m_Context = ctx;
                m_Context.IncrementScope();
            }

            public void Dispose()
            {
                m_Context.DecrementScope();
            }
        }

        private class StackFrameKey : IDisposable
        {
            private ScriptContext m_Context;
            public StackFrameKey(ScriptContext ctx)
            {
                m_Context = ctx;
                m_Context.m_Stack.Push(new StackFrame());
            }

            public void Dispose()
            {
                m_Context.m_Stack.Pop();
            }
        }

        private class StackFrame
        {
            #region Inner Classes
            private class Scope
            {
                private Dictionary<string, ScriptVariable> m_Variables;

                public Scope()
                {
                    m_Variables = new Dictionary<string, ScriptVariable>();
                }

                public Scope(IDictionary<string, object> values)
                {
                    m_Variables = values.Where(p => p.Value != null).ToDictionary(p => p.Key, p => new ScriptVariable()
                    {
                        Name = p.Key,
                        Value = p.Value,
                        Type = p.Value.GetType(),
                        IsDefined = true
                    });
                }

                public IDictionary<string, ScriptVariable> Variables
                {
                    get
                    {
                        return m_Variables;
                    }
                }

                public void SetVariable(string name, object value)
                {
                    m_Variables[name] = new ScriptVariable()
                    {
                        Name = name,
                        Value = value,
                        Type = value != null ? value.GetType() : null,
                        IsDefined = true
                    };
                }

                public ScriptVariable DeclareVariable(string name, Type type)
                {
                    if (string.IsNullOrEmpty(name))
                    {
                        throw new ArgumentNullException("name");
                    }

                    return m_Variables[name] = new ScriptVariable()
                    {
                        Name = name,
                        Type = type
                    };
                }

                public bool TryGetVariable(string name, out ScriptVariable value)
                {
                    return m_Variables.TryGetValue(name, out value);
                }
            }
            #endregion

            private Stack<Scope> m_Scope = new Stack<Scope>();
            private Scope m_RootScope = new Scope();

            public StackFrame()
            {
                m_Scope.Push(new Scope());
            }

            public StackFrame(IDictionary<string, object> values)
            {
                m_Scope.Push(new Scope(values));
            }

            public IDictionary<string, ScriptVariable> Globals
            {
                get
                {
                    return m_RootScope.Variables;
                }
            }

            public void SetGlobal(string name, object value)
            {
                m_RootScope.SetVariable(name, value);
            }

            public void SetGlobal(ScriptVariable variable)
            {
                m_RootScope.Variables[variable.Name] = variable;
            }

            public ScriptVariable DeclareGlobal(string name, Type type)
            {
                return m_RootScope.DeclareVariable(name, type);
            }

            public void IncrementScope()
            {
                m_Scope.Push(new Scope());
            }

            public void DecrementScope()
            {
                m_Scope.Pop();
            }

            public void SetLocal(ScriptVariable var)
            {
                if (m_Scope.Count > 0)
                {
                    m_Scope.Peek().Variables[var.Name] = var;
                }
                else
                {
                    Globals[var.Name] = var;
                }
            }

            public void SetLocal(string name, object value)
            {
                ScriptVariable var;
                if (TryGetVariable(name, out var))
                {
                    var.Value = value;
                }
                else if (m_Scope.Count > 0)
                {
                    m_Scope.Peek().SetVariable(name, value);
                }
                else
                {
                    SetGlobal(name, value);
                }
            }

            public ScriptVariable DeclareLocal(string name, Type type)
            {
                if (m_Scope.Count > 0)
                {
                    return m_Scope.Peek().DeclareVariable(name, type);
                }
                else
                {
                    return DeclareGlobal(name, type);
                }
            }

            public bool TryGetVariable(string name, out ScriptVariable value)
            {
                foreach (var scope in m_Scope)
                {
                    if (scope.TryGetVariable(name, out value))
                    {
                        return true;
                    }
                }

                return m_RootScope.TryGetVariable(name, out value);
            }

            public ScriptVariable GetVariable(string name)
            {
                ScriptVariable value;
                foreach (var scope in m_Scope)
                {
                    if (scope.TryGetVariable(name, out value))
                    {
                        return value;
                    }
                }

                if (m_RootScope.TryGetVariable(name, out value))
                {
                    return value;
                }

                throw new KeyNotFoundException(name);
            }
        }
        #endregion
        #region Constants
        public const string RESERVED_NAME_THIS = "@this";
        public const string RESERVED_NAME_CLASS = "@class";
        #endregion
        #region Variables
        private StackFrame m_RootFrame;
        private Stack<StackFrame> m_Stack = new Stack<StackFrame>();
        private References m_References = new References();
        private MemberInvoker m_Invoker;
        #endregion

        #region Constructors
        public ScriptContext()
        {
            m_RootFrame = new StackFrame();
            SupportsLocalScope = true;
        }

        public ScriptContext(IDictionary<string, object> globals)
        {
            m_RootFrame = new StackFrame(globals);
            SupportsLocalScope = true;
        }
        #endregion

        #region Properties
        public IDictionary<string, ScriptVariable> Globals
        {
            get
            {
                return m_RootFrame.Globals;
            }
        }

        public ScriptEvaluator Evaluator { get; set; }

        public MemberInvoker MemberInvoker 
        { 
            get {
                if (m_Invoker == null)
                {
                    m_Invoker = new MemberInvoker(this);
                }

                return m_Invoker; 
            } 
        }

        public bool SupportsLocalScope { get; set; }

        public References References { get { return m_References; } }

        private StackFrame ActiveFrame
        {
            get
            {
                if (m_Stack.Count > 0)
                {
                    return m_Stack.Peek();
                }

                return m_RootFrame;
            }
        }
        #endregion

        #region Methods
        public ScriptContext GlobalClone()
        {
            var ctx = new ScriptContext();
            foreach (var global in Globals)
            {
                ctx.m_RootFrame.SetGlobal(global.Value);
            }

            ctx.m_References = m_References;
            ctx.Evaluator = Evaluator;
            return ctx;
        }

        public Type BindToType(string typeName)
        {
            switch (typeName)
            {
                case "bool":
                    return typeof(bool);
                case "byte":
                    return typeof(byte);
                case "sbyte":
                    return typeof(sbyte);
                case "char":
                    return typeof(char);
                case "short":
                    return typeof(short);
                case "ushort":
                    return typeof(ushort);
                case "int":
                    return typeof(int);
                case "uint":
                    return typeof(uint);
                case "long":
                    return typeof(long);
                case "ulong":
                    return typeof(ulong);
                case "float":
                    return typeof(float);
                case "double":
                    return typeof(double);
                case "string":
                    return typeof(string);
            }

            return References.BindToType(typeName);
        }

        public void SetGlobal(string name, object value)
        {
            m_RootFrame.SetGlobal(name, value);
        }

        public ScriptVariable DeclareGlobal(string name, Type type)
        {
            return m_RootFrame.DeclareGlobal(name, type);
        }

        public IDisposable NewStackFrame()
        {
            return new StackFrameKey(this);
        }

        public IDisposable NewScope()
        {
            return new ScopeKey(this);
        }

        protected void IncrementScope()
        {
            if (SupportsLocalScope)
            {
                ActiveFrame.IncrementScope();
            }
        }

        protected void DecrementScope()
        {
            if (SupportsLocalScope)
            {
                ActiveFrame.DecrementScope();
            }
        }

        public void SetLocal(ScriptVariable var)
        {
            ActiveFrame.SetLocal(var);
        }

        public void SetLocal(string name, object value)
        {
            ActiveFrame.SetLocal(name, value);
        }

        public ScriptVariable DeclareLocal(string name, Type type)
        {
            return ActiveFrame.DeclareLocal(name, type);
        }

        public bool TryGetVariable(string name, out ScriptVariable value)
        {
            if (ActiveFrame.TryGetVariable(name, out value))
            {
                return true;
            }

            return m_RootFrame.TryGetVariable(name, out value);
        }

        public ScriptVariable GetVariable(string name)
        {
            ScriptVariable value;
            if (TryGetVariable(name, out value))
            {
                return value;
            }

            throw new KeyNotFoundException(name);
        }
        #endregion
    }
}
