﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator.Runtime
{
    public interface IMetadataAccessible
    {
        void SetMember(string name, object value);
        TValue GetMember<TValue>(string name);
        TValue InvokeMember<TValue>(string name, params object[] parameters);
    }
}
