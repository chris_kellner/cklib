﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator.Runtime
{
    public class MemberInvoker
    {
        public const BindingFlags ALL_FLAGS =
            BindingFlags.Instance | BindingFlags.Static |
            BindingFlags.Public | BindingFlags.NonPublic |
            BindingFlags.FlattenHierarchy;

        public const BindingFlags CLASS_FLAGS = BindingFlags.Static | BindingFlags.Public |
            BindingFlags.NonPublic | BindingFlags.FlattenHierarchy;

        private Binder m_Binder;
        private BindingFlags m_MemberFlags;

        public MemberInvoker(ScriptContext context)
            : this(context, ALL_FLAGS)
        {
        }

        public MemberInvoker(ScriptContext context, BindingFlags memberFlags)
        {
            m_MemberFlags = memberFlags;
            Context = context;
        }

        public Binder Binder
        {
            get
            {
                if (m_Binder == null)
                {
                    m_Binder = new ScriptBinder();
                }

                return m_Binder;
            }
            set
            {
                m_Binder = value;
            }
        }

        public ScriptContext Context { get; private set; }

        private IEnumerable<MethodBase> GetMethods(Type cType, string name, Type[] genericTypes, bool checkInterfaces)
        {
            if (cType == null) { yield break; }
            var methods = cType.GetMethods(m_MemberFlags);
            foreach (var meth in ProcessMethods(methods, name, genericTypes))
            {
                yield return meth;
            }

            if (cType != typeof(object))
            {
                foreach (var meth in GetMethods(cType.BaseType, name, genericTypes, false))
                {
                    yield return meth;
                }
            }

            if (checkInterfaces)
            {
                foreach (var iFace in cType.GetInterfaces())
                {
                    foreach (var meth in GetMethods(iFace, name, genericTypes, false))
                    {
                        yield return meth;
                    }
                }
            }
        }

        private IEnumerable<MethodBase> ProcessMethods(IEnumerable<MethodInfo> candidates, string name, Type[] genericTypes)
        {
            foreach (var meth in candidates)
            {
                if (meth.Name == name)
                {
                    if (genericTypes != null)
                    {
                        if (meth.IsGenericMethodDefinition)
                        {
                            var m = meth.MakeGenericMethod(genericTypes);
                            if (!m.ContainsGenericParameters)
                            {
                                yield return m;
                            }
                        }
                        else if (meth.IsGenericMethod)
                        {
                            var types = meth.GetGenericArguments();
                            if (types.Length == genericTypes.Length)
                            {
                                bool match = true;
                                for (int i = 0; i < types.Length; i++)
                                {
                                    if (types[i] != genericTypes[i])
                                    {
                                        match = false;
                                        break;
                                    }
                                }

                                if (match)
                                {
                                    yield return meth;
                                }
                            }
                        }
                    }
                    else if (!meth.IsGenericMethodDefinition)
                    {
                        yield return meth;
                    }
                }
            }
        }

        public object InvokeMethod(object target, string methodName, object[] parameters)
        {
            MethodBase method;
            return InvokeMethod(target, methodName, null, parameters, out method);
        }

        public object InvokeMethod(object target, string methodName, object[] parameters, out MethodBase method)
        {
            return InvokeMethod(target, methodName, null, parameters, out method);
        }

        public object InvokeMethod(object target, string methodName, string[] parameterNames, object[] parameters, out MethodBase method)
        {
            return InvokeMethod(target, GetCallSiteType(target), methodName, parameterNames, parameters, out method);
        }

        public object InvokeMethod(object target, Type siteType, string methodName, string[] parameterNames, object[] parameters, out MethodBase method)
        {
            return InvokeMethod(target, siteType, methodName, null, parameterNames, parameters, out method);
        }

        public object InvokeMethod(object target, Type siteType, string methodName, Type[] genericTypes, string[] parameterNames, object[] parameters, out MethodBase method)
        {
            IMetadataAccessible meta = target as IMetadataAccessible;
            if (meta != null)
            {
                method = null;
                return meta.InvokeMember<object>(methodName, parameters);
            }
            else
            {
                siteType = siteType ?? GetCallSiteType(target);
                MethodBase[] methods = GetMethods(siteType, methodName, genericTypes, true).ToArray();
                if (methods.Length == 0)
                {
                    if (Context != null)
                    {
                        methods = ProcessMethods(Context.References.GetExtensionMethods(methodName), methodName, genericTypes)
                            .ToArray();
                        try
                        {
                            return InvokeExtensionMethod(target, parameters, out method, methods);
                        }
                        catch (MemberAccessException ex)
                        {
                            throw new MemberAccessException(string.Format("Method not found: {0} on type {1}", methodName, siteType), ex);
                        }
                    }

                    throw new MemberAccessException(string.Format("Method not found: {0} on type {1}", methodName, siteType));
                }
                
                try
                {
                    var retVal = InvokeMember(target, parameters, out method, methods);
                    return retVal;
                }
                catch (MemberAccessException ex)
                {
                    throw new MemberAccessException(string.Format("Method not found: {0} on type {1}", methodName, siteType), ex);
                }
            }
        }

        private object InvokeMember(object target, object[] parameters, out MethodBase theMethod, params MethodBase[] candidates)
        {
            if (candidates.Length == 0)
            {
                throw new MemberAccessException("no candidates detected");
            }

            object binderState;
            theMethod = Binder.BindToMethod(
                m_MemberFlags,
                candidates,
                ref parameters,
                null,   // ParameterModifiers[]
                System.Globalization.CultureInfo.CurrentCulture,
                null,
                out binderState);
            if (theMethod == null)
            {
                throw new MemberAccessException("Method not found");
            }

            var retVal = theMethod.Invoke(target, parameters);
            if (binderState != null)
            {
                Binder.ReorderArgumentArray(ref parameters, binderState);
            }

            return retVal;
        }

        private object InvokeExtensionMethod(object target, object[] parameters, out MethodBase theMethod, params MethodBase[] candidates)
        {
            if (candidates.Length == 0)
            {
                throw new ArgumentNullException("candidates");
            }

            var p = new object[parameters.Length + 1];
            p[0] = target;
            parameters.CopyTo(p, 1);

            object binderState;
            theMethod = Binder.BindToMethod(
                m_MemberFlags,
                candidates,
                ref p,
                null,   // ParameterModifiers[]
                System.Globalization.CultureInfo.CurrentCulture,
                null,
                out binderState);

            if (theMethod == null)
            {
                throw new MemberAccessException("Method not found");
            }

            var retVal = theMethod.Invoke(null, p);
            if (binderState != null)
            {
                Binder.ReorderArgumentArray(ref p, binderState);
            }

            return retVal;
        }

        public bool TryGetMember(object target, string memberName, object[] indexers, out object value)
        {
            try
            {
                value = GetMember(target, memberName, indexers);
                return true;
            }
            catch (MissingMethodException)
            {
                value = null;
                return false;
            }
        }

        public object GetMember(object target, string memberName, object[] indexers)
        {
            return GetMember(target, GetCallSiteType(target), memberName, indexers);
        }

        public object GetMember(object target, Type targetType, string memberName, object[] indexers)
        {
            IMetadataAccessible meta = target as IMetadataAccessible;
            if (meta != null)
            {
                return meta.GetMember<object>(memberName);
            }
            else
            {
                var type = targetType ?? GetCallSiteType(target);
                var methods = GetMethods(type, "get_" + memberName, null, true).ToArray();
                if (methods.Length > 0)
                {
                    MethodBase method;
                    var retVal = InvokeMember(target, indexers, out method, methods);
                    return retVal;
                }
                else
                {
                    var fields = type.GetFields(m_MemberFlags).Where(f => f.Name == memberName).ToArray();
                    if (fields.Length == 0)
                    {
                        throw new MemberAccessException(string.Format("Field not found: {0} on type {1}", memberName, targetType));
                    }

                    FieldInfo field = Binder.BindToField(m_MemberFlags, fields, null, System.Globalization.CultureInfo.CurrentCulture);
                    var retVal = field.GetValue(target);
                    return retVal;
                }
            }
        }

        public bool TrySetMember(object target, string memberName, object[] indexers, object value)
        {
            try
            {
                SetMember(target, memberName, indexers, value);
                return true;
            }
            catch (MissingMethodException)
            {
                return false;
            }
        }

        public void SetMember(object target, string memberName, object[] indexers, object value)
        {
            SetMember(target, GetCallSiteType(target), memberName, indexers, value);
        }

        public void SetMember(object target, Type targetType, string memberName, object[] indexers, object value)
        {
            IMetadataAccessible meta = target as IMetadataAccessible;
            if (meta != null)
            {
                meta.SetMember(memberName, value);
            }
            else
            {
                var type = targetType ?? GetCallSiteType(target);
                var methods = GetMethods(type, "set_" + memberName, null, true).ToArray();
                if (methods.Length > 0)
                {
                    MethodBase method;
                    object[] args = null;
                    if (indexers != null && indexers.Length > 0)
                    {
                        args = new object[indexers.Length + 1];
                        indexers.CopyTo(args, 0);
                        args[args.Length - 1] = value;
                    }
                    else
                    {
                        args = new object[] { value };
                    }

                    InvokeMember(target, args, out method, methods);
                }
                else
                {
                    var fields = type.GetFields(m_MemberFlags).Where(f => f.Name == memberName).ToArray();
                    if (fields.Length == 0)
                    {
                        throw new MemberAccessException(string.Format("Field not found: {0} on type {1}", memberName, targetType));
                    }
                    FieldInfo field = Binder.BindToField(m_MemberFlags, fields, null, System.Globalization.CultureInfo.CurrentCulture);
                    field.SetValue(target, value);
                }
            }
        }

        public static Type GetCallSiteType(object callsite)
        {
            var custom = callsite as ICustomTypeProvider;
            if (custom != null)
            {
                return custom.GetCustomType();
            }

            return callsite.GetType();
        }
    }
}
