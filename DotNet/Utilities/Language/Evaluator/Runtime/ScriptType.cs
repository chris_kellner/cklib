﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator.Runtime
{
    public class ScriptType : Type, IMetadataAccessible
    {
        #region Inner Classes
        private class MethodBag
        {
            private Dictionary<string, List<MethodInfo>> m_Methods;
            private bool m_CaseSensitive;
            private bool m_AllowOverloading;

            public MethodBag()
                : this(true, true)
            {
            }

            public MethodBag(bool caseSensitive, bool allowOverloading)
            {
                m_CaseSensitive = caseSensitive;
                m_AllowOverloading = allowOverloading;
                m_Methods = new Dictionary<string, List<MethodInfo>>();
            }

            public bool TryAddMethod(MethodInfo info)
            {
                if (info == null)
                {
                    throw new ArgumentNullException("info");
                }

                var slot = GetSlot(info.Name);
                foreach (var method in slot)
                {
                    if (HasSimilarSignature(method, info))
                    {
                        return false;
                    }
                }

                slot.Add(info);
                return true;
            }

            public MethodInfo[] GetMethods(string name)
            {
                return GetSlot(name).ToArray();
            }

            public IEnumerable<MethodInfo> GetMethods(BindingFlags flags)
            {
                return m_Methods.Values
                    .SelectMany(l => l)
                    .Where(m => ScriptType.FilterMethod(m, flags));
            }

            public MethodInfo[] GetMethods(string name, BindingFlags flags)
            {
                return GetSlot(name).ToArray();
            }

            private bool HasSimilarSignature(MethodInfo left, MethodInfo right)
            {
                if (left.Name == right.Name)
                {
                    // If the method can't be overloaded, the name is good enough
                    if (m_AllowOverloading)
                    {
                        var lParams = left.GetParameters();
                        var rParams = right.GetParameters();
                        if (lParams.Length == rParams.Length)
                        {
                            // Check the parameter types for compatibility
                            for (int i = 0; i < lParams.Length; i++)
                            {
                                var lType = lParams[i].ParameterType;
                                var rType = rParams[i].ParameterType;
                                if (lType.IsByRef == rType.IsByRef)
                                {
                                    if (lType.IsAssignableFrom(rType) || rType.IsAssignableFrom(lType))
                                    {
                                        // Parameters are equivalent types
                                        continue;
                                    }

                                    return false;
                                }
                            }
                        }
                    }

                    return true;
                }

                return false;
            }

            private List<MethodInfo> GetSlot(string name)
            {
                List<MethodInfo> slot;
                if (!m_CaseSensitive)
                {
                    name = name.ToLower();
                }

                if (!m_Methods.TryGetValue(name, out slot))
                {
                    slot = new List<MethodInfo>();
                    m_Methods[name] = slot;
                }

                return slot;
            }
        }
        #endregion

        #region Constants
        public const string ANONYMOUS_TYPE_NAME = "__anon__";
        #endregion

        #region Variables
        private Type m_BaseType;
        private ScriptContext m_ClassContext;
        private MethodBag m_Methods;
        private List<ConstructorInfo> m_Constructors;
        private Dictionary<string, FieldInfo> m_Fields;
        private Dictionary<string, PropertyInfo> m_Properties;
        private Dictionary<string, EventInfo> m_Events;
        private Dictionary<string, Type> m_NestedTypes;
        private Dictionary<string, Type> m_DefinedInterfaces;

        private string m_Namespace;
        private string m_Name;
        private bool m_IsByRef;
        #endregion

        public ScriptType(ScriptContext classContext, Type baseType)
            : this(string.Empty, ANONYMOUS_TYPE_NAME, classContext, baseType)
        {
        }

        public ScriptType(string @namespace, string name, ScriptContext classContext, Type baseType)
        {
            m_Namespace = @namespace;
            m_Name = name;
            m_BaseType = baseType;
            m_ClassContext = classContext;
            m_Methods = new MethodBag();
        }

        public ScriptObject CreateInstance()
        {
            return new ScriptObject(this);
        }

        public ScriptContext Context
        {
            get
            {
                return m_ClassContext;
            }
        }

        public static bool MemberFilter(MemberInfo info, BindingFlags flags)
        {
            return true;
        }

        public override System.Reflection.Assembly Assembly
        {
            get { return Module.Assembly; }
        }

        public override string AssemblyQualifiedName
        {
            get 
            { 
                throw new NotImplementedException(); 
            }
        }

        public override Type BaseType
        {
            get { return m_BaseType; }
        }

        public override string FullName
        {
            get { return m_Namespace + Type.Delimiter + m_Name; }
        }

        public override Guid GUID
        {
            get { throw new NotImplementedException(); }
        }

        protected override System.Reflection.TypeAttributes GetAttributeFlagsImpl()
        {
            throw new NotImplementedException();
        }

        protected override System.Reflection.ConstructorInfo GetConstructorImpl(System.Reflection.BindingFlags bindingAttr, System.Reflection.Binder binder, System.Reflection.CallingConventions callConvention, Type[] types, System.Reflection.ParameterModifier[] modifiers)
        {
            return (ConstructorInfo)binder.SelectMethod(bindingAttr, m_Constructors.ToArray(), types, modifiers);
        }

        public override System.Reflection.ConstructorInfo[] GetConstructors(System.Reflection.BindingFlags bindingAttr)
        {
            throw new NotImplementedException();
        }

        public override Type GetElementType()
        {
            throw new NotImplementedException();
        }

        public override System.Reflection.EventInfo GetEvent(string name, System.Reflection.BindingFlags bindingAttr)
        {
            throw new NotImplementedException();
        }

        public override System.Reflection.EventInfo[] GetEvents(System.Reflection.BindingFlags bindingAttr)
        {
            throw new NotImplementedException();
        }

        public override System.Reflection.FieldInfo GetField(string name, System.Reflection.BindingFlags bindingAttr)
        {
            throw new NotImplementedException();
        }

        public override System.Reflection.FieldInfo[] GetFields(System.Reflection.BindingFlags bindingAttr)
        {
            throw new NotImplementedException();
        }

        public override Type GetInterface(string name, bool ignoreCase)
        {
            if (ignoreCase)
            {
                return m_DefinedInterfaces
                    .FirstOrDefault(pair => name.Equals(pair.Key, StringComparison.CurrentCultureIgnoreCase))
                    .Value; // OK to use value on FirstOrDefault here because KeyValuePair<,> is a value type.
            }
            else
            {
                Type t;
                m_DefinedInterfaces.TryGetValue(name, out t);
                return t;
            }
        }

        public override Type[] GetInterfaces()
        {
            return m_DefinedInterfaces.Values.ToArray();
        }

        public override System.Reflection.MemberInfo[] GetMembers(System.Reflection.BindingFlags bindingAttr)
        {
            throw new NotImplementedException();
        }

        protected override System.Reflection.MethodInfo GetMethodImpl(string name, System.Reflection.BindingFlags bindingAttr, System.Reflection.Binder binder, System.Reflection.CallingConventions callConvention, Type[] types, System.Reflection.ParameterModifier[] modifiers)
        {
            return (MethodInfo)binder.SelectMethod(bindingAttr, m_Methods.GetMethods(name), types, modifiers);
        }

        public override System.Reflection.MethodInfo[] GetMethods(BindingFlags bindingAttr)
        {
            IEnumerable<MethodInfo> methods = m_Methods.GetMethods(bindingAttr);
            if (m_BaseType != null && (bindingAttr & BindingFlags.FlattenHierarchy) == BindingFlags.FlattenHierarchy)
            {
                methods = methods.Concat(m_BaseType.GetMethods(bindingAttr));
            }

            return methods.ToArray();
        }

        public override Type GetNestedType(string name, System.Reflection.BindingFlags bindingAttr)
        {
            throw new NotImplementedException();
        }

        public override Type[] GetNestedTypes(System.Reflection.BindingFlags bindingAttr)
        {
            throw new NotImplementedException();
        }

        public override System.Reflection.PropertyInfo[] GetProperties(BindingFlags bindingAttr)
        {
            return Properties.Where(p => FilterProperty(p, bindingAttr)).ToArray();
        }

        protected override System.Reflection.PropertyInfo GetPropertyImpl(string name, BindingFlags bindingAttr, Binder binder, Type returnType, Type[] types, ParameterModifier[] modifiers)
        {
            throw new NotImplementedException();
        }

        protected override bool HasElementTypeImpl()
        {
            return false;
        }

        public override object InvokeMember(string name, BindingFlags invokeAttr, Binder binder, object target, object[] args, ParameterModifier[] modifiers, CultureInfo culture, string[] namedParameters)
        {
            binder = binder ?? Type.DefaultBinder;
            if ((invokeAttr & BindingFlags.GetProperty) == BindingFlags.GetProperty)
            {
                object binderState;
                name = "get_" + name;
                var method = binder.BindToMethod(
                    invokeAttr,
                    GetMethods(invokeAttr).Where(m => m.Name == name).ToArray(),
                    ref args,
                    modifiers,
                    culture,
                    namedParameters,
                    out binderState);
                if (method != null)
                {
                    var retVal = method.Invoke(target, args);
                    binder.ReorderArgumentArray(ref args, binderState);
                    return retVal;
                }
            }

            if ((invokeAttr & BindingFlags.SetProperty) == BindingFlags.SetProperty)
            {
                object binderState;
                name = "set_" + name;
                var method = binder.BindToMethod(
                    invokeAttr,
                    GetMethods(invokeAttr).Where(m => m.Name == name).ToArray(),
                    ref args,
                    modifiers,
                    culture,
                    namedParameters,
                    out binderState);
                if (method != null)
                {
                    var retVal = method.Invoke(target, args);
                    binder.ReorderArgumentArray(ref args, binderState);
                    return retVal;
                }
            }

            if ((invokeAttr & BindingFlags.InvokeMethod) == BindingFlags.InvokeMethod)
            {
                object binderState;
                var method = binder.BindToMethod(
                    invokeAttr,
                    GetMethods(invokeAttr).Where(m => m.Name == name).ToArray(),
                    ref args,
                    modifiers,
                    culture,
                    namedParameters,
                    out binderState);
                if (method != null)
                {
                    var retVal = method.Invoke(target, args);
                    binder.ReorderArgumentArray(ref args, binderState);
                    return retVal;
                }
            }

            throw new MissingMemberException(FullName, name);
        }

        protected override bool IsArrayImpl()
        {
            return false;
        }

        protected override bool IsByRefImpl()
        {
            return m_IsByRef;
        }

        protected override bool IsCOMObjectImpl()
        {
            return false;
        }

        protected override bool IsPointerImpl()
        {
            return false;
        }

        protected override bool IsPrimitiveImpl()
        {
            return false;
        }

        public override System.Reflection.Module Module
        {
            get { throw new NotImplementedException(); }
        }

        public override string Namespace
        {
            get { return m_Namespace; }
        }

        public override Type UnderlyingSystemType
        {
            get { return typeof(ScriptObject); }
        }

        public override object[] GetCustomAttributes(Type attributeType, bool inherit)
        {
            throw new NotImplementedException();
        }

        public override object[] GetCustomAttributes(bool inherit)
        {
            throw new NotImplementedException();
        }

        public override bool IsDefined(Type attributeType, bool inherit)
        {
            throw new NotImplementedException();
        }

        public override string Name
        {
            get { return m_Name; }
        }

        public IEnumerable<PropertyInfo> Properties
        {
            get
            {
                return m_Properties.Values;
            }
        }

        #region IMetadataAccessible
        void IMetadataAccessible.SetMember(string name, object value)
        {
            throw new NotImplementedException();
        }

        TValue IMetadataAccessible.GetMember<TValue>(string name)
        {
            throw new NotImplementedException();
        }

        TValue IMetadataAccessible.InvokeMember<TValue>(string name, params object[] parameters)
        {
            throw new NotImplementedException();
        }
        #endregion

        private static bool FilterMethod(MethodInfo member, BindingFlags flags)
        {
            if (member.IsStatic)
            {
                if ((flags & BindingFlags.Static) != BindingFlags.Static)
                {
                    return false;
                }
            }
            else if ((flags & BindingFlags.Instance) != BindingFlags.Instance
                   && flags != BindingFlags.Default)
            {

                return false;
            }

            if (!member.IsPublic)
            {
                return (flags & BindingFlags.NonPublic) != BindingFlags.NonPublic;
            }
            else
            {
                return flags == BindingFlags.Default ||
                    ((flags & BindingFlags.Public) != BindingFlags.Public);
            }
        }

        private static bool FilterProperty(PropertyInfo member, BindingFlags flags)
        {
            var getter = member.GetGetMethod(true);
            return FilterMethod(getter, flags);
        }
    }
}
