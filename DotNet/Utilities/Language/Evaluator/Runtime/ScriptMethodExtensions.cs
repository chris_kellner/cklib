﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator.Runtime
{
    /// <summary>
    /// This class provides the functionality for creating delegates to late-bound dynamic script method.
    /// As this functionality is not supported by the .Net runtime, this additional step is necessary.
    /// </summary>
    public static class ScriptMethodExtensions
    {
        #region Inner Classes
        // Don't mind this ugliness, its necessary to support binding script method into runtime callbacks of different signatures
        private delegate void RefAction_1<T>(ref T t);

        private delegate void RefAction_01<T1, T2>(T1 t1, ref T2 t2);
        private delegate void RefAction_10<T1, T2>(ref T1 t1, T2 t2);
        private delegate void RefAction_11<T1, T2>(ref T1 t1, ref T2 t2);

        private delegate void RefAction_001<T1, T2, T3>(T1 t1, T2 t2, ref T3 t3);
        private delegate void RefAction_010<T1, T2, T3>(T1 t1, ref T2 t2, T3 t3);
        private delegate void RefAction_011<T1, T2, T3>(T1 t1, ref T2 t2, ref T3 t3);
        private delegate void RefAction_100<T1, T2, T3>(ref T1 t1, T2 t2, T3 t3);
        private delegate void RefAction_101<T1, T2, T3>(ref T1 t1, T2 t2, ref T3 t3);
        private delegate void RefAction_110<T1, T2, T3>(ref T1 t1, ref T2 t2, T3 t3);
        private delegate void RefAction_111<T1, T2, T3>(ref T1 t1, ref T2 t2, ref T3 t3);

        private delegate void RefAction_0001<T1, T2, T3, T4>(T1 t1, T2 t2, T3 t3, ref T4 t4);
        private delegate void RefAction_0010<T1, T2, T3, T4>(T1 t1, T2 t2, ref T3 t3, T4 t4);
        private delegate void RefAction_0011<T1, T2, T3, T4>(T1 t1, T2 t2, ref T3 t3, ref T4 t4);
        private delegate void RefAction_0100<T1, T2, T3, T4>(T1 t1, ref T2 t2, T3 t3, T4 t4);
        private delegate void RefAction_0101<T1, T2, T3, T4>(T1 t1, ref T2 t2, T3 t3, ref T4 t4);
        private delegate void RefAction_0110<T1, T2, T3, T4>(T1 t1, ref T2 t2, ref T3 t3, T4 t4);
        private delegate void RefAction_0111<T1, T2, T3, T4>(T1 t1, ref T2 t2, ref T3 t3, ref T4 t4);
        private delegate void RefAction_1000<T1, T2, T3, T4>(ref T1 t1, T2 t2, T3 t3, T4 t4);
        private delegate void RefAction_1001<T1, T2, T3, T4>(ref T1 t1, T2 t2, T3 t3, ref T4 t4);
        private delegate void RefAction_1010<T1, T2, T3, T4>(ref T1 t1, T2 t2, ref T3 t3, T4 t4);
        private delegate void RefAction_1011<T1, T2, T3, T4>(ref T1 t1, T2 t2, ref T3 t3, ref T4 t4);
        private delegate void RefAction_1100<T1, T2, T3, T4>(ref T1 t1, ref T2 t2, T3 t3, T4 t4);
        private delegate void RefAction_1101<T1, T2, T3, T4>(ref T1 t1, ref T2 t2, T3 t3, ref T4 t4);
        private delegate void RefAction_1110<T1, T2, T3, T4>(ref T1 t1, ref T2 t2, ref T3 t3, T4 t4);
        private delegate void RefAction_1111<T1, T2, T3, T4>(ref T1 t1, ref T2 t2, ref T3 t3, ref T4 t4);

        private delegate TResult RefFunc_1<T, TResult>(ref T t);

        private delegate TResult RefFunc_01<T1, T2, TResult>(T1 t1, ref T2 t2);
        private delegate TResult RefFunc_10<T1, T2, TResult>(ref T1 t1, T2 t2);
        private delegate TResult RefFunc_11<T1, T2, TResult>(ref T1 t1, ref T2 t2);

        private delegate TResult RefFunc_001<T1, T2, T3, TResult>(T1 t1, T2 t2, ref T3 t3);
        private delegate TResult RefFunc_010<T1, T2, T3, TResult>(T1 t1, ref T2 t2, T3 t3);
        private delegate TResult RefFunc_011<T1, T2, T3, TResult>(T1 t1, ref T2 t2, ref T3 t3);
        private delegate TResult RefFunc_100<T1, T2, T3, TResult>(ref T1 t1, T2 t2, T3 t3);
        private delegate TResult RefFunc_101<T1, T2, T3, TResult>(ref T1 t1, T2 t2, ref T3 t3);
        private delegate TResult RefFunc_110<T1, T2, T3, TResult>(ref T1 t1, ref T2 t2, T3 t3);
        private delegate TResult RefFunc_111<T1, T2, T3, TResult>(ref T1 t1, ref T2 t2, ref T3 t3);

        private delegate TResult RefFunc_0001<T1, T2, T3, T4, TResult>(T1 t1, T2 t2, T3 t3, ref T4 t4);
        private delegate TResult RefFunc_0010<T1, T2, T3, T4, TResult>(T1 t1, T2 t2, ref T3 t3, T4 t4);
        private delegate TResult RefFunc_0011<T1, T2, T3, T4, TResult>(T1 t1, T2 t2, ref T3 t3, ref T4 t4);
        private delegate TResult RefFunc_0100<T1, T2, T3, T4, TResult>(T1 t1, ref T2 t2, T3 t3, T4 t4);
        private delegate TResult RefFunc_0101<T1, T2, T3, T4, TResult>(T1 t1, ref T2 t2, T3 t3, ref T4 t4);
        private delegate TResult RefFunc_0110<T1, T2, T3, T4, TResult>(T1 t1, ref T2 t2, ref T3 t3, T4 t4);
        private delegate TResult RefFunc_0111<T1, T2, T3, T4, TResult>(T1 t1, ref T2 t2, ref T3 t3, ref T4 t4);
        private delegate TResult RefFunc_1000<T1, T2, T3, T4, TResult>(ref T1 t1, T2 t2, T3 t3, T4 t4);
        private delegate TResult RefFunc_1001<T1, T2, T3, T4, TResult>(ref T1 t1, T2 t2, T3 t3, ref T4 t4);
        private delegate TResult RefFunc_1010<T1, T2, T3, T4, TResult>(ref T1 t1, T2 t2, ref T3 t3, T4 t4);
        private delegate TResult RefFunc_1011<T1, T2, T3, T4, TResult>(ref T1 t1, T2 t2, ref T3 t3, ref T4 t4);
        private delegate TResult RefFunc_1100<T1, T2, T3, T4, TResult>(ref T1 t1, ref T2 t2, T3 t3, T4 t4);
        private delegate TResult RefFunc_1101<T1, T2, T3, T4, TResult>(ref T1 t1, ref T2 t2, T3 t3, ref T4 t4);
        private delegate TResult RefFunc_1110<T1, T2, T3, T4, TResult>(ref T1 t1, ref T2 t2, ref T3 t3, T4 t4);
        private delegate TResult RefFunc_1111<T1, T2, T3, T4, TResult>(ref T1 t1, ref T2 t2, ref T3 t3, ref T4 t4);
        #endregion

        #region Interface
        public static Delegate MakeDelegate(Type delegateType, MethodInfo method, object target, bool throwOnBindFailure)
        {
            var scriptMethod = method as ScriptMethod;
            if (scriptMethod != null)
            {
                return MakeScriptDelegate(delegateType, scriptMethod, target, throwOnBindFailure);
            }
            else
            {
                return Delegate.CreateDelegate(delegateType, target, method, throwOnBindFailure);
            }
        }

        public static TDelegate MakeDelegate<TDelegate>(this MethodInfo method, object target) where TDelegate : class
        {
            return MakeDelegate<TDelegate>(method, target, true);
        }

        public static TDelegate MakeDelegate<TDelegate>(this MethodInfo method) where TDelegate : class
        {
            return MakeDelegate<TDelegate>(method, null, true);
        }

        public static TDelegate MakeDelegate<TDelegate>(this MethodInfo method, bool throwOnBindFailure) where TDelegate : class
        {
            return MakeDelegate<TDelegate>(method, null, throwOnBindFailure);
        }

        public static TDelegate MakeDelegate<TDelegate>(this MethodInfo method, object target, bool throwOnBindFailure) where TDelegate : class
        {
            return MakeDelegate(typeof(TDelegate), method, target, throwOnBindFailure) as TDelegate;
        }

        public static Delegate CastDelegate(Type delegateType, Delegate @delegate)
        {
            if (!delegateType.IsInstanceOfType(delegateType))
            {
                if (@delegate is ScriptFunction)
                {
                    return MakeScriptDelegate(delegateType, @delegate.Target as MethodInfo, null, true);
                }

                return Delegate.CreateDelegate(delegateType, @delegate.Target, @delegate.Method);
            }

            return @delegate;
        }

        public static TDelegate CastDelegate<TDelegate>(this Delegate @delegate) where TDelegate : class
        {
            return CastDelegate(typeof(TDelegate), @delegate) as TDelegate;
        }
        #endregion

        #region Helpers
        private static bool CheckTypeParams(MethodInfo info, bool throwOnBindFailure, Type returnType, params Type[] typeParams)
        {
            return true;
        }

        private static Delegate MakeScriptDelegate(Type delegateType, MethodInfo info, object target, bool throwOnBindFailure)
        {
            if (delegateType == typeof(ScriptFunction) && info is ScriptMethod)
            {
                return ((ScriptMethod)info).CreateDelegate(target);
            }

            Delegate del = null;
            var invoke = delegateType.GetMethod("Invoke");
            if (invoke.ReturnType == null || invoke.ReturnType == typeof(void))
            {
                del = MakeScriptAction(invoke, info, target, throwOnBindFailure);
            }
            else
            {
                del = MakeScriptFunc(invoke, info, target, throwOnBindFailure);
            }

            if (del != null && !delegateType.IsInstanceOfType(del))
            {
                del = Delegate.CreateDelegate(delegateType, del.Target, del.Method, throwOnBindFailure);
            }

            return del;
        }

        private static Delegate MakeScriptAction(MethodInfo invokeMethod, MethodInfo info, object target, bool throwOnBindFailure)
        {
            Type[] types;
            Type returnType;
            string code = GetParameterCode(invokeMethod, out returnType, out types);
            var makeAction = typeof(ScriptMethodExtensions).GetMethod("MakeAction_" + code, BindingFlags.NonPublic | BindingFlags.Static);
            if (types.Length > 0)
            {
                makeAction = makeAction.MakeGenericMethod(types);
            }

            Delegate del = makeAction.Invoke(null, new object[] { target, info, throwOnBindFailure }) as Delegate;
            return del;
        }

        private static Delegate MakeScriptFunc(MethodInfo invokeMethod, MethodInfo info, object target, bool throwOnBindFailure)
        {
            Type[] types;
            Type returnType;
            string code = GetParameterCode(invokeMethod, out returnType, out types);
            var makeAction = typeof(ScriptMethodExtensions).GetMethod("MakeFunc_" + code, BindingFlags.NonPublic | BindingFlags.Static);
            if (types.Length > 0)
            {
                Type[] tParams = new Type[types.Length + 1];
                types.CopyTo(tParams, 1);
                tParams[0] = returnType;
                makeAction = makeAction.MakeGenericMethod(tParams);
            }
            else
            {
                makeAction = makeAction.MakeGenericMethod(returnType);
            }

            Delegate del = makeAction.Invoke(null, new object[] { target, info, throwOnBindFailure }) as Delegate;
            return del;
        }

        private static string GetParameterCode(MethodInfo invoke, out Type returnType, out Type[] parameterTypes)
        {
            returnType = invoke.ReturnType;
            parameterTypes = invoke.GetParameters().Select(p => p.ParameterType).ToArray();
            string ret = string.Empty;
            foreach (var type in parameterTypes)
            {
                if (type.IsByRef)
                {
                    ret += "1";
                }
                else
                {
                    ret += "0";
                }
            }

            return ret;
        }

        #endregion

        #region Actions
        #region 0 Parameters
        private static Action MakeAction_(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, null, Type.EmptyTypes))
            {
                return null;
            }

            return () => info.Invoke(target, new object[0] { });
        }
        #endregion

        #region 1 Parameter
        private static Action<T> MakeAction_0<T>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, null, new Type[] { typeof(T) }))
            {
                return null;
            }

            return (_1) => info.Invoke(target, new object[] { _1 });
        }

        private static RefAction_1<T> MakeAction_1<T>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, null, new Type[] { typeof(T).MakeByRefType() }))
            {
                return null;
            }

            return delegate(ref T _1)
            {
                var obj = new object[] { _1 };
                info.Invoke(target, obj);
                _1 = (T)obj[0];
            };
        }
        #endregion

        #region 2 Parameters
        private static Action<T1, T2> MakeAction_00<T1, T2>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, null, new Type[] { typeof(T1), typeof(T2) }))
            {
                return null;
            }

            return (_1, _2) => info.Invoke(target, new object[] { _1, _2 });
        }

        private static RefAction_01<T1, T2> MakeAction_01<T1, T2>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, null, new Type[] { typeof(T1), typeof(T2).MakeByRefType() }))
            {
                return null;
            }

            return delegate(T1 _1, ref T2 _2)
            {
                var obj = new object[] { _1, _2 };
                info.Invoke(target, obj);
                _2 = (T2)obj[1];
            };
        }

        private static RefAction_10<T1, T2> MakeAction_10<T1, T2>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, null, new Type[] { typeof(T1).MakeByRefType(), typeof(T2) }))
            {
                return null;
            }

            return delegate(ref T1 _1, T2 _2)
            {
                var obj = new object[] { _1, _2 };
                info.Invoke(target, obj);
                _1 = (T1)obj[0];
            };
        }

        private static RefAction_11<T1, T2> MakeAction_11<T1, T2>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, null, new Type[] { typeof(T1).MakeByRefType(), typeof(T2).MakeByRefType() }))
            {
                return null;
            }

            return delegate(ref T1 _1, ref T2 _2)
            {
                var obj = new object[] { _1, _2 };
                info.Invoke(target, obj);
                _1 = (T1)obj[0];
                _2 = (T2)obj[1];
            };
        }
        #endregion

        #region 3 Parameters
        private static Action<T1, T2, T3> MakeAction_000<T1, T2, T3>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, null, new Type[] { typeof(T1), typeof(T2), typeof(T3) }))
            {
                return null;
            }

            return (_1, _2, _3) => info.Invoke(target, new object[] { _1, _2, _3 });
        }

        private static RefAction_001<T1, T2, T3> MakeAction_001<T1, T2, T3>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, null, new Type[] { typeof(T1), typeof(T2), typeof(T3).MakeByRefType() }))
            {
                return null;
            }

            return delegate(T1 _1, T2 _2, ref T3 _3)
            {
                var obj = new object[] { _1, _2, _3 };
                info.Invoke(target, obj);
                _3 = (T3)obj[2];
            };
        }

        private static RefAction_010<T1, T2, T3> MakeAction_010<T1, T2, T3>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, null, new Type[] { typeof(T1), typeof(T2).MakeByRefType(), typeof(T3) }))
            {
                return null;
            }

            return delegate(T1 _1, ref T2 _2, T3 _3)
            {
                var obj = new object[] { _1, _2, _3 };
                info.Invoke(target, obj);
                _2 = (T2)obj[1];
            };
        }

        private static RefAction_011<T1, T2, T3> MakeAction_011<T1, T2, T3>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, null, new Type[] { typeof(T1), typeof(T2).MakeByRefType(), typeof(T3).MakeByRefType() }))
            {
                return null;
            }

            return delegate(T1 _1, ref T2 _2, ref T3 _3)
            {
                var obj = new object[] { _1, _2, _3 };
                info.Invoke(target, obj);
                _2 = (T2)obj[1];
                _3 = (T3)obj[2];
            };
        }

        private static RefAction_100<T1, T2, T3> MakeAction_100<T1, T2, T3>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, null, new Type[] { typeof(T1).MakeByRefType(), typeof(T2), typeof(T3) }))
            {
                return null;
            }

            return delegate(ref T1 _1, T2 _2, T3 _3)
            {
                var obj = new object[] { _1, _2, _3 };
                info.Invoke(target, obj);
                _1 = (T1)obj[0];
            };
        }

        private static RefAction_101<T1, T2, T3> MakeAction_101<T1, T2, T3>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, null, new Type[] { typeof(T1).MakeByRefType(), typeof(T2), typeof(T3).MakeByRefType() }))
            {
                return null;
            }

            return delegate(ref T1 _1, T2 _2, ref T3 _3)
            {
                var obj = new object[] { _1, _2, _3 };
                info.Invoke(target, obj);
                _1 = (T1)obj[0];
                _3 = (T3)obj[2];
            };
        }

        private static RefAction_110<T1, T2, T3> MakeAction_110<T1, T2, T3>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, null, new Type[] { typeof(T1).MakeByRefType(), typeof(T2).MakeByRefType(), typeof(T3) }))
            {
                return null;
            }

            return delegate(ref T1 _1, ref T2 _2, T3 _3)
            {
                var obj = new object[] { _1, _2, _3 };
                info.Invoke(target, obj);
                _1 = (T1)obj[0];
                _2 = (T2)obj[1];
            };
        }

        private static RefAction_111<T1, T2, T3> MakeAction_111<T1, T2, T3>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, null, new Type[] { typeof(T1).MakeByRefType(), typeof(T2).MakeByRefType(), typeof(T3).MakeByRefType() }))
            {
                return null;
            }

            return delegate(ref T1 _1, ref T2 _2, ref T3 _3)
            {
                var obj = new object[] { _1, _2, _3 };
                info.Invoke(target, obj);
                _1 = (T1)obj[0];
                _2 = (T2)obj[1];
                _3 = (T3)obj[2];
            };
        }
        #endregion

        #region 4 Parameters
        private static Action<T1, T2, T3, T4> MakeAction_0000<T1, T2, T3, T4>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, null, new Type[] { typeof(T1), typeof(T2), typeof(T3), typeof(T4) }))
            {
                return null;
            }

            return (_1, _2, _3, _4) => info.Invoke(target, new object[] { _1, _2, _3, _4 });
        }

        private static RefAction_0001<T1, T2, T3, T4> MakeAction_0001<T1, T2, T3, T4>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, null,
                new Type[] { typeof(T1), typeof(T2), typeof(T3), typeof(T4).MakeByRefType() }))
            {
                return null;
            }

            return delegate(T1 _1, T2 _2, T3 _3, ref T4 _4)
            {
                var obj = new object[] { _1, _2, _3, _4 };
                info.Invoke(target, obj);
                _4 = (T4)obj[3];
            };
        }

        private static RefAction_0010<T1, T2, T3, T4> MakeAction_0010<T1, T2, T3, T4>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, null,
                new Type[] { typeof(T1), typeof(T2), typeof(T3).MakeByRefType(), typeof(T4) }))
            {
                return null;
            }

            return delegate(T1 _1, T2 _2, ref T3 _3, T4 _4)
            {
                var obj = new object[] { _1, _2, _3, _4 };
                info.Invoke(target, obj);
                _3 = (T3)obj[2];
            };
        }

        private static RefAction_0011<T1, T2, T3, T4> MakeAction_0011<T1, T2, T3, T4>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, null,
                new Type[] { typeof(T1), typeof(T2), typeof(T3).MakeByRefType(), typeof(T4).MakeByRefType() }))
            {
                return null;
            }

            return delegate(T1 _1, T2 _2, ref T3 _3, ref T4 _4)
            {
                var obj = new object[] { _1, _2, _3, _4 };
                info.Invoke(target, obj);
                _3 = (T3)obj[2];
                _4 = (T4)obj[3];
            };
        }

        private static RefAction_0100<T1, T2, T3, T4> MakeAction_0100<T1, T2, T3, T4>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, null,
                new Type[] { typeof(T1), typeof(T2).MakeByRefType(), typeof(T3), typeof(T4) }))
            {
                return null;
            }

            return delegate(T1 _1, ref T2 _2, T3 _3, T4 _4)
            {
                var obj = new object[] { _1, _2, _3, _4 };
                info.Invoke(target, obj);
                _2 = (T2)obj[1];
            };
        }

        private static RefAction_0101<T1, T2, T3, T4> MakeAction_0101<T1, T2, T3, T4>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, null,
                new Type[] { typeof(T1), typeof(T2).MakeByRefType(), typeof(T3), typeof(T4).MakeByRefType() }))
            {
                return null;
            }

            return delegate(T1 _1, ref T2 _2, T3 _3, ref T4 _4)
            {
                var obj = new object[] { _1, _2, _3, _4 };
                info.Invoke(target, obj);
                _2 = (T2)obj[1];
                _4 = (T4)obj[3];
            };
        }

        private static RefAction_0111<T1, T2, T3, T4> MakeAction_0111<T1, T2, T3, T4>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, null,
                new Type[] { typeof(T1), typeof(T2).MakeByRefType(), typeof(T3).MakeByRefType(), typeof(T4).MakeByRefType() }))
            {
                return null;
            }

            return delegate(T1 _1, ref T2 _2, ref T3 _3, ref T4 _4)
            {
                var obj = new object[] { _1, _2, _3, _4 };
                info.Invoke(target, obj);
                _2 = (T2)obj[1];
                _3 = (T3)obj[2];
                _4 = (T4)obj[3];
            };
        }

        private static RefAction_1000<T1, T2, T3, T4> MakeAction_1000<T1, T2, T3, T4>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, null,
                new Type[] { typeof(T1).MakeByRefType(), typeof(T2), typeof(T3), typeof(T4) }))
            {
                return null;
            }

            return delegate(ref T1 _1, T2 _2, T3 _3, T4 _4)
            {
                var obj = new object[] { _1, _2, _3, _4 };
                info.Invoke(target, obj);
                _1 = (T1)obj[0];
            };
        }

        private static RefAction_1001<T1, T2, T3, T4> MakeAction_1001<T1, T2, T3, T4>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, null,
                new Type[] { typeof(T1).MakeByRefType(), typeof(T2), typeof(T3), typeof(T4).MakeByRefType() }))
            {
                return null;
            }

            return delegate(ref T1 _1, T2 _2, T3 _3, ref T4 _4)
            {
                var obj = new object[] { _1, _2, _3, _4 };
                info.Invoke(target, obj);
                _1 = (T1)obj[0];
                _4 = (T4)obj[3];
            };
        }

        private static RefAction_1010<T1, T2, T3, T4> MakeAction_1010<T1, T2, T3, T4>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, null,
                new Type[] { typeof(T1).MakeByRefType(), typeof(T2), typeof(T3).MakeByRefType(), typeof(T4) }))
            {
                return null;
            }

            return delegate(ref T1 _1, T2 _2, ref T3 _3, T4 _4)
            {
                var obj = new object[] { _1, _2, _3, _4 };
                info.Invoke(target, obj);
                _1 = (T1)obj[0];
                _3 = (T3)obj[2];
            };
        }

        private static RefAction_1011<T1, T2, T3, T4> MakeAction_1011<T1, T2, T3, T4>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, null,
                new Type[] { typeof(T1).MakeByRefType(), typeof(T2), typeof(T3).MakeByRefType(), typeof(T4).MakeByRefType() }))
            {
                return null;
            }

            return delegate(ref T1 _1, T2 _2, ref T3 _3, ref T4 _4)
            {
                var obj = new object[] { _1, _2, _3, _4 };
                info.Invoke(target, obj);
                _1 = (T1)obj[0];
                _3 = (T3)obj[2];
                _4 = (T4)obj[3];
            };
        }

        private static RefAction_1100<T1, T2, T3, T4> MakeAction_1100<T1, T2, T3, T4>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, null,
                new Type[] { typeof(T1).MakeByRefType(), typeof(T2).MakeByRefType(), typeof(T3), typeof(T4) }))
            {
                return null;
            }

            return delegate(ref T1 _1, ref T2 _2, T3 _3, T4 _4)
            {
                var obj = new object[] { _1, _2, _3, _4 };
                info.Invoke(target, obj);
                _1 = (T1)obj[0];
                _2 = (T2)obj[1];
            };
        }

        private static RefAction_1101<T1, T2, T3, T4> MakeAction_1101<T1, T2, T3, T4>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, null,
                new Type[] { typeof(T1).MakeByRefType(), typeof(T2).MakeByRefType(), typeof(T3), typeof(T4).MakeByRefType() }))
            {
                return null;
            }

            return delegate(ref T1 _1, ref T2 _2, T3 _3, ref T4 _4)
            {
                var obj = new object[] { _1, _2, _3, _4 };
                info.Invoke(target, obj);
                _1 = (T1)obj[0];
                _2 = (T2)obj[1];
                _4 = (T4)obj[3];
            };
        }

        private static RefAction_1110<T1, T2, T3, T4> MakeAction_1110<T1, T2, T3, T4>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, null,
                new Type[] { typeof(T1).MakeByRefType(), typeof(T2).MakeByRefType(), typeof(T3).MakeByRefType(), typeof(T4) }))
            {
                return null;
            }

            return delegate(ref T1 _1, ref T2 _2, ref T3 _3, T4 _4)
            {
                var obj = new object[] { _1, _2, _3, _4 };
                info.Invoke(target, obj);
                _1 = (T1)obj[0];
                _2 = (T2)obj[1];
                _3 = (T3)obj[2];
            };
        }

        private static RefAction_1111<T1, T2, T3, T4> MakeAction_1111<T1, T2, T3, T4>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, null,
                new Type[] { typeof(T1).MakeByRefType(), typeof(T2).MakeByRefType(), typeof(T3).MakeByRefType(), typeof(T4).MakeByRefType() }))
            {
                return null;
            }

            return delegate(ref T1 _1, ref T2 _2, ref T3 _3, ref T4 _4)
            {
                var obj = new object[] { _1, _2, _3, _4 };
                info.Invoke(target, obj);
                _1 = (T1)obj[0];
                _2 = (T2)obj[1];
                _3 = (T3)obj[2];
                _4 = (T4)obj[3];
            };
        }
        #endregion
        #endregion

        #region Funcs
        #region 0 Parameters
        private static Func<TResult> MakeFunc_<TResult>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, typeof(TResult), Type.EmptyTypes))
            {
                return null;
            }

            return () => (TResult)info.Invoke(target, new object[0] { });
        }
        #endregion

        #region 1 Parameter
        private static Func<T, TResult> MakeFunc_0<T, TResult>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, typeof(TResult), new Type[] { typeof(T) }))
            {
                return null;
            }

            return (_1) => (TResult)info.Invoke(target, new object[] { _1 });
        }

        private static RefFunc_1<T1, TResult> MakeFunc_1<T1, TResult>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, typeof(TResult), new Type[] { typeof(T1) }))
            {
                return null;
            }

            return delegate(ref T1 _1)
            {
                var obj = new object[] { _1 };
                var rslt = info.Invoke(target, obj);
                _1 = (T1)obj[0];
                return (TResult)rslt;
            };
        }
        #endregion

        #region 2 Parameters
        private static Func<T1, T2, TResult> MakeFunc_00<T1, T2, TResult>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, typeof(TResult), new Type[] { typeof(T1), typeof(T2) }))
            {
                return null;
            }

            return (_1, _2) => (TResult)info.Invoke(target, new object[] { _1, _2 });
        }

        private static RefFunc_01<T1, T2, TResult> MakeFunc_01<T1, T2, TResult>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, typeof(TResult), new Type[] { typeof(T1), typeof(T2).MakeByRefType() }))
            {
                return null;
            }

            return delegate(T1 _1, ref T2 _2)
            {
                var obj = new object[] { _1, _2 };
                var rslt = info.Invoke(target, obj);
                _2 = (T2)obj[1];
                return (TResult)rslt;
            };
        }

        private static RefFunc_10<T1, T2, TResult> MakeFunc_10<T1, T2, TResult>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, typeof(TResult), new Type[] { typeof(T1).MakeByRefType(), typeof(T2) }))
            {
                return null;
            }

            return delegate(ref T1 _1, T2 _2)
            {
                var obj = new object[] { _1, _2 };
                var rslt = info.Invoke(target, obj);
                _1 = (T1)obj[0];
                return (TResult)rslt;
            };
        }

        private static RefFunc_11<T1, T2, TResult> MakeFunc_11<T1, T2, TResult>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, typeof(TResult), new Type[] { typeof(T1).MakeByRefType(), typeof(T2).MakeByRefType() }))
            {
                return null;
            }

            return delegate(ref T1 _1, ref T2 _2)
            {
                var obj = new object[] { _1, _2 };
                var rslt = info.Invoke(target, obj);
                _1 = (T1)obj[0];
                _2 = (T2)obj[1];
                return (TResult)rslt;
            };
        }
        #endregion

        #region 3 Parameters
        private static Func<T1, T2, T3, TResult> MakeFunc_000<T1, T2, T3, TResult>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, typeof(TResult), new Type[] { typeof(T1), typeof(T2), typeof(T3) }))
            {
                return null;
            }

            return (_1, _2, _3) => (TResult)info.Invoke(target, new object[] { _1, _2, _3 });
        }

        private static RefFunc_001<T1, T2, T3, TResult> MakeFunc_001<T1, T2, T3, TResult>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, typeof(TResult),
                new Type[] { typeof(T1), typeof(T2), typeof(T3).MakeByRefType() }))
            {
                return null;
            }

            return delegate(T1 _1, T2 _2, ref T3 _3)
            {
                var obj = new object[] { _1, _2, _3 };
                var rslt = info.Invoke(target, obj);
                _3 = (T3)obj[2];
                return (TResult)rslt;
            };
        }

        private static RefFunc_010<T1, T2, T3, TResult> MakeFunc_010<T1, T2, T3, TResult>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, typeof(TResult),
                new Type[] { typeof(T1), typeof(T2).MakeByRefType(), typeof(T3) }))
            {
                return null;
            }

            return delegate(T1 _1, ref T2 _2, T3 _3)
            {
                var obj = new object[] { _1, _2, _3 };
                var rslt = info.Invoke(target, obj);
                _2 = (T2)obj[1];
                return (TResult)rslt;
            };
        }

        private static RefFunc_011<T1, T2, T3, TResult> MakeFunc_011<T1, T2, T3, TResult>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, typeof(TResult),
                new Type[] { typeof(T1), typeof(T2).MakeByRefType(), typeof(T3).MakeByRefType() }))
            {
                return null;
            }

            return delegate(T1 _1, ref T2 _2, ref T3 _3)
            {
                var obj = new object[] { _1, _2, _3 };
                var rslt = info.Invoke(target, obj);
                _2 = (T2)obj[1];
                _3 = (T3)obj[2];
                return (TResult)rslt;
            };
        }

        private static RefFunc_100<T1, T2, T3, TResult> MakeFunc_100<T1, T2, T3, TResult>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, typeof(TResult),
                new Type[] { typeof(T1).MakeByRefType(), typeof(T2), typeof(T3) }))
            {
                return null;
            }

            return delegate(ref T1 _1, T2 _2, T3 _3)
            {
                var obj = new object[] { _1, _2, _3 };
                var rslt = info.Invoke(target, obj);
                _1 = (T1)obj[0];
                return (TResult)rslt;
            };
        }

        private static RefFunc_101<T1, T2, T3, TResult> MakeFunc_101<T1, T2, T3, TResult>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, typeof(TResult),
                new Type[] { typeof(T1).MakeByRefType(), typeof(T2), typeof(T3).MakeByRefType() }))
            {
                return null;
            }

            return delegate(ref T1 _1, T2 _2, ref T3 _3)
            {
                var obj = new object[] { _1, _2, _3 };
                var rslt = info.Invoke(target, obj);
                _1 = (T1)obj[0];
                _3 = (T3)obj[2];
                return (TResult)rslt;
            };
        }

        private static RefFunc_110<T1, T2, T3, TResult> MakeFunc_110<T1, T2, T3, TResult>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, typeof(TResult),
                new Type[] { typeof(T1).MakeByRefType(), typeof(T2).MakeByRefType(), typeof(T3)}))
            {
                return null;
            }

            return delegate(ref T1 _1, ref T2 _2, T3 _3)
            {
                var obj = new object[] { _1, _2, _3 };
                var rslt = info.Invoke(target, obj);
                _1 = (T1)obj[0];
                _2 = (T2)obj[1];
                return (TResult)rslt;
            };
        }

        private static RefFunc_111<T1, T2, T3, TResult> MakeFunc_111<T1, T2, T3, TResult>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, typeof(TResult),
                new Type[] { typeof(T1).MakeByRefType(), typeof(T2).MakeByRefType(), typeof(T3).MakeByRefType() }))
            {
                return null;
            }

            return delegate(ref T1 _1, ref T2 _2, ref T3 _3)
            {
                var obj = new object[] { _1, _2, _3 };
                var rslt = info.Invoke(target, obj);
                _1 = (T1)obj[0];
                _2 = (T2)obj[1];
                _3 = (T3)obj[2];
                return (TResult)rslt;
            };
        }
        #endregion

        #region 4 Parameters
        private static Func<T1, T2, T3, T4, TResult> MakeFunc_0000<T1, T2, T3, T4, TResult>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, typeof(TResult), new Type[] { typeof(T1), typeof(T2), typeof(T3), typeof(T4) }))
            {
                return null;
            }

            return (_1, _2, _3, _4) => (TResult)info.Invoke(target, new object[] { _1, _2, _3, _4 });
        }

        private static RefFunc_0001<T1, T2, T3, T4, TResult> MakeFunc_0001<T1, T2, T3, T4, TResult>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, typeof(TResult),
                new Type[] { typeof(T1), typeof(T2), typeof(T3), typeof(T4).MakeByRefType() }))
            {
                return null;
            }

            return delegate(T1 _1, T2 _2, T3 _3, ref T4 _4)
            {
                var obj = new object[] { _1, _2, _3, _4 };
                var rslt = info.Invoke(target, obj);
                _4 = (T4)obj[3];
                return (TResult)rslt;
            };
        }

        private static RefFunc_0010<T1, T2, T3, T4, TResult> MakeFunc_0010<T1, T2, T3, T4, TResult>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, typeof(TResult),
                new Type[] { typeof(T1), typeof(T2), typeof(T3).MakeByRefType(), typeof(T4) }))
            {
                return null;
            }

            return delegate(T1 _1, T2 _2, ref T3 _3, T4 _4)
            {
                var obj = new object[] { _1, _2, _3, _4 };
                var rslt = info.Invoke(target, obj);
                _3 = (T3)obj[2];
                return (TResult)rslt;
            };
        }

        private static RefFunc_0011<T1, T2, T3, T4, TResult> MakeFunc_0011<T1, T2, T3, T4, TResult>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, typeof(TResult),
                new Type[] { typeof(T1), typeof(T2), typeof(T3).MakeByRefType(), typeof(T4).MakeByRefType() }))
            {
                return null;
            }

            return delegate(T1 _1, T2 _2, ref T3 _3, ref T4 _4)
            {
                var obj = new object[] { _1, _2, _3, _4 };
                var rslt = info.Invoke(target, obj);
                _3 = (T3)obj[2];
                _4 = (T4)obj[3];
                return (TResult)rslt;
            };
        }

        private static RefFunc_0100<T1, T2, T3, T4, TResult> MakeFunc_0100<T1, T2, T3, T4, TResult>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, typeof(TResult),
                new Type[] { typeof(T1), typeof(T2).MakeByRefType(), typeof(T3), typeof(T4) }))
            {
                return null;
            }

            return delegate(T1 _1, ref T2 _2, T3 _3, T4 _4)
            {
                var obj = new object[] { _1, _2, _3, _4 };
                var rslt = info.Invoke(target, obj);
                _2 = (T2)obj[1];
                return (TResult)rslt;
            };
        }

        private static RefFunc_0101<T1, T2, T3, T4, TResult> MakeFunc_0101<T1, T2, T3, T4, TResult>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, typeof(TResult),
                new Type[] { typeof(T1), typeof(T2).MakeByRefType(), typeof(T3), typeof(T4).MakeByRefType() }))
            {
                return null;
            }

            return delegate(T1 _1, ref T2 _2, T3 _3, ref T4 _4)
            {
                var obj = new object[] { _1, _2, _3, _4 };
                var rslt = info.Invoke(target, obj);
                _2 = (T2)obj[1];
                _4 = (T4)obj[3];
                return (TResult)rslt;
            };
        }

        private static RefFunc_0110<T1, T2, T3, T4, TResult> MakeFunc_0110<T1, T2, T3, T4, TResult>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, typeof(TResult),
                new Type[] { typeof(T1), typeof(T2).MakeByRefType(), typeof(T3).MakeByRefType(), typeof(T4) }))
            {
                return null;
            }

            return delegate(T1 _1, ref T2 _2, ref T3 _3, T4 _4)
            {
                var obj = new object[] { _1, _2, _3, _4 };
                var rslt = info.Invoke(target, obj);
                _2 = (T2)obj[1];
                _3 = (T3)obj[2];
                return (TResult)rslt;
            };
        }

        private static RefFunc_0111<T1, T2, T3, T4, TResult> MakeFunc_0111<T1, T2, T3, T4, TResult>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, typeof(TResult),
                new Type[] { typeof(T1), typeof(T2).MakeByRefType(), typeof(T3).MakeByRefType(), typeof(T4).MakeByRefType() }))
            {
                return null;
            }

            return delegate(T1 _1, ref T2 _2, ref T3 _3, ref T4 _4)
            {
                var obj = new object[] { _1, _2, _3, _4 };
                var rslt = info.Invoke(target, obj);
                _2 = (T2)obj[1];
                _3 = (T3)obj[2];
                _4 = (T4)obj[3];
                return (TResult)rslt;
            };
        }

        private static RefFunc_1000<T1, T2, T3, T4, TResult> MakeFunc_1000<T1, T2, T3, T4, TResult>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, typeof(TResult),
                new Type[] { typeof(T1).MakeByRefType(), typeof(T2), typeof(T3), typeof(T4) }))
            {
                return null;
            }

            return delegate(ref T1 _1, T2 _2, T3 _3, T4 _4)
            {
                var obj = new object[] { _1, _2, _3, _4 };
                var rslt = info.Invoke(target, obj);
                _1 = (T1)obj[0];
                return (TResult)rslt;
            };
        }

        private static RefFunc_1001<T1, T2, T3, T4, TResult> MakeFunc_1001<T1, T2, T3, T4, TResult>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, typeof(TResult),
                new Type[] { typeof(T1).MakeByRefType(), typeof(T2), typeof(T3), typeof(T4).MakeByRefType() }))
            {
                return null;
            }

            return delegate(ref T1 _1, T2 _2, T3 _3, ref T4 _4)
            {
                var obj = new object[] { _1, _2, _3, _4 };
                var rslt = info.Invoke(target, obj);
                _1 = (T1)obj[0];
                _4 = (T4)obj[3];
                return (TResult)rslt;
            };
        }

        private static RefFunc_1010<T1, T2, T3, T4, TResult> MakeFunc_1010<T1, T2, T3, T4, TResult>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, typeof(TResult),
                new Type[] { typeof(T1).MakeByRefType(), typeof(T2), typeof(T3).MakeByRefType(), typeof(T4) }))
            {
                return null;
            }

            return delegate(ref T1 _1, T2 _2, ref T3 _3, T4 _4)
            {
                var obj = new object[] { _1, _2, _3, _4 };
                var rslt = info.Invoke(target, obj);
                _1 = (T1)obj[0];
                _3 = (T3)obj[2];
                return (TResult)rslt;
            };
        }

        private static RefFunc_1011<T1, T2, T3, T4, TResult> MakeFunc_1011<T1, T2, T3, T4, TResult>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, typeof(TResult),
                new Type[] { typeof(T1).MakeByRefType(), typeof(T2), typeof(T3).MakeByRefType(), typeof(T4).MakeByRefType() }))
            {
                return null;
            }

            return delegate(ref T1 _1, T2 _2, ref T3 _3, ref T4 _4)
            {
                var obj = new object[] { _1, _2, _3, _4 };
                var rslt = info.Invoke(target, obj);
                _1 = (T1)obj[0];
                _3 = (T3)obj[2];
                _4 = (T4)obj[3];
                return (TResult)rslt;
            };
        }

        private static RefFunc_1100<T1, T2, T3, T4, TResult> MakeFunc_1100<T1, T2, T3, T4, TResult>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, typeof(TResult),
                new Type[] { typeof(T1).MakeByRefType(), typeof(T2).MakeByRefType(), typeof(T3), typeof(T4) }))
            {
                return null;
            }

            return delegate(ref T1 _1, ref T2 _2, T3 _3, T4 _4)
            {
                var obj = new object[] { _1, _2, _3, _4 };
                var rslt = info.Invoke(target, obj);
                _1 = (T1)obj[0];
                _2 = (T2)obj[1];
                return (TResult)rslt;
            };
        }

        private static RefFunc_1101<T1, T2, T3, T4, TResult> MakeFunc_1101<T1, T2, T3, T4, TResult>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, typeof(TResult),
                new Type[] { typeof(T1).MakeByRefType(), typeof(T2).MakeByRefType(), typeof(T3), typeof(T4).MakeByRefType() }))
            {
                return null;
            }

            return delegate(ref T1 _1, ref T2 _2, T3 _3, ref T4 _4)
            {
                var obj = new object[] { _1, _2, _3, _4 };
                var rslt = info.Invoke(target, obj);
                _1 = (T1)obj[0];
                _2 = (T2)obj[1];
                _4 = (T4)obj[3];
                return (TResult)rslt;
            };
        }

        private static RefFunc_1110<T1, T2, T3, T4, TResult> MakeFunc_1110<T1, T2, T3, T4, TResult>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, typeof(TResult),
                new Type[] { typeof(T1).MakeByRefType(), typeof(T2).MakeByRefType(), typeof(T3).MakeByRefType(), typeof(T4) }))
            {
                return null;
            }

            return delegate(ref T1 _1, ref T2 _2, ref T3 _3, T4 _4)
            {
                var obj = new object[] { _1, _2, _3, _4 };
                var rslt = info.Invoke(target, obj);
                _1 = (T1)obj[0];
                _2 = (T2)obj[1];
                _3 = (T3)obj[2];
                return (TResult)rslt;
            };
        }

        private static RefFunc_1111<T1, T2, T3, T4, TResult> MakeFunc_1111<T1, T2, T3, T4, TResult>(object target, MethodInfo info, bool throwOnBindFailure)
        {
            if (!CheckTypeParams(info, throwOnBindFailure, typeof(TResult),
                new Type[] { typeof(T1).MakeByRefType(), typeof(T2).MakeByRefType(), typeof(T3).MakeByRefType(), typeof(T4).MakeByRefType() }))
            {
                return null;
            }

            return delegate(ref T1 _1, ref T2 _2, ref T3 _3, ref T4 _4)
            {
                var obj = new object[] { _1, _2, _3, _4 };
                var rslt = info.Invoke(target, obj);
                _1 = (T1)obj[0];
                _2 = (T2)obj[1];
                _3 = (T3)obj[2];
                _4 = (T4)obj[3];
                return (TResult)rslt;
            };
        }
        #endregion
        #endregion
    }
}
