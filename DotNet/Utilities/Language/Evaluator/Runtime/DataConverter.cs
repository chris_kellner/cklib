﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator.Runtime
{
    public class DataConverter
    {
        public static object ConvertData(object input, Type toType)
        {
            var cvt = TypeDescriptor.GetConverter(input);
            if (cvt.CanConvertTo(toType))
            {
                return cvt.ConvertTo(input, toType);
            }

            cvt = TypeDescriptor.GetConverter(toType);
            if (cvt.CanConvertFrom(input.GetType()))
            {
                return cvt.ConvertFrom(input);
            }

            return Convert.ChangeType(input, toType);
        }

        public static TReturn ConvertData<TReturn>(object input)
        {
            return (TReturn)ConvertData(input, typeof(TReturn));
        }
    }
}
