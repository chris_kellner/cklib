﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator.Runtime
{
    public class ScriptNamespace : IMetadataAccessible
    {
        private string m_FullName;

        private Dictionary<string, ScriptNamespace> m_Children = new Dictionary<string, ScriptNamespace>();
        private Dictionary<string, Type> m_Types = new Dictionary<string, Type>();

        public ScriptNamespace(References references, ScriptNamespace parent, string name)
        {
            if (name == null) { throw new ArgumentNullException("name"); }

            Parent = parent;
            Name = name;
        }

        public ScriptNamespace Parent { get; private set; }

        public string Name { get; private set; }

        public References References { get; private set; }

        public string FullName
        {
            get
            {
                if (m_FullName == null)
                {
                    ScriptNamespace ns = Parent;
                    string fullName = Name;
                    while (ns != null)
                    {
                        fullName = ns.Name + "." + fullName;
                        ns = ns.Parent;
                    }

                    m_FullName = fullName;
                }

                return m_FullName;
            }
        }

        public void AddType(ScriptType type)
        {
            m_Types.Add(type.Name, type);
        }

        public void AddChildNamespace(ScriptNamespace @namespace)
        {
            m_Children.Add(@namespace.Name, @namespace);
        }

        public IEnumerable<Type> GetTypes()
        {
            return m_Types.Values
                .Concat(References.GetTypes(FullName));
        }

        #region IMetadataAccessible
        void IMetadataAccessible.SetMember(string name, object value)
        {
            throw new NotSupportedException();
        }

        TValue IMetadataAccessible.GetMember<TValue>(string name)
        {
            Type t;
            if (m_Types.TryGetValue(name, out t))
            {
                if (typeof(TValue).IsAssignableFrom(t))
                {
                    return (TValue)(object)t;
                }

                if (typeof(ClassName).IsAssignableFrom(typeof(TValue)))
                {
                    return (TValue)(object)new ClassName(t);
                }
            }

            if (typeof(TValue).IsAssignableFrom(typeof(ScriptNamespace)))
            {
                ScriptNamespace ns;
                if (!m_Children.TryGetValue(name, out ns))
                {
                    ns = new ScriptNamespace(References, this, name);
                    m_Children.Add(name, ns);
                }

                return (TValue)(object)ns;
            }

            return default(TValue);
        }

        TValue IMetadataAccessible.InvokeMember<TValue>(string name, params object[] parameters)
        {
            throw new NotSupportedException();
        }
        #endregion
    }
}
