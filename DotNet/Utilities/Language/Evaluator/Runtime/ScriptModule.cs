﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator.Runtime
{
    public class ScriptModule
    {
        private ScriptType m_BackingType;

        static ScriptModule()
        {
        }

        public ScriptModule(Assembly assembly, string moduleName)
        {
        }

        private ScriptType GetBackingType()
        {
            if (m_BackingType == null)
            {
                m_BackingType = new ScriptType(new ScriptContext(), null);
            }

            return m_BackingType;
        }
    }
}
