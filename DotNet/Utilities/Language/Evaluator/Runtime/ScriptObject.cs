﻿using CKLib.Utilities.DataStructures;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator.Runtime
{
    public class ScriptObject : ICustomTypeProvider, INotifyPropertyChanged, IDictionary<string, object>, ICustomTypeDescriptor, IMetadataAccessible
    {
        #region Variables
        private ScriptType m_Type;
        private readonly bool m_IsThreadSafe;
        private IDictionary<string, object> m_Members;
        #endregion

        #region Constructors
        public ScriptObject(ScriptType typeInfo)
            : this(false)
        {
            m_Type = typeInfo;
        }

        public ScriptObject()
            : this(false)
        {
        }

        public ScriptObject(bool threadSafe)
        {
            m_IsThreadSafe = threadSafe;
            if (m_IsThreadSafe)
            {
                m_Members = new SynchronizedDictionary<string, object>();
            }
            else
            {
                m_Members = new Dictionary<string, object>();
            }

            m_Type = new ScriptType(new ScriptContext(), typeof(object));
        }
        #endregion

        public bool IsThreadSafe
        {
            get
            {
                return m_IsThreadSafe;
            }
        }

        #region Methods
        public Type GetCustomType()
        {
            return m_Type;
        }

        public virtual void SetMember(string name, object value)
        {
            m_Members[name] = value;
            OnPropertyChanged(name);
        }

        public virtual TValue GetMember<TValue>(string name)
        {
            return (TValue)m_Members[name];
        }

        public virtual TValue InvokeMember<TValue>(string name, params object[] parameters)
        {
            return default(TValue);
        }
        #endregion

        #region INotifyPropertyChanged
        protected void OnPropertyChanged(string propertyName)
        {
            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region IDictionary
        void IDictionary<string, object>.Add(string key, object value)
        {
            throw new NotImplementedException();
        }

        bool IDictionary<string, object>.ContainsKey(string key)
        {
            return m_Members.ContainsKey(key);
        }

        ICollection<string> IDictionary<string, object>.Keys
        {
            get { return m_Members.Keys; }
        }

        bool IDictionary<string, object>.Remove(string key)
        {
            return m_Members.Remove(key);
        }

        bool IDictionary<string, object>.TryGetValue(string key, out object value)
        {
            return m_Members.TryGetValue(key, out value);
        }

        ICollection<object> IDictionary<string, object>.Values
        {
            get { return m_Members.Values; }
        }

        object IDictionary<string, object>.this[string key]
        {
            get
            {
                return GetMember<object>(key);
            }
            set
            {
                SetMember(key, value);
            }
        }

        void ICollection<KeyValuePair<string, object>>.Add(KeyValuePair<string, object> item)
        {
            throw new NotImplementedException();
        }

        void ICollection<KeyValuePair<string, object>>.Clear()
        {
            throw new NotImplementedException();
        }

        bool ICollection<KeyValuePair<string, object>>.Contains(KeyValuePair<string, object> item)
        {
            throw new NotImplementedException();
        }

        void ICollection<KeyValuePair<string, object>>.CopyTo(KeyValuePair<string, object>[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        int ICollection<KeyValuePair<string, object>>.Count
        {
            get { return m_Members.Count; }
        }

        bool ICollection<KeyValuePair<string, object>>.IsReadOnly
        {
            get { return m_Type != null; }
        }

        bool ICollection<KeyValuePair<string, object>>.Remove(KeyValuePair<string, object> item)
        {
            return m_Members.Remove(item.Key);
        }

        public IEnumerator<KeyValuePair<string, object>> GetEnumerator()
        {
            return m_Members.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        #endregion

        #region ICustomTypeDescriptor
        AttributeCollection ICustomTypeDescriptor.GetAttributes()
        {
            throw new NotImplementedException();
        }

        string ICustomTypeDescriptor.GetClassName()
        {
            if (m_Type != null)
            {
                return m_Type.FullName;
            }

            return ScriptType.ANONYMOUS_TYPE_NAME;
        }

        string ICustomTypeDescriptor.GetComponentName()
        {
            throw new NotImplementedException();
        }

        TypeConverter ICustomTypeDescriptor.GetConverter()
        {
            throw new NotImplementedException();
        }

        EventDescriptor ICustomTypeDescriptor.GetDefaultEvent()
        {
            throw new NotImplementedException();
        }

        PropertyDescriptor ICustomTypeDescriptor.GetDefaultProperty()
        {
            throw new NotImplementedException();
        }

        object ICustomTypeDescriptor.GetEditor(Type editorBaseType)
        {
            throw new NotImplementedException();
        }

        EventDescriptorCollection ICustomTypeDescriptor.GetEvents(Attribute[] attributes)
        {
            throw new NotImplementedException();
        }

        EventDescriptorCollection ICustomTypeDescriptor.GetEvents()
        {
            throw new NotImplementedException();
        }

        PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties(Attribute[] attributes)
        {
            throw new NotImplementedException();
        }

        PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties()
        {
            throw new NotImplementedException();
        }

        object ICustomTypeDescriptor.GetPropertyOwner(PropertyDescriptor pd)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
