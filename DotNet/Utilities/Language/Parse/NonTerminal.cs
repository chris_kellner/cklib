﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CKLib.Utilities.Language.Evaluator;

namespace CKLib.Utilities.Language.Parse
{
    public class NonTerminal : AGrammarElement
    {
        #region inner classes
        private class PatternRuleCollection
        {
            private Dictionary<string, PatternRule> m_Rules = new Dictionary<string, PatternRule>();
            private NonTerminal m_Owner;
            public PatternRuleCollection(NonTerminal owner)
            {
                m_Owner = owner;
            }

            public PatternRule GetPatternRule(string pattern)
            {
                PatternRule rule;
                if (!m_Rules.TryGetValue(pattern, out rule))
                {
                    rule = new PatternRule(m_Owner, pattern);
                    m_Rules[pattern] = rule;
                }

                return rule;
            }
        }
        #endregion

        private bool m_Init;
        private PatternRuleCollection m_Patterns;

        public NonTerminal(Grammar grammar, string name)
            : base(grammar)
        {
            Grammar = grammar;
            Name = name;
            m_Patterns = new PatternRuleCollection(this);
        }

        public NonTerminal(Grammar grammar, string name, string pattern)
            : this(grammar, name)
        {
            Pattern = pattern;
        }

        public List<NonTerminal> SubPatterns { get; set; }

        public override void Initialize()
        {
            if (!m_Init)
            {
                m_Init = true;
                if (!string.IsNullOrEmpty(Pattern))
                {
                    m_Patterns.GetPatternRule(Pattern);
                }

                if (SubPatterns != null)
                {
                    foreach (var pattern in SubPatterns)
                    {
                        pattern.Parent = this;
                        Initialize();
                    }
                }
            }
        }

        public virtual bool Find(ElementList tokens)
        {
            IEnumerable<FailureReason> reasons;
            return Find(tokens, out reasons);
        }

        public virtual bool Find(ElementList tokens, out IEnumerable<FailureReason> reasons)
        {
            Initialize();
            reasons = Enumerable.Empty<FailureReason>();
            if (Pattern != null)
            {
                ParseMatch match = m_Patterns.GetPatternRule(Pattern).Find(tokens, 0);
                if (match.Success)
                {
                    match.Accept();
                    tokens.ReplaceRange(match.Index, match.ElementsConsumed, match.Element);
                    return true;
                }
                else
                {
                    reasons = match.FailureReasons;
                }
            }
            else if (SubPatterns != null)
            {
                var reasonList = new List<FailureReason>();
                foreach (var sub in SubPatterns)
                {
                    if (sub.Find(tokens, out reasons))
                    {
                        return true;
                    }
                    else if(reasons != null)
                    {
                        reasonList.AddRange(reasons);
                    }
                }

                reasons = reasonList;
            }

            return false;
        }

        public override ParseMatch Matches(ElementList tokens, int index)
        {
            if (tokens[index].IsType(this))
            {
                return new ParseMatch(tokens[index], 1, null);
            }

            return ParseMatch.NoMatch;
        }

        public string Pattern { get; set; }

        public override string ToString()
        {
            return string.Format(
                "G->P{{ \"{0}\": [{1}], {2} }}",
                Name,
                Pattern,
                SubPatterns != null ? String.Join(", ", SubPatterns.Select(t => t.ToString()).ToArray()) : string.Empty);
        }
    }
}
