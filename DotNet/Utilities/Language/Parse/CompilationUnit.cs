﻿using CKLib.Utilities.Language.Evaluator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Parse
{
    public class CompilationUnit : NonTerminal
    {
        public CompilationUnit(Grammar grammar)
            : base(grammar, "compilation-unit")
        {
            Evaluator = new RootNodeEvaluator();
        }

        public ASTNode MakeRoot(IEnumerable<AElement> parsedTokens)
        {
            var pattern = new LanguagePattern(this, parsedTokens.ToArray());
            return new ASTNode(pattern);
        }
    }
}
