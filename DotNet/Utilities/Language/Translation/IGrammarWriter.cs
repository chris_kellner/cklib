﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Translation
{
    public interface IGrammarWriter
    {
        void WriteGrammar(Grammar grammar);
    }
}
