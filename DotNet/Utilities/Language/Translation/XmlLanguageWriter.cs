﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CKLib.Utilities.Language.Translation
{
    public class XmlLanguageWriter : ILanguageWriter
    {
        private XmlDocument m_Document;
        private XmlElement m_RootElement;
        public XmlLanguageWriter(Grammar grammar)
        {
            m_Document = new XmlDocument();
            m_RootElement = m_Document.CreateElement("AbstractSyntaxTree");
            m_Document.AppendChild(m_RootElement);
        }

        public XmlDocument Document { get { return m_Document; } }

        public bool IncludeSource { get; set; }

        public string PrettyPrint()
        {
            var builder = new StringBuilder();
            var writer = XmlWriter.Create(builder, new XmlWriterSettings()
            {
                Indent = true,
                IndentChars = "  ",
                NewLineChars = Environment.NewLine,
                ConformanceLevel = ConformanceLevel.Document
            });

            Document.Save(writer);
            return builder.ToString();
        }

        public void Write(ASTNode node)
        {
            Write(node, m_RootElement);
        }

        public void Save(string fileName)
        {
            m_Document.Save(fileName);
        }

        private void Write(ASTNode node, XmlElement parent)
        {
            var element = m_Document.CreateElement(node.TokenType.Name);
            if (node.NonExecutable)
            {
                element.SetAttribute("executable", false.ToString());
            }

            var roles = node.GetRoles();
            if (roles.Any())
            {
                var rolesNode = m_Document.CreateElement("roles");
                element.AppendChild(rolesNode);
                foreach (var role in roles)
                {
                    var rNode = m_Document.CreateElement("role");
                    rNode.InnerText = role;
                    rolesNode.AppendChild(rNode);
                }
            }

            if (IncludeSource)
            {
                var src = m_Document.CreateElement("source");
                element.AppendChild(src);
                src.SetAttribute("line", node.Line.ToString());
                src.SetAttribute("column", node.Column.ToString());
                src.SetAttribute("position", node.Position.ToString());
            }

            parent.AppendChild(element);

            if (node.IsTerminal)
            {
                element.SetAttribute("value", node.Value);
            }
            else
            {
                foreach (var child in node.Nodes)
                {
                    Write(child, element);
                }
            }
        }
    }
}
