﻿using System;
using System.Collections.Generic;
using System.Diagnostics.SymbolStore;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using System.Text;

namespace CKLib.Utilities.Language.Compile
{
    public interface ILWriter
    {
        // Summary:
        //     Begins a catch block.
        //
        // Parameters:
        //   exceptionType:
        //     The System.Type object that represents the exception.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The catch block is within a filtered exception.
        //
        //   System.ArgumentNullException:
        //     exceptionType is null, and the exception filter block has not returned a
        //     value that indicates that finally blocks should be run until this catch block
        //     is located.
        //
        //   System.NotSupportedException:
        //     The Microsoft intermediate language (MSIL) being generated is not currently
        //     in an exception block.
        void BeginCatchBlock(Type exceptionType);
        
        //
        // Summary:
        //     Begins an exception block for a filtered exception.
        //
        // Exceptions:
        //   System.NotSupportedException:
        //     The Microsoft intermediate language (MSIL) being generated is not currently
        //     in an exception block. -or- This System.Reflection.Emit.ILGenerator belongs
        //     to a System.Reflection.Emit.DynamicMethod.
        void BeginExceptFilterBlock();
       
        //
        // Summary:
        //     Begins an exception block for a non-filtered exception.
        //
        // Returns:
        //     The label for the end of the block. This will leave you in the correct place
        //     to execute finally blocks or to finish the try.
        Label BeginExceptionBlock();
        
        //
        // Summary:
        //     Begins an exception fault block in the Microsoft intermediate language (MSIL)
        //     stream.
        //
        // Exceptions:
        //   System.NotSupportedException:
        //     The MSIL being generated is not currently in an exception block. -or- This
        //     System.Reflection.Emit.ILGenerator belongs to a System.Reflection.Emit.DynamicMethod.
        void BeginFaultBlock();
       
        //
        // Summary:
        //     Begins a finally block in the Microsoft intermediate language (MSIL) instruction
        //     stream.
        //
        // Exceptions:
        //   System.NotSupportedException:
        //     The MSIL being generated is not currently in an exception block.
        void BeginFinallyBlock();
        
        //
        // Summary:
        //     Begins a lexical scope.
        //
        // Exceptions:
        //   System.NotSupportedException:
        //     This System.Reflection.Emit.ILGenerator belongs to a System.Reflection.Emit.DynamicMethod.
        void BeginScope();
        
        //
        // Summary:
        //     Declares a local variable of the specified type.
        //
        // Parameters:
        //   localType:
        //     A System.Type object that represents the type of the local variable.
        //
        // Returns:
        //     The declared local variable.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     localType is null.
        //
        //   System.InvalidOperationException:
        //     The containing type has been created by the System.Reflection.Emit.TypeBuilder.CreateType()
        //     method.
        LocalBuilder DeclareLocal(Type localType);
        
        //
        // Summary:
        //     Declares a local variable of the specified type, optionally pinning the object
        //     referred to by the variable.
        //
        // Parameters:
        //   localType:
        //     A System.Type object that represents the type of the local variable.
        //
        //   pinned:
        //     true to pin the object in memory; otherwise, false.
        //
        // Returns:
        //     A System.Reflection.Emit.LocalBuilder object that represents the local variable.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     localType is null.
        //
        //   System.InvalidOperationException:
        //     The containing type has been created by the System.Reflection.Emit.TypeBuilder.CreateType()
        //     method.  -or- The method body of the enclosing method has been created by
        //     the System.Reflection.Emit.MethodBuilder.CreateMethodBody(System.Byte[],System.Int32)
        //     method.
        //
        //   System.NotSupportedException:
        //     The method with which this System.Reflection.Emit.ILGenerator is associated
        //     is not represented by a System.Reflection.Emit.MethodBuilder.
        LocalBuilder DeclareLocal(Type localType, bool pinned);
        
        //
        // Summary:
        //     Declares a new label.
        //
        // Returns:
        //     Returns a new label that can be used as a token for branching.
        Label DefineLabel();
        
        //
        // Summary:
        //     Puts the specified instruction onto the stream of instructions.
        //
        // Parameters:
        //   opcode:
        //     The Microsoft Intermediate Language (MSIL) instruction to be put onto the
        //     stream.
        void Emit(OpCode opcode);
        
        //
        // Summary:
        //     Puts the specified instruction and character argument onto the Microsoft
        //     intermediate language (MSIL) stream of instructions.
        //
        // Parameters:
        //   opcode:
        //     The MSIL instruction to be put onto the stream.
        //
        //   arg:
        //     The character argument pushed onto the stream immediately after the instruction.
        void Emit(OpCode opcode, byte arg);
        
        //
        // Summary:
        //     Puts the specified instruction and metadata token for the specified constructor
        //     onto the Microsoft intermediate language (MSIL) stream of instructions.
        //
        // Parameters:
        //   opcode:
        //     The MSIL instruction to be emitted onto the stream.
        //
        //   con:
        //     A ConstructorInfo representing a constructor.
        [ComVisible(true)]
        void Emit(OpCode opcode, ConstructorInfo con);
        
        //
        // Summary:
        //     Puts the specified instruction and numerical argument onto the Microsoft
        //     intermediate language (MSIL) stream of instructions.
        //
        // Parameters:
        //   opcode:
        //     The MSIL instruction to be put onto the stream. Defined in the OpCodes enumeration.
        //
        //   arg:
        //     The numerical argument pushed onto the stream immediately after the instruction.
        void Emit(OpCode opcode, double arg);
        
        //
        // Summary:
        //     Puts the specified instruction and metadata token for the specified field
        //     onto the Microsoft intermediate language (MSIL) stream of instructions.
        //
        // Parameters:
        //   opcode:
        //     The MSIL instruction to be emitted onto the stream.
        //
        //   field:
        //     A FieldInfo representing a field.
        void Emit(OpCode opcode, FieldInfo field);
        
        //
        // Summary:
        //     Puts the specified instruction and numerical argument onto the Microsoft
        //     intermediate language (MSIL) stream of instructions.
        //
        // Parameters:
        //   opcode:
        //     The MSIL instruction to be put onto the stream.
        //
        //   arg:
        //     The Single argument pushed onto the stream immediately after the instruction.
        void Emit(OpCode opcode, float arg);
        
        //
        // Summary:
        //     Puts the specified instruction and numerical argument onto the Microsoft
        //     intermediate language (MSIL) stream of instructions.
        //
        // Parameters:
        //   opcode:
        //     The MSIL instruction to be put onto the stream.
        //
        //   arg:
        //     The numerical argument pushed onto the stream immediately after the instruction.
        void Emit(OpCode opcode, int arg);
        
        //
        // Summary:
        //     Puts the specified instruction onto the Microsoft intermediate language (MSIL)
        //     stream and leaves space to include a label when fixes are done.
        //
        // Parameters:
        //   opcode:
        //     The MSIL instruction to be emitted onto the stream.
        //
        //   label:
        //     The label to which to branch from this location.
        void Emit(OpCode opcode, Label label);
        
        //
        // Summary:
        //     Puts the specified instruction onto the Microsoft intermediate language (MSIL)
        //     stream and leaves space to include a label when fixes are done.
        //
        // Parameters:
        //   opcode:
        //     The MSIL instruction to be emitted onto the stream.
        //
        //   labels:
        //     The array of label objects to which to branch from this location. All of
        //     the labels will be used.
        void Emit(OpCode opcode, Label[] labels);
        
        //
        // Summary:
        //     Puts the specified instruction onto the Microsoft intermediate language (MSIL)
        //     stream followed by the index of the given local variable.
        //
        // Parameters:
        //   opcode:
        //     The MSIL instruction to be emitted onto the stream.
        //
        //   local:
        //     A local variable.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The parent method of the local parameter does not match the method associated
        //     with this System.Reflection.Emit.ILGenerator.
        //
        //   System.ArgumentNullException:
        //     local is null.
        //
        //   System.InvalidOperationException:
        //     opcode is a single-byte instruction, and local represents a local variable
        //     with an index greater than Byte.MaxValue.
        void Emit(OpCode opcode, LocalBuilder local);
        
        //
        // Summary:
        //     Puts the specified instruction and numerical argument onto the Microsoft
        //     intermediate language (MSIL) stream of instructions.
        //
        // Parameters:
        //   opcode:
        //     The MSIL instruction to be put onto the stream.
        //
        //   arg:
        //     The numerical argument pushed onto the stream immediately after the instruction.
        void Emit(OpCode opcode, long arg);
        
        //
        // Summary:
        //     Puts the specified instruction onto the Microsoft intermediate language (MSIL)
        //     stream followed by the metadata token for the given method.
        //
        // Parameters:
        //   opcode:
        //     The MSIL instruction to be emitted onto the stream.
        //
        //   meth:
        //     A MethodInfo representing a method.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     meth is null.
        //
        //   System.NotSupportedException:
        //     meth is a generic method for which the System.Reflection.MethodInfo.IsGenericMethodDefinition
        //     property is false.
        void Emit(OpCode opcode, MethodInfo meth);
        
        //
        // Summary:
        //     Puts the specified instruction and character argument onto the Microsoft
        //     intermediate language (MSIL) stream of instructions.
        //
        // Parameters:
        //   opcode:
        //     The MSIL instruction to be put onto the stream.
        //
        //   arg:
        //     The character argument pushed onto the stream immediately after the instruction.
        [CLSCompliant(false)]
        void Emit(OpCode opcode, sbyte arg);
       
        //
        // Summary:
        //     Puts the specified instruction and numerical argument onto the Microsoft
        //     intermediate language (MSIL) stream of instructions.
        //
        // Parameters:
        //   opcode:
        //     The MSIL instruction to be emitted onto the stream.
        //
        //   arg:
        //     The Int argument pushed onto the stream immediately after the instruction.
        void Emit(OpCode opcode, short arg);
       
        //
        // Summary:
        //     Puts the specified instruction and a signature token onto the Microsoft intermediate
        //     language (MSIL) stream of instructions.
        //
        // Parameters:
        //   opcode:
        //     The MSIL instruction to be emitted onto the stream.
        //
        //   signature:
        //     A helper for constructing a signature token.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     signature is null.
        void Emit(OpCode opcode, SignatureHelper signature);
       
        //
        // Summary:
        //     Puts the specified instruction onto the Microsoft intermediate language (MSIL)
        //     stream followed by the metadata token for the given string.
        //
        // Parameters:
        //   opcode:
        //     The MSIL instruction to be emitted onto the stream.
        //
        //   str:
        //     The Text to be emitted.
        void Emit(OpCode opcode, string str);
       
        //
        // Summary:
        //     Puts the specified instruction onto the Microsoft intermediate language (MSIL)
        //     stream followed by the metadata token for the given type.
        //
        // Parameters:
        //   opcode:
        //     The MSIL instruction to be put onto the stream.
        //
        //   cls:
        //     A Type.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     cls is null.
        void Emit(OpCode opcode, Type cls);
        
        //
        // Summary:
        //     Puts a call or callvirt instruction onto the Microsoft intermediate language
        //     (MSIL) stream to call a varargs method.
        //
        // Parameters:
        //   opcode:
        //     The MSIL instruction to be emitted onto the stream. Must be System.Reflection.Emit.OpCodes.Call,
        //     System.Reflection.Emit.OpCodes.Callvirt, or System.Reflection.Emit.OpCodes.Newobj.
        //
        //   methodInfo:
        //     The varargs method to be called.
        //
        //   optionalParameterTypes:
        //     The types of the optional arguments if the method is a varargs method; otherwise,
        //     null.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     methodInfo is null.
        //
        //   System.InvalidOperationException:
        //     The calling convention for the method is not varargs, but optional parameter
        //     types are supplied. This exception is thrown in the .NET Framework versions
        //     1.0 and 1.1, In subsequent versions, no exception is thrown.
        void EmitCall(OpCode opcode, MethodInfo methodInfo, Type[] optionalParameterTypes);
        
        //
        // Summary:
        //     Puts a System.Reflection.Emit.OpCodes.Calli instruction onto the Microsoft
        //     intermediate language (MSIL) stream, specifying an unmanaged calling convention
        //     for the indirect call.
        //
        // Parameters:
        //   opcode:
        //     The MSIL instruction to be emitted onto the stream. Must be System.Reflection.Emit.OpCodes.Calli.
        //
        //   unmanagedCallConv:
        //     The unmanaged calling convention to be used.
        //
        //   returnType:
        //     The System.Type of the result.
        //
        //   parameterTypes:
        //     The types of the required arguments to the instruction.
        void EmitCalli(OpCode opcode, CallingConvention unmanagedCallConv, Type returnType, Type[] parameterTypes);
        
        //
        // Summary:
        //     Puts a System.Reflection.Emit.OpCodes.Calli instruction onto the Microsoft
        //     intermediate language (MSIL) stream, specifying a managed calling convention
        //     for the indirect call.
        //
        // Parameters:
        //   opcode:
        //     The MSIL instruction to be emitted onto the stream. Must be System.Reflection.Emit.OpCodes.Calli.
        //
        //   callingConvention:
        //     The managed calling convention to be used.
        //
        //   returnType:
        //     The System.Type of the result.
        //
        //   parameterTypes:
        //     The types of the required arguments to the instruction.
        //
        //   optionalParameterTypes:
        //     The types of the optional arguments for varargs calls.
        //
        // Exceptions:
        //   System.InvalidOperationException:
        //     optionalParameterTypes is not null, but callingConvention does not include
        //     the System.Reflection.CallingConventions.VarArgs flag.
        void EmitCalli(OpCode opcode, CallingConventions callingConvention, Type returnType, Type[] parameterTypes, Type[] optionalParameterTypes);
        
        //
        // Summary:
        //     Emits the Microsoft intermediate language (MSIL) necessary to call Overload:System.Console.WriteLine
        //     with the given field.
        //
        // Parameters:
        //   fld:
        //     The field whose value is to be written to the console.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     There is no overload of the Overload:System.Console.WriteLine method that
        //     accepts the type of the specified field.
        //
        //   System.ArgumentNullException:
        //     fld is null.
        //
        //   System.NotSupportedException:
        //     The type of the field is System.Reflection.Emit.TypeBuilder or System.Reflection.Emit.EnumBuilder,
        //     which are not supported.
        void EmitWriteLine(FieldInfo fld);

        //
        // Summary:
        //     Emits the Microsoft intermediate language (MSIL) necessary to call Overload:System.Console.WriteLine
        //     with the given local variable.
        //
        // Parameters:
        //   localBuilder:
        //     The local variable whose value is to be written to the console.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The type of localBuilder is System.Reflection.Emit.TypeBuilder or System.Reflection.Emit.EnumBuilder,
        //     which are not supported. -or- There is no overload of Overload:System.Console.WriteLine
        //     that accepts the type of localBuilder.
        //
        //   System.ArgumentNullException:
        //     localBuilder is null.
        void EmitWriteLine(LocalBuilder localBuilder);

        //
        // Summary:
        //     Emits the Microsoft intermediate language (MSIL) to call Overload:System.Console.WriteLine
        //     with a string.
        //
        // Parameters:
        //   value:
        //     The string to be printed.
        void EmitWriteLine(string value);

        //
        // Summary:
        //     Ends an exception block.
        //
        // Exceptions:
        //   System.InvalidOperationException:
        //     The end exception block occurs in an unexpected place in the code stream.
        //
        //   System.NotSupportedException:
        //     The Microsoft intermediate language (MSIL) being generated is not currently
        //     in an exception block.
        void EndExceptionBlock();

        //
        // Summary:
        //     Ends a lexical scope.
        //
        // Exceptions:
        //   System.NotSupportedException:
        //     This System.Reflection.Emit.ILGenerator belongs to a System.Reflection.Emit.DynamicMethod.
        void EndScope();

        //
        // Summary:
        //     Marks the Microsoft intermediate language (MSIL) stream's current position
        //     with the given label.
        //
        // Parameters:
        //   loc:
        //     The label for which to set an index.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     loc represents an invalid index into the label array.  -or- An index for
        //     loc has already been defined.
        void MarkLabel(Label loc);

        //
        // Summary:
        //     Marks a sequence point in the Microsoft intermediate language (MSIL) stream.
        //
        // Parameters:
        //   document:
        //     The document for which the sequence point is being defined.
        //
        //   startLine:
        //     The line where the sequence point begins.
        //
        //   startColumn:
        //     The column in the line where the sequence point begins.
        //
        //   endLine:
        //     The line where the sequence point ends.
        //
        //   endColumn:
        //     The column in the line where the sequence point ends.
        //
        // Exceptions:
        //   System.ArgumentOutOfRangeException:
        //     startLine or endLine is <= 0.
        //
        //   System.NotSupportedException:
        //     This System.Reflection.Emit.ILGenerator belongs to a System.Reflection.Emit.DynamicMethod.
        void MarkSequencePoint(ISymbolDocumentWriter document, int startLine, int startColumn, int endLine, int endColumn);
        
        //
        // Summary:
        //     Emits an instruction to throw an exception.
        //
        // Parameters:
        //   excType:
        //     The class of the type of exception to throw.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     excType is not the System.Exception class or a derived class of System.Exception.
        //      -or- The type does not have a default constructor.
        //
        //   System.ArgumentNullException:
        //     excType is null.
        void ThrowException(Type excType);

        //
        // Summary:
        //     Specifies the namespace to be used in evaluating locals and watches for the
        //     current active lexical scope.
        //
        // Parameters:
        //   usingNamespace:
        //     The namespace to be used in evaluating locals and watches for the current
        //     active lexical scope
        //
        // Exceptions:
        //   System.ArgumentException:
        //     Length of usingNamespace is zero.
        //
        //   System.ArgumentNullException:
        //     usingNamespace is null.
        //
        //   System.NotSupportedException:
        //     This System.Reflection.Emit.ILGenerator belongs to a System.Reflection.Emit.DynamicMethod.
        void UsingNamespace(string usingNamespace);
    }
}
