﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Compile
{
    public class EvCode
    {
        public EvCodes Code { get; private set; }
        public object Metadata { get; private set; }

        public static readonly EvCode Nop = new EvCode(EvCodes.Nop);
        public static readonly EvCode Add = new EvCode(EvCodes.Add);


        private EvCode(EvCodes code)
        {
            Code = code;
        }
    }
}
