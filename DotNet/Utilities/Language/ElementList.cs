﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language
{
    public class ElementList : ConsumableList<AElement>
    {
        public ElementList() : base() { }
        public ElementList(IEnumerable<AElement> elements) : base(elements) { }
    }

    public class ConsumableList<T> : List<T>
    {
        public ConsumableList()
            : base()
        {
        }

        public ConsumableList(IEnumerable<T> elements)
            : base(elements)
        {
        }

        public void ReplaceRange(int index, int count, T newItem)
        {
            RemoveRange(index, count);
            if (newItem != null)
            {
                Insert(index, newItem);
            }
        }

        public void ReplaceRange(int index, int count, IEnumerable<T> newItems)
        {
            RemoveRange(index, count);
            if (newItems != null)
            {
                InsertRange(index, newItems);
            }
        }

        public T[] Slice(int startIndex, int endIndex)
        {
            int count = endIndex - startIndex;
            T[] cpy = new T[count];
            this.CopyTo(startIndex, cpy, 0, count);
            return cpy;
        }
    }
}
