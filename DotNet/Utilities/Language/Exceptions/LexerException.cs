﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language
{
    public class LexerException : LanguageException
    {
        public LexerException(string message, int position)
            : base(message)
        {
            Position = position;
        }

        public LexerException(string message, AElement element)
            : base(message, element)
        {
        }

        public int Position { get; private set; }
    }
}
