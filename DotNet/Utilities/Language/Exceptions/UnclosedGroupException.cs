﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Exceptions
{
    public class UnclosedGroupException : ParserException
    {
        public UnclosedGroupException(AElement openNode) : base("Unclosed group detected", openNode) { }
    }
}
