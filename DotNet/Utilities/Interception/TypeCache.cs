﻿using CKLib.Utilities.DataStructures;
using CKLib.Utilities.Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using CKLib.Utilities.Emit;

namespace CKLib.Utilities.Interception
{
    internal class TypeCache : ITypeCache
    {
        #region Internal Structures
        private struct ProxyCacheKey
        {
            #region Constants
            /// <summary>
            /// Empty CacheKey
            /// </summary>
            public static readonly ProxyCacheKey Empty;
            #endregion
            #region Variables
            /// <summary>
            /// The Types held by this proxy cache key.
            /// </summary>
            Type[] m_types;
            #endregion
            #region Constructors
            /// <summary>
            /// Initializes static members of the ProxyCacheKey struct.
            /// </summary>
            static ProxyCacheKey()
            {
                Empty = new ProxyCacheKey();
            }

            /// <summary>
            /// Initializes a new instance of the ProxyCacheKey struct.
            /// </summary>
            /// <param name="types">The types used to construct the proxy.</param>
            public ProxyCacheKey(Type[] types)
            {
                m_types = types;
            }
            #endregion
            #region Properties
            /// <summary>
            /// Gets the types used to construct this ProxyCacheKey.
            /// </summary>
            /// <returns>The types that comprise this key.</returns>
            public Type[] ProxyTypes
            {
                get
                {
                    return (Type[])m_types.Clone();
                }
            }
            #endregion
            #region Methods
            /// <summary>
            /// Gets a value indicating whether this instance fully contains the other key.
            /// </summary>
            /// <param name="other">The other key to check.</param>
            /// <returns>True if this key contains all elements of the other key.</returns>
            public bool IsSuperset(ProxyCacheKey other)
            {
                if (m_types != null && other.m_types != null)
                {
                    return m_types.Length > other.m_types.Length && other.m_types.Except(this.m_types).FirstOrDefault() == null;
                }

                return false;
            }

            /// <summary>
            /// Gets a value indicating whether the specified instance fully contains this key.
            /// </summary>
            /// <param name="other">The other key to check.</param>
            /// <returns>True if the other key contains all elements of this key.</returns>
            public bool IsSubset(ProxyCacheKey other)
            {
                if (m_types != null && other.m_types != null)
                {
                    return m_types.Length < other.m_types.Length && m_types.Except(other.m_types).FirstOrDefault() == null;
                }

                return false;
            }

            /// <summary>
            /// Returns a hash code for this instance.
            /// </summary>
            /// <returns>A hashcode for this instance.</returns>
            public override int GetHashCode()
            {
                int c = 0;
                foreach (var type in m_types.OrderBy(t => t.Name))
                {
                    c ^= type.GetHashCode();
                }

                return c;
            }

            /// <summary>
            /// Checks the equality of the two objects.
            /// </summary>
            /// <param name="obj">The other object to check equality of.</param>
            /// <returns>True if the objects are equal, false otherwise</returns>
            public override bool Equals(object obj)
            {
                if (obj is ProxyCacheKey)
                {
                    return this.Equals((ProxyCacheKey)obj);
                }

                return false;
            }

            /// <summary>
            /// Checks the equality of the two objects.
            /// </summary>
            /// <param name="other">The other proxy cache key to check.</param>
            /// <returns>True if the objects are equal, false otherwise.</returns>
            public bool Equals(ProxyCacheKey other)
            {
                return m_types.Except(other.m_types).FirstOrDefault() == null;
            }

            /// <summary>
            /// Checks the equality of the two objects.
            /// </summary>
            /// <param name="left">The ProxyCacheKey left of the equals sign.</param>
            /// <param name="right">The ProxyCacheKey right of the equals sign.</param>
            /// <returns>True if the two are equal, false otherwise.</returns>
            public static bool operator ==(ProxyCacheKey left, ProxyCacheKey right)
            {
                return left.Equals(right);
            }

            /// <summary>
            /// Checks the inequality of the two objects.
            /// </summary>
            /// <param name="left">The ProxyCacheKey left of the not equals sign.</param>
            /// <param name="right">The ProxyCacheKey right of the not equals sign.</param>
            /// <returns>True if the two are Not equal, false otherwise.</returns>
            public static bool operator !=(ProxyCacheKey left, ProxyCacheKey right)
            {
                return !left.Equals(right);
            }
            #endregion
        }
        #endregion
        #region Variables
        /// <summary>
        /// Type cache for already constructed types, this saves a lot of time 
        /// when creating multiple instances of the same type.
        /// </summary>
        private SynchronizedDictionary<ProxyCacheKey, Type> m_typeCache = new SynchronizedDictionary<ProxyCacheKey, Type>();

        /// <summary>
        /// Internal collection of constructors created for the types made by this factory.
        /// </summary>
        private SynchronizedDictionary<Type, HashSet<ConstructorInfo>> m_constructorCache = new SynchronizedDictionary<Type, HashSet<ConstructorInfo>>();

        private SynchronizedDictionary<Type, Func<object, object>> m_Factories = new SynchronizedDictionary<Type, Func<object, object>>();
        #endregion

        #region Other
        public void CacheType(Type type, Type typeOfObjectToWrap, params Type[] interfaces)
        {
            var key = BuildCacheKey(typeOfObjectToWrap, interfaces);
            if (!m_typeCache.ContainsKey(key))
            {
                m_typeCache[key] = type;
            }
        }

        /// <summary>
        /// Gets the already produced proxy type from the cache.
        /// </summary>
        /// <param name="typeOfObjectToWrap">The type of the object being wrapped.</param>
        /// <param name="interfaces">The interfaces that the wrapped type is implementing.</param>
        /// <returns>An already produced type or null if no match is found.</returns>
        public Type GetTypeFromCache(Type typeOfObjectToWrap, params Type[] interfaces)
        {
            var key = BuildCacheKey(typeOfObjectToWrap, interfaces);
            Type ret;
            if (!m_typeCache.TryGetValue(key, out ret))
            {
                ret = m_typeCache.FirstOrDefault(k => k.Key.IsSuperset(key)).Value;
            }

            return ret;
        }

        /// <summary>
        /// Gets the already produced proxy type from the cache.
        /// </summary>
        /// <param name="typeOfObjectToWrap">The type of the object being wrapped.</param>
        /// <param name="interfaces">The interfaces that the wrapped type is implementing.</param>
        /// <returns>An already produced type or null if no match is found.</returns>
        public Type GetTypeFromCache(Type typeOfObjectToWrap, Type @interface)
        {
            var allInterfaces = FactoryHelpers.ExpandTypeInheritance(@interface).ToArray();
            return GetTypeFromCache(typeOfObjectToWrap, allInterfaces);
        }

        /// <summary>
        /// Builds the cache key based on the types being used to create a proxy.
        /// </summary>
        /// <param name="typeOfObjectToWrap">The type of the object being wrapped.</param>
        /// <param name="interfaces">The interfaces that the wrapped type is implementing.</param>
        /// <returns>Creates a cache key out of the base type and interfaces.</returns>
        private ProxyCacheKey BuildCacheKey(Type typeOfObjectToWrap, Type[] interfaces)
        {
            if (typeOfObjectToWrap != null && interfaces != null && interfaces.Length > 0)
            {
                Type[] key = new Type[interfaces.Length + 1];
                key[0] = typeOfObjectToWrap;
                interfaces.CopyTo(key, 1);
                return new ProxyCacheKey(key);
            }

            return ProxyCacheKey.Empty;
        }

        /// <summary>
        /// Stores the constructor to the internal cache of available proxy constructors.
        /// </summary>
        /// <param name="typeOfObjectToWrap">Type of object being wrapped.</param>
        /// <param name="constructor">The constructor used to create a proxy to that type.</param>
        public void AddConstructor(Type typeOfObjectToWrap, ConstructorInfo constructor)
        {
            HashSet<ConstructorInfo> constructors;
            if (!m_constructorCache.TryGetValue(typeOfObjectToWrap, out constructors))
            {
                constructors = new HashSet<ConstructorInfo>();
                m_constructorCache.Add(typeOfObjectToWrap, constructors);
            }

            constructors.Add(constructor);
        }

        /// <summary>
        /// Removes the constructor from the internal cache of available proxy constructors.
        /// </summary>
        /// <param name="typeOfObjectToWrap">Type of object being wrapped.</param>
        /// <param name="constructor">The constructor used to create a proxy to that type.</param>
        public void RemoveConstructor(Type typeOfObjectToWrap, ConstructorInfo constructor)
        {
            HashSet<ConstructorInfo> constructors;
            if (m_constructorCache.TryGetValue(typeOfObjectToWrap, out constructors))
            {
                constructors.Remove(constructor);
            }
        }

        /// <summary>
        /// Gets an already created proxy constructor for the specified type.
        /// </summary>
        /// <param name="typeOfObjectToWrap">Type of object that is wrapped by the proxy we want to create.</param>
        /// <param name="exposedType">Type of proxy we want the constructor for. This can be an interface used to create
        /// a proxy, or the proxy type itself.</param>
        /// <returns>The constructor used to construct a proxy, or null.</returns>
        public virtual ConstructorInfo GetProxyConstructor(Type typeOfObjectToWrap, Type exposedType)
        {
            HashSet<ConstructorInfo> constructors;
            if (m_constructorCache.TryGetValue(typeOfObjectToWrap, out constructors))
            {
                foreach (var con in constructors)
                {
                    if (exposedType.IsAssignableFrom(con.ReflectedType))
                    {
                        return con;
                    }
                }
            }

            return null;
        }

        public object CreateInstance(Type type, object inner)
        {
            Func<object, object> factory = m_Factories.GetOrAdd(type, t =>
            {
                return CreateConstructor(type, inner.GetType());
            });

            return factory(inner);
        }

        private Func<object, object> CreateConstructor(Type type, Type innerType)
        {
            var flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;
            var cons = type.GetConstructor(flags, null, new Type[] { innerType }, null);
            DynamicMethod meth = null;
            if (cons != null)
            {
                meth = FactoryHelpers.DefineDynamicMethod(type.Name + "_constructor_" + Guid.NewGuid(), type, new Type[] { typeof(object) });
                meth.GetILBuilder()
                    .LoadArgument(0)
                    .Is(innerType)  
                    .If(
                        then: t =>
                        {
                            t.LoadArgument(0).Cast(typeof(object), innerType).New(cons);
                        },
                        @else: t =>
                        {
                            t.Throw<InvalidOperationException>("Object must be of type: " + innerType);
                        }
                    )
                    .Return();
            }

            if (meth != null)
            {
                return (Func<object, object>)meth.CreateDelegate(typeof(Func<object, object>));
            }

            return null;
        }

        public void Clear()
        {
            if (m_typeCache != null)
            {
                m_typeCache.Clear();
                m_typeCache = null;
            }

            if (m_constructorCache != null)
            {
                m_constructorCache.Clear();
                m_constructorCache = null;
            }
        }
        #endregion
    }
}
