﻿using CKLib.Utilities.Emit;
using CKLib.Utilities.Proxy;
using CKLib.Utilities.Proxy.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

namespace CKLib.Utilities.Interception
{
    public abstract class AInterceptorFactory : IInterceptorFactory, ITypeCacheProvider, IDisposable
    {
        #region Variables
        private TypeCache m_TypeCache = new TypeCache();
        private bool m_IncludeBaseInterfaces = true;
        #endregion

        #region IInterceptorFactory Methods
        public TInterface CreateInterceptor<TInterface>(object inner) where TInterface : class
        {
            return CreateInterceptor(inner, typeof(TInterface), null) as TInterface;
        }

        public TInterface CreateInterceptor<TInterface, TBase>(object inner) where TInterface : class
        {
            return CreateInterceptor(inner, typeof(TInterface), typeof(TBase)) as TInterface;
        }

        public object CreateInterceptor(object inner, Type tInterface, Type tBase)
        {
            if (IsObjectAlreadyWrapped(inner, tInterface, tBase))
            {
                return inner;
            }

            Type t = CreateType(GetInnerObjectType(inner, tInterface), tInterface, tBase, true);
            if (t != null)
            {
                object obj = m_TypeCache.CreateInstance(t, inner);
                ((IInterceptor)obj).CreatorFactory = this;
                return obj;
            }

            return null;
        }

        public bool IsCompatible(object inner, Type tInterface)
        {
            if (inner != null && tInterface.IsInterface)
            {
                var allInterfaces = GetAllInterfaces(inner.GetType(), tInterface).ToArray();
                return ValidateCompatibility(GetInnerObjectType(inner, tInterface), allInterfaces, false);
            }

            return false;
        }

        public bool IsCompatible(Type innerType, Type tInterface)
        {
            if (innerType != null && tInterface.IsInterface)
            {
                var allInterfaces = GetAllInterfaces(innerType, tInterface).ToArray();
                return ValidateCompatibility(innerType, allInterfaces, false);
            }

            return false;
        }
        #endregion

        #region Properties
        public bool IncludeBaseInterfaces
        {
            get { return m_IncludeBaseInterfaces; }
            set { m_IncludeBaseInterfaces = value; }
        }

        #endregion

        #region HelperMethods
        protected IEnumerable<Type> GetAllInterfaces(Type innerType, Type @interface)
        {
            if (!IncludeBaseInterfaces)
            {
                return FactoryHelpers.ExpandTypeInheritance(@interface);
            }
            else
            {
                return FactoryHelpers.ExpandTypeInheritance(@interface)
                    .Concat(FactoryHelpers.ExpandTypeInheritance(innerType))
                    .Distinct()
                    .Where(i=>i != typeof(IInterceptor));
            }
        }

        private Type CreateType(Type typeofWrappedObject, Type @interface, Type @base, bool throwOnError)
        {
            var allInterfaces = GetAllInterfaces(typeofWrappedObject, @interface).ToArray();
            Type ret = m_TypeCache.GetTypeFromCache(typeofWrappedObject, allInterfaces);
            if (ret != null)
            {
                return ret;
            }

            IMethodBuilderArgs args = DefineType(typeofWrappedObject, @interface, @base);

            // Add constructors
            var wrapper = BuildWrapperConstructor(args);
            m_TypeCache.AddConstructor(typeofWrappedObject, wrapper);
            try
            {
                DefineBaseClassConstructors(args.TypeBuilder).ToArray();
                foreach (var iFace in allInterfaces)
                {
                    ImplementInterface(args, iFace);
                }

                ret = args.TypeBuilder.CreateType();
            }
            catch (Exception ex)
            {
                CKLib.Utilities.Diagnostics.Error.WriteEntry("Failed to create type: " + args.TypeBuilder.ToString(), ex);
                if (throwOnError)
                {
                    throw;
                }
            }

            return ret;
        }

        protected virtual bool IsObjectAlreadyWrapped(object inner, Type @interface, Type @base)
        {
            if (@interface.IsInstanceOfType(inner))
            {
                if (@base != null)
                {
                    return @base.IsInstanceOfType(inner);
                }

                return true;
            }

            return false;
        }

        protected virtual Type GetInnerObjectType(object inner, Type interfaceType)
        {
            return inner.GetType();
        }

        protected virtual IMethodBuilderArgs DefineType(Type innerType, Type @interface, Type @base)
        {
            string typeName = @interface.Name + Guid.NewGuid();
            var allInterfaces = GetAllInterfaces(innerType, @interface).ToArray();
            if (ValidateCompatibility(innerType, allInterfaces, true))
            {
                TypeBuilder typeBuilder = FactoryHelpers.GetTypeBuilder(typeName, @base, allInterfaces.ToArray());
                FieldInfo m_Inner = typeBuilder.DefineField("m_Inner", innerType, FieldAttributes.Private);
                MethodBuilderArgs args = new MethodBuilderArgs(typeBuilder, @interface, innerType)
                {
                    BuilderStorageMember = m_Inner
                };

                ImplementIInterceptor(typeBuilder, args);
                DefineToString(typeBuilder, m_Inner);
                return args;
            }

            return null;
        }

        protected virtual void ImplementInterface(IMethodBuilderArgs args, Type @interface)
        {
            // Implement all of the properties required by the current interface
            var properties = @interface.GetProperties();
            for (int p = 0; p < properties.Length; p++)
            {
                var cProperty = properties[p];
                BuildProperty(args, cProperty);
            }

            // Implement all of the events required by the current interface
            var events = @interface.GetEvents();
            for (int e = 0; e < events.Length; e++)
            {
                var cEvent = events[e];
                BuildEvent(args, cEvent);
            }

            // Implement all of the methods required by the current interface
            var methods = @interface.GetMethods();
            for (int m = 0; m < methods.Length; m++)
            {
                // Skip the "special names" (Constructors, properties, and events)
                var cMethod = methods[m];
                if (!cMethod.IsSpecialName)
                {
                    var body = BuildMethod(args, cMethod);
                }
            }
        }

        /// <summary>
        /// Builds a property for the proxy type whose signature matches that of the specified PropertyInfo.
        /// </summary>
        /// <param name="proxyBuilder">The IProxyBuilder that will be used to create the property.</param>
        /// <param name="args">The instructions used to describe to the builder how the member should be built.</param>
        /// <param name="cProperty">The property on the interface which should be built by this method.</param>
        private void BuildProperty(IMethodBuilderArgs args, PropertyInfo cProperty)
        {
            TypeBuilder builder = args.TypeBuilder;
            FieldInfo storageMember = args.BuilderStorageMember;
            args.InterfaceMemberToBuild = cProperty;
            if (builder != null && cProperty != null)
            {
                string propertyName = cProperty.Name;
                Type propertyType = cProperty.PropertyType;
                var propBuilder = FactoryHelpers.GetPropertyBuilder(builder, propertyName, propertyType);
                if (propBuilder != null)
                {
                    var innerProperty = storageMember.FieldType.GetProperty(propertyName);
                    if (innerProperty.CanRead && cProperty.CanRead)
                    {
                        string getName = string.Format("get_{0}", propertyName);
                        var method1 = FactoryHelpers.GetPropertyMethodBuilder(builder, getName, propertyType, Type.EmptyTypes);
                        if (method1 != null)
                        {
                            propBuilder.SetGetMethod(method1);
                            args.InterfaceMethodToBuild = cProperty.GetGetMethod();
                            args.CurrentMethodBuilder = method1;
                            args.ReturnType = propertyType;
                            args.ParameterTypes = Type.EmptyTypes;
                            DefineMethodBody(args);
                            builder.DefineMethodOverride(method1, cProperty.GetGetMethod());
                        }
                    }

                    if (innerProperty.CanWrite && cProperty.CanWrite)
                    {
                        string setName = string.Format("set_{0}", propertyName);
                        var method2 = FactoryHelpers.GetPropertyMethodBuilder(builder, setName, null, new Type[] { propertyType });
                        if (method2 != null)
                        {
                            propBuilder.SetSetMethod(method2);
                            args.InterfaceMethodToBuild = cProperty.GetSetMethod();
                            args.CurrentMethodBuilder = method2;
                            args.ReturnType = null;
                            args.ParameterTypes = new Type[] { propertyType };
                            DefineMethodBody(args);
                            builder.DefineMethodOverride(method2, cProperty.GetSetMethod());
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Builds a event for the proxy type whose signature matches that of the specified EventInfo.
        /// </summary>
        /// <param name="proxyBuilder">The IProxyBuilder that will be used to create the event.</param>
        /// <param name="args">The instructions used to describe to the builder how the member should be built.</param>
        /// <param name="cEvent">The event on the interface which should be built by this method.</param>
        private void BuildEvent(IMethodBuilderArgs args, EventInfo cEvent)
        {
            TypeBuilder typeBuilder = args.TypeBuilder;
            FieldInfo fieldObject = args.BuilderStorageMember;
            args.InterfaceMemberToBuild = cEvent;
            if (typeBuilder != null && cEvent != null)
            {
                string eventName = cEvent.Name;
                Type eventHandlerType = cEvent.EventHandlerType;
                var eventBuilder = FactoryHelpers.GetEventBuilder(typeBuilder, eventName, eventHandlerType);
                if (eventBuilder != null)
                {
                    var innerEvent = fieldObject.FieldType.GetEventRecursive(eventName);
                    string addName = string.Format("add_{0}", eventName);
                    var method1 = FactoryHelpers.GetPropertyMethodBuilder(typeBuilder, addName, null, new Type[] { eventHandlerType });
                    if (method1 != null)
                    {
                        eventBuilder.SetAddOnMethod(method1);

                        args.InterfaceMethodToBuild = cEvent.GetAddMethod();
                        args.CurrentMethodBuilder = method1;
                        args.ReturnType = null;
                        args.ParameterTypes = new Type[] { eventHandlerType };
                        DefineMethodBody(args);
                        typeBuilder.DefineMethodOverride(method1, cEvent.GetAddMethod());
                    }

                    string removeName = string.Format("remove_{0}", eventName);
                    var method2 = FactoryHelpers.GetPropertyMethodBuilder(typeBuilder, removeName, null, new Type[] { eventHandlerType });
                    if (method2 != null)
                    {
                        eventBuilder.SetRemoveOnMethod(method2);
                        args.InterfaceMethodToBuild = cEvent.GetRemoveMethod();
                        args.CurrentMethodBuilder = method2;
                        args.ReturnType = null;
                        args.ParameterTypes = new Type[] { eventHandlerType };
                        DefineMethodBody(args);
                        typeBuilder.DefineMethodOverride(method2, cEvent.GetRemoveMethod());
                    }
                }
            }
        }

        /// <summary>
        /// Builds a method for the proxy type whose signature matches that of the specified MethodInfo.
        /// </summary>
        /// <param name="proxyBuilder">The IProxyBuilder that will be used to create the method.</param>
        /// <param name="args">The instructions used to describe to the builder how the member should be built.</param>
        /// <param name="interfaceMethod">The method on the interface which should be built by this method.</param>
        private MethodBuilder BuildMethod(IMethodBuilderArgs args, MethodInfo interfaceMethod)
        {
            args.InterfaceMemberToBuild = interfaceMethod;
            TypeBuilder builder = args.TypeBuilder;
            FieldInfo storageMember = args.BuilderStorageMember;
            string methodName = interfaceMethod.Name;
            Type returnType = interfaceMethod.ReturnType;
            Type[] parameterTypes = FactoryHelpers.GetMethodParameterTypes(interfaceMethod);
            var method = FactoryHelpers.GetMethodBuilder(builder, methodName, returnType, parameterTypes);
            if (method != null)
            {
                args.InterfaceMethodToBuild = interfaceMethod;
                args.CurrentMethodBuilder = method;
                args.ReturnType = returnType;
                args.ParameterTypes = parameterTypes;

                DefineMethodBody(args);
                builder.DefineMethodOverride(method, interfaceMethod);
            }

            return method;
        }

        protected abstract void DefineMethodBody(IMethodBuilderArgs args);

        /// <summary>
        /// Gets a value indicating whether or not the specified inner type is compatible with all of the provided interfaces.
        /// </summary>
        /// <param name="innerType"></param>
        /// <param name="interfaces"></param>
        /// <param name="throwOnError"></param>
        /// <returns></returns>
        protected abstract bool ValidateCompatibility(Type innerType, Type[] interfaces, bool throwOnError);

        /// <summary>
        /// Provides a pass-through implementation of "ToString" which calls the ToString method on the object being wrapped.
        /// </summary>
        /// <param name="builder">The IProxyBuilder that is building the rest of the proxy.</param>
        /// <param name="args">The instructions used to describe to the builder how the member should be built.</param>
        /// <returns>A MethodBuilder object for the ToString method.</returns>
        protected MethodBuilder DefineToString(TypeBuilder builder, FieldInfo storageMember)
        {
            if (storageMember != null && !storageMember.FieldType.IsInterface)
            {
                string methodName = "ToString";
                Type returnType = typeof(string);
                Type[] parameterTypes = Type.EmptyTypes;
                var methodBuilder = FactoryHelpers.GetMethodBuilder(builder, methodName, returnType, parameterTypes);
                if (methodBuilder != null)
                {
                    var innerFunction = storageMember.FieldType.GetMethod("ToString", Type.EmptyTypes);
                    methodBuilder.GetILBuilder(parameterTypes)
                        .This().LoadField(storageMember)
                        .Call(innerFunction)
                        .Return();
                    builder.DefineMethodOverride(methodBuilder, typeof(object).GetMethod("ToString"));
                }

                return methodBuilder;
            }

            return null;
        }

        /// <summary>
        /// Implements the <see cref="IInterceptor"/> interface explicitly on the proxy.
        /// </summary>
        /// <param name="builder">The TypeBuilder that is building the rest of the proxy.</param>
        private void ImplementIInterceptor(TypeBuilder builder, IMethodBuilderArgs args)
        {
            var storageMember = args.BuilderStorageMember;
            args.CreatorFactoryField = builder.DefineField("m_CreatorFactory", typeof(IInterceptorFactory), FieldAttributes.Private);
            builder.AddInterfaceImplementation(typeof(IInterceptor));
            var method = builder.DefineMethod("GetInner", FactoryHelpers.ExplicitImplementation, typeof(object), null);
            if (method != null)
            {
                if (storageMember != null)
                {
                    method.GetILBuilder(Type.EmptyTypes).This().LoadField(storageMember).Box(storageMember.FieldType).Return();
                }
                else
                {
                    method.GetILBuilder(Type.EmptyTypes)
                        .This()
                        .Return();
                }

                builder.DefineMethodOverride(method, typeof(IInterceptor).GetMethod("GetInner"));
            }

            method = builder.DefineMethod("GetRoot", FactoryHelpers.ExplicitImplementation, typeof(object), null);
            if (method != null)
            {
                if (storageMember != null)
                {
                    method.GetILBuilder(Type.EmptyTypes)
                        .This().LoadField(storageMember)
                        .Is(typeof(IInterceptor))
                        .If(
                            then: t =>
                            {
                                t.This().LoadField(storageMember)
                                    .Call(typeof(IInterceptor).GetMethod("GetRoot"));
                            },
                            @else: t=> t.This().LoadField(storageMember).Box(storageMember.FieldType)
                        ).Return();
                }
                else
                {
                    method.GetILBuilder(Type.EmptyTypes)
                        .This()
                        .Return();
                }
                builder.DefineMethodOverride(method, typeof(IInterceptor).GetMethod("GetRoot"));
            }

            var creatorFactory = FactoryHelpers.GetPropertyBuilder(builder, "CreatorFactory", typeof(IInterceptorFactory));
            var cf_getter = FactoryHelpers.GetPropertyMethodBuilder(builder, "get_CreatorFactory", typeof(IInterceptorFactory), Type.EmptyTypes);
            var ilB = cf_getter.GetILBuilder(Type.EmptyTypes);
            ilB
                .This().LoadField(args.CreatorFactoryField)
                .Return();
            creatorFactory.SetGetMethod(cf_getter);
            builder.DefineMethodOverride(cf_getter, typeof(IInterceptor).GetMethod("get_CreatorFactory"));

            var cf_setter = FactoryHelpers.GetPropertyMethodBuilder(builder, "set_CreatorFactory", null, new Type[] { typeof(IInterceptorFactory) } );
            ilB = cf_setter.GetILBuilder(new Type[] { typeof(IInterceptorFactory) });
            ilB
                .This().LoadArgument(1).StoreField(args.CreatorFactoryField)
                .Return();
            creatorFactory.SetSetMethod(cf_setter);
            builder.DefineMethodOverride(cf_setter, typeof(IInterceptor).GetMethod("set_CreatorFactory"));
        }

        /// <summary>
        /// Builds a constructor for the proxy type which accepts an object whose type is compatible with the type
        /// being wrapped.
        /// </summary>
        /// <param name="builder">The IProxyBuilder that will be used to create the constructor.</param>
        /// <param name="args">The instructions used to describe to the builder how the member should be built.</param>
        /// <returns>A ConstructorBuilder object, or null.</returns>
        private ConstructorBuilder BuildWrapperConstructor(IMethodBuilderArgs args)
        {
            TypeBuilder builder = args.TypeBuilder;
            FieldInfo storageMember = args.BuilderStorageMember;
            if (builder != null)
            {
                Type[] parameters = new Type[] { storageMember.FieldType };
                var constructorBuilder = builder.DefineConstructor(
                   FactoryHelpers.ConstructorImplementation,
                    CallingConventions.HasThis | CallingConventions.Standard,
                    parameters);
                if (constructorBuilder != null)
                {
                    constructorBuilder.GetILBuilder(parameters)
                        .This()
                        .LoadArgument(1)
                        .StoreField(storageMember)
                        .Return();
                }

                return constructorBuilder;
            }

            return null;
        }

        /// <summary>
        /// Adds an implementation for each of the constructors found on the base object so that the proxy may be constructed using
        /// each of them.
        /// </summary>
        /// <param name="builder">The IProxyBuilder that will be used to create each constructor.</param>
        /// <param name="args">The instructions used to describe to the builder how the member should be built.</param>
        /// <returns>An enumeration of constructors which can later be used to construct the proxy.</returns>
        private IEnumerable<ConstructorBuilder> DefineBaseClassConstructors(TypeBuilder builder)
        {
            if (builder != null)
            {
                var baseType = builder.BaseType;
                if (baseType != null)
                {
                    var constructors = baseType.GetConstructors();
                    for (int i = 0; i < constructors.Length; i++)
                    {
                        var cInfo = constructors[i];
                        var ctor = BuildBaseConstructor(builder, cInfo);
                        if (ctor != null)
                        {
                            yield return ctor;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Builds a constructor for the proxy type whose signature matches that of the specified ConstructorInfo.
        /// </summary>
        /// <param name="builder">The IProxyBuilder that will be used to create the constructor.</param>
        /// <param name="info">The base class constructor which should be called by the constructor being built.</param>
        /// <returns>A ConstructorBuilder object, or null.</returns>
        private ConstructorBuilder BuildBaseConstructor(TypeBuilder builder, ConstructorInfo info)
        {
            if (builder != null && info != null)
            {
                Type[] parameterTypes = FactoryHelpers.GetMethodParameterTypes(info);
                var constructorBuilder = builder.DefineConstructor(
                  FactoryHelpers.ConstructorImplementation,
                   CallingConventions.HasThis | CallingConventions.Standard,
                   parameterTypes);
                if (constructorBuilder != null)
                {
                    var ilGen = constructorBuilder.GetILGenerator();
                    ProxyBuildingPatterns.CallBasePassthrough(ilGen,
                        constructorBuilder,
                        parameterTypes);
                    ilGen.Emit(OpCodes.Ret);
                }

                return constructorBuilder;
            }

            return null;
        }
        #endregion

        #region ITypeCacheProvider Methods
        public ITypeCache GetTypeCache()
        {
            return m_TypeCache;
        }
        #endregion

        #region IDisposable
        private bool m_IsDisposed = false;

        public bool IsDisposed
        {
            get
            {
                return m_IsDisposed;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!m_IsDisposed)
            {
                m_IsDisposed = true;
                if (disposing)
                {
                    m_TypeCache.Clear();
                    m_TypeCache = null;
                }
            }
        }

        ~AInterceptorFactory()
        {
            Dispose(false);
        }
        #endregion
    }
}
