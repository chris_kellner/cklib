﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Interception
{
    public interface ITypeCacheProvider
    {
        ITypeCache GetTypeCache();
    }
}
