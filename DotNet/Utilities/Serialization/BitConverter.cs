﻿#region Usage Rights
/******************************************************************************
Copyright (c) 2012-2013, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.Serialization
{
    #region Using List
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using SystemConverter = System.BitConverter;
    #endregion

    /// <summary>
    /// This class is used for transforming data types to-and-from a byte stream.
    /// </summary>
    public class BitConverter
    {
        #region Variables
        /// <summary>
        /// Indicates whether multi-byte data types are written in big or little endian style.
        /// </summary>
        private readonly bool m_LittleEndian;

        /// <summary>
        /// Indicates whether or not the current endianness matches the system setting.
        /// </summary>
        private readonly bool m_MatchesSystemEndian;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the BitConverter class.
        /// </summary>
        /// <param name="bigEndian">Indicates whether to serialize data types as big or little endian.</param>
        public BitConverter(bool bigEndian)
        {
            m_LittleEndian = !bigEndian;
            m_MatchesSystemEndian = m_LittleEndian == SystemConverter.IsLittleEndian;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets a value indicating whether multi-byte data types are written in big or little endian style.
        /// </summary>
        public bool IsLittleEndian
        {
            get
            {
                return m_LittleEndian;
            }
        }
        #endregion

        #region Methods
        #region GetBytes
        public byte[] GetBytes(string value, Encoding encoding)
        {
            return encoding.GetBytes(value);
        }

        public byte[] GetBytes(int value)
        {
            byte[] b = SystemConverter.GetBytes(value);
            if (!m_MatchesSystemEndian)
            {
                Array.Reverse(b);
            }

            return b;
        }

        public byte[] GetBytes(uint value)
        {
            byte[] b = SystemConverter.GetBytes(value);
            if (!m_MatchesSystemEndian)
            {
                Array.Reverse(b);
            }

            return b;
        }

        public byte[] GetBytes(long value)
        {
            byte[] b = SystemConverter.GetBytes(value);
            if (!m_MatchesSystemEndian)
            {
                Array.Reverse(b);
            }

            return b;
        }

        public byte[] GetBytes(ulong value)
        {
            byte[] b = SystemConverter.GetBytes(value);
            if (!m_MatchesSystemEndian)
            {
                Array.Reverse(b);
            }

            return b;
        }

        public byte[] GetBytes(short value)
        {
            byte[] b = SystemConverter.GetBytes(value);
            if (!m_MatchesSystemEndian)
            {
                Array.Reverse(b);
            }

            return b;
        }

        public byte[] GetBytes(ushort value)
        {
            byte[] b = SystemConverter.GetBytes(value);
            if (!m_MatchesSystemEndian)
            {
                Array.Reverse(b);
            }

            return b;
        }

        public byte[] GetBytes(float value)
        {
            byte[] b = SystemConverter.GetBytes(value);
            if (!m_MatchesSystemEndian)
            {
                Array.Reverse(b);
            }

            return b;
        }

        public byte[] GetBytes(double value)
        {
            byte[] b = SystemConverter.GetBytes(value);
            if (!m_MatchesSystemEndian)
            {
                Array.Reverse(b);
            }

            return b;
        }

        public byte[] GetBytes(bool value)
        {
            byte[] b = SystemConverter.GetBytes(value);
            if (!m_MatchesSystemEndian)
            {
                Array.Reverse(b);
            }

            return b;
        }

        public byte[] GetBytes(char value)
        {
            byte[] b = SystemConverter.GetBytes(value);
            if (!m_MatchesSystemEndian)
            {
                Array.Reverse(b);
            }

            return b;
        }

        public byte[] GetBytes(Enum value)
        {
            return GetBytes(Convert.ToInt64(value));
        }

        public byte[] GetBytes(DateTime value)
        {
            return GetBytes(value.ToUniversalTime().Ticks);
        }

        public byte[] GetBytes(TimeSpan value)
        {
            return GetBytes(value.Ticks);
        }

        public byte[] SevenBitEncode(int value)
        {
            const byte CONTINUATION_VALUE = 0x80;
            const int VALUE_MASK = 0x7F;
            const int SHIFT = 7;
            int cByte = 0;
            byte[] target = new byte[5];
            do
            {
                byte bVal = (byte)(value & VALUE_MASK);
                value >>= SHIFT;
                if (value != 0)
                {
                    bVal |= CONTINUATION_VALUE;
                }

                target[cByte++] = bVal;
            } while (value != 0);
            Array.Resize(ref target, cByte);
            return target;
        }
        #endregion

        #region FromBytes
        public string ToString(byte[] bytes, int startIndex)
        {
            return Encoding.UTF8.GetString(bytes, startIndex, bytes.Length);
        }

        public string ToString(byte[] bytes, int startIndex, int length)
        {
            return Encoding.UTF8.GetString(bytes, startIndex, length);
        }

        public string ToString(byte[] bytes, int startIndex, int length, Encoding encoding)
        {
            return encoding.GetString(bytes, startIndex, length);
        }

        public bool ToBoolean(byte[] bytes, int startIndex)
        {
            return SystemConverter.ToBoolean(Slice(bytes, startIndex, sizeof(bool)), 0);
        }

        public byte ToByte(byte[] bytes, int startIndex)
        {
            return bytes[startIndex];
        }

        public sbyte ToSByte(byte[] bytes, int startIndex)
        {
            return (sbyte)bytes[startIndex];
        }

        public char ToChar(byte[] bytes, int startIndex)
        {
            return SystemConverter.ToChar(Slice(bytes, startIndex, sizeof(char)), 0);
        }

        public short ToInt16(byte[] bytes, int startIndex)
        {
            return SystemConverter.ToInt16(Slice(bytes, startIndex, 2), 0);
        }

        public ushort ToUInt16(byte[] bytes, int startIndex)
        {
            return SystemConverter.ToUInt16(Slice(bytes, startIndex, 2), 0);
        }

        public int ToInt32(byte[] bytes, int startIndex)
        {
            return SystemConverter.ToInt32(Slice(bytes, startIndex, 4), 0);
        }

        public uint ToUInt32(byte[] bytes, int startIndex)
        {
            return SystemConverter.ToUInt32(Slice(bytes, startIndex, 4), 0);
        }

        public long ToInt64(byte[] bytes, int startIndex)
        {
            return SystemConverter.ToInt64(Slice(bytes, startIndex, 8), 0);
        }

        public ulong ToUInt64(byte[] bytes, int startIndex)
        {
            return SystemConverter.ToUInt64(Slice(bytes, startIndex, 8), 0);
        }

        public float ToSingle(byte[] bytes, int startIndex)
        {
            return SystemConverter.ToSingle(Slice(bytes, startIndex, 4), 0);
        }

        public double ToDouble(byte[] bytes, int startIndex)
        {
            return SystemConverter.ToDouble(Slice(bytes, startIndex, 8), 0);
        }

        public TimeSpan ToTimeSpan(byte[] bytes, int startIndex)
        {
            return new TimeSpan(ToInt64(bytes, startIndex));
        }

        public DateTime ToDateTime(byte[] bytes, int startIndex)
        {
            return new DateTime(ToInt64(bytes, startIndex));
        }

        /// <summary>
        /// Converts the specified byte array into the enum of the specified type consuming 8 bytes from the array.
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="startIndex"></param>
        /// <param name="enumType"></param>
        /// <returns></returns>
        public Enum ToEnum(byte[] bytes, int startIndex, Type enumType)
        {
            long value = ToInt64(bytes, startIndex);
            return (Enum)Enum.ToObject(enumType, value);
        }

        /// <summary>
        /// Converts the specified byte array into the enum of the specified type consuming 8 bytes from the array.
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="bytes"></param>
        /// <param name="startIndex"></param>
        /// <returns></returns>
        public TEnum ToEnum<TEnum>(byte[] bytes, int startIndex)
        {
            long value = ToInt64(bytes, startIndex);
            return (TEnum)Enum.ToObject(typeof(TEnum), value);
        }
        #endregion

        #region Stream
        public void SevenBitEncode(Stream stream, int value)
        {
            const uint CONTINUATION_BYTE = 0x80;
            const int SHIFT = 7;
            uint num = (uint)value;
            while (num >= CONTINUATION_BYTE)
            {
                stream.WriteByte((byte)(num | CONTINUATION_BYTE));
                num >>= SHIFT;
            }
            stream.WriteByte((byte)num);
        }

        public byte[] ReadBytes(Stream stream, int count)
        {
            byte[] data = new byte[count];
            count = stream.Read(data, 0, count);
            if (data.Length != count)
            {
                if (count < 0)
                {
                    throw new EndOfStreamException();
                }

                Array.Resize(ref data, count);
            }

            return data;
        }

        public int SevenBitDecodeInt32(Stream stream)
        {
            const byte CONTINUATION_VALUE = 0x80;
            const byte VALUE_MASK = 0x7F;
            const int SHIFT = 7;
            const int END_OF_STREAM = -1;
            int value = 0;
            int cByte = 0;
            int iVal;
            do
            {
                iVal = stream.ReadByte();
                if (iVal == END_OF_STREAM)
                {
                    throw new EndOfStreamException();
                }

                value |= ((iVal & VALUE_MASK) << (SHIFT * cByte));
                cByte++;
            } while (iVal >= CONTINUATION_VALUE);

            value &= ~(CONTINUATION_VALUE << (SHIFT * (cByte - 1)));
            return value;
        }
        #endregion

        #region Helpers
        private byte[] Slice(byte[] bytes, int startIndex, int length)
        {
            byte[] b = new byte[length];
            Array.Copy(bytes, startIndex, b, 0, length);
            if (!m_MatchesSystemEndian)
            {
                Array.Reverse(b);
            }

            return b;
        }
        #endregion
        #endregion
    }
}
