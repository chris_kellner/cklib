﻿#region Usage Rights
/******************************************************************************
Copyright (c) 2012-2013, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.Serialization
{
    #region Using List
    using System;
    using System.Runtime.Serialization;
    using System.Xml.Serialization;
    #endregion

    /// <summary>
    /// This class provides an implementation of the <c>ISerializer</c> interface that supports
    /// the use of the System.Xml.XmlSerializer during serialization.
    /// </summary>
    public class SystemXmlSerializer : ISerializer
    {
        #region Constants
        /// <summary>
        /// The FormatName to specify to use this serializer during serialization.
        /// </summary>
        public const string FORMAT_NAME = "XML";
        #endregion

        #region Variables
        private XmlSerializer m_Serializer;
        private SerializationBinder m_Binder;
        private BitConverter m_Converter;
        private ISerializerProvider m_SerializerProvider;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the SystemXmlSerializer class.
        /// </summary>
        /// <param name="objectType">The type of object to be (de)serialized.</param>
        public SystemXmlSerializer(Type objectType)
        {
            m_Serializer = new XmlSerializer(objectType);
            Converter = new BitConverter(false);
            Binder = new LenientBinder();
        }
        #endregion

        #region Properties
        public string FormatName
        {
            get { return "XML"; }
        }

        public SerializationBinder Binder
        {
            get
            {
                if (m_Binder == null)
                {
                    m_Binder = new LenientBinder();
                }

                return m_Binder;
            }

            set
            {
                m_Binder = value;
            }
        }

        public StreamingContext Context
        {
            get;
            set;
        }

        public BitConverter Converter
        {
            get
            {
                if (m_Converter == null)
                {
                    m_Converter = new BitConverter(false);
                }

                return m_Converter;
            }

            set
            {
                m_Converter = value;
            }
        }

        /// <summary>
        /// Gets or sets the object used for retrieving serializers of other formats.
        /// </summary>
        public ISerializerProvider SerializerProvider
        {
            get
            {
                if (m_SerializerProvider == null)
                {
                    m_SerializerProvider = new DefaultSerializerProvider();
                }

                return m_SerializerProvider;
            }
            set
            {
                m_SerializerProvider = value;
            }
        }
        #endregion

        #region Methods
        public object Deserialize(System.IO.Stream serializationStream)
        {
            return m_Serializer.Deserialize(serializationStream);
        }

        public void Serialize(System.IO.Stream serializationStream, object graph)
        {
            m_Serializer.Serialize(serializationStream, graph);
        }
        #endregion
    }
}
