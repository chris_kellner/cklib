﻿#region Usage Rights
/******************************************************************************
Copyright (c) 2012-2013, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.Serialization
{
    #region Using List
    using CKLib.Utilities.Serialization.Json;
    using System;
    using System.Collections.Generic;
    #endregion

    /// <summary>
    /// This class serves as a default implementation for the <c>ISerializerProvider</c> class.
    /// </summary>
    public class DefaultSerializerProvider : ISerializerProvider
    {
        private Dictionary<string, Func<Type, ISerializer>> m_Constructors = new Dictionary<string, Func<Type, ISerializer>>()
        {
            { BinarySerializer.FORMAT_NAME.ToUpper(), t => new BinarySerializer() },
            { JsonFormatter.FORMAT_NAME.ToUpper(), t => new JsonFormatter(t) },
            { SystemXmlSerializer.FORMAT_NAME.ToUpper(), t => new SystemXmlSerializer(t) }
        };

        public virtual ISerializer GetSerializer(string formatName, Type objectType)
        {
            Func<Type, ISerializer> func;
            if (m_Constructors.TryGetValue(formatName.ToUpper(), out func))
            {
                var serial = func(objectType);
                serial.SerializerProvider = this;
                return serial;
            }

            return null;
        }

        public virtual ISerializer GetDeserializer(string formatName, Type objectType)
        {
            return GetSerializer(formatName, objectType);
        }

        public void RegisterSerializer(string formatName, Func<Type, ISerializer> factory)
        {
            if (string.IsNullOrEmpty(formatName))
            {
                throw new ArgumentNullException("formatName");
            }

            if (factory == null)
            {
                throw new ArgumentNullException("factory");
            }

            m_Constructors.Add(formatName.ToUpper(), factory);
        }
    }
}
