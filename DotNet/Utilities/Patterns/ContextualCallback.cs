#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.Patterns
{
    #region Using List
    using System;
    using System.Threading;
    using CKLib.Utilities.Exceptions;
    #endregion
    /// <summary>
    /// This class is used to contain a callback with its synchronization context.
    /// </summary>
    /// <typeparam name="TCallback">The type of callback to store.</typeparam>
    public struct ContextualCallback<TCallback>
    {
        #region Variables
        /// <summary>
        /// The stored callback.
        /// </summary>
        private TCallback m_Callback;
        
        /// <summary>
        /// The synchronization context to use when invoking the callback.
        /// </summary>
        private SynchronizationContext m_SynchContext;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ContextualCallback class.
        /// </summary>
        /// <param name="callback">The callback to store.</param>
        /// <param name="synchronized">A flag indicating whether to synchronize the callback using the current synchronization context.</param>
        /// <exception cref="ArgumentNullException">Throws an exception if callback is null.</exception>
        /// <exception cref="InvalidSynchronizationContextException">Throws an exception if there is no synchronization context to use
        /// for a synchronized callback.</exception>
        public ContextualCallback(TCallback callback, bool synchronized)
        {
            if (callback == null)
            {
                throw new ArgumentNullException("callback");
            }

            m_Callback = callback;
            if (synchronized)
            {
                m_SynchContext = SynchronizationContext.Current;

                if (m_SynchContext == null)
                {
                    throw new InvalidSynchronizationContextException(
                        "Cannot add a synchronized callback to a thread which does not contain a synchronization context." +
                        " Try adding an asynchronous callback and synchronizing it manually.");
                }
            }
            else
            {
                m_SynchContext = null;
            }
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets the callback.
        /// </summary>
        public TCallback Callback
        {
            get
            {
                return m_Callback;
            }
        }

        /// <summary>
        /// Gets the synchronization context.
        /// </summary>
        public SynchronizationContext Context
        {
            get
            {
                return m_SynchContext;
            }
        }
        #endregion
        #region Methods
        /// <summary>
        /// Compares this instance to another object for equality.
        /// </summary>
        /// <param name="obj">The object to compare to.</param>
        /// <returns>True if the objects are equal, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (obj is ContextualCallback<TCallback>)
            {
                return base.Equals((ContextualCallback<TCallback>)obj);
            }
            return false;
        }

        /// <summary>
        /// Compares this instance to another ContextualCallback of the same type for equality.
        /// </summary>
        /// <param name="other">The ContextualCallback to compare.</param>
        /// <returns>True if the callbacks are equal, false otherwise.</returns>
        public bool Equals(ContextualCallback<TCallback> other)
        {
            return m_Callback.Equals(other.m_Callback);
        }

        /// <summary>
        /// Gets the hashcode for the callback.
        /// </summary>
        /// <returns>The hashcode for the callback.</returns>
        public override int GetHashCode()
        {
            return m_Callback.GetHashCode();
        }
        #endregion
    }
}
