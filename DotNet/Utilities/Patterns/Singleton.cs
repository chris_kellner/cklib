#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.Patterns
{
    #region Using List
    using System;
    using System.Collections.Generic;
    #endregion
    /// <summary>
    /// Provides the base class for all singleton&lt;T&gt;'s.
    /// </summary>
    public abstract class Singleton
    {
        #region Variables
        /// <summary>
        /// Object used for locking the singleton.
        /// </summary>
        protected static readonly object m_SingletonLock;

        /// <summary>
        /// Collection of disposal method for all singletons.
        /// </summary>
        private static readonly Dictionary<Type, Action> m_Singletons;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes static members of the Singleton class.
        /// </summary>
        static Singleton()
        {
            m_SingletonLock = new object();
            m_Singletons = new Dictionary<Type, Action>();
        }
        #endregion
        #region Methods
        /// <summary>
        /// Releases resources held by all singletons.
        /// </summary>
        public static void ReleaseAllSingletons()
        {
            lock (m_SingletonLock)
            {
                foreach (var singleton in m_Singletons)
                {
                    if (singleton.Value != null)
                    {
                        singleton.Value();
                    }
                }
                m_Singletons.Clear();
            }
        }

        /// <summary>
        /// Registers a singleton with the base class.
        /// </summary>
        /// <typeparam name="T">The type of singleton to register.</typeparam>
        /// <param name="cleanupAction">The method to invoke on cleanup.</param>
        protected static void RegisterSingleton<T>(Action cleanupAction) where T : Singleton
        {
            if (!m_Singletons.ContainsKey(typeof(T)))
            {
                lock (m_SingletonLock)
                {
                    if (!m_Singletons.ContainsKey(typeof(T)))
                    {
                        m_Singletons.Add(typeof(T), cleanupAction);
                    }
                }
            }
        }
        #endregion
    }

    /// <summary>
    /// Encapsulates the Singleton pattern.
    /// </summary>
    /// <typeparam name="TSingleton">The class type to make a singleton for.</typeparam>
    public class Singleton<TSingleton> : Singleton where TSingleton : class
    {
        #region Variables
        /// <summary>
        /// Reference to the singleton object.
        /// </summary>
        private static volatile TSingleton m_Singleton;

        /// <summary>
        /// A method to invoke when the Instance property is asked for if the singleton has not been created yet.
        /// </summary>
        private static volatile Func<TSingleton> m_SingletonFactory;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes static members of the Singleton class.
        /// </summary>
        static Singleton()
        {
            RegisterSingleton<Singleton<TSingleton>>(Release);
        }
        #endregion
        /// <summary>
        /// Gets a value indicating whether the value has been assigned for this singleton.
        /// </summary>
        public static bool IsValueAssigned
        {
            get{
                if (m_Singleton != null)
                {
                    return true;
                }
                else
                {
                    lock (m_SingletonLock)
                    {
                        return m_Singleton != null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the Instance for this singleton.
        /// </summary>
        public static TSingleton Instance
        {
            get{
                if (m_Singleton == null)
                {
                    lock (m_SingletonLock)
                    {
                        if (m_Singleton == null)
                        {
                            if (m_SingletonFactory != null)
                            {
                                m_Singleton = m_SingletonFactory();
                                ValueFactory = null;
                            }
                            else
                            {
                                try
                                {
                                    m_Singleton = Activator.CreateInstance(typeof(TSingleton), true) as TSingleton;
                                }
                                catch (Exception ex)
                                {
                                    throw new InvalidOperationException("No value factory was provided for singleton of type " + typeof(TSingleton).Name, ex);
                                }
                            }

                            var initializable = m_Singleton as IPostConstructionInitializable;
                            if (initializable != null && !initializable.IsInitialized)
                            {
                                initializable.Initialize();
                            }
                        }
                    }
                }
                return m_Singleton;
            }
        }

        /// <summary>
        /// Releases resources held by this singleton.
        /// </summary>
        public static void Release()
        {
            if (m_Singleton != null || m_SingletonFactory != null)
            {
                lock (m_SingletonLock)
                {
                    if (m_Singleton != null)
                    {
                        IDisposable disposal = m_Singleton as IDisposable;
                        if (disposal != null)
                        {
                            try
                            {
                                disposal.Dispose();
                            }
                            catch (Exception e)
                            {
                                System.Diagnostics.Debug.Write(
                                    string.Format("Error disposing singleton<{0}>: {1}",
                                    typeof(TSingleton).Name,
                                    e.Message));
                            }
                        }
                    }
                    m_Singleton = null;
                    m_SingletonFactory = null;
                }
            }
        }

        /// <summary>
        /// Gets or sets the method to invoke when the Instance property is invoked but the value has not been assigned yet.
        /// </summary>
        public static Func<TSingleton> ValueFactory
        {
            get
            {
                if (m_SingletonFactory == null)
                {
                    lock (m_SingletonLock)
                    {
                        return m_SingletonFactory;
                    }
                }

                return m_SingletonFactory;
            }

            set
            {
                lock (m_SingletonLock)
                {
                    m_SingletonFactory = value;
                }
            }
        }
    }
}
