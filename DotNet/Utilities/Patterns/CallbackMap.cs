#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.Patterns
{
    #region Using List
    using System;
    using System.Collections.Generic;
    using System.Threading;
    #endregion
    /// <summary>
    /// This class is used to map message types to callback sets.
    /// </summary>
    /// <typeparam name="TCallback"></typeparam>
    public class CallbackMap<TCallback>
    {
        #region Variables
        /// <summary>
        /// A Mapping of callbacks by type.
        /// </summary>
        private Dictionary<Type, HashSet<ContextualCallback<TCallback>>> m_CallbackMapping;

        /// <summary>
        /// Used to synchronize access to the callback map.
        /// </summary>
        private ReaderWriterLockSlim m_CallbackLock;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the CallbackMap class.
        /// </summary>
        public CallbackMap()
        {
            m_CallbackMapping = new Dictionary<Type, HashSet<ContextualCallback<TCallback>>>();
            m_CallbackLock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);
        }
        #endregion
        #region Methods
        /// <summary>
        /// Empties the list of callbacks.
        /// </summary>
        public void Clear()
        {
            m_CallbackLock.EnterWriteLock(); try
            {
                m_CallbackMapping.Clear();
            }
            finally
            {
                m_CallbackLock.ExitWriteLock();
            }
        }

        /// <summary>
        /// Adds a callback for the specified message type.
        /// </summary>
        /// <param name="messageType">The type of message to be notified of.</param>
        /// <param name="callback">The callback to invoke when the message is received.</param>
        public void AddCallback(Type messageType, TCallback callback)
        {
            AddCallback(messageType, callback, false);
        }

        /// <summary>
        /// Adds a callback for the specified message type.
        /// </summary>
        /// <param name="messageType">The type of message to be notified of.</param>
        /// <param name="callback">The callback to invoke when the message is received.</param>
        /// <param name="synchronous">True to add the callback synchronously, false to allow it to register Asynchrously.</param>
        /// <exception cref=" CKLib.Utilities.Exceptions.InvalidSynchronizationContextException">Throws an exception if there is no synchronization context
        /// to use when registering a synchronous callback.</exception>
        public void AddCallback(Type messageType, TCallback callback, bool synchronous)
        {
            if (messageType != null && callback != null)
            {
                if (!HasCallback(messageType, callback, true))
                {
                    m_CallbackLock.EnterWriteLock(); try
                    {
                        HashSet<ContextualCallback<TCallback>> callbackSet;

                        if (!HasCallbacksExplicit(messageType))
                        {
                            callbackSet = new HashSet<ContextualCallback<TCallback>>();
                            m_CallbackMapping.Add(messageType, callbackSet);
                        }
                        else
                        {
                            callbackSet = m_CallbackMapping[messageType];
                        }
                        callbackSet.Add(new ContextualCallback<TCallback>(callback, synchronous));
                    }
                    finally
                    {
                        m_CallbackLock.ExitWriteLock();
                    }
                }
            }
        }

        /// <summary>
        /// Removes a callback for the specified message type.
        /// </summary>
        /// <param name="messageType">The type of message to no longer be notified of.</param>
        /// <param name="callback">The callback to remove.</param>
        public void RemoveCallback(Type messageType, TCallback callback)
        {
            if (messageType != null && callback != null)
            {
                if (HasCallback(messageType, callback, true))
                {
                    m_CallbackLock.EnterWriteLock(); try
                    {
                        HashSet<ContextualCallback<TCallback>> callbackSet;
                        if (m_CallbackMapping.TryGetValue(messageType, out callbackSet))
                        {
                            ContextualCallback<TCallback> cb = new ContextualCallback<TCallback>(callback, false);
                            callbackSet.Remove(cb);
                            if (callbackSet.Count < 1)
                            {
                                m_CallbackMapping.Remove(messageType);
                            }
                        }
                    }
                    finally
                    {
                        m_CallbackLock.ExitWriteLock();
                    }
                }
            }
        }

        /// <summary>
        /// Determines whether the specified message type has any callbacks associated explicitly with it.
        /// </summary>
        /// <param name="messageType">Type of the message.</param>
        /// <returns>
        ///   <c>true</c> if [has callbacks explicit] [the specified message type]; otherwise, <c>false</c>.
        /// </returns>
        protected virtual bool HasCallbacksExplicit(Type messageType)
        {
            m_CallbackLock.EnterReadLock(); try
            {
                return m_CallbackMapping.ContainsKey(messageType);
            }
            finally
            {
                m_CallbackLock.ExitReadLock();
            }
        }

        /// <summary>
        /// Gets a value indicating whether there are any callbacks for the specified message type.
        /// </summary>
        /// <param name="messageType">The type of message to check for callbacks.</param>
        /// <returns>True if callbacks are found, false otherwise.</returns>
        public bool HasCallbacks(Type messageType)
        {
            m_CallbackLock.EnterReadLock(); try
            {
                foreach (var type in m_CallbackMapping.Keys)
                {
                    if (CheckTypes(type, messageType))
                    {
                        return true;
                    }
                }
                return false;
            }
            finally
            {
                m_CallbackLock.ExitReadLock();
            }
        }

        /// <summary>
        /// Gets a value indicating whether the input callback has been specified for the message type.
        /// </summary>
        /// <param name="messageType">The message type to check for callback the callback.</param>
        /// <param name="callback">The callback to check for.</param>
        /// <returns>True if the callback will be invoked when the message type is received, false otherwise.</returns>
        public bool HasCallback(Type messageType, TCallback callback)
        {
            return HasCallback(messageType, callback, false);
        }

        /// <summary>
        /// Gets a value indicating whether the input callback has been specified for the message type.
        /// </summary>
        /// <param name="messageType">The message type to check for callback the callback.</param>
        /// <param name="callback">The callback to check for.</param>
        /// <param name="exactTypeMatch">False to allow subtypes of the message type to count toward the result.</param>
        /// <returns>True if the callback will be invoked when the message type is received, false otherwise.</returns>
        public bool HasCallback(Type messageType, TCallback callback, bool exactTypeMatch)
        {
            m_CallbackLock.EnterReadLock(); try
            {
                var cb = new ContextualCallback<TCallback>(callback, false);
                foreach (var pair in m_CallbackMapping)
                {
                    if (CheckTypes(pair.Key, messageType) && (!exactTypeMatch || pair.Key == messageType))
                    {
                        if (pair.Value.Contains(cb))
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
            finally
            {
                m_CallbackLock.ExitReadLock();
            }
        }

        /// <summary>
        /// Checks to see the two types are compatible.
        /// </summary>
        /// <param name="internalType">The internal message type which has had callbacks registered.</param>
        /// <param name="messageType">The input message type to compare against.</param>
        /// <returns>True if the messages are compatible, false otherwise.</returns>
        protected virtual bool CheckTypes(Type internalType, Type messageType)
        {
            if (internalType != null)
            {
                if (messageType != null)
                {
                    if (internalType == messageType || internalType.IsAssignableFrom(messageType))
                    {
                        return true;
                    }
                }
                else
                {
                    return false;
                }
            }
            else if (messageType != null)
            {
                return false;
            }
            return false;
        }

        /// <summary>
        /// Gets all of the callbacks which have been registered for the input message type.
        /// </summary>
        /// <param name="messageType">The type of message to get the callbacks for.</param>
        /// <returns>An enumeration of callbacks.</returns>
        public IEnumerable<ContextualCallback<TCallback>> GetCallbacks(Type messageType)
        {
            HashSet<ContextualCallback<TCallback>> viableCallbacks = new HashSet<ContextualCallback<TCallback>>();
            m_CallbackLock.EnterReadLock(); try
            {
                foreach (var pair in m_CallbackMapping)
                {
                    if (CheckTypes(pair.Key, messageType))
                    {
                        viableCallbacks.UnionWith(pair.Value);
                    }
                }
                return viableCallbacks;
            }
            finally
            {
                m_CallbackLock.ExitReadLock();
            }
        }
        #endregion
    }
}
