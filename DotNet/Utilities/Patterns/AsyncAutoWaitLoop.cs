#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.Patterns
{
    #region Using List
    using System;
    using System.Threading;
    #endregion
    /// <summary>
    /// This class represents a looping pattern which does not consume system resources, nor does it cause unnecessary
    /// context switching. Instead is waits for a signal to continue through each iteration.
    /// </summary>
    /// <typeparam name="T">The type of the state to pass to the repeatable action.</typeparam>
    public class AsyncAutoWaitLoop<T> : IDisposable
    {
        #region Variables
        /// <summary>
        /// Value which is used to test whether a wait handle was set.
        /// </summary>
        private const int CHECK_IF_SET = 0;

        /// <summary>
        /// When this event is set the loop will terminate.
        /// </summary>
        private ManualResetEvent m_TerminationEvent;

        /// <summary>
        /// When this event is set the loop will process one cycle.
        /// </summary>
        private AutoResetEvent m_ContinuationEvent;

        /// <summary>
        /// This is the action that will be executed each iteration.
        /// </summary>
        private Action<T> m_RepeatingAction;

        /// <summary>
        /// The object to be passed to each loop iteration.
        /// </summary>
        private T m_StateObject;

        /// <summary>
        /// A Flag indicating whether the run loop has exited.
        /// </summary>
        private bool m_HasExited;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the AsyncAutoWaitLoop class.
        /// </summary>
        /// <param name="repeater">The action to execute each iteration of the loop.</param>
        /// <param name="state">A state object passed to the action on each iteration.</param>
        public AsyncAutoWaitLoop(Action<T> repeater, T state)
        {
            if (repeater == null)
            {
                throw new ArgumentNullException("repeater");
            }

            m_StateObject = state;
            m_RepeatingAction = repeater;
            m_ContinuationEvent = new AutoResetEvent(true);
            m_TerminationEvent = new ManualResetEvent(true);
            m_HasExited = true;
        }
        #endregion
        #region Methods
        /// <summary>
        /// Runs a lightweight loop which requires a signal to be set each iteration.
        /// Useful for message loops that are not constantly filling.
        /// </summary>
        public void RunLoop()
        {
            if (HasExited())
            {
                ThreadPool.QueueUserWorkItem((o) =>
                {
                    m_HasExited = false;
                    m_TerminationEvent.Reset();
                    WaitHandle[] handles = { m_ContinuationEvent, m_TerminationEvent };
                    while (true)
                    {
                        WaitHandle.WaitAny(handles, -1);
                        if (m_TerminationEvent == null || m_TerminationEvent.WaitOne(CHECK_IF_SET))
                        {
                            break;
                        }

                        m_RepeatingAction(m_StateObject);
                    }
                    m_HasExited = true;
                }, null);
            }
        }

        /// <summary>
        /// Signals the RunLoop to proceed for one iteration.
        /// </summary>
        public void SignalIteration()
        {
            if (m_ContinuationEvent != null)
            {
                m_ContinuationEvent.Set();
            }
        }

        /// <summary>
        /// Exits the RunLoop in a safe manner.
        /// </summary>
        public void Exit()
        {
            if (m_TerminationEvent != null)
            {
                m_TerminationEvent.Set();
            }
        }

        /// <summary>
        /// Gets a value indicating whether the run loop has exited.
        /// </summary>
        /// <returns>True if the loop has exited, false otherwise.</returns>
        public bool HasExited()
        {
            if (m_TerminationEvent != null)
            {
                return m_TerminationEvent.WaitOne(CHECK_IF_SET) && m_HasExited;
            }

            return true;
        }

        /// <summary>
        /// Waits for the RunLoop to exit.
        /// </summary>
        public void WaitForThreadExit()
        {
            if (m_TerminationEvent != null)
            {
                m_TerminationEvent.WaitOne();
            }
        }

        #region IDisposable Members

        /// <summary>
        /// Disposes of all resources held by the AsyncAutoWaitLoop class.
        /// </summary>
        public void Dispose()
        {
            Exit();
            if (!HasExited())
            {
                WaitForThreadExit();
            }
            m_RepeatingAction = null;
            m_StateObject = default(T);
            if (m_ContinuationEvent != null)
            {
                m_ContinuationEvent.Close();
            }

            if (m_TerminationEvent != null)
            {
                m_TerminationEvent.Close();
            }
        }

        #endregion
        #endregion
    }
}
