using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WCFTest.Patterns;
using System.ServiceModel;
using WCFTest.Messages;

namespace WCFTest.Connection
{
    public class ServerHost : IDisposable
    {
        private string m_NetworkAddress;
        private string m_ServerName;

        private ServiceHost m_Host;
        private AsyncAutoWaitLoop<DataServerMessageCallback> m_ProcessLoop;
        private CallbackMap<DataServerMessageCallback> m_CallbackMap;

        private bool m_IsRunning;

        public ServerHost()
            : this(null, null)
        {
        }

        public ServerHost(string networkAddress, string serverName)
        {
            if (string.IsNullOrEmpty(networkAddress))
            {
                networkAddress = "net.pipe://localhost";
            }

            if (string.IsNullOrEmpty(serverName))
            {
                serverName = "DeveloperServer";
            }

            m_NetworkAddress = networkAddress;
            m_ServerName = serverName;
            m_CallbackMap = new CallbackMap<DataServerMessageCallback>();
            m_ProcessLoop = new AsyncAutoWaitLoop<DataServerMessageCallback>(Server.Instance.ProcessMessages, IncomingMessage);
            m_Host = new ServiceHost(Server.Instance, new Uri(networkAddress));
            m_Host.AddServiceEndpoint(typeof(IServer), new NetNamedPipeBinding(), serverName);

            Server.Instance.ClientConnected += new EventHandler<ClientEventArgs>(Instance_ClientConnected);
            Server.Instance.ClientDisconnected += new EventHandler<ClientEventArgs>(Instance_ClientDisconnected);
            Server.Instance.MessageReceived += new EventHandler<ClientEventArgs>(Instance_MessageReceived);
        }

        public void StartServer()
        {
            m_Host.Open();
            Server.Instance.IsRunning = m_IsRunning = true;
            m_ProcessLoop.RunLoop();
        }

        public void StopServer()
        {
            if (m_IsRunning)
            {
                m_ProcessLoop.Dispose();

                foreach (var disp in m_Host.ChannelDispatchers)
                {
                    disp.Close();
                }

                m_Host.Close();
            }
        }

        public void AddMessageCallback(Type messageType, DataServerMessageCallback callback)
        {
            m_CallbackMap.AddCallback(messageType, callback);
        }

        public void RemoveMessageCallback(Type messsageType, DataServerMessageCallback callback)
        {
            m_CallbackMap.RemoveCallback(messsageType, callback);
        }

        void Instance_MessageReceived(object sender, ClientEventArgs e)
        {
            m_ProcessLoop.SignalIteration();
        }

        void Instance_ClientDisconnected(object sender, ClientEventArgs e)
        {
            Console.WriteLine(e.Client.Identifier + " has disconnected.");
        }

        void Instance_ClientConnected(object sender, ClientEventArgs e)
        {
            Console.WriteLine(e.Client.Identifier + " has connected.");
        }

        /// <summary>
        /// Processes all incoming messages and dispatches then based on callbacks.
        /// </summary>
        /// <param name="sender">The client who sent the message, will be null if the message originates from the server.</param>
        /// <param name="e">The event args containing the message.</param>
        private void IncomingMessage(Client sender, DataMessageEventArgs e)
        {
            if (e != null)
            {
                if (e.Message is ShutdownMessage)
                {
                    StopServer();
                }
                Type messageType = e.Message.GetType();
                if (m_CallbackMap.HasCallbacks(messageType))
                {
                    // Cache the result so as to avoid recursive locks.
                    var callbacks = m_CallbackMap.GetCallbacks(messageType).ToArray();
                    foreach (var callback in callbacks)
                    {
                        callback(sender, e);
                    }
                }
            }
        }

        #region IDisposable Members
        /// <summary>
        /// Releases all resources held by the server host.
        /// </summary>
        public void Dispose()
        {
            StopServer();
        }

        #endregion
    }
}
