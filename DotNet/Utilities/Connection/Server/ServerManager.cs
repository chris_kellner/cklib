#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.Connection.Server
{
    #region Using List
    using System;
    using System.Linq;
    using System.Threading;
    using CKLib.Utilities.DataStructures;
    using CKLib.Utilities.Patterns;
    #endregion
    public class ServerManager : ILockable, IDisposable
    {
        #region Variables
        /// <summary>
        /// Collection of known servers.
        /// </summary>
        private SynchronizedDictionary<string, Server> m_KnownServers;

        /// <summary>
        /// Object used for synchronization of server execution.
        /// </summary>
        private object m_Locker = new object();
        #endregion
        #region Constructors
        /// <summary>
        /// Prevents construction of a ServerManager class.
        /// </summary>
        private ServerManager()
        {
            m_KnownServers = new SynchronizedDictionary<string, Server>();
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets the singleton instance of the ServerManager class.
        /// </summary>
        public static ServerManager Instance
        {
            get
            {
                return Singleton<ServerManager>.Instance;
            }
        }

        /// <summary>
        /// Gets the Server with the input name.
        /// </summary>
        /// <param name="uniqueServerId">The name of the server to retrieve.</param>
        /// <returns>A Server object, or null if the server could not be located.</returns>
        public Server this[string uniqueServerId]
        {
            get
            {
                if (!string.IsNullOrEmpty(uniqueServerId))
                {
                    var server = ServerManager.Instance.m_KnownServers.GetOrAdd(uniqueServerId, (s) => new Server());
                    return server;
                }

                return null;
            }
        }
        #endregion
        #region Methods
        /// <summary>
        /// Shutsdown all known Servers and disposes of resources.
        /// </summary>
        public static void Shutdown()
        {
            var pairs = ServerManager.Instance.m_KnownServers.ToArray();
            for (int i = 0; i < pairs.Length; i++)
            {
                pairs[i].Value.Dispose();
            }

            ServerManager.Instance.m_KnownServers.Clear();
        }

        /// <summary>
        /// Begins the connection process asynchronously.
        /// </summary>
        public static void RunServerAsync(string uniqueServerId, ServerAccessType serverType)
        {
            ThreadPool.QueueUserWorkItem((o) =>
            {
                RunServer(uniqueServerId, serverType);
            }, null);
        }

        /// <summary>
        /// Begins the connection process asynchronously.
        /// </summary>
        /// <param name="uniqueServerId"></param>
        /// <param name="serverType"></param>
        /// <param name="serverName"></param>
        public static void RunServerAsync(string uniqueServerId, ServerAccessType serverType, string serverName)
        {
            ThreadPool.QueueUserWorkItem((o) =>
            {
                RunServer(uniqueServerId, serverType, o as string);
            }, serverName);
        }

        /// <summary>
        /// Starts the server with the specified access availability and name.
        /// </summary>
        /// <param name="uniqueServerId"></param>
        /// <param name="serverType"></param>
        public static void RunServer(string uniqueServerId, ServerAccessType serverType)
        {
            ServerManager.Instance.Lock(); try
            {
                var server = ServerManager.Instance.m_KnownServers.GetOrAdd(uniqueServerId, (s) => new Server());
                server.RunServer(serverType);
            }
            finally
            {
                ServerManager.Instance.Unlock();
            }
        }

        /// <summary>
        /// Starts the server with the specified access availability and name.
        /// </summary>
        /// <param name="uniqueServerId"></param>
        /// <param name="serverType"></param>
        /// <param name="serverName"></param>
        public static void RunServer(string uniqueServerId, ServerAccessType serverType, string serverName)
        {
            ServerManager.Instance.Lock(); try
            {
                var server = ServerManager.Instance.m_KnownServers.GetOrAdd(uniqueServerId, (s) => new Server());
                server.RunServer(serverType, serverName);
            }
            finally
            {
                ServerManager.Instance.Unlock();
            }
        }

        /// <summary>
        /// Stops this server from processing.
        /// </summary>
        /// <param name="uniqueServerId"></param>
        public static void StopServer(string uniqueServerId)
        {
            ServerManager.Instance.Lock(); try
            {
                Server theServer;
                if (ServerManager.Instance.m_KnownServers.TryGetValue(uniqueServerId, out theServer))
                {
                    theServer.StopServer();
                }
            }
            finally
            {
                ServerManager.Instance.Unlock();
            }
        }
        #endregion
        #region ILockable Members

        void ILockable.Lock()
        {
            Lock();
        }

        void ILockable.Unlock()
        {
            Unlock();
        }

        /// <summary>
        /// Gets or sets the LockId of the object.
        /// </summary>
        /// <remarks>This property is reserved for future use and should only be implemented as a default property.</remarks>
        int ILockable.LockId { get; set; }

        private void Lock()
        {
            Monitor.Enter(m_Locker);
        }

        private void Unlock()
        {
            Monitor.Exit(m_Locker);
        }

        public bool IsSynchronized
        {
            get { return true; }
        }

        #endregion

        #region IDisposable Members
        /// <summary>
        /// Releases all resources held by this instance.
        /// </summary>
        public void Dispose()
        {
            Shutdown();
        }

        #endregion
    }
}
