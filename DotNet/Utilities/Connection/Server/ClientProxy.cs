#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.Connection.Server
{
    #region Using List
    using System;
    using System.Linq;
    using System.ServiceModel;
    using CKLib.Utilities.Connection.Client;
    #endregion
    public class ClientProxy
    {
        #region Variables
        /// <summary>
        /// A value indicating whether this client is from the local machine.
        /// </summary>
        private readonly bool m_IsLocalClient;
        /// <summary>
        /// The server that controls this client proxy instance.
        /// </summary>
        private readonly Server m_OwningServer;

        /// <summary>
        /// A value containing information about the connection.
        /// </summary>
        private readonly ConnectionInformation m_ConnectionInformation;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ClientProxy class.
        /// </summary>
        /// <param name="owningServer"></param>
        /// <param name="callback"></param>
        /// <param name="isLoopback">True if this connection is from the localmachine.</param>
        /// <param name="information"></param>
        public ClientProxy(Server owningServer, IClientCallback callback, bool isLoopback, ConnectionInformation information)
        {
            if (owningServer == null)
            {
                throw new ArgumentNullException("owningServer");
            }

            m_ConnectionInformation = information;
            m_OwningServer = owningServer;
            ClientCallback = callback;
            Communicator.Faulted += new EventHandler(Communicator_Faulted);
            Communicator.Closed += new EventHandler(Communicator_Faulted);
            m_IsLocalClient = isLoopback;
            m_ConnectionInformation.ConnectionStatus = ConnectionStatus.Connected;
}
        #endregion
        #region Properties
        public Guid Identifier
        {
            get
            {
                return Information.ConnectionId;
            }
        }

        /// <summary>
        /// Gets information about this connection.
        /// </summary>
        public ConnectionInformation Information
        {
            get
            {
                return m_ConnectionInformation;
            }
        }

        public IClientCallback ClientCallback
        {
            get;
            private set;
        }

        public ICommunicationObject Communicator
        {
            get
            {
                return ClientCallback as ICommunicationObject;
            }
        }

        public bool ConnectionOk
        {
            get
            {
                if (Communicator != null)
                {
                    return Communicator.State == CommunicationState.Opened;
                }

                return false;
            }
        }

        public bool IsLocalClient
        {
            get
            {
                return m_IsLocalClient;
            }
        }
        #endregion

        #region Methods
        public TObject GetSharedObject<TObject>() where TObject : class
        {
            var shares = m_OwningServer.SharedObjectPool.GetObjectsSharedByOwner(Identifier);
            var obj = shares.FirstOrDefault(o => o.ObjectType.IsAssignableFrom(typeof(TObject)));

            if (obj != null)
            {
                return (TObject)obj;
            }

            return null;
        }

        public TObject GetSharedObject<TObject>(Guid objectId) where TObject : class
        {
            return m_OwningServer.SharedObjectPool.GetSharedObject<TObject>(objectId);
        }

        void Communicator_Faulted(object sender, EventArgs e)
        {
            m_ConnectionInformation.ConnectionStatus = ConnectionStatus.NotConnected;
            m_OwningServer.ClientFaulted(this);
            if (ConnectionFaulted != null)
            {
                ConnectionFaulted(this, e);
            }
        }
        #endregion

        public event EventHandler ConnectionFaulted;
    }
}
