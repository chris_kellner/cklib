#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.Connection.Server
{
    #region Using List
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.ServiceModel;
    using CKLib.Utilities.Connection.Client;
    using CKLib.Utilities.Connection.Extensions;
    using CKLib.Utilities.DataStructures;
    using CKLib.Utilities.DataStructures.Configuration;
    using CKLib.Utilities.Messages;
    using CKLib.Utilities.Proxy.ObjectSharing;
    using CKLib.Utilities.Security;
    using CKLib.Utilities.Diagnostics;
    #endregion
    [ServiceBehavior(
        InstanceContextMode = InstanceContextMode.Single,
        ConcurrencyMode = ConcurrencyMode.Multiple,
        IncludeExceptionDetailInFaults = true)]
    public class Server : IServer, IMessenger, IDisposable
    {
        #region Variables
        /// <summary>
        /// Maintains the set of currently connected clients.
        /// </summary>
        private SynchronizedDictionary<ClientProxy, IClientCallback> m_Clients;

        /// <summary>
        /// Contains references to all of the shared objects.
        /// </summary>
        private SharedObjectPool m_SharedObjectPool;

        /// <summary>
        /// The main message pump for all server side activities.
        /// </summary>
        private ProcessQueue m_ProcessQueue;

        /// <summary>
        /// The unique id for this server.
        /// </summary>
        private Guid m_ServerId;

        /// <summary>
        /// A value indicating whether the server is running.
        /// </summary>
        private bool m_IsServerRunning;

        /// <summary>
        /// The ExtensionManager for the server.
        /// </summary>
        private ExtensionManager m_ExtensionManager;
        #endregion
        /// <summary>
        /// Initializes a new instance of the Server class.
        /// </summary>
        public Server()
        {
            m_Clients = new SynchronizedDictionary<ClientProxy, IClientCallback>();
            m_ProcessQueue = new ProcessQueue();
            m_ServerId = Guid.NewGuid();
            m_SharedObjectPool = new SharedObjectPool();
            m_SharedObjectPool.Initialize(this);
            m_ExtensionManager = new ExtensionManager();
            StartMessagePump();
        }

        #region Service Methods
        /// <summary>
        /// Starts the server with the specified access availability with the name of the current process.
        /// </summary>
        /// <param name="serverType"></param>
        public void RunServer(ServerAccessType serverType)
        {
            string serverName = null;
            var group = ConfigurationManager.Current[SettingGroup.LOCAL_SERVER_SETTINGS_GROUP];
            if (group != null)
            {
                group.TryReadValue(Setting.SERVER_NAME_SETTING, out serverName);
            }

            RunServer(serverType, serverName);
        }

        /// <summary>
        /// Starts the server with the specified access availability and name.
        /// </summary>
        /// <param name="serverType"></param>
        /// <param name="serverName"></param>
        public void RunServer(ServerAccessType serverType, string serverName)
        {
            int port = 6969;
            var group = ConfigurationManager.Current[SettingGroup.LOCAL_SERVER_SETTINGS_GROUP];
            if (group != null)
            {
                var portSetting = group[Setting.SERVER_TCP_PORT];
                if (portSetting != null)
                {
                    port = portSetting.AsInt32();
                }
            }

            RunServer(serverType, serverName, port);
        }

        /// <summary>
        /// Starts the server with the specified access availability, name, and port.
        /// </summary>
        /// <param name="serverType"></param>
        /// <param name="serverName"></param>
        /// <param name="port">The port to host a network server on.</param>
        public void RunServer(ServerAccessType serverType, string serverName, int port)
        {
            if (string.IsNullOrEmpty(serverName))
            {
                serverName = Process.GetCurrentProcess().Id.ToString();
            }

            if (!IsRunning)
            {
                bool bLocal = (serverType & ServerAccessType.LocalOnly) == ServerAccessType.LocalOnly;
                bool bNetwork = (serverType & ServerAccessType.NetworkOnly) == ServerAccessType.NetworkOnly;
                ServerName = serverName;
                var localNetworkAddress = "net.pipe://localhost/DeveloperServers/" + serverName;
                var remoteNetworkAddress = string.Format("net.tcp://localhost:{0}/DeveloperServers/{1}", port, serverName);

                Host = new ServiceHost(this);
                Host.Opened += Host_Opened;
                Host.Closed += Host_Closed;
                Host.Faulted += Host_Closed;
                if (bLocal)
                {
                    Host.AddServiceEndpoint(typeof(IServer), new NetNamedPipeBinding()
                    {
                        SendTimeout = TimeSpan.FromHours(20f),
                        ReceiveTimeout = TimeSpan.FromHours(20f)
                    }, localNetworkAddress);
                }

                if (bNetwork)
                {
                    var binding = new NetTcpBinding(SecurityMode.None) {
                        //PortSharingEnabled = true
                        SendTimeout = TimeSpan.FromHours(20f),
                        ReceiveTimeout = TimeSpan.FromHours(20f)
                    };
                    Host.AddServiceEndpoint(typeof(IServer), binding, remoteNetworkAddress);
                }

                try
                {
                    Host.Open();
                }
                catch (Exception e)
                {
                    SendLocalMessage(new StringMessage(e.Message));
                }
            }
        }

        /// <summary>
        /// Called when the server is stopped.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Host_Closed(object sender, EventArgs e)
        {
            IsRunning = false;
            Host = null;
        }

        /// <summary>
        /// Called when the server is started successfully.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Host_Opened(object sender, EventArgs e)
        {
            IsRunning = true;
        }

        /// <summary>
        /// Stops this server from processing.
        /// </summary>
        public void StopServer()
        {
            if (IsRunning)
            {
                SendNetworkMessage(new ShutdownMessage());

                SharedObjectPool.Release();
                m_Clients.Clear();

                try
                {
                    Host.Close();
                }
                catch (Exception)
                {
                    // Already closed
                }
            }
        }

        /// <summary>
        /// Gets the service host used by this server.
        /// </summary>
        public ServiceHost Host
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the name of the server.
        /// </summary>
        public string ServerName
        {
            get;
            private set;
        }
        #endregion

        /// <summary>
        /// Removes a client from the server.
        /// </summary>
        /// <param name="clientId">The client to remove.</param>
        public void KickClient(Guid clientId)
        {
            SendNetworkMessage(clientId, new ShutdownMessage());
        }

        /// <summary>
        /// Gets the shared object pool used to share objects between client server.
        /// </summary>
        public ISharedObjectPool SharedObjectPool
        {
            get
            {
                return m_SharedObjectPool;
            }
        }

        /// <summary>
        /// Gets a reference to the ExtensionManager for the server.
        /// </summary>
        public ExtensionManager ExtensionManager
        {
            get
            {
                return m_ExtensionManager;
            }
        }

        /// <summary>
        /// Finds the client whose id matches the input clientId.
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public ClientProxy GetClient(Guid clientId)
        {
            ClientProxy client = m_Clients.Keys.FirstOrDefault(c => c.Identifier == clientId);
            return client;
        }

        /// <summary>
        /// Occurs when a client gets disconnected.
        /// </summary>
        /// <param name="client">The client that has disconnected.</param>
        internal void ClientFaulted(ClientProxy client)
        {
            SharedObjectPool.DisposeObjectsForOwner(client.Identifier);
            ((IServer)this).UnregisterClient(client.Identifier);
        }

        #region IServer Members
        /// <summary>
        /// Registers a client with this server and returns the id of this server.
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        Guid IServer.RegisterClient(ConnectionInformation clientId)
        {
            IClientCallback callback =
                OperationContext.Current.GetCallbackChannel<IClientCallback>();
            var messageProperties = OperationContext.Current.IncomingMessageProperties;
            if (messageProperties != null && clientId != null)
            {
                var endpointProperty = messageProperties.Via;
                if (endpointProperty != null)
                {
                    clientId.ConnectionAddress = endpointProperty;
                    bool isLoopback = endpointProperty.IsLoopback;

                    ClientProxy c = new ClientProxy(this, callback, isLoopback, clientId);

                    m_Clients.Add(c, callback);

                    OnClientConnected(new ClientEventArgs() { Client = c });
                    Standard.WriteEntry(string.Format(LogStrings.STD_SERVER_CLIENT_CONNECTED, c.Information));
                    return MessengerId;
                }
            }

            if (callback != null)
            {
                OperationContext.Current.Channel.Close();
            }

            return Guid.Empty;
        }

        /// <summary>
        /// Called when a client connects to the server.
        /// </summary>
        /// <param name="e">The arguments describing the client.</param>
        protected virtual void OnClientConnected(ClientEventArgs e)
        {
            SendLocalMessage(new ConnectionOpenedMessage(e.Client.Identifier));
            if (ClientConnected != null)
            {
                ClientConnected(this, e);
            }
        }

        /// <summary>
        /// Called when a client disconnects from the server.
        /// </summary>
        /// <param name="e">The arguments describing the client.</param>
        protected virtual void OnClientDisconnected(ClientEventArgs e)
        {
            SendLocalMessage(new ConnectionClosedMessage(e.Client.Identifier));
            if (ClientDisconnected != null)
            {
                ClientDisconnected(this, e);
            }
        }

        /// <summary>
        /// Unregisters a client from this server.
        /// </summary>
        /// <param name="clientId"></param>
        void IServer.UnregisterClient(Guid clientId)
        {
            foreach (var c in m_Clients.TryRemoveWhere(p => p.Key.Identifier == clientId))
            {
                if (c.Key != null)
                {
                    m_ExtensionManager.RevokeConnectionFromAllExtensions(clientId);
                    OnClientDisconnected(new ClientEventArgs() { Client = c.Key });
                    Standard.WriteEntry(string.Format(LogStrings.STD_SERVER_CLIENT_DISCONNECTED, c.Key.Information));
                }
            }
        }

        /// <summary>
        /// Resends all extensions to the specified client.
        /// </summary>
        /// <param name="clientId">The id of the client to resend extension information to.</param>
        void IServer.ResendExtensionsAndShares(Guid clientId)
        {
            if (m_ExtensionManager != null)
            {
                m_ExtensionManager.NotifyConnectionOfAvailableExtensions(clientId);
            }
        }

        /// <summary>
        /// Sends a message to the server.
        /// </summary>
        /// <param name="message"></param>
        void IServer.SendMessage(IDataMessage message)
        {
            if (LocalOnlyAttribute.CheckAccess(message.GetType(), this, message.SenderId))
            {
                Guid senderId = message.SenderId;
                ClientProxy sender = null;
                sender = m_Clients.Keys.FirstOrDefault(c => c.Identifier == senderId);

                if (sender != null)
                {
                    m_ProcessQueue.SendMessage(new MessageEnvelope(senderId, message));
                }

                if (MessageReceived != null)
                {
                    MessageReceived(this, new ClientEventArgs());
                }
            }
        }

        #endregion

        #region Events
        public event EventHandler<ClientEventArgs> ClientConnected;
        public event EventHandler<ClientEventArgs> ClientDisconnected;
        public event EventHandler<ClientEventArgs> MessageReceived;
        public event EventHandler ServerStarted;
        public event EventHandler ServerStopped;
        #endregion

        #region IMessenger Members

        public void AddCallback(Type messageType, DataMessageCallback callback)
        {
            m_ProcessQueue.AddCallback(messageType, callback);
        }

        public void AddCallback(Type messageType, DataMessageCallback callback, bool synchronous)
        {
            m_ProcessQueue.AddCallback(messageType, callback, synchronous);
        }

        public void RemoveCallback(Type messageType, DataMessageCallback callback)
        {
            m_ProcessQueue.RemoveCallback(messageType, callback);
        }

        /// <summary>
        /// Starts the automatic message pump.
        /// </summary>
        public void StartMessagePump()
        {
            m_ProcessQueue.StartMessagePump();
        }

        /// <summary>
        /// Stops the automatic message pump.
        /// </summary>
        public void StopMessagePump()
        {
            m_ProcessQueue.StopMessagePump();
        }

        /// <summary>
        /// Forces processing of messages on the queue.
        /// </summary>
        public void ProcessMessages()
        {
            m_ProcessQueue.ProcessMessages();
        }

        /// <summary>
        /// Gets the unique identifier for this server.
        /// </summary>
        public Guid MessengerId
        {
            get
            {
                return m_ServerId;
            }
        }

        /// <summary>
        /// Gets information about the specified connection.
        /// </summary>
        /// <param name="connectionId">The id of the connection to retrieve information about.</param>
        /// <returns></returns>
        public IConnectionInformation GetConnectionInformation(Guid connectionId)
        {
            ClientProxy proxy = m_Clients.Keys.FirstOrDefault(c => c.Identifier == connectionId);
            if (proxy != null)
            {
                return proxy.Information;
            }

            return null;
        }

        /// <summary>
        /// Gets a value indicating whether this messenger is a server.
        /// </summary>
        public bool IsServer
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets an array of identifiers corresponding to the active connections.
        /// </summary>
        /// <returns>An array of identifiers corresponding to the active connections.</returns>
        public Guid[] GetConnectionIdentifiers()
        {
            return m_Clients.Keys.Select(c => c.Identifier).ToArray();
        }

        /// <summary>
        /// Places a message on the local message queue.
        /// </summary>
        /// <param name="message"></param>
        public void SendLocalMessage(IDataMessage message)
        {
            message.SenderId = m_ServerId;
            m_ProcessQueue.SendMessage(new MessageEnvelope(m_ServerId, message));

            if (MessageReceived != null)
            {
                MessageReceived(this, new ClientEventArgs());
            }
        }

        /// <summary>
        /// Sends a message to a specific client.
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="message"></param>
        public void SendNetworkMessage(Guid clientId, IDataMessage message)
        {
            if (LocalOnlyAttribute.CheckAccess(message.GetType(), this, clientId))
            {
                message.SenderId = m_ServerId;
                var client = m_Clients.FirstOrDefault(pair => pair.Key.Identifier == clientId);
                if (client.Value != null)
                {
                    client.Value.SendMessage(message);
                }
            }
        }

        /// <summary>
        /// Sends a message to all connected clients.
        /// </summary>
        /// <param name="message"></param>
        public void SendNetworkMessage(IDataMessage message)
        {
            HashSet<ClientProxy> faultedClients = null;
            message.SenderId = m_ServerId;
            foreach (var client in m_Clients)
            {
                try
                {
                    SendNetworkMessage(client.Key.Identifier, message);
                }
                catch (CommunicationObjectFaultedException)
                {
                    if (faultedClients == null)
                    {
                        faultedClients = new HashSet<ClientProxy>();
                    }

                    faultedClients.Add(client.Key);
                }
                catch (CommunicationObjectAbortedException)
                {
                    if (faultedClients == null)
                    {
                        faultedClients = new HashSet<ClientProxy>();
                    }

                    faultedClients.Add(client.Key);
                }
            }
            if (faultedClients != null)
            {
                foreach (var faultedClient in faultedClients)
                {
                    m_Clients.Remove(faultedClient);
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether the input remote connection id is local or not.
        /// </summary>
        /// <param name="connectionId">The connection id.</param>
        /// <returns>True if the id belongs to a localhost connection, false otherwise.</returns>
        public bool IsConnectionLocal(Guid connectionId)
        {
            var client = GetClient(connectionId);
            if (client != null)
            {
                return client.IsLocalClient;
            }

            return false;
        }

        /// <summary>
        /// Gets the current connection status of the server.
        /// </summary>
        public CommunicationState ConnectionStatus
        {
            get
            {
                if (Host != null)
                {
                    return Host.State;
                }

                return CommunicationState.Closed;
            }
        }
        #endregion

        /// <summary>
        /// Gets a reference to the ServerManager which produces and manages servers.
        /// </summary>
        public static ServerManager Instance
        {
            get
            {
                return ServerManager.Instance;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this server is running.
        /// </summary>
        public bool IsRunning
        {
            get
            {
                return m_IsServerRunning;
            }

            set
            {
                m_IsServerRunning = value;
                if (IsRunning)
                {
                    if (ServerStarted != null)
                    {
                        ServerStarted(this, EventArgs.Empty);
                    }

                    var pool = m_SharedObjectPool as SharedObjectPool;
                    if (pool != null)
                    {
                        pool.SendPendingNotifications();
                    }

                    Standard.WriteEntry(string.Format(LogStrings.STD_SERVER_STARTED, ServerName));
                }
                else
                {
                    m_IsServerRunning = false;
                    if (ServerStopped != null)
                    {
                        ServerStopped(this, EventArgs.Empty);
                    }

                    Standard.WriteEntry(string.Format(LogStrings.STD_SERVER_STOPPED, ServerName));
                }
            }
        }

        /// <summary>
        /// Releases resources held by this instance.
        /// </summary>
        public void Dispose()
        {
            if (m_ExtensionManager != null)
            {
                m_ExtensionManager.Release();
            }

            StopMessagePump();
            StopServer();
        }
    }

    public class ClientEventArgs : EventArgs
    {
        public ClientProxy Client { get; set; }
    }
}
