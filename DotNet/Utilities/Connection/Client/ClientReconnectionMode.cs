#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.Connection.Client
{
    /// <summary>
    /// Describes the action a client should take when a connection is interrupted unexpectedly.
    /// </summary>
    public enum ClientReconnectionMode : int
    {
        /// <summary>
        /// The client should remain disconnected.
        /// </summary>
        Default = 0,

        /// <summary>
        /// The client should remain disconnected. (This is the default).
        /// </summary>
        DoNotReconnect = 0,

        /// <summary>
        /// The client should attempt to reconnect one time. [NYI]
        /// </summary>
        RetryOnce = 1,

        /// <summary>
        /// The client should attempt to reconnect until it is successful.
        /// </summary>
        RetryForever = 2,

        /// <summary>
        /// The client should prompt the system for reconnection instructions. [NYI]
        /// </summary>
        PromptForRetry = 3
    }
}
