﻿#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.Diagnostics
{
    #region Using List
    using System;
    using CKLib.Utilities.Patterns;
    #endregion
    /// <summary>
    /// This class is used as a central API for logging Fatal errors that occur.
    /// </summary>
    public class Fatal : ADiagnosticChannel<Fatal>
    {
        #region Constructors
        /// <summary>
        /// Initializes static members of the Fatal class.
        /// </summary>
        static Fatal()
        {
            Singleton<Fatal>.ValueFactory = () => new Fatal();
        }

        /// <summary>
        /// Prevents external initialization of the Fatal class.
        /// </summary>
        private Fatal()
            : base()
        {
        }

        #endregion
        #region Methods
        /// <summary>
        /// Called when messages are written to this channel.
        /// </summary>
        /// <param name="e">Arguments describing what is being written.</param>
        protected override void OutputWritten(OutputEventArgs e)
        {
            Console.Error.WriteLine(e.ToString());
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets the default output level of this channel.
        /// </summary>
        protected override OutputLevel OutputLevel
        {
            get { return Diagnostics.OutputLevel.Fatal; }
        }
        #endregion
    }
}
