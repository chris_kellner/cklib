#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.Messages
{
    #region Using List
    using System;
    #endregion
    /// <summary>
    /// This class represents a message and information about the message.
    /// </summary>
    [Obsolete("This class is OBE",true)]
    public class MessagePair
    {
        #region Constructors
        /// <summary>
        /// Initializes a new intance of the MessagePair class.
        /// </summary>
        /// <param name="sender">The sender of the message.</param>
        /// <param name="message">The event args containing the message being sent.</param>
        public MessagePair(Guid sender, MessageEnvelope message)
        {
            Sender = sender;
            MessageInformation = message;
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets the sender of the message.
        /// </summary>
        public Guid Sender { get; private set; }

        /// <summary>
        /// Gets the message event args.
        /// </summary>
        public MessageEnvelope MessageInformation { get; private set; }
        #endregion
    }
}
