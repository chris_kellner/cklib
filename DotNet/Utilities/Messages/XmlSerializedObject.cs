#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.Messages
{
    #region Using List
    using System;
    using System.IO;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Xml.Serialization;
    #endregion
    /// <summary>
    /// This class represents an object that has been xmlserialized to be sent accross the pipeline.
    /// </summary>
    [DataContract(Name="XmlSerializedObject")]
    public class XmlSerializedObject
    {
        #region Variables
        /// <summary>
        /// Used to cache the result of the deserialized string.
        /// </summary>
        private object m_DeserializedValue;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the XmlSerializedObject class.
        /// </summary>
        /// <param name="obj">The object value</param>
        /// <exception cref="ArgumentNullException">Throws an exception if obj is null.</exception>
        public XmlSerializedObject(object obj)
        {
            if (obj != null)
            {
                ObjectType = obj.GetType();
                Value = obj;
            }
        }
        #endregion
        #region Properties
        /// <summary>
        /// The type of object being deserialized.
        /// </summary>
        [XmlIgnore]
        public Type ObjectType
        {
            get
            {
                if (!string.IsNullOrEmpty(AssemblyQualifiedTypeName))
                {
                    return Proxy.FactoryHelpers.FindType(AssemblyQualifiedTypeName);
                }
                return typeof(void);
            }
            set
            {
                AssemblyQualifiedTypeName = value.AssemblyQualifiedName;
            }
        }

        /// <summary>
        /// The AssemblyQualifiedName of the type to deserialize.
        /// </summary>
        [DataMember(IsRequired=true)]
        public string AssemblyQualifiedTypeName
        {
            get;
            private set;
        }

        /// <summary>
        /// The Value of the object.
        /// </summary>
        [XmlIgnore]
        public object Value
        {
            get
            {
                if (m_DeserializedValue == null && ObjectType != null && ObjectType != typeof(void))
                {
                    XmlSerializer cereal = new XmlSerializer(ObjectType);
                    using (StringReader reader = new StringReader(XmlSerializedValue))
                    {
                        m_DeserializedValue = cereal.Deserialize(reader);
                    }
                }
                return m_DeserializedValue;
            }
            private set
            {
                var val = Cast(ObjectType, value);
                XmlSerializer cereal = new XmlSerializer(ObjectType);
                StringBuilder builder = new StringBuilder();
                using (StringWriter writer = new StringWriter(builder))
                {
                    cereal.Serialize(writer, val);
                    XmlSerializedValue = builder.ToString();
                }
            }
        }

        /// <summary>
        /// The XmlSerialized version of the object.
        /// </summary>
        [DataMember(IsRequired = true)]
        public string XmlSerializedValue
        {
            get;
            private set;
        }
        #endregion
        #region Methods
        /// <summary>
        /// Attempts to cast the value to the specified type.
        /// </summary>
        /// <param name="t">The type to cast it as.</param>
        /// <param name="o">The object to cast.</param>
        /// <returns></returns>
        public static object Cast(Type t, object o)
        {
            var method = typeof(XmlSerializedObject).GetMethod("DoCast", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
            if (t != null)
            {
                var method2 = method.MakeGenericMethod(t);
                return method2.Invoke(null, new object[] { o });
            }

            return o;
        }

        /// <summary>
        /// Casts the input object as the specified type T.
        /// </summary>
        /// <typeparam name="T">The type to cast the object to.</typeparam>
        /// <param name="o">The object to cast.</param>
        /// <returns>The object as T.</returns>
        private static T DoCast<T>(object o)
        {
            if (o != null)
            {
                return (T)o;
            }

            return default(T);
        }
        #endregion
    }
}
