#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.Messages
{
    #region Using List
    using System;
    using System.Runtime.Serialization;
    using CKLib.Utilities.Proxy;
    using CKLib.Utilities.Proxy.ObjectSharing;
    #endregion
    /// <summary>
    /// This class is used to alert a connection of an available shared object.
    /// </summary>
    [DataContract]
    public class SharedObjectAvailabilityMessage : IDataMessage
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the SharedObjectAvailabilityMessage class.
        /// </summary>
        public SharedObjectAvailabilityMessage()
        {
        }

        /// <summary>
        /// Initializes a new intance of the SharedObjectAvailabilityMessage class.
        /// </summary>
        /// <param name="objectShare">The shared object.</param>
        /// <param name="available">A flag indicating whether the object is available or disposing.</param>
        public SharedObjectAvailabilityMessage(ISharedObject objectShare, bool available)
        {
            if (objectShare != null)
            {
                InstanceId = objectShare.InstanceId;
                ObjectType = objectShare.ObjectType;
                IsAvailable = available;
            }
            else
            {
                throw new ArgumentNullException("objectShare");
            }
        }

        /// <summary>
        /// Initializes a new intance of the SharedObjectAvailabilityMessage class.
        /// </summary>
        /// <param name="instanceId">The instance id of the shared object.</param>
        /// <param name="objectType">The type of the shared object.</param>
        /// <param name="isAvailable">A flag indicating whether the object is available or disposing.</param>
        public SharedObjectAvailabilityMessage(Guid instanceId, Type objectType, bool isAvailable)
        {
            InstanceId = instanceId;
            ObjectType = objectType;
            IsAvailable = isAvailable;
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets the instance id of the shared object.
        /// </summary>
        [DataMember(IsRequired = true)]
        public Guid InstanceId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the assembly qualified name of the type.
        /// </summary>
        [DataMember(IsRequired = true)]
        public string AssemblyQualifiedTypeName
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets a value indicating whether the object was created (IsAvailable) or was cleaned up (!IsAvailable).
        /// </summary>
        [DataMember(IsRequired=true)]
        public bool IsAvailable
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the type of object being shared.
        /// </summary>
        public Type ObjectType
        {
            get
            {
                return FactoryHelpers.FindType(AssemblyQualifiedTypeName);
            }

            private set
            {
                AssemblyQualifiedTypeName = value.AssemblyQualifiedName;
            }
        }
        #endregion
    }
}
