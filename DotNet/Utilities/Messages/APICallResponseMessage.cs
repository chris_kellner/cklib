#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.Messages
{
    #region Using List
    using System;
    using System.Runtime.Serialization;
    using System.Xml.Serialization;
    #endregion
    /// <summary>
    /// This message is used by the SharedObjectPool to submit a method call return value.
    /// </summary>
    [DataContract]
    public class APICallResponseMessage : IDataMessage
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the APICallResponseMessage class.
        /// </summary>
        public APICallResponseMessage()
        {
        }

        /// <summary>
        /// Initializes a new instance of the APICallResponseMessage class.
        /// </summary>
        /// <param name="requestId">The request id that this message is in response to.</param>
        /// <param name="assemblyQualifiedName">The typename of the shared object.</param>
        /// <param name="instanceId">The instance id of the shared object.</param>
        /// <param name="response">The return value of the method call.</param>
        public APICallResponseMessage(Guid requestId, string assemblyQualifiedName, Guid instanceId, object response)
        {
            RequestId = requestId;
            AssemblyQualifiedTypeName = assemblyQualifiedName;
            Response = response;
            InstanceId = instanceId;
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets the instance id of the shared object.
        /// </summary>
        [DataMember(IsRequired = true)]
        public Guid InstanceId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the request id of the method call request.
        /// </summary>
        [DataMember(IsRequired = true)]
        public Guid RequestId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the type name of the shared object.
        /// </summary>
        [DataMember(IsRequired = true)]
        public string AssemblyQualifiedTypeName
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the return value in xml form.
        /// </summary>
        [DataMember]
        public XmlSerializedObject XmlSerializedResponse
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the return value of the method call.
        /// </summary>
        [XmlIgnore]
        public object Response
        {
            get
            {
                return XmlSerializedResponse.Value;
            }
            private set
            {
                XmlSerializedResponse = new XmlSerializedObject(value);
            }
        }
        #endregion
    }
}
