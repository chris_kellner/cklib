#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.DataStructures
{
    #region Using List
    using System;
    using System.Collections.Specialized;
    using System.Windows.Threading;
    #endregion
    public class SynchronizedObservableQueue<TQueue> : SynchronizedQueue<TQueue>, INotifyCollectionChanged
    {
        #region Variables
        /// <summary>
        /// The dispatcher to route the collection changed event through.
        /// </summary>
        private Dispatcher m_Dispatcher;

        /// <summary>
        /// Indicates whether this collection should lock itself when additions or subtractions are made.
        /// </summary>
        private bool m_IsSynchronized;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the SynchronizedObservableQueue class.
        /// </summary>
        /// <param name="observingDispatcher">The dispatcher to route the collection changed event through.</param>
        public SynchronizedObservableQueue(Dispatcher observingDispatcher)
            : base()
        {
            m_Dispatcher = observingDispatcher;
        }
        #endregion
        #region INotifyCollectionChanged Members
        /// <summary>
        /// Occurs when the collection changes.
        /// </summary>
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        /// <summary>
        /// Raises the CollectionChanged event.
        /// </summary>
        /// <param name="e">Event Args describing how the collection changed.</param>
        protected virtual void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (CollectionChanged != null)
            {
                if (m_Dispatcher != null && !m_Dispatcher.CheckAccess())
                {
                    Action<NotifyCollectionChangedEventArgs> cb = new Action<NotifyCollectionChangedEventArgs>(OnCollectionChanged);
                    m_Dispatcher.Invoke(cb, e);
                }
                else
                {
                    CollectionChanged(this, e);
                }
            }
        }
        #endregion
        #region Overrides
        /// <summary>
        /// Pushes a value onto the queue.
        /// </summary>
        /// <param name="obj">The value to push onto the queue.</param>
        public override void Enqueue(TQueue obj)
        {
            base.Enqueue(obj);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, obj));
        }

        /// <summary>
        /// Pops a value from the queue.
        /// </summary>
        /// <returns>The popped value.</returns>
        public override TQueue Dequeue()
        {
            TQueue retVal = base.Dequeue();
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, retVal));
            return retVal;
        }

        /// <summary>
        /// Clears the queue of all items.
        /// </summary>
        public override void Clear()
        {
            base.Clear();
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        /// <summary>
        /// Gets a value indicating whether this collection is synchronized.
        /// </summary>
        public override bool IsSynchronized
        {
            get
            {
                return Synchronized;
            }
        }

        /// <summary>
        /// Locks the collection.
        /// </summary>
        protected override void Lock()
        {
            if (IsSynchronized)
            {
                base.Lock();
            }
        }

        /// <summary>
        /// Unlocks the collection.
        /// </summary>
        protected override void Unlock()
        {
            if (IsSynchronized)
            {
                base.Unlock();
            }
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether this collection is synchronized.
        /// </summary>
        public bool Synchronized
        {
            get
            {
                return m_IsSynchronized;
            }

            set
            {
                if (m_IsSynchronized != value)
                {
                    Lock(); try
                    {
                        m_IsSynchronized = value;
                    }
                    finally
                    {
                        Unlock();
                    }
                }
            }
        }
        #endregion
    }
}
