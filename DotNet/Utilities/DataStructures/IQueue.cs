#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.DataStructures
{
    #region Using List
    using System.Collections;
    using System.Collections.Generic;
    #endregion
    /// <summary>
    /// Exposes the basic functionality of a queue.
    /// </summary>
    /// <typeparam name="TQueue"></typeparam>
    public interface IQueue<TQueue> : IEnumerable<TQueue>, IEnumerable, ICollection<TQueue>
    {
        /// <summary>
        /// Pushes an object to the end of the queue.
        /// </summary>
        /// <param name="obj">The object to enqueue.</param>
        void Enqueue(TQueue obj);

        /// <summary>
        /// Pops an object from the start of the queue.
        /// </summary>
        /// <returns>The object that was dequeued.</returns>
        TQueue Dequeue();

        /// <summary>
        /// Peeks the next object on the queue without consuming it.
        /// </summary>
        /// <returns>The next object on the queue.</returns>
        TQueue Peek();
    }
}
