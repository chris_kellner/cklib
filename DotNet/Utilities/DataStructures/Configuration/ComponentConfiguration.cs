﻿#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.DataStructures.Configuration
{
    #region Using List
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.ComponentModel;
    using CKLib.Utilities.DataStructures.Configuration.View;
    #endregion

    /// <summary>
    /// This class is intended to be used as a base class for a configuration settings class for a given component,
    /// it provides helper functions for getting and setting properties, as well as change notifications for them.
    /// </summary>
    [ConfigurationEditorBrowsable(true)]
    public class ComponentConfiguration : INotifyPropertyChanged, IDisposable
    {
        #region Inner Classes
        private class WeakNotifier : System.Windows.IWeakEventListener, IDisposable
        {
            /// <summary>
            /// Initializes a new instance of the WeakNotifier class.
            /// </summary>
            /// <param name="owner"></param>
            /// <param name="propertyName"></param>
            /// <param name="handler"></param>
            private WeakNotifier(ComponentConfiguration owner, string propertyName, PropertyChangedEventHandler handler)
            {
                Handler = handler;
                Owner = owner;
                PropertyName = propertyName;
            }

            /// <summary>
            /// Gets the name of the property being notified.
            /// </summary>
            public string PropertyName
            {
                get;
                private set;
            }

            /// <summary>
            /// Gets the owning configuration.
            /// </summary>
            public ComponentConfiguration Owner
            {
                get;
                private set;
            }

            /// <summary>
            /// Gets the handler to invoke.
            /// </summary>
            public PropertyChangedEventHandler Handler
            {
                get;
                private set;
            }

            /// <summary>
            /// Creates a new instance of the WeakNotifier class.
            /// </summary>
            /// <param name="owner">The ComponentConfiguration that owns this notifier.</param>
            /// <param name="propertyName">The property to be notified of changes on.</param>
            /// <param name="handler">The handler to invoke on change.</param>
            /// <returns>A new instance of the WeakNotifier class.</returns>
            public static WeakNotifier Create(ComponentConfiguration owner, string propertyName, PropertyChangedEventHandler handler)
            {
                if (owner == null)
                {
                    throw new ArgumentNullException("owner");
                }

                if (string.IsNullOrEmpty(propertyName))
                {
                    throw new ArgumentNullException("propertyName");
                }

                if (handler != null)
                {
                    WeakNotifier notify = new WeakNotifier(owner, propertyName, handler);
                    PropertyChangedEventManager.AddListener(owner, notify, propertyName);
                    return notify;
                }

                return null;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="managerType"></param>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            /// <returns></returns>
            public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
            {
                if (Handler != null && managerType == typeof(PropertyChangedEventManager))
                {
                    Handler(sender, (PropertyChangedEventArgs)e);
                    return true;
                }

                return false;
            }

            /// <summary>
            /// Disposes of resources held by this instance.
            /// </summary>
            public void Dispose()
            {
                PropertyChangedEventManager.RemoveListener(Owner, this, PropertyName);
            }
        }
        #endregion
        #region Variables
        /// <summary>
        /// The internal setting group used to maintain the settings.
        /// </summary>
        private SettingGroup m_SettingGroup;

        /// <summary>
        /// The name of the configuration.
        /// </summary>
        private readonly string m_ConfigurationName;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ComponentConfiguration class.
        /// </summary>
        /// <param name="componentName">The name of the component.</param>
        public ComponentConfiguration(string componentName)
        {
            m_ConfigurationName = componentName;
            m_SettingGroup = ConfigurationManager.Current[m_ConfigurationName, true];
            m_SettingGroup.PropertyChanged += m_HostSettings_PropertyChanged;
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets the name of the component that this configuration provides settings for.
        /// </summary>
        public string ComponentName
        {
            get
            {
                return m_ConfigurationName;
            }
        }

        /// <summary>
        /// Gets the setting group where the actual values are stored.
        /// </summary>
        protected SettingGroup RawSettings
        {
            get
            {
                return m_SettingGroup;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Loads configuiration settings from disk if they are not yet loaded.
        /// </summary>
        /// <param name="componentName">The name of the configuration to load.</param>
        public static void LoadConfiguration(string componentName)
        {
            var settings = ConfigurationManager.Current[componentName, false];
            if (settings == null)
            {
                ConfigurationManager.LoadSettingFile(componentName);
            }
        }

        /// <summary>
        /// Gets the value of the specified setting.
        /// </summary>
        /// <typeparam name="TSetting">The type of the setting.</typeparam>
        /// <param name="settingName">The name of the setting to get.</param>
        /// <param name="defaultValue">The value to return if the setting has not yet been set.</param>
        /// <returns>The current value of the specified setting.</returns>
        public TSetting GetValue<TSetting>(string settingName, TSetting defaultValue)
        {
            if (m_SettingGroup != null)
            {
                TSetting value;
                if (m_SettingGroup.TryReadValue(settingName, out value))
                {
                    return value;
                }

                m_SettingGroup.SetValue(settingName, defaultValue);
            }

            return defaultValue;
        }

        /// <summary>
        /// Gets the value of the specified setting.
        /// </summary>
        /// <typeparam name="TSetting">The type of the setting.</typeparam>
        /// <param name="settingName">The name of the setting to get.</param>
        /// <param name="valueConverter">A converter used to changed the value to and from a string.</param>
        /// <param name="defaultValue">The value to return if the setting has not yet been set.</param>
        /// <returns>The current value of the specified setting.</returns>
        public TSetting GetValue<TSetting>(string settingName, TypeConverter valueConverter, TSetting defaultValue)
        {
            string value;

            if (m_SettingGroup != null && valueConverter != null)
            {
                if (m_SettingGroup.TryReadValue(settingName, out value))
                {
                    if (!string.IsNullOrEmpty(value))
                    {
                        return (TSetting)valueConverter.ConvertFromString(value);
                    }
                }

                m_SettingGroup.SetValue(settingName, valueConverter.ConvertToString(defaultValue));
            }

            return defaultValue;
        }

        /// <summary>
        /// Gets the value of the specified setting.
        /// </summary>
        /// <typeparam name="TSetting">The type of the setting.</typeparam>
        /// <param name="settingName">The name of the setting to get.</param>
        /// <param name="defaultValues">The value to return if the setting has not yet been set.</param>
        /// <returns>The current value of the specified setting.</returns>
        public TSetting[] GetValues<TSetting>(string settingName, TSetting[] defaultValues)
        {
            if (m_SettingGroup != null)
            {
                TSetting[] value;
                if (m_SettingGroup.TryReadValues(settingName, out value))
                {
                    return value;
                }

                m_SettingGroup.SetValue(settingName, defaultValues);
            }

            return defaultValues;
        }

        /// <summary>
        /// Gets the value of the specified setting.
        /// </summary>
        /// <typeparam name="TSetting">The type of the setting.</typeparam>
        /// <param name="settingName">The name of the setting to get.</param>
        /// <param name="valueConverter">A converter used to changed the value to and from a string.</param>
        /// <param name="defaultValues">The value to return if the setting has not yet been set.</param>
        /// <returns>The current value of the specified setting.</returns>
        public TSetting[] GetValues<TSetting>(string settingName, TypeConverter valueConverter, TSetting[] defaultValues)
        {
            string[] value;

            if (m_SettingGroup != null && valueConverter != null)
            {
                if (m_SettingGroup.TryReadValues(settingName, out value))
                {
                    if (value != null)
                    {
                        return value.Select(s => (TSetting)valueConverter.ConvertFromString(s)).ToArray();
                    }
                }

                m_SettingGroup.SetValue(settingName, defaultValues.Select(v=>valueConverter.ConvertToString(v)).ToArray());
            }

            return defaultValues;
        }

        /// <summary>
        /// Sets the value of the specified setting.
        /// </summary>
        /// <typeparam name="TSetting">The type of the setting.</typeparam>
        /// <param name="settingName">The name of the setting to set.</param>
        /// <param name="value">The new value to set for the setting.</param>
        public void SetValue<TSetting>(string settingName, TSetting value)
        {
            if (m_SettingGroup != null)
            {
                m_SettingGroup.SetValue(settingName, value);
            }
        }

        /// <summary>
        /// Sets the value of the specified setting.
        /// </summary>
        /// <typeparam name="TSetting">The type of the setting.</typeparam>
        /// <param name="settingName">The name of the setting to set.</param>
        /// <param name="value">The new value to set for the setting.</param>
        /// <param name="valueConverter">A converter used to changed the value to and from a string.</param>
        public void SetValue<TSetting>(string settingName, TSetting value,  TypeConverter valueConverter)
        {
            if (m_SettingGroup != null && valueConverter != null)
            {
                string conversion = valueConverter.ConvertToString(value);
                if (!string.IsNullOrEmpty(settingName))
                {
                    m_SettingGroup.SetValue(settingName, conversion);
                }
            }
        }

        /// <summary>
        /// Sets the value of the specified setting.
        /// </summary>
        /// <typeparam name="TSetting">The type of the setting.</typeparam>
        /// <param name="settingName">The name of the setting to set.</param>
        /// <param name="values">The new value to set for the setting.</param>
        public void SetValue<TSetting>(String settingName, TSetting[] values)
        {
            if (m_SettingGroup != null)
            {
                m_SettingGroup.SetValue(settingName, values);
            }
        }

        /// <summary>
        /// Sets the value of the specified setting.
        /// </summary>
        /// <typeparam name="TSetting">The type of the setting.</typeparam>
        /// <param name="settingName">The name of the setting to set.</param>
        /// <param name="values">The new value to set for the setting.</param>
        /// <param name="valueConverter">A converter used to changed the value to and from a string.</param>
        public void SetValue<TSetting>(string settingName, TSetting[] values, TypeConverter valueConverter)
        {
            if (m_SettingGroup != null && valueConverter != null && values != null)
            {
                string[] conversion = values.Select(value => valueConverter.ConvertToString(value)).ToArray();
                if (!string.IsNullOrEmpty(settingName))
                {
                    m_SettingGroup.SetValue(settingName, conversion);
                }
            }
        }

        /// <summary>
        /// Gets a weak event listener for the specified propertyName.
        /// </summary>
        /// <param name="propertyName">The name of the property to listen (weakly) to.</param>
        /// <param name="handler">The handler to invoke when the specified property changes.</param>
        /// <returns>A weak event listener which may be retracted manually by disposal, or null.</returns>
        /// <exception cref="ArgumentNullException">Throws an exception if the propertyName is not specified.</exception>
        public IDisposable GetWeakNotifier(string propertyName, PropertyChangedEventHandler handler)
        {
            return WeakNotifier.Create(this, propertyName, handler);
        }

        /// <summary>
        /// Saves the user settings to disk.
        /// </summary>
        public virtual void Save()
        {
            ConfigurationManager.Save(m_ConfigurationName);
        }
        #endregion

        #region INotifyPropertyChanged Members
        /// <summary>
        /// Called when a property changes in this configuration.
        /// Invokes the PropertyChanged event.
        /// </summary>
        /// <param name="propertyName">The name of the property that was changed.</param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Called when a setting changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_HostSettings_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(e.PropertyName);
        }

        /// <summary>
        /// Occurs when a property changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region IDisposable Members
        /// <summary>
        /// Releases resources held by this instance.
        /// </summary>
        public virtual void Dispose()
        {
            m_SettingGroup.PropertyChanged -= m_HostSettings_PropertyChanged;
        }
        #endregion
    }
}
