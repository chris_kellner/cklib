#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.DataStructures.Configuration
{
    #region Using List
    using System.ComponentModel;
    using System.Linq;
    using System.Xml.Serialization;
    #endregion
    /// <summary>
    /// Encapsulates a set of settings in the configuration.
    /// </summary>
    public class SettingGroup : INotifyPropertyChanged
    {
        #region Constants
        /// <summary>
        /// Gets the name of the setting group for local server settings.
        /// </summary>
        public const string LOCAL_SERVER_SETTINGS_GROUP = "LocalServerConfiguration";
        #endregion
        #region Variables
        /// <summary>
        /// Container for all settings.
        /// </summary>
        private SynchronizedDictionary<string, Setting> m_Settings;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the SettingGroup class.
        /// </summary>
        public SettingGroup()
        {
            m_Settings = new SynchronizedDictionary<string, Setting>();
            ComponentName = ConfigurationManager.ProductName;
        }

        /// <summary>
        /// Initializes a new instance of the SettingGroup class.
        /// </summary>
        /// <param name="name">Name for the setting group.</param>
        public SettingGroup(string name)
            : this()
        {
            Name = name;
        }

        /// <summary>
        /// Initializes a new instance of the SettingGroup class.
        /// </summary>
        /// <param name="other"></param>
        public SettingGroup(SettingGroup other)
            : this()
        {
            if (other != null)
            {
                Name = other.Name;
                Override(other);
            }
        }
        #endregion
        #region Methods
        /// <summary>
        /// Overrides all properties in this setting group that are found in the other setting group.
        /// </summary>
        /// <param name="other">The setting group containing the higher priority settings.</param>
        internal void Override(SettingGroup other)
        {
            var otherSettings = other.Settings;
            if (Name == other.Name && otherSettings != null)
            {
                ComponentName = other.ComponentName;
                for (int i = 0; i < otherSettings.Length; i++)
                {
                    var cOverride = other.Settings[i];
                    var mySetting = this[cOverride.Name];
                    if (mySetting != null)
                    {
                        mySetting.Override(cOverride);
                    }
                    else
                    {
                        this[cOverride.Name] = cOverride;
                    }
                }
            }
        }

        /// <summary>
        /// Attempts to read the value of the specified setting.
        /// </summary>
        /// <param name="settingName"></param>
        /// <param name="value"></param>
        /// <returns>True if the value was returned, false otherwise.</returns>
        public bool TryReadValue<TResult>(string settingName, out TResult value)
        {
            var setting = this[settingName];
            if (setting != null && setting.Value != null)
            {
                value = setting.Value.Cast<TResult>();
                return true;
            }

            value = default(TResult);
            return false;
        }

        /// <summary>
        /// Attempts to read the values of the specified setting.
        /// </summary>
        /// <param name="settingName"></param>
        /// <param name="value"></param>
        /// <returns>True if the values were returned, false otherwise.</returns>
        public bool TryReadValues<TArray>(string settingName, out TArray[] value)
        {
            var setting = this[settingName];
            if (setting != null && setting.Values != null)
            {
                value = setting.Values.ToArray<TArray>();
                return true;
            }

            value = null;
            return false;
        }

        /// <summary>
        /// Sets the specified setting to the specified value.
        /// </summary>
        /// <param name="settingName">Name of the setting to set.</param>
        /// <param name="value">Value to set it to.</param>
        public void SetValue<TSetting>(string settingName, TSetting value)
        {
            var setting = this[settingName];
            if (setting != null)
            {
                setting.Value = value.ToSettingValue();
            }
            else
            {
                this[settingName] = new Setting(settingName, value);
            }
        }

        /// <summary>
        /// Sets the specified setting to the specified values.
        /// </summary>
        /// <param name="settingName">Name of the setting to set.</param>
        /// <param name="values">Values to set it to.</param>
        public void SetValue<TSetting>(string settingName, TSetting[] values)
        {
            var setting = this[settingName];
            if (setting != null)
            {
                setting.Values = values.ToSettingValueArray();
            }
            else
            {
                this[settingName] = new Setting(settingName, values);
            }
        }

        /// <summary>
        /// Called when a setting changes.
        /// </summary>
        /// <param name="settingName">The name of the setting that changed.</param>
        protected virtual void OnSettingChanged(string settingName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(settingName));
            }
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets the name of the setting group.
        /// </summary>
        [XmlAttribute("name")]
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the name of the component which defines this setting group.
        /// </summary>
        [XmlIgnore]
        public string ComponentName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the settings in this group.
        /// </summary>
        [XmlArray("Settings")]
        public Setting[] Settings
        {
            get
            {
                return m_Settings.Values.ToArray();
            }

            set
            {
                var locker = m_Settings as ILockable;
                locker.Lock(); try
                {
                    m_Settings.Clear();
                    if (value != null)
                    {
                        for (int i = 0; i < value.Length; i++)
                        {
                            this[value[i].Name] = value[i];
                        }
                    }
                }
                finally
                {
                    locker.Unlock();
                }
            }
        }

        /// <summary>
        /// Gets the setting whose name matches the input name.
        /// </summary>
        /// <param name="settingName">The name of the setting to get.</param>
        /// <returns>The setting whose name is the specified name, or null.</returns>
        public Setting this[string settingName]
        {
            get
            {
                if (!string.IsNullOrEmpty(settingName))
                {
                    Setting setting;
                    if (!m_Settings.TryGetValue(settingName, out setting))
                    {
                        setting = new Setting(settingName, null);
                        m_Settings.Add(settingName, setting);
                        setting.PropertyChanged += new PropertyChangedEventHandler(set_PropertyChanged);
                    }

                    return setting;
                }

                return null;
            }

            set
            {
                if (!string.IsNullOrEmpty(settingName))
                {
                    Setting set;
                    if (m_Settings.TryGetValue(settingName, out set))
                    {
                        set.Override(value);
                    }
                    else
                    {
                        set = new Setting(value);
                        if (set.Name != settingName)
                        {
                            set.Name = settingName;
                        }

                        m_Settings[settingName] = set;
                        set.PropertyChanged += new PropertyChangedEventHandler(set_PropertyChanged);
                    }
                }
            }
        }

        /// <summary>
        /// Occurs when the value of values of a setting has changed.
        /// </summary>
        /// <param name="sender">The changed setting.</param>
        /// <param name="e"></param>
        void set_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Setting s = sender as Setting;
            if (s != null)
            {
                OnSettingChanged(s.Name);
            }
        }
        #endregion
        #region INotifyPropertyChanged Members
        /// <summary>
        /// Occurs when a setting changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
    }
}
