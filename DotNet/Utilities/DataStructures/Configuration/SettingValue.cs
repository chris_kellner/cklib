#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.DataStructures.Configuration
{
    #region Using List
    using System;
    using System.Xml;
    using System.Xml.Serialization;
    using CKLib.Utilities.Proxy;
    #endregion
    /// <summary>
    /// Encapsulates the actual value of a configuration setting.
    /// </summary>
    public class SettingValue : IXmlSerializable
    {
        #region Variables
        /// <summary>
        /// Stores the true value.
        /// </summary>
        private object m_Value;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the SettingValue class.
        /// </summary>
        public SettingValue()
        {
        }

        /// <summary>
        /// Initializes a new instance of the SettingValue class.
        /// </summary>
        /// <param name="value"></param>
        public SettingValue(object value)
        {
            m_Value = value;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the value of this SettingValue.
        /// </summary>
        public object Value
        {
            get
            {
                return m_Value;
            }
        }

        /// <summary>
        /// Gets the type of the value.
        /// </summary>
        public Type Type
        {
            get
            {
                if (Value != null)
                {
                    return Value.GetType();
                }

                return null;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Gets the value as the specified TResult type.
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <returns></returns>
        public TResult Cast<TResult>()
        {
            if (Value is TResult)
            {
                return (TResult)Value;
            }

            return default(TResult);
        }
        #endregion

        #region IXmlSerializable Members
        /// <summary>
        /// Always returns null.
        /// </summary>
        /// <returns></returns>
        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        /// <summary>
        /// Reads a Setting value from an xml stream.
        /// </summary>
        /// <param name="reader"></param>
        public void ReadXml(System.Xml.XmlReader reader)
        {
            reader.MoveToElement();
            string sType = reader.GetAttribute("type");
            Type tValue = FactoryHelpers.FindType(sType);
            ReadXml(reader, tValue);
        }

        /// <summary>
        /// Reads a Setting value from an xml stream.
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="tValue">The type of the value being read from the reader.</param>
        public void ReadXml(System.Xml.XmlReader reader, Type tValue)
        {
            reader.MoveToElement();
            if (tValue == null)
            {
                tValue = typeof(string);
            }

            if (reader.MoveToAttribute("value"))
            {
                m_Value = ReadXmlObject(tValue, reader);
            }

            bool isEmpty = reader.IsEmptyElement;
            reader.ReadStartElement();
            if (!isEmpty && m_Value == null)
            {
                m_Value = ReadXmlObject(tValue, reader);
                reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Reads an object out of the xml stream.
        /// </summary>
        /// <param name="tValue"></param>
        /// <param name="reader"></param>
        /// <returns></returns>
        internal static object ReadXmlObject(Type tValue, XmlReader reader)
        {
            var code = Type.GetTypeCode(tValue);
            if (reader.NodeType == XmlNodeType.Element)
            {
                if (reader.IsEmptyElement)
                {
                    return reader.ReadElementContentAs(tValue, default(IXmlNamespaceResolver));
                }
                else
                {
                    XmlSerializer cereal = new XmlSerializer(tValue);
                    return cereal.Deserialize(reader);
                }
            }
            else
            {
                if (code == TypeCode.Boolean)
                {
                    string value = reader.ReadContentAsString();
                    return Convert.ChangeType(value, tValue);
                }
                else if (tValue.IsEnum)
                {
                    string toParse = reader.ReadContentAsString();
                    return Enum.Parse(tValue, toParse);
                }
                else
                {
                    return reader.ReadContentAs(tValue, default(IXmlNamespaceResolver));
                }
            }
        }

        /// <summary>
        /// Writes a Setting value to an xml stream.
        /// </summary>
        /// <param name="writer"></param>
        public void WriteXml(System.Xml.XmlWriter writer)
        {
            if (Value != null)
            {
                WriteXmlType(writer, Type);
                WriteXmlContent(writer, true, Value);
            }
        }

        public static void WriteXmlType(XmlWriter writer, Type valueType)
        {
            TypeCode code = Type.GetTypeCode(valueType);
            if (valueType == typeof(string))
            {
            }
            else if (valueType.Assembly == typeof(string).Assembly)
            {
                writer.WriteAttributeString("type", valueType.FullName);
            }
            else
            {
                writer.WriteAttributeString("type", valueType.AssemblyQualifiedName);
            }
        }

        public static void WriteXmlContent(XmlWriter writer, bool writeTags, object value)
        {
            if (value != null)
            {
                var type = value.GetType();
                TypeCode code = Type.GetTypeCode(type);
                if (code == TypeCode.Object)
                {
                    if (writeTags)
                    {
                        writer.WriteStartElement("Value");
                    }

                    XmlSerializer cerial = new XmlSerializer(type);
                    cerial.Serialize(writer, value);

                    if (writeTags)
                    {
                        writer.WriteEndElement();
                    }
                }
                else
                {
                    writer.WriteStartAttribute("value");
                    writer.WriteValue(value.ToString());
                    writer.WriteEndAttribute();
                }
            }
        }
        #endregion
    }
}
