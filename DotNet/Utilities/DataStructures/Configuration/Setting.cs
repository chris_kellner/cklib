#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.DataStructures.Configuration
{
    #region Using List
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Xml;
    using System.Xml.Serialization;
    using CKLib.Utilities.Proxy;
    #endregion
    /// <summary>
    /// Encapsulates a setting in the configuration.
    /// </summary>
    public class Setting : INotifyPropertyChanged, IXmlSerializable
    {
        #region Constants
        /// <summary>
        /// Gets a key for the server name in the server settings group equal to "ServerName".
        /// </summary>
        public const string SERVER_NAME_SETTING = "ServerName";

        /// <summary>
        /// Gets a key for the server tcp port in the server settings group equal to "Port".
        /// </summary>
        public const string SERVER_TCP_PORT = "Port";

        /// <summary>
        /// Gets a key for the log level in the server settings group equal to "LogLevel".
        /// </summary>
        public const string LOG_LEVEL = "LogLevel";
        #endregion
        #region Variables
        /// <summary>
        /// The current setting value.
        /// </summary>
        private SettingValue m_Value;

        /// <summary>
        /// The current setting values.
        /// </summary>
        private SettingValue[] m_Values;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the Setting class.
        /// </summary>
        public Setting()
        {
            Values = new SettingValue[0];
        }

        /// <summary>
        /// Initializes a new instance of the Setting class from another instance.
        /// </summary>
        /// <param name="other">The setting object to copy the values of.</param>
        public Setting(Setting other)
            : this()
        {
            if (other != null)
            {
                Name = other.Name;
                Override(other);
            }
        }

        /// <summary>
        /// Initializes a new instance of the Setting class.
        /// </summary>
        /// <param name="name">Name of the setting.</param>
        /// <param name="value">Value of the setting.</param>
        public Setting(string name, object value)
            : this()
        {
            Name = name;
            Value = value.ToSettingValue();
        }

        /// <summary>
        /// Initializes a new instance of the Setting class.
        /// </summary>
        /// <param name="name">Name of the setting.</param>
        /// <param name="values">Values for the setting.</param>
        public Setting(string name, object[] values)
        {
            Name = name;
            Values = values.ToSettingValueArray();
        }
        #endregion
        #region Methods
        /// <summary>
        /// Overrides the value of this setting with that of the input setting, provided that the names match.
        /// </summary>
        /// <param name="other">The setting to override with.</param>
        public void Override(Setting other)
        {
            if (Name == other.Name)
            {
                Value = other.Value;
                if (other.Values != null)
                {
                    Values = new SettingValue[other.Values.Length];
                    other.Values.CopyTo(Values, 0);
                }
            }
        }

        /// <summary>
        /// Gets the value as an int.
        /// </summary>
        /// <returns>The value as an int, or 0 if the value cannot be read.</returns>
        public int AsInt32()
        {
            return Value.Cast<int>();
        }

        /// <summary>
        /// Converts the string value into the specified type.
        /// </summary>
        /// <typeparam name="TConvert"></typeparam>
        /// <returns></returns>
        public TConvert ParseAs<TConvert>() where TConvert : IConvertible
        {
            object obj = Convert.ChangeType(Value.Value, typeof(TConvert));
            return (TConvert)obj;
        }

        /// <summary>
        /// Called when a property is changed.
        /// </summary>
        /// <param name="propertyName">The name of the property that changed.</param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets the name of the setting.
        /// </summary>
        [XmlAttribute("name")]
        public string Name
        {
            get;
            set;
        }


        /// <summary>
        /// Gets or sets the string representation of the value of the setting.
        /// </summary>
        [XmlAttribute("value")]
        public SettingValue Value
        {
            get
            {
                return m_Value;
            }

            set
            {
                if (value == null)
                {
                    value = new SettingValue(null);
                }

                m_Value = value;
                OnPropertyChanged("Value");
            }
        }

        /// <summary>
        /// Gets or sets the string representation of the values of the setting.
        /// </summary>
        [XmlArray("Values")]
        public SettingValue[] Values
        {
            get
            {
                return m_Values;
            }

            set
            {
                m_Values = value;
                OnPropertyChanged("Values");
            }
        }
        #endregion
        #region INotifyPropertyChanged Members
        /// <summary>
        /// Occurs when a property changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
        #region IXmlSerializable Members
        /// <summary>
        /// Always returns null.
        /// </summary>
        /// <returns></returns>
        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        /// <summary>
        /// Reads a setting from an xml stream.
        /// </summary>
        /// <param name="reader"></param>
        public void ReadXml(XmlReader reader)
        {
            reader.MoveToContent();
            Name = reader.GetAttribute("name");
            string sType = reader.GetAttribute("type");
            Type tValue = FactoryHelpers.FindType(sType);
            if (tValue == null)
            {
                tValue = typeof(string);
            }

            if (reader.MoveToAttribute("value"))
            {
                m_Value = SettingValue.ReadXmlObject(tValue, reader).ToSettingValue();
            }

            bool isEmpty = reader.IsEmptyElement;
            reader.ReadStartElement();
            if (!isEmpty && m_Value == null)
            {
                List<SettingValue> buffer = new List<SettingValue>();
                bool doRead = true;
                if (reader.Name == "Values")
                {
                    while (doRead)
                    {
                        switch (reader.Name)
                        {
                            case "Values":
                                reader.Read();
                                break;
                            case "Value":
                                SettingValue val = new SettingValue();
                                val.ReadXml(reader, tValue);
                                buffer.Add(val);
                                break;
                            default:
                                doRead = false;
                                break;
                        }
                    }

                    Values = buffer.ToArray();
                }
                else
                {
                    SettingValue val = new SettingValue();
                    val.ReadXml(reader, tValue);
                    Value = val;
                }

                reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Writes a setting to an xml stream.
        /// </summary>
        /// <param name="writer"></param>
        public void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("name", Name);
            if (Value != null)
            {
                Value.WriteXml(writer);
            }

            if (Values != null && Values.Length > 0)
            {
                var type = Values[0].Type;
                SettingValue.WriteXmlType(writer, type);

                writer.WriteStartElement("Values");
                try
                {
                    for (int i = 0; i < Values.Length; i++)
                    {
                        writer.WriteStartElement("Value");
                        SettingValue.WriteXmlContent(writer, false, Values[i].Value);
                        //Values[i].WriteXml(writer);
                        writer.WriteEndElement();
                    }
                }
                finally
                {
                    writer.WriteEndElement();
                }
            }
        }
        #endregion
    }
}
