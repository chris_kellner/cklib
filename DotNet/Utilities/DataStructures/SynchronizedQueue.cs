#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.DataStructures
{
    #region Using List
    using System.Collections.Generic;
    using System.Threading;
    #endregion
    /// <summary>
    /// This class provides a thread-safe queue.
    /// </summary>
    /// <typeparam name="TQueue">The type of object being stored in the queue.</typeparam>
    public class SynchronizedQueue<TQueue> : Queue<TQueue>
    {
        #region Variables
        /// <summary>
        /// Object used for synchronization of the Queue methods.
        /// </summary>
        private readonly object m_QueueLock = new object();
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the SynchronizedQueue class.
        /// </summary>
        public SynchronizedQueue()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the SynchronizedQueue class.
        /// </summary>
        /// <param name="collection">A collection of objects to store in this queue.</param>
        public SynchronizedQueue(IEnumerable<TQueue> collection)
            : base(collection)
        {
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets a thread safe iteration of the queue.
        /// </summary>
        public IEnumerable<TQueue> IterateSafe
        {
            get
            {
                if (InternalCollection != null)
                {
                    TQueue[] set = new TQueue[InternalCollection.Count];
                    InternalCollection.CopyTo(set, 0);
                    for (int i = 0; i < set.Length; i++)
                    {
                        yield return set[i];
                    }
                }
            }
        }
        
        /// <summary>
        /// Gets a value indicating whether this instance is synchronized.
        /// </summary>
        public override bool IsSynchronized
        {
            get
            {
                return true;
            }
        }
        #endregion
        #region Overrides
        /// <summary>
        /// Gets a thread safe iteration of the queue.
        /// </summary>
        /// <returns>A Thread-safe iterator for the queue.</returns>
        public override IEnumerator<TQueue> GetEnumerator()
        {
            return IterateSafe.GetEnumerator();
        }

        /// <summary>
        /// Locks this instance.
        /// </summary>
        protected override void Lock()
        {
            Monitor.Enter(m_QueueLock);
        }

        /// <summary>
        /// Unlocks this instance.
        /// </summary>
        protected override void Unlock()
        {
            Monitor.Exit(m_QueueLock);
        }
        #endregion
    }
}
