#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.DataStructures
{
    #region Using List
    using System;
    using System.Collections;
    using System.Collections.Generic;
    #endregion
    public class Queue<TQueue> : IQueue<TQueue>, ILockable, IDisposable
    {
        #region Variables
        /// <summary>
        /// The internal object used to store the items in the queue.
        /// </summary>
        private LinkedList<TQueue> m_InternalCollection;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the queue class.
        /// </summary>
        public Queue()
        {
            m_InternalCollection = new LinkedList<TQueue>();
        }

        /// <summary>
        /// Initializes a new instance of the queue class.
        /// </summary>
        /// <param name="collection">The queue to copy the objects from.</param>
        public Queue(IEnumerable<TQueue> collection)
        {
            m_InternalCollection = new LinkedList<TQueue>(collection);
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets the internal collection.
        /// </summary>
        protected ICollection<TQueue> InternalCollection
        {
            get
            {
                return m_InternalCollection;
            }
        }
        #endregion
        #region Methods
        /// <summary>
        /// Gets a synchronized instance of this queue.
        /// </summary>
        /// <returns></returns>
        public Queue<TQueue> GetSynchronizedQueue()
        {
            return new SynchronizedQueue<TQueue>(m_InternalCollection);
        }
        #endregion
        #region IQueue<TQueue> Members
        /// <summary>
        /// Enqueues an object into this queue.
        /// </summary>
        /// <param name="obj">The object to enqueue.</param>
        public virtual void Enqueue(TQueue obj)
        {
            Lock(); try
            {
                if (obj != null)
                {
                    m_InternalCollection.AddLast(obj);
                }
            }
            finally
            {
                Unlock();
            }
        }

        /// <summary>
        /// Dequeues the next object from this queue.
        /// </summary>
        /// <returns>The next object in the queue.</returns>
        /// <exception cref="System.InvalidOperationException">Throws an exception if the collection is empty.</exception>
        public virtual TQueue Dequeue()
        {
            Lock(); try
            {
                var node = m_InternalCollection.First;
                if (node != null)
                {
                    try
                    {
                        return node.Value;
                    }
                    finally
                    {
                        m_InternalCollection.RemoveFirst();
                    }
                }

                throw new InvalidOperationException("Collection is empty.");
            }
            finally
            {
                Unlock();
            }
        }

        /// <summary>
        /// Returns the next object from the queue without consuming it.
        /// </summary>
        /// <returns>The next item in the queue.</returns>
        /// <exception cref="System.InvalidOperationException">Throws an exception if the collection is empty.</exception>
        public virtual TQueue Peek()
        {
            Lock(); try
            {
                var node = m_InternalCollection.First;
                if (node != null)
                {
                    return node.Value;
                }

                throw new InvalidOperationException("Collection is empty.");
            }
            finally
            {
                Unlock();
            }
        }

        #endregion

        #region IEnumerable<TQueue> Members
        /// <summary>
        /// Gets an enumerator for this instance.
        /// </summary>
        /// <returns>An enumerator for this queue.</returns>
        public virtual IEnumerator<TQueue> GetEnumerator()
        {
            return m_InternalCollection.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members
        /// <summary>
        /// Gets an enumerator for this instance.
        /// </summary>
        /// <returns>An enumerator for this queue.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region ICollection<TQueue> Members
        /// <summary>
        /// Enqueues an object into this queue.
        /// </summary>
        /// <param name="item">The item to enqueue.</param>
        public virtual void Add(TQueue item)
        {
            Enqueue(item);
        }

        /// <summary>
        /// Clears all items out of the queue.
        /// </summary>
        public virtual void Clear()
        {
            Lock(); try
            {
                m_InternalCollection.Clear();
            }
            finally
            {
                Unlock();
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance contains the specified item.
        /// </summary>
        /// <param name="item">The item to check for.</param>
        /// <returns>True if the item is found, false otherwise.</returns>
        public virtual bool Contains(TQueue item)
        {
            Lock(); try
            {
                return m_InternalCollection.Contains(item);
            }
            finally
            {
                Unlock();
            }
        }

        /// <summary>
        /// Copies the contents of this queue into the specified array.
        /// </summary>
        /// <param name="array">The array to copy to.</param>
        /// <param name="arrayIndex">The start index in the target array to begin the copy.</param>
        public virtual void CopyTo(TQueue[] array, int arrayIndex)
        {
            Lock(); try
            {
                m_InternalCollection.CopyTo(array, arrayIndex);
            }
            finally
            {
                Unlock();
            }
        }

        /// <summary>
        /// Gets the number of items in this queue.
        /// </summary>
        public virtual int Count
        {
            get 
            {
                Lock(); try
                {
                    return m_InternalCollection.Count;
                }
                finally
                {
                    Unlock();
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance of read only.
        /// </summary>
        public virtual bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Removes the item from the queue.
        /// </summary>
        /// <param name="item">The item to remove.</param>
        /// <returns>True if the item was removed, false otherwise.</returns>
        /// <remarks>If the specified item is NOT the next item in the queue, nothing is removed.</remarks>
        public virtual bool Remove(TQueue item)
        {
            if (item != null)
            {
                if (item.Equals(Peek()))
                {
                    Dequeue();
                    return true;
                }
                else
                {
                    return false;
                }
            }

            return false;
        }

        #endregion

        #region ILockable Members
        /// <summary>
        /// Locks this intance.
        /// </summary>
        void ILockable.Lock()
        {
            Lock();
        }

        /// <summary>
        /// Unlocks this instance.
        /// </summary>
        void ILockable.Unlock()
        {
            Unlock();
        }

        /// <summary>
        /// Gets or sets the LockId of the object.
        /// </summary>
        /// <remarks>This property is reserved for future use and should only be implemented as a default property.</remarks>
        int ILockable.LockId { get; set; }

        /// <summary>
        /// Locks this instance when overridden in a derived class.
        /// </summary>
        protected virtual void Lock()
        {
        }

        /// <summary>
        /// Unlocks this instance when overridden in a derived class.
        /// </summary>
        protected virtual void Unlock()
        {
        }

        /// <summary>
        /// Gets a value indicating whether this instance is synchronized.
        /// </summary>
        public virtual bool IsSynchronized
        {
            get { return false; }
        }

        #endregion

        #region IDisposable Members
        /// <summary>
        /// Releases resources held by this instance.
        /// </summary>
        public virtual void Dispose()
        {
            Clear();
        }

        #endregion
    }
}
