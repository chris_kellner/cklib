#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.DataStructures
{
    #region Using List
    using CKLib.Utilities.Messages;
    using CKLib.Utilities.Patterns;
    #endregion
    /// <summary>
    /// Provides an automatic asynchronous message queue system.
    /// </summary>
    public class ProcessQueue : MessageQueue
    {
        #region Variables
        /// <summary>
        /// The asynchronous processing loop.
        /// </summary>
        private AsyncAutoWaitLoop<ProcessQueue> m_ProcessLoop;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ProcessQueue class.
        /// </summary>
        public ProcessQueue()
            : base()
        {
            m_ProcessLoop = new AsyncAutoWaitLoop<ProcessQueue>(ProcessMessages, this);
        }
        #endregion
        #region Methods
        /// <summary>
        /// Starts the processing of the messages.
        /// </summary>
        public void StartMessagePump()
        {
            m_ProcessLoop.RunLoop();
        }

        /// <summary>
        /// Processes all messages on the queue.
        /// </summary>
        /// <param name="myself"></param>
        private void ProcessMessages(ProcessQueue myself)
        {
            ProcessMessages();
        }

        /// <summary>
        /// Stops the processing of the messages.
        /// </summary>
        public void StopMessagePump()
        {
            m_ProcessLoop.Exit();
        }

        /// <summary>
        /// Closes the process queue.
        /// </summary>
        public void Close()
        {
            StopMessagePump();
            Dispose();
        }

        /// <summary>
        /// Sends a message on the process queue.
        /// </summary>
        /// <param name="message">The message pair object containing the message to send.</param>
        /// <returns>True if the message was enqueued, false otherwise.</returns>
        public override bool SendMessage(MessageEnvelope message)
        {
            if (base.SendMessage(message))
            {
                m_ProcessLoop.SignalIteration();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Releases of resources held by this instance.
        /// </summary>
        /// <param name="disposing">True to dispose of managed objects, false otherwise.</param>
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (m_ProcessLoop != null)
            {
                m_ProcessLoop.Dispose();
                m_ProcessLoop = null;
            }
        }
        #endregion
    }
}
