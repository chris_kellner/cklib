#region Usage Rights
/******************************************************************************
Copyright (c) 2012-2013, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.DataStructures
{
    #region Using List
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using CKLib.Utilities.Diagnostics;
    #endregion
    /// <summary>
    /// This class provides synchronization mechanisms for a dictionary structure.
    /// </summary>
    /// <typeparam name="TKey">The type of the key.</typeparam>
    /// <typeparam name="TValue">The type of the value.</typeparam>
    public class SynchronizedDictionary<TKey, TValue> : IDictionary<TKey, TValue>, ILockable, IDictionary, IList<KeyValuePair<TKey, TValue>>
    {
        #region Inner Classes
        /// <summary>
        /// Class used to enumerate the dictionary in a non-generic way.
        /// </summary>
        private class DictEnumerator : IDictionaryEnumerator, IDisposable
        {
            #region Variables
            /// <summary>
            /// The real enumerator.
            /// </summary>
            private IEnumerator<KeyValuePair<TKey, TValue>> m_Enumerator;
            #endregion
            #region Constructors
            /// <summary>
            /// Initializes a new instance of the DictEnumerator class.
            /// </summary>
            /// <param name="collection">The collection of pairs to enumerate.</param>
            public DictEnumerator(IEnumerable<KeyValuePair<TKey, TValue>> collection)
            {
                m_Enumerator = collection.GetEnumerator();
            }
            #endregion
            #region Properties
            /// <summary>
            /// Gets The current dictionary entry.
            /// </summary>
            public DictionaryEntry Entry
            {
                get { return new DictionaryEntry(m_Enumerator.Current.Key, m_Enumerator.Current.Value); }
            }

            /// <summary>
            /// Gets the current key.
            /// </summary>
            public object Key
            {
                get { return Entry.Key; }
            }

            /// <summary>
            /// Gets the current value.
            /// </summary>
            public object Value
            {
                get { return Entry.Value; }
            }

            /// <summary>
            /// Gets the current entry.
            /// </summary>
            public object Current
            {
                get { return Entry; }
            }
            #endregion
            #region Methods
            /// <summary>
            /// Moves to the next element.
            /// </summary>
            /// <returns></returns>
            public bool MoveNext()
            {
                return m_Enumerator.MoveNext();
            }

            /// <summary>
            /// Resets the enumerator.
            /// </summary>
            public void Reset()
            {
                m_Enumerator.Reset();
            }

            /// <summary>
            /// Disposes the enumerator.
            /// </summary>
            public void Dispose()
            {
                if (m_Enumerator != null)
                {
                    m_Enumerator.Dispose();
                }

                m_Enumerator = null;
            }

            /// <summary>
            /// Finalizes an instance of the DictEnumerator class.
            /// </summary>
            ~DictEnumerator()
            {
                Dispose();
            }
            #endregion
        }
        #endregion
        #region Variables
        /// <summary>
        /// Used to store all of the data for this collection.
        /// </summary>
        private Dictionary<TKey, TValue> m_InnerStorage;

        /// <summary>
        /// Lock object used for synchronization.
        /// </summary>
        private readonly object m_Locker = new object();

        /// <summary>
        /// ArrayList used to track the indexes of the keys.
        /// </summary>
        private List<TKey> m_KeyIndexes = new List<TKey>();
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="SynchronizedDictionary&lt;TKey, TValue&gt;"/> class.
        /// </summary>
        public SynchronizedDictionary()
        {
            m_InnerStorage = new Dictionary<TKey, TValue>();
        }
        #endregion
        #region Methods
        #region IDictionary<TKey,TValue> Members

        /// <summary>
        /// Adds an element with the provided key and value to the <see cref="T:System.Collections.Generic.IDictionary`2"/>.
        /// </summary>
        /// <param name="key">The object to use as the key of the element to add.</param>
        /// <param name="value">The object to use as the value of the element to add.</param>
        /// <exception cref="T:System.ArgumentNullException">
        ///   <paramref name="key"/> is null.
        ///   </exception>
        /// <exception cref="T:System.NotSupportedException">
        /// The <see cref="T:System.Collections.Generic.IDictionary`2"/> is read-only.
        ///   </exception>
        public void Add(TKey key, TValue value)
        {
            TValue old;
            AddOrSetInternal(key, value, out old);
        }

        /// <summary>
        /// Gets a value by the input key and adds it if it does not exist.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="valueFactory"></param>
        /// <returns></returns>
        public TValue GetOrAdd(TKey key, Func<TKey, TValue> valueFactory)
        {
            TValue retVal;
            GetOrAddInternal(key, valueFactory, out retVal);
            return retVal;
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.IDictionary`2"/> contains an element with the specified key.
        /// </summary>
        /// <param name="key">The key to locate in the <see cref="T:System.Collections.Generic.IDictionary`2"/>.</param>
        /// <returns>
        /// true if the <see cref="T:System.Collections.Generic.IDictionary`2"/> contains an element with the key; otherwise, false.
        /// </returns>
        /// <exception cref="T:System.ArgumentNullException">
        ///   <paramref name="key"/> is null.
        ///   </exception>
        public bool ContainsKey(TKey key)
        {
            Lock(); try
            {
                return m_InnerStorage.ContainsKey(key);
            }
            finally
            {
                Unlock();
            }
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.IDictionary`2"/> contains an element with the specified value.
        /// </summary>
        /// <param name="value">The value to locate in the <see cref="T:System.Collections.Generic.IDictionary`2"/>.</param>
        /// <returns>
        /// true if the <see cref="T:System.Collections.Generic.IDictionary`2"/> contains an element with the value; otherwise, false.
        /// </returns>
        /// <exception cref="T:System.ArgumentNullException">
        ///   <paramref name="value"/> is null.
        ///   </exception>
        public bool ContainsValue(TValue value)
        {
            Lock(); try
            {
                return m_InnerStorage.ContainsValue(value);
            }
            finally
            {
                Unlock();
            }
        }


        /// <summary>
        /// Gets an <see cref="T:System.Collections.Generic.ICollection`1"/> containing the keys of the <see cref="T:System.Collections.Generic.IDictionary`2"/>.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.Generic.ICollection`1"/> containing the keys of the object that implements <see cref="T:System.Collections.Generic.IDictionary`2"/>.
        ///   </returns>
        public ICollection<TKey> Keys
        {
            get
            {
                Lock(); try
                {
                    return m_KeyIndexes.AsReadOnly();
                    //TKey[] retArr = new TKey[m_InnerStorage.Count];
                    //m_InnerStorage.Keys.CopyTo(retArr, 0);
                    //return retArr;
                }
                finally
                {
                    Unlock();
                }
            }
        }

        /// <summary>
        /// Removes the element with the specified key from the <see cref="T:System.Collections.Generic.IDictionary`2"/>.
        /// </summary>
        /// <param name="key">The key of the element to remove.</param>
        /// <returns>
        /// true if the element is successfully removed; otherwise, false.  This method also returns false if <paramref name="key"/> was not found in the original <see cref="T:System.Collections.Generic.IDictionary`2"/>.
        /// </returns>
        /// <exception cref="T:System.ArgumentNullException">
        ///   <paramref name="key"/> is null.
        ///   </exception>
        ///   
        /// <exception cref="T:System.NotSupportedException">
        /// The <see cref="T:System.Collections.Generic.IDictionary`2"/> is read-only.
        ///   </exception>
        public bool Remove(TKey key)
        {
            TValue val;
            int idx;
            return RemoveInternal(key, out val, out idx);
        }

        /// <summary>
        /// Attempts to remove an item from the collection, if anything was removed it is passed out through
        /// the value parameter.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <returns>True if the value was removed, false otherwise.</returns>
        public bool TryRemove(TKey key, out TValue value)
        {
            int idx;
            return RemoveInternal(key, out value, out idx);
        }

        /// <summary>
        /// Removes items from the dictionary that match the input Predicate pattern.
        /// </summary>
        /// <param name="pattern">The pattern.</param>
        /// <returns>The set of removed items.</returns>
        public IEnumerable<KeyValuePair<TKey, TValue>> TryRemoveWhere(Func<KeyValuePair<TKey, TValue>, bool> pattern)
        {
            if (pattern != null)
            {
                KeyValuePair<TKey, TValue>[] removeSet = this.Where(pattern).ToArray();
                for (int i = 0; i < removeSet.Length; i++)
                {
                    TValue val;
                    int idx;
                    RemoveInternal(removeSet[i].Key, out val, out idx);
                }

                return removeSet;
            }
            else
            {
                var removeSet = m_InnerStorage.ToArray();
                ClearInternal();
                return removeSet;
            }
        }

        /// <summary>
        /// Gets the value associated with the specified key.
        /// </summary>
        /// <param name="key">The key whose value to get.</param>
        /// <param name="value">When this method returns, the value associated with the specified key, if the key is found; otherwise, the default value for the type of the <paramref name="value"/> parameter. This parameter is passed uninitialized.</param>
        /// <returns>
        /// true if the object that implements <see cref="T:System.Collections.Generic.IDictionary`2"/> contains an element with the specified key; otherwise, false.
        /// </returns>
        /// <exception cref="T:System.ArgumentNullException">
        ///   <paramref name="key"/> is null.
        ///   </exception>
        public bool TryGetValue(TKey key, out TValue value)
        {
            Lock(); try
            {
                return m_InnerStorage.TryGetValue(key, out value);
            }
            finally
            {
                Unlock();
            }
        }

        public bool TryAddValue(TKey key, TValue value)
        {
            TValue temp;
            return GetOrAddInternal(key, k => value, out temp);
        }

        /// <summary>
        /// Gets an <see cref="T:System.Collections.Generic.ICollection`1"/> containing the values in the <see cref="T:System.Collections.Generic.IDictionary`2"/>.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.Generic.ICollection`1"/> containing the values in the object that implements <see cref="T:System.Collections.Generic.IDictionary`2"/>.
        ///   </returns>
        public ICollection<TValue> Values
        {
            get
            {
                Lock(); try
                {
                    TValue[] retVal = new TValue[Count];
                    m_InnerStorage.Values.CopyTo(retVal, 0);
                    return retVal;
                }
                finally
                {
                    Unlock();
                }
            }
        }

        /// <summary>
        /// Gets or sets the element with the specified key.
        /// </summary>
        /// <returns>
        /// The element with the specified key.
        ///   </returns>
        ///   
        /// <exception cref="T:System.ArgumentNullException">
        ///   <paramref name="key"/> is null.
        ///   </exception>
        ///   
        /// <exception cref="T:System.Collections.Generic.KeyNotFoundException">
        /// The property is retrieved and <paramref name="key"/> is not found.
        ///   </exception>
        ///   
        /// <exception cref="T:System.NotSupportedException">
        /// The property is set and the <see cref="T:System.Collections.Generic.IDictionary`2"/> is read-only.
        ///   </exception>
        public TValue this[TKey key]
        {
            get
            {
                Lock(); try
                {
                    return m_InnerStorage[key];
                }
                finally
                {
                    Unlock();
                }
            }
            set
            {
                TValue old;
                AddOrSetInternal(key, value, out old);
            }
        }

        #endregion

        #region ICollection<KeyValuePair<TKey,TValue>> Members

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1"/>.
        /// </summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1"/>.</param>
        /// <exception cref="T:System.NotSupportedException">
        /// The <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only.
        ///   </exception>
        void ICollection<KeyValuePair<TKey, TValue>>.Add(KeyValuePair<TKey, TValue> item)
        {
            Add(item.Key, item.Value);
        }

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1"/>.
        /// </summary>
        /// <exception cref="T:System.NotSupportedException">
        /// The <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only.
        ///   </exception>
        public void Clear()
        {
            ClearInternal();
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1"/> contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1"/>.</param>
        /// <returns>
        /// true if <paramref name="item"/> is found in the <see cref="T:System.Collections.Generic.ICollection`1"/>; otherwise, false.
        /// </returns>
        bool ICollection<KeyValuePair<TKey, TValue>>.Contains(KeyValuePair<TKey, TValue> item)
        {
            Lock(); try
            {
                return ContainsKey(item.Key);
            }
            finally
            {
                Unlock();
            }
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.Generic.ICollection`1"/> to an <see cref="T:System.Array"/>, starting at a particular <see cref="T:System.Array"/> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array"/> that is the destination of the elements copied from <see cref="T:System.Collections.Generic.ICollection`1"/>. The <see cref="T:System.Array"/> must have zero-based indexing.</param>
        /// <param name="arrayIndex">The zero-based index in <paramref name="array"/> at which copying begins.</param>
        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            Lock(); try
            {
                var theArray = m_InnerStorage.ToArray();
                theArray.CopyTo(array, arrayIndex);
            }
            finally
            {
                Unlock();
            }
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1"/>.
        /// </summary>
        /// <returns>
        /// The number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1"/>.
        ///   </returns>
        public virtual int Count
        {
            get 
            {
                Lock(); try
                {
                    return m_InnerStorage.Count;
                }
                finally
                {
                    Unlock();
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only.
        /// </summary>
        /// <returns>true if the <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only; otherwise, false.
        ///   </returns>
        public virtual bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1"/>.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1"/>.</param>
        /// <returns>
        /// true if <paramref name="item"/> was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1"/>; otherwise, false. This method also returns false if <paramref name="item"/> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1"/>.
        /// </returns>
        /// <exception cref="T:System.NotSupportedException">
        /// The <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only.
        ///   </exception>
        bool ICollection<KeyValuePair<TKey, TValue>>.Remove(KeyValuePair<TKey, TValue> item)
        {
            throw new NotSupportedException("Use TryRemove instead.");
        }

        #endregion

        #region IEnumerable<KeyValuePair<TKey,TValue>> Members

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1"/> that can be used to iterate through the collection.
        /// </returns>
        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return IterateLocked().GetEnumerator();
        }

        /// <summary>
        /// Iterates the locked.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<KeyValuePair<TKey, TValue>> IterateLocked()
        {
            Lock(); try
            {
                return m_InnerStorage.ToArray();
            }
            finally
            {
                Unlock();
            }
        }
        #endregion

        #region IEnumerable Members

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator"/> object that can be used to iterate through the collection.
        /// </returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region ILockable Members
        /// <summary>
        /// Locks this instance.
        /// </summary>
        void ILockable.Lock()
        {
            Lock();
        }

        /// <summary>
        /// Gets or sets the LockId of the object.
        /// </summary>
        /// <remarks>This property is reserved for future use and should only be implemented as a default property.</remarks>
        int ILockable.LockId { get; set; }

        /// <summary>
        /// Unlocks this instance.
        /// </summary>
        void ILockable.Unlock()
        {
            Unlock();
        }

        /// <summary>
        /// Locks this instance.
        /// </summary>
        protected virtual void Lock()
        {
            Monitor.Enter(m_Locker);
        }

        /// <summary>
        /// Unlocks this instance.
        /// </summary>
        protected virtual void Unlock()
        {
            Monitor.Exit(m_Locker);
        }

        /// <summary>
        /// Gets a value indicating whether this instance is synchronized.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is synchronized; otherwise, <c>false</c>.
        /// </value>
        public bool IsSynchronized
        {
            get { return true; }
        }

        #endregion

        #region Funnel Methods
        /// <summary>
        /// Indexes the input key.
        /// </summary>
        /// <param name="key">The key to add.</param>
        /// <param name="desiredIndex">The desired index of the key, -1 for "Don't Care".</param>
        /// <returns>The index of the key.</returns>
        /// <exception cref="System.ArgumentException">Throws an exception if the key is already in the dictionary.</exception>
        protected int IndexKey(TKey key, int desiredIndex)
        {
            if (m_KeyIndexes.IndexOf(key) < 0)
            {
                if (desiredIndex < 0)
                {
                    m_KeyIndexes.Add(key);
                }
                else
                {
                    m_KeyIndexes.Insert(desiredIndex, key);
                }

                return m_KeyIndexes.IndexOf(key);
            }
            else
            {
                throw new ArgumentException(string.Format("Key {0} is already present in the dictionary.", key));
            }
        }

        /// <summary>
        /// Gets the index of the key which can be used for databinding.
        /// </summary>
        /// <param name="key">The key to retrieve the index of.</param>
        /// <returns>The index of the input key, or -1 if the key is not found.</returns>
        public virtual int IndexOf(TKey key)
        {
            Lock(); try
            {
                return m_KeyIndexes.IndexOf(key);
            }
            finally
            {
                Unlock();
            }
        }

        /// <summary>
        /// Removes an item whose key is the input key from the collection.
        /// </summary>
        /// <param name="key">The key of the item to remove.</param>
        /// <param name="value">The removed value.</param>
        /// <param name="oldIndex">The index that the key previously resided in.</param>
        /// <returns>True if the item was removed.</returns>
        protected virtual bool RemoveInternal(TKey key, out TValue value, out int oldIndex)
        {
            Lock(); try
            {
                if (m_InnerStorage.TryGetValue(key, out value))
                {
                    oldIndex = m_KeyIndexes.IndexOf(key);
                    m_KeyIndexes.Remove(key);
                    return m_InnerStorage.Remove(key);
                }

                oldIndex = -1;
                return false;
            }
            finally
            {
                Unlock();
            }
        }

        /// <summary>
        /// Adds a new or sets an existing key with the input value.
        /// </summary>
        /// <param name="key">The key to set the value of.</param>
        /// <param name="value">The value to set for the key.</param>
        /// <param name="oldValue">The value that was replaced.</param>
        /// <returns>True if an item was added, false if an item was set.</returns>
        protected bool AddOrSetInternal(TKey key, TValue value, out TValue oldValue)
        {
            return AddOrSetInternal(key, value, -1, out oldValue);
        }

        /// <summary>
        /// Adds a new or sets an existing key with the input value.
        /// </summary>
        /// <param name="key">The key to set the value of.</param>
        /// <param name="value">The value to set for the key.</param>
        /// <param name="oldValue">The value that was replaced.</param>
        /// <returns>True if an item was added, false if an item was set.</returns>
        protected virtual bool AddOrSetInternal(TKey key, TValue value, int index, out TValue oldValue)
        {
            Lock(); try
            {
                if (m_InnerStorage.TryGetValue(key, out oldValue))
                {
                    m_InnerStorage[key] = value;
                    return false;
                }
                else
                {
                    IndexKey(key, index);
                    m_InnerStorage.Add(key, value);
                    return true;
                }
            }
            finally
            {
                Unlock();
            }
        }

        /// <summary>
        /// Attempts to retrieve an item by key, adding one using the value factory is necessary.
        /// </summary>
        /// <param name="key">The key to store the item under.</param>
        /// <param name="valueFactory">A function to call to generate the value.</param>
        /// <param name="value">The value that is being returned.</param>
        /// <returns>True if a value was added.</returns>
        protected virtual bool GetOrAddInternal(TKey key, Func<TKey, TValue> valueFactory, out TValue value)
        {
            Lock(); try
            {
                if (!m_InnerStorage.TryGetValue(key, out value))
                {
                    IndexKey(key, -1);
                    value = valueFactory(key);
                    m_InnerStorage.Add(key, value);
                    return true;
                }

                return false;
            }
            finally
            {
                Unlock();
            }
        }

        /// <summary>
        /// Clears all items from the internal storage.
        /// </summary>
        protected virtual bool ClearInternal()
        {
            if (Count > 0)
            {
                Lock(); try
                {
                    m_KeyIndexes.Clear();
                    m_InnerStorage.Clear();
                    return true;
                }
                finally
                {
                    Unlock();
                }
            }

            return false;
        }
        #endregion
        #endregion

        #region IDictionary Members
        /// <summary>
        /// Adds the value to the dictionary with the specified key.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        void IDictionary.Add(object key, object value)
        {
            Add((TKey)key, (TValue)value);
        }

        /// <summary>
        /// Empties the dictionary of all elements.
        /// </summary>
        void IDictionary.Clear()
        {
            Clear();
        }

        /// <summary>
        /// Gets a value indicating whether the dictionary contains the specified key.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        bool IDictionary.Contains(object key)
        {
            return ContainsKey((TKey)key);
        }

        /// <summary>
        /// Gets a non-generic enumerator for the dictionary.
        /// </summary>
        /// <returns></returns>
        IDictionaryEnumerator IDictionary.GetEnumerator()
        {
            return new DictEnumerator(IterateLocked());
        }

        /// <summary>
        /// Returns false.
        /// </summary>
        bool IDictionary.IsFixedSize
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a value indicating whether this dictionary is read only.
        /// </summary>
        bool IDictionary.IsReadOnly
        {
            get { return IsReadOnly; }
        }

        /// <summary>
        /// Gets the collection of keys.
        /// </summary>
        ICollection IDictionary.Keys
        {
            get { return (ICollection)Keys; }
        }

        /// <summary>
        /// Removes an entry from the dictionary by the specified key.
        /// </summary>
        /// <param name="key"></param>
        void IDictionary.Remove(object key)
        {
            Remove((TKey)key);
        }

        /// <summary>
        /// Gets the collection of values.
        /// </summary>
        ICollection IDictionary.Values
        {
            get { return (ICollection)Values; }
        }

        /// <summary>
        /// Gets or sets a value in this dictionary.
        /// </summary>
        /// <param name="key">The key to get or set.</param>
        /// <returns>The value at the specified key.</returns>
        object IDictionary.this[object key]
        {
            get
            {
                return this[(TKey)key];
            }
            set
            {
                this[(TKey)key] = (TValue)value;
            }
        }

        /// <summary>
        /// Copies the elements from this dictionary into the input array starting at the specified index.
        /// </summary>
        /// <param name="array">The array to copy the elements to.</param>
        /// <param name="index">The index in the array to begin the copy.</param>
        void ICollection.CopyTo(Array array, int index)
        {
            this.ToArray().CopyTo(array, index);
        }

        /// <summary>
        /// Gets the number of items in this collection.
        /// </summary>
        int ICollection.Count
        {
            get { return Count; }
        }

        /// <summary>
        /// Gets a value indicating whether this collection is synchronized.
        /// </summary>
        bool ICollection.IsSynchronized
        {
            get { return IsSynchronized; }
        }

        /// <summary>
        /// Gets a synchronization object which can be used to lock the collection via Monitor.Enter/Exit.
        /// </summary>
        public object SyncRoot
        {
            get { return m_Locker; }
        }
        #endregion
        #region IList<KeyValuePair<TKey, TValue> Members
        /// <summary>
        /// Finds the index of the specified key.
        /// </summary>
        /// <param name="item">The item to retrieve.</param>
        /// <returns>The index of the specified pair, or -1 if the item is not found.</returns>
        public int IndexOf(KeyValuePair<TKey, TValue> item)
        {
            return IndexOf(item.Key);
        }

        /// <summary>
        /// Inserts an item into the dictionary with the desired index.
        /// </summary>
        /// <param name="index">The index to use for the item.</param>
        /// <param name="item">The item to insert at the index.</param>
        public void Insert(int index, KeyValuePair<TKey, TValue> item)
        {
            TValue oldValue;
            AddOrSetInternal(item.Key, item.Value, index, out oldValue);
        }

        /// <summary>
        /// Removes the item at the specified index.
        /// </summary>
        /// <param name="index">The index of the item to remove.</param>
        /// <exception cref="System.IndexOutOfRangeException"></exception>
        public void RemoveAt(int index)
        {
            TKey key;
            Lock(); try
            {
                key = m_KeyIndexes[index];
            }
            finally
            {
                Unlock();
            }

            Remove(key);
        }

        /// <summary>
        /// Gets or sets the pair at the specified index.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        /// <exception cref="System.IndexOutOfRangeException"></exception>
        public KeyValuePair<TKey, TValue> this[int index]
        {
            get
            {
                TKey key;
                Lock(); try
                {
                    key = m_KeyIndexes[index];
                }
                finally
                {
                    Unlock();
                }

                return new KeyValuePair<TKey, TValue>(key, this[key]);
            }
            set
            {
                TKey key;
                Lock(); try
                {
                    key = m_KeyIndexes[index];
                }
                finally
                {
                    Unlock();
                }

                if (!key.Equals(value.Key))
                {
                    Remove(key);
                }

                TValue oldValue;
                AddOrSetInternal(value.Key, value.Value, index, out oldValue);
            }
        }
        #endregion
    }
}
