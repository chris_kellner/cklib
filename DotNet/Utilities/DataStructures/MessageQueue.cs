#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.DataStructures
{
    #region Using List
    using System;
    using CKLib.Utilities.Messages;
    using CKLib.Utilities.Patterns;
    using CKLib.Utilities.Connection;
    #endregion
    /// <summary>
    /// Represents a message queue which allows for the registration of callbacks.
    /// </summary>
    public class MessageQueue : IDisposable
    {
        #region Variables
        /// <summary>
        /// The queue used to synchronize the message system.
        /// </summary>
        private SynchronizedQueue<MessageEnvelope> m_InnerQueue;

        /// <summary>
        /// A callback map which is used to identify which callbacks should get invoked when a message is processed.
        /// </summary>
        private CallbackMap<DataMessageCallback> m_Callbacks;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the MessageQueue class.
        /// </summary>
        public MessageQueue()
        {
            m_InnerQueue = new SynchronizedQueue<MessageEnvelope>();
            m_Callbacks = new CallbackMap<DataMessageCallback>();
        }
        #endregion
        #region Methods
        /// <summary>
        /// Sends a message on the message queue.
        /// </summary>
        /// <param name="pair">The message pair describing the message being sent, and who sent it.</param>
        /// <returns>True if the message was enqueued, false otherwise.</returns>
        public virtual bool SendMessage(MessageEnvelope pair)
        {
            if (!IsDisposed && pair != null && pair.Message != null)
            {
                var type = pair.Message.GetType();
                if (m_Callbacks.HasCallbacks(type))
                {
                    m_InnerQueue.Enqueue(pair);
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Pops all messages from the queue and invokes the callbacks associated with each of them.
        /// </summary>
        public virtual void ProcessMessages()
        {
            if (!IsDisposed)
            {
                int count = m_InnerQueue.Count;
                while (count-- > 0)
                {
                    var message = m_InnerQueue.Dequeue();
                    var type = message.Message.GetType();
                    if (m_Callbacks.HasCallbacks(type))
                    {
                        var callbacks = m_Callbacks.GetCallbacks(type);
                        foreach (var callback in callbacks)
                        {
                            var cb = callback;
                            if (cb.Context != null)
                            {
                                cb.Context.Post(p =>
                                {
                                    cb.Callback(p as MessageEnvelope);
                                }, message);

                            }
                            else
                            {
                                cb.Callback(message);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Adds an Asynchronous callback for the specified message type.
        /// </summary>
        /// <param name="messageType">The type of message to add a callback for.</param>
        /// <param name="callback">The callback to invoke.</param>
        public virtual void AddCallback(Type messageType, DataMessageCallback callback)
        {
            if (!IsDisposed)
            {
                m_Callbacks.AddCallback(messageType, callback);
            }
        }

        /// <summary>
        /// Adds a callback for the specified message type.
        /// </summary>
        /// <param name="messageType">The type of message to add a callback for.</param>
        /// <param name="callback">The callback to invoke.</param>
        /// <param name="synchronous">A flag indicating whether to invoke the message synchronously with the caller
        /// of this method call. False to invoke it on the message queue thread.</param>
        public virtual void AddCallback(Type messageType, DataMessageCallback callback, bool synchronous)
        {
            if (!IsDisposed)
            {
                m_Callbacks.AddCallback(messageType, callback, synchronous);
            }
        }

        /// <summary>
        /// Removes a callback for the specified message type.
        /// </summary>
        /// <param name="messageType">The type of message to remove the callback from.</param>
        /// <param name="callback">The callback to remove.</param>
        public virtual void RemoveCallback(Type messageType, DataMessageCallback callback)
        {
            if (!IsDisposed)
            {
                m_Callbacks.RemoveCallback(messageType, callback);
            }
        }
        #endregion
        #region IDisposable Members
        /// <summary>
        /// Gets a value indicating whether this instance is disposed.
        /// </summary>
        public bool IsDisposed
        {
            get;
            private set;
        }

        /// <summary>
        /// Disposes of all resources held by this instance.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        /// <summary>
        /// Disposes of all resources held by this instance.
        /// </summary>
        /// <param name="disposing">True to dispose of managed objects as well.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                IsDisposed = true;
                if (m_Callbacks != null)
                {
                    m_Callbacks.Clear();
                    m_Callbacks = null;
                }

                if (m_InnerQueue != null)
                {
                    m_InnerQueue.Dispose();
                    m_InnerQueue = null;
                }
                GC.SuppressFinalize(this);
            }
        }

        /// <summary>
        /// Finalizes an instance of the MessageQueue class.
        /// </summary>
        ~MessageQueue()
        {
            Dispose(false);
        }

        #endregion
    }
}
