#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.DataStructures
{
    #region Using List
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading;
    using System.Windows.Threading;
    using System.ComponentModel;
    #endregion
    /// <summary>
    /// Provides a synchronized observable collection.
    /// </summary>
    /// <typeparam name="TCollection"></typeparam>
    public class SynchronizedObservableCollection<TCollection> : SynchronizedCollection<TCollection>, INotifyCollectionChanged, INotifyPropertyChanged
    {
        #region Variables
        /// <summary>
        /// The dispatcher which will process the collection changed event.
        /// </summary>
        private Dispatcher m_Dispatcher;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the SynchronizedObservableCollection class.
        /// </summary>
        /// <param name="observingDispatcher">The dispatcher which will process the collection changed event.</param>
        public SynchronizedObservableCollection(Dispatcher observingDispatcher)
            : base()
        {
            m_Dispatcher = observingDispatcher;
        }

        /// <summary>
        /// Initializes a new instance of the SynchronizedObservableCollection class.
        /// </summary>
        /// <param name="observingDispatcher">The dispatcher which will process the collection changed event.</param>
        /// <param name="fill">Data to fill this collection with.</param>
        public SynchronizedObservableCollection(Dispatcher observingDispatcher, IEnumerable<TCollection> fill)
            : this(observingDispatcher)
        {
            if (fill != null)
            {
                foreach (var item in fill)
                {
                    Add(item);
                }
            }
        }
        #endregion
        #region INotifyCollectionChanged Members
        /// <summary>
        /// Occurs when the collection changes.
        /// </summary>
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        /// <summary>
        /// Raises the CollectionChanged event.
        /// </summary>
        /// <param name="e">Event Args describing how the collection changed.</param>
        protected virtual void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (CollectionChanged != null)
            {
                if (m_Dispatcher != null && !m_Dispatcher.CheckAccess())
                {
                    Action<NotifyCollectionChangedEventArgs> cb = new Action<NotifyCollectionChangedEventArgs>(OnCollectionChanged);
                    m_Dispatcher.Invoke(cb, e);
                }
                else
                {
                    CollectionChanged(this, e);
                }
            }
        }
        #endregion
        #region Methods
        #region Overrides
        /// <summary>
        /// Clears the collection.
        /// </summary>
        protected override void ClearItems()
        {
            base.ClearItems();
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            OnPropertyChanged("Count");
            OnPropertyChanged("this[]");
        }

        /// <summary>
        /// Raises the reset notification.
        /// </summary>
        public void RaiseResetEvent()
        {
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            OnPropertyChanged("Count");
            OnPropertyChanged("this[]");
        }

        /// <summary>
        /// Inserts an item into the collection at a specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the collection where the object is to be inserted.</param>
        /// <param name="item">The object to be inserted into the collection.</param>
        protected override void InsertItem(int index, TCollection item)
        {
            base.InsertItem(index, item);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, index));
            OnPropertyChanged("Count");
            OnPropertyChanged("this[]");
        }

        /// <summary>
        /// Removes a range of elements from the collection.
        /// </summary>
        /// <param name="index">The zero-based starting index of the range of elements to remove.</param>
        /// <param name="count">The numbers of elements to remove.</param>
        public virtual void RemoveRange(int index, int count)
        {
            object obj;
            TCollection[] old = new TCollection[count];
            Monitor.Enter(obj = SyncRoot); try
            {
                Items.CopyTo(0, old, index, count);
                Items.RemoveRange(index, count);
            }
            finally
            {
                Monitor.Exit(obj);
            }

            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, old, index));
            OnPropertyChanged("Count");
            OnPropertyChanged("this[]");
        }

        /// <summary>
        /// Removes an item at a specified index from the collection.
        /// </summary>
        /// <param name="index">The zero-based index of the element to be retrieved from the collection.</param>
        protected override void RemoveItem(int index)
        {
            var removedItem = this[index];
            base.RemoveItem(index);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, removedItem, index));
            OnPropertyChanged("Count");
            OnPropertyChanged("this[]");
        }

        /// <summary>
        /// Replaces the item at a specified index with another item.
        /// </summary>
        /// <param name="index">The zero-based index of the object to be replaced.</param>
        /// <param name="item">The object to replace.</param>
        protected override void SetItem(int index, TCollection item)
        {
            var oldItem = this[index];
            base.SetItem(index, item);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, item, oldItem, index));
            OnPropertyChanged("this[]");
        }
        #endregion
        #endregion
        #region Properties
        /// <summary>
        /// Gets the Dispatcher associated with this collection.
        /// </summary>
        public Dispatcher Dispatcher
        {
            get
            {
                return m_Dispatcher;
            }
        }
        #endregion

        #region INotifyPropertyChanged Members
        /// <summary>
        /// Occurs when a property changes.
        /// </summary>
        /// <param name="propertyName">The name of the property that changed.</param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Occurs when a property changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
    }
}
