#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.Proxy
{
    #region Using List
    using System;
    using System.Collections.Generic;
using System.Threading;
using System.Reflection;
using CKLib.Utilities.DataStructures;
    #endregion
    /// <summary>
    /// This class is used to convert the signature of an event whose EventArgs are not defined in the calling assembly.
    /// </summary>
    /// <typeparam name="TRealEventArgs">The type of event args required by the actual event.</typeparam>
    public class ConversionEventHandler<TRealEventArgs> where TRealEventArgs : EventArgs
    {
        #region Constants
        /// <summary>
        /// Gets the type key for this conversion type.
        /// Equal to "ConversionEventHandler".
        /// </summary>
        public const string CONVERSION_KEY = "ConversionEventHandler";
        #endregion

        #region Variables
        /// <summary>
        /// Used to track all previously constructed delegates.
        /// </summary>
        private static SynchronizedDictionary<Delegate, ConversionEventHandler<TRealEventArgs>> m_Mapping;

        /// <summary>
        /// Tracks all of the registered conversion types.
        /// </summary>
        private static SynchronizedDictionary<string, Func<Delegate, ConversionEventHandler<TRealEventArgs>>> m_RegisteredConversions;

        /// <summary>
        /// The wrapped delegate.
        /// </summary>
        private Delegate m_Method;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes static members of the ConversionEventHandler class.
        /// </summary>
        static ConversionEventHandler()
        {
            m_Mapping = new SynchronizedDictionary<Delegate, ConversionEventHandler<TRealEventArgs>>();
            m_RegisteredConversions = new SynchronizedDictionary<string, Func<Delegate, ConversionEventHandler<TRealEventArgs>>>();
            RegisterConversion(CONVERSION_KEY, (d) => new ConversionEventHandler<TRealEventArgs>(d));
        }

        /// <summary>
        /// Initializes a new instance of the EventHandlerClosure class.
        /// </summary>
        /// <param name="yourEvent">The event handler to wrap.</param>
        protected ConversionEventHandler(Delegate yourEvent)
        {
            m_Method = yourEvent;
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets the wrapped handler.
        /// </summary>
        public Delegate Method
        {
            get
            {
                return m_Method;
            }
        }
        #endregion
        #region Methods
        /// <summary>
        /// Creates or returns a ConversionEventHandler for the specified callback.
        /// </summary>
        /// <param name="yourCallback">The callback to invoke when the real event occurs.</param>
        /// <returns>The ConversionEventHandler for the callback.</returns>
        public static ConversionEventHandler<TRealEventArgs> Create(Delegate yourCallback)
        {
            return m_Mapping.GetOrAdd(yourCallback, (cb) =>
            {
                return new ConversionEventHandler<TRealEventArgs>(cb);
            });
        }

        /// <summary>
        /// Creates or returns a ConversionEventHandler for the specified callback.
        /// </summary>
        /// <param name="yourCallback">The callback to invoke when the real event occurs.</param>
        /// <returns>The ConversionEventHandler for the callback.</returns>
        public static ConversionEventHandler<TRealEventArgs> Create(string conversionKey, Delegate yourCallback)
        {
            return m_Mapping.GetOrAdd(yourCallback, (cb) =>
            {
                Func<Delegate, ConversionEventHandler<TRealEventArgs>> valueFactory;
                if (m_RegisteredConversions.TryGetValue(conversionKey, out valueFactory))
                {
                    return valueFactory(cb);
                }

                return null;
            });
        }

        /// <summary>
        /// Creates or returns a ConversionEventHandler for the specified callback.
        /// </summary>
        /// <param name="valueFactory">The function to use when generating a new conversion handler.</param>
        /// <param name="yourCallback">The callback to invoke when the real event occurs.</param>
        /// <returns>The ConversionEventHandler for the callback.</returns>
        public static ConversionEventHandler<TRealEventArgs> Create<TConversion>(Func<Delegate, TConversion> valueFactory, Delegate yourCallback)
            where TConversion : ConversionEventHandler<TRealEventArgs>
        {
            return m_Mapping.GetOrAdd(yourCallback, (cb) =>
            {
                return valueFactory(cb);
            });
        }

        /// <summary>
        /// Returns and removes a ConversionEventHandler for the specified callback.
        /// </summary>
        /// <param name="yourCallback">The callback to remove from invocation when the real event occurs.</param>
        /// <returns>The ConversionEventhandler for the callback.</returns>
        public static ConversionEventHandler<TRealEventArgs> Pop(Delegate yourCallback)
        {
            ConversionEventHandler<TRealEventArgs> value;
            if (m_Mapping.TryRemove(yourCallback, out value))
            {
                return value;
            }

            return new ConversionEventHandler<TRealEventArgs>(null);
        }

        /// <summary>
        /// Registers a derived conversion type as a valid event conversion.
        /// </summary>
        /// <param name="conversionKey">The key to store the conversion as.</param>
        /// <param name="valueFactory">The function to call when constructing a new conversion.</param>
        /// <returns>True if the conversion was registered, false otherwise.</returns>
        protected static bool RegisterConversion(string conversionKey, Func<Delegate, ConversionEventHandler<TRealEventArgs>> valueFactory)
        {
            return m_RegisteredConversions.TryAddValue(conversionKey, valueFactory);
        }

        /// <summary>
        /// Provides a callback that can be registered with the real event, but will pass through the call to the wrapped delegate.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The EventArgs.</param>
        public virtual void EventHandler(object sender, TRealEventArgs args)
        {
            if (m_Method != null && m_Method.Method.GetParameters().Length == 2)
            {
                m_Method.DynamicInvoke(sender, args);
            }
        }

        /// <summary>
        /// Checks for equality.
        /// </summary>
        /// <param name="obj">The other object to compare.</param>
        /// <returns>True if they are equal, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (obj is ConversionEventHandler<TRealEventArgs>)
            {
                return Equals((ConversionEventHandler<TRealEventArgs>)obj);
            }

            return false;
        }

        /// <summary>
        /// Checks for equality of delegates.
        /// </summary>
        /// <param name="other">The handler to compare against this one.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public virtual bool Equals(ConversionEventHandler<TRealEventArgs> other)
        {
            if (other != null)
            {
                if (Method == other.Method)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Gets the hashcode of the method.
        /// </summary>
        /// <returns>A hashcode for the method to callback.</returns>
        public override int GetHashCode()
        {
            return Method.GetHashCode();
        }

        /// <summary>
        /// Checks the equality of two handlers.
        /// </summary>
        /// <param name="left">The handler on the left side of the equals sign.</param>
        /// <param name="right">The handler on the right side of the equals sign.</param>
        /// <returns>True if they are equal, false otherwise.</returns>
        public static bool operator ==(ConversionEventHandler<TRealEventArgs> left, ConversionEventHandler<TRealEventArgs> right)
        {
            if (null == left)
            {
                if (null == right)
                {
                    return true;
                }

                return false;
            }
            else
            {
                if (null == right)
                {
                    return false;
                }

                return left.Equals(right);
            }
        }

        /// <summary>
        /// Checks the inequality of two handlers.
        /// </summary>
        /// <param name="left">The handler on the left side of the not equals sign.</param>
        /// <param name="right">The handler on the right side of the not equals sign.</param>
        /// <returns>True if they are Not equal, false otherwise.</returns>
        public static bool operator !=(ConversionEventHandler<TRealEventArgs> left, ConversionEventHandler<TRealEventArgs> right)
        {
            return !(left == right);
        }
        #endregion
    }

    /// <summary>
    /// This class is used to convert the signature of an event whose EventArgs are not defined in the calling assembly,
    /// this callback will also route the event to the same synchronization context that constructed it.
    /// </summary>
    /// <typeparam name="TIn">The type of event args required by the actual event.</typeparam>
    public class RoutingConversionEventHandler<TIn> : ConversionEventHandler<TIn>  where TIn : EventArgs
    {
        #region  Constants
        /// <summary>
        /// Gets the conversion key for this type.
        /// Equal to "RoutingConversionEventHandler".
        /// </summary>
        public const string CONVERSION_KEY = "RoutingConversionEventHandler";
        #endregion
        #region Variables
        /// <summary>
        /// The context to use when invoking the callback.
        /// </summary>
        private SynchronizationContext m_SynchContext;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes static members of the RoutingConversionEventHandler class.
        /// </summary>
        static RoutingConversionEventHandler()
        {
            RegisterConversion(CONVERSION_KEY, (d) => new RoutingConversionEventHandler<TIn>(d));
        }

        /// <summary>
        /// Initializes a new instance of the RoutingConversionEventHandler class.
        /// </summary>
        /// <param name="yourEvent">The event to wrap.</param>
        protected RoutingConversionEventHandler(Delegate yourEvent)
            : base(yourEvent)
        {
            m_SynchContext = SynchronizationContext.Current;
        }

        /// <summary>
        /// Allows any EventHandler to be implicitly converted to a RoutingConversionEventHandler.
        /// </summary>
        /// <param name="yourEvent">The handler to convert.</param>
        /// <returns>The new handler or null if the conversion is incompatible.</returns>
        public static implicit operator RoutingConversionEventHandler<TIn>(Delegate yourEvent)
        {
            if (yourEvent != null && yourEvent.Method.GetParameters().Length == 2)
            {
                return new RoutingConversionEventHandler<TIn>(yourEvent);
            }

            return null;
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets the SynchronizationContext to use when invoking the registered delegate.
        /// </summary>
        public SynchronizationContext Context
        {
            get
            {
                return m_SynchContext;
            }

            set
            {
                m_SynchContext = value;
            }
        }
        #endregion
        #region Methods
        /// <summary>
        /// Provides a callback that can be registered with the real event, but will pass through the call to the wrapped delegate.
        /// The wrapped delegate will be invoked using the provided synchronization context.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The EventArgs.</param>
        public override void EventHandler(object sender, TIn args)
        {
            if (m_SynchContext != null)
            {
                m_SynchContext.Post((o) =>
                {
                    base.EventHandler(sender, args);
                }, this);
            }
            else
            {
                base.EventHandler(sender, args);
            }
        }
        #endregion
    }

    public static class ConversionExtensions
    {
        public static ConversionEventHandler<TRealEventArgs> ConvertEvent<TRealEventArgs>(this Delegate d, string conversionKey)
            where TRealEventArgs : EventArgs
        {
            return ConversionEventHandler<TRealEventArgs>.Create(conversionKey, d);
        }
    }
}
