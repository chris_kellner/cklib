#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.Proxy.ObjectSharing
{
    #region Using List
    using System;
    using System.Reflection.Emit;
    using CKLib.Utilities.Proxy.Builder;
    #endregion
    /// <summary>
    /// This builder is used to create a shared object both locally and remotely.
    /// </summary>
    public class SharedObjectBuilder : PassThroughTypeConversionBuilder
    {
        #region Methods
        /// <summary>
        /// Builds the constructor described by the input schematic.
        /// </summary>
        /// <param name="proxyFactory">The proxy factory.</param>
        /// <param name="eventArgs">The arguments describing how to build the constructor.</param>
        public override void BuildConstructor(IProxyFactory proxyFactory, IConstructorBuildingSchematic eventArgs)
        {
            var info = eventArgs.CurrentConstructorBuilder;
            if (info != null)
            {
                Type[] parameters = eventArgs.ParameterTypes;

                ProxyBuildingPatterns.CallBasePassthrough(
                    eventArgs.MethodGenerator,
                    info, eventArgs.ParameterTypes);

                // Check to see if it is the special constructor
                if (parameters.Length == 1 && parameters[0].IsAssignableFrom(eventArgs.TypeOfObjectBeingWrapped))
                {
                    eventArgs.MethodGenerator.Emit(OpCodes.Ldarg_0);    // push this
                    eventArgs.MethodGenerator.Emit(OpCodes.Ldarg_1);    // push wrap object
                    eventArgs.MethodGenerator.Emit(OpCodes.Stfld, eventArgs.BuilderStorageMember);    // store wrap object in the field.
                }

                eventArgs.MethodGenerator.Emit(OpCodes.Ret);
            }
        }

        /// <summary>
        /// Builds the method.
        /// </summary>
        /// <param name="proxyFactory">The proxy factory.</param>
        /// <param name="eventArgs">The arguments describing how to build the method.</param>
        public override void BuildMethod(IProxyFactory proxyFactory, IMethodBuildingSchematic eventArgs)
        {
            var baseType = eventArgs.TypeBuilder.BaseType;
            var ilGen = eventArgs.MethodGenerator;
            var parameters = eventArgs.ParameterTypes;
            // Construct object array
            var objectArrayLocal = ilGen.DeclareLocal(typeof(object[]));    // object[] array;
            ilGen.Emit(OpCodes.Ldc_I4, parameters.Length);                  // params.length
            ilGen.Emit(OpCodes.Newarr, typeof(object));                     // = new object[params.length]
            ilGen.Emit(OpCodes.Stloc, objectArrayLocal);

            // Fill with arguments
            for (int i = 0; i < parameters.Length; i++)
            {
                ilGen.Emit(OpCodes.Ldloc, objectArrayLocal);
                ilGen.Emit(OpCodes.Ldc_I4, i);
                ilGen.Emit(OpCodes.Ldarg, i + 1);   // Arg_0 is 'this'
                if (parameters[i].IsValueType)
                {
                    ilGen.Emit(OpCodes.Box, parameters[i]);
                }

                ilGen.Emit(OpCodes.Stelem_Ref);
            }

            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.Emit(OpCodes.Ldstr, eventArgs.CurrentMethodBuilder.Name);
            ilGen.Emit(OpCodes.Ldloc, objectArrayLocal);
            ilGen.Emit(OpCodes.Callvirt, typeof(ISharedObject).GetMethod("CallMethod"));

            if (eventArgs.ReturnType == typeof(void))
            {
                ilGen.Emit(OpCodes.Pop);
            }
            else if (eventArgs.ReturnType.IsValueType)
            {
                // If value type return default(T), else let it return null:
                Label returnDefault = ilGen.DefineLabel();
                LocalBuilder returnVal = ilGen.DeclareLocal(typeof(object));
                ilGen.Emit(OpCodes.Stloc, returnVal);
                ilGen.Emit(OpCodes.Ldloc, returnVal);
                ProxyBuildingPatterns.GotoLabelIfStackPopIsTrue(ilGen, returnDefault);

                var getTypeInfo = typeof(Type).GetMethod("GetTypeFromHandle");
                ilGen.Emit(OpCodes.Ldtoken, eventArgs.ReturnType);
                ilGen.Emit(OpCodes.Call, getTypeInfo);
                var activate = typeof(Activator).GetMethod(
                    "CreateInstance",
                    new Type[] { typeof(Type) });
                ilGen.Emit(OpCodes.Call, activate);
                ilGen.Emit(OpCodes.Stloc, returnVal);
                ilGen.MarkLabel(returnDefault);

                // Unbox the return val:
                ilGen.Emit(OpCodes.Ldloc, returnVal);
                ilGen.Emit(OpCodes.Unbox_Any, eventArgs.ReturnType);
            }
            ilGen.Emit(OpCodes.Ret);
        }

        /// <summary>
        /// Builds the property getter.
        /// </summary>
        /// <param name="proxyFactory">The proxy factory.</param>
        /// <param name="eventArgs">The arguments describing how to build the property getter.</param>
        public override void BuildPropertyGetter(IProxyFactory proxyFactory, IPropertyBuildingSchematic eventArgs)
        {
            BuildMethod(proxyFactory, (IMethodBuildingSchematic)eventArgs);
        }

        /// <summary>
        /// Builds the property setter.
        /// </summary>
        /// <param name="proxyFactory">The proxy factory.</param>
        /// <param name="eventArgs">The arguments describing how to build the property setter.</param>
        public override void BuildPropertySetter(IProxyFactory proxyFactory, IPropertyBuildingSchematic eventArgs)
        {
            BuildMethod(proxyFactory, (IMethodBuildingSchematic)eventArgs);
        }
        #endregion
    }
}
