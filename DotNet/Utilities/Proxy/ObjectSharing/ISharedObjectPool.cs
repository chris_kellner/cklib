#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.Proxy.ObjectSharing
{
    #region Using List
    using System;
    using System.Collections.Generic;
    using CKLib.Utilities.Connection;
    #endregion
    /// <summary>
    /// This interface exposes key functionality of the object pool class.
    /// </summary>
    public interface ISharedObjectPool
    {
        #region Methods
        /// <summary>
        /// Shares the specified object as the specified interface type.
        /// </summary>
        /// <typeparam name="T">The interface to expose the object as.</typeparam>
        /// <param name="objectToShare">The object to be shared.</param>
        /// <returns>An ISharedObject, or null if the object failed to be shared.</returns>
        ISharedObject ShareObject<T>(object objectToShare) where T : class;

        /// <summary>
        /// Unshares an object by share reference.
        /// </summary>
        /// <param name="obj">An object that was previously shared.</param>
        void UnshareObject(object obj);

        /// <summary>
        /// Unshares an object by its wrapping ISharedObject.
        /// </summary>
        /// <param name="objectToUnshare">The ISharedObject to unshare.</param>
        void UnshareObject(ISharedObject objectToUnshare);

        /// <summary>
        /// Gets all objects shared by the specified owner.
        /// </summary>
        /// <param name="ownerId">The connection id of the owner to get the objects for.</param>
        /// <returns>An enumeration of ISharedObjects.</returns>
        IEnumerable<ISharedObject> GetObjectsSharedByOwner(Guid ownerId);

        /// <summary>
        /// Gets a shared object by instance id.
        /// </summary>
        /// <typeparam name="TShare">The type to return the object as.</typeparam>
        /// <param name="instanceId">The instance id of the object.</param>
        /// <returns>The shared object, or null.</returns>
        TShare GetSharedObject<TShare>(Guid instanceId) where TShare : class;

        /// <summary>
        /// Initializes the ISharedObject pool
        /// </summary>
        /// <param name="messenger">The IMessenger that owns this ISharedObjectPool.</param>
        /// <returns>True if the initialization was successful, false otherwise.</returns>
        bool Initialize(IMessenger messenger);

        /// <summary>
        /// Releases all resources held by this instance.
        /// </summary>
        void Release();

        /// <summary>
        /// Disposes of all remote objects owned by the specified connection.
        /// </summary>
        /// <param name="ownerId">The id of the connecton to dispose the objects of.</param>
        void DisposeObjectsForOwner(Guid ownerId);
        #endregion
        #region Events
        /// <summary>
        /// Called when a shared object is received.
        /// </summary>
        event EventHandler<SharedObjectAvailabilityEventArgs> SharedObjectReceived;

        /// <summary>
        /// Called when a shared object is disposed.
        /// </summary>
        event EventHandler<SharedObjectAvailabilityEventArgs> SharedObjectDisposing;
        #endregion
    }
}
