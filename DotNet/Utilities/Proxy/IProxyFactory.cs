#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.Proxy
{
    #region Using List
    using System;
    using System.Reflection;
    using CKLib.Utilities.Proxy.Builder;
    using CKLib.Utilities.Interception;
    #endregion
    /// <summary>
    /// Exposes key functionality of the ProxyFactory.
    /// </summary>
    public interface IProxyFactory : ITypeCacheProvider
    {
        /// <summary>
        /// Gets an already created proxy constructor for the specified type.
        /// </summary>
        /// <param name="typeOfObjectToWrap">Type of object that is wrapped by the proxy we want to create.</param>
        /// <param name="exposedType">Type of proxy we want the constructor for. This can be an interface used to create
        /// a proxy, or the proxy type itself.</param>
        /// <returns>The constructor used to construct a proxy, or null.</returns>
        ConstructorInfo GetProxyConstructor(Type typeOfObjectToWrap, Type exposedType);

        /// <summary>
        /// Releases all resources held by the instance.
        /// </summary>
        void Release();

        Type CreateType<TInterface>(IProxyBuilder builder, Type baseType) where TInterface : class;
        Type CreateType(IProxyBuilder builder, Type baseType, Type interfaceToImplement);

        object CreateProxy(object objToWrap, Type interfaceType, bool throwOnError);
        object CreateProxy(IProxyBuilder builder, object objToWrap, Type baseType, Type[] interfacesToImplement, bool throwOnError);
        TInterface CreateProxy<TInterface>(object objToWrap, bool throwOnError) where TInterface : class;
        TInterface CreateProxy<TInterface>(IProxyBuilder builder, object objToWrap, bool throwOnError) where TInterface : class;
        TInterface CreateProxy<TInterface>(IProxyBuilder builder, object objToWrap, Type inheritedType, bool throwOnError) where TInterface : class;
    }
}
