#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL CHRISTOPHER KELLNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.Proxy.Builder
{
    #region Using List
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Reflection.Emit;
    using System.Reflection;
    #endregion
    public class ProxyBuildingSchematic : IProxyBuildingSchematic, IMethodBuildingSchematic, IPropertyBuildingSchematic, IConstructorBuildingSchematic, IEventBuildingSchematic
    {
        #region Variables
        /// <summary>
        /// Cache of already defined members.
        /// </summary>
        private Dictionary<MemberInfo, Type[]> m_DefinedMembers;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="ProxyBuildingSchematic"/> class.
        /// </summary>
        /// <param name="typeBuilder">The type builder.</param>
        /// <param name="wrappedObjectType">Type of the wrapped object.</param>
        /// <param name="validator">The validator.</param>
        public ProxyBuildingSchematic(TypeBuilder typeBuilder, Type wrappedObjectType, ProxyTypeValidator validator)
        {
            m_DefinedMembers = new Dictionary<MemberInfo, Type[]>();
            TypeBuilder = typeBuilder;
            TypeOfObjectBeingWrapped = wrappedObjectType;
            Validator = validator;
        }
        #endregion
        #region Methods
        /// <summary>
        /// Clears the settings for the next member to be defined.
        /// </summary>
        internal void NextMember()
        {
            InterfaceMemberToBuild = null;
            MethodGenerator = null;
            ((IMethodBuildingSchematic)this).CurrentMethodBuilder = null;
            ((IConstructorBuildingSchematic)this).CurrentConstructorBuilder = null;
            ((IPropertyBuildingSchematic)this).CurrentPropertyBuilder = null;
            ((IEventBuildingSchematic)this).CurrentEventBuilder = null;
        }

        /// <summary>
        /// Adds the defined member.
        /// </summary>
        /// <param name="memberBuilder">The member builder.</param>
        internal void AddDefinedMember(MemberInfo memberBuilder)
        {
            m_DefinedMembers.Add(memberBuilder, Type.EmptyTypes);
            NextMember();
        }

        /// <summary>
        /// Adds the defined member.
        /// </summary>
        /// <param name="memberBuilder">The member builder.</param>
        /// <param name="parameterTypes">The parameter types.</param>
        internal void AddDefinedMember(MemberInfo memberBuilder, Type[] parameterTypes)
        {
            m_DefinedMembers.Add(memberBuilder, parameterTypes);
            NextMember();
        }

        /// <summary>
        /// Determines whether [has member defined] [the specified member].
        /// </summary>
        /// <param name="member">The member.</param>
        /// <returns>
        ///   <c>true</c> if [has member defined] [the specified member]; otherwise, <c>false</c>.
        /// </returns>
        public bool HasMemberDefined(MemberInfo member)
        {
            if (m_DefinedMembers.ContainsKey(member))
            {
                return true;
            }
            else if (member is MethodBase)
            {
                {
                    var method = member as MethodBase;
                    Type[] parameters = FactoryHelpers.GetMethodParameterTypes(method);
                    var matches = (from pair in m_DefinedMembers
                                   where pair.Key.MemberType == MemberTypes.Constructor || pair.Key.MemberType == MemberTypes.Method
                                   where pair.Key.Name == member.Name
                                   where pair.Value.Except(parameters).FirstOrDefault() == null
                                   select pair.Key);
                    if (matches.FirstOrDefault() != null)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets the type builder.
        /// </summary>
        public TypeBuilder TypeBuilder { get; private set; }
        /// <summary>
        /// Gets the type of object being wrapped.
        /// </summary>
        public Type TypeOfObjectBeingWrapped { get; private set; }
        /// <summary>
        /// Gets the validator.
        /// </summary>
        public ProxyTypeValidator Validator { get; private set; }
        /// <summary>
        /// The interface type which this proxy is exposing functionality through.
        /// </summary>
        public Type InterfaceBeingImplemented { get; set; }

        /// <summary>
        /// The field object used to route all function calls through.
        /// </summary>
        public FieldInfo BuilderStorageMember { get; set; }
        /// <summary>
        /// Gets or sets the interface member to build.
        /// </summary>
        /// <value>
        /// The interface member to build.
        /// </value>
        public MemberInfo InterfaceMemberToBuild { get; set; }
        /// <summary>
        /// Gets or sets the method generator.
        /// </summary>
        /// <value>
        /// The method generator.
        /// </value>
        public ILGenerator MethodGenerator { get; set; }
        #endregion
        #region IMethodBuildingSchematic Members

        /// <summary>
        /// Gets or sets the current method builder.
        /// </summary>
        /// <value>
        /// The current method builder.
        /// </value>
        MethodBuilder IMethodBuildingSchematic.CurrentMethodBuilder
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the parameter types of the current method builder.
        /// </summary>
        /// <value>
        /// The parameter types of the current method builder.
        /// </value>
        Type[] IMethodBuildingSchematic.ParameterTypes { get; set; }
        /// <summary>
        /// Gets or sets the return type of the current method builder.
        /// </summary>
        /// <value>
        /// The return type of the current method builder.
        /// </value>
        Type IMethodBuildingSchematic.ReturnType { get; set; }

        #endregion
        #region IPropertyBuildingSchematic Members

        /// <summary>
        /// Gets or sets the current property builder.
        /// </summary>
        /// <value>
        /// The current property builder.
        /// </value>
        PropertyBuilder IPropertyBuildingSchematic.CurrentPropertyBuilder
        {
            get;
            set;
        }

        #endregion
        #region IConstructorBuildingSchematic Members

        /// <summary>
        /// Gets or sets the current constructor builder.
        /// </summary>
        /// <value>
        /// The current constructor builder.
        /// </value>
        ConstructorBuilder IConstructorBuildingSchematic.CurrentConstructorBuilder
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the parameter types of the current method builder.
        /// </summary>
        /// <value>
        /// The parameter types of the current method builder.
        /// </value>
        Type[] IConstructorBuildingSchematic.ParameterTypes { get; set; }

        #endregion
        #region IEventBuildingSchematic Members

        /// <summary>
        /// Gets or sets the current event builder.
        /// </summary>
        /// <value>
        /// The current event builder.
        /// </value>
        EventBuilder IEventBuildingSchematic.CurrentEventBuilder
        {
            get;
            set;
        }

        #endregion
    }
}
