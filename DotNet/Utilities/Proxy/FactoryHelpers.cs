#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.Proxy
{
    #region Using List
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Reflection.Emit;
    #endregion
    /// <summary>
    /// This class provides functionality which aides in the creation of type proxies.
    /// </summary>
    public static class FactoryHelpers
    {
        #region Constants
        /// <summary>
        /// Defines a method that should be implemented explicitly.
        /// </summary>
        public const MethodAttributes ExplicitImplementation =
            MethodAttributes.Private |
            MethodAttributes.Virtual |
            MethodAttributes.Final |
            MethodAttributes.HideBySig |
            MethodAttributes.NewSlot;

        /// <summary>
        /// Defines a method that should be implemented implicitly.
        /// </summary>
        public const MethodAttributes ImplicitImplementation =
            MethodAttributes.Public |
            MethodAttributes.Virtual |
            MethodAttributes.HideBySig;

        /// <summary>
        /// Defines a constructor.
        /// </summary>
        public const MethodAttributes ConstructorImplementation =
            MethodAttributes.Public |
            MethodAttributes.HideBySig |
            MethodAttributes.SpecialName |
            MethodAttributes.RTSpecialName;

        /// <summary>
        /// Defines a 'special name' method like a 'get' or 'set' on a property.
        /// </summary>
        public const MethodAttributes SpecialNameMethodImplementation =
            MethodAttributes.HideBySig |
            MethodAttributes.Public |
            MethodAttributes.SpecialName |
            MethodAttributes.Virtual;
        #endregion
        #region Variables
        /// <summary>
        /// The assembly builder for this assembly.
        /// </summary>
        static AssemblyBuilder m_assemblyBuilder;

        /// <summary>
        /// The module builder for this assembly.
        /// </summary>
        static ModuleBuilder m_moduleBuider;
        #endregion
        #region Methods
        /// <summary>
        /// Gets an array of <c>Type</c> objects corresponding to the parameters required by the specified method.
        /// </summary>
        /// <param name="method">The method.</param>
        /// <returns>An array of types that correspond to the parameters that the method is expecting.</returns>
        public static Type[] GetMethodParameterTypes(MethodBase method)
        {
            return method.GetParameters().Select(p => p.ParameterType).ToArray();
        }

        /// <summary>
        /// Gets all interfaces defined through the type hierarchy for all of the interfaces in the specified enumeration.
        /// </summary>
        /// <param name="types">The types to expand the inheritance of.</param>
        /// <returns>All interfaces defined through inheritance.</returns>
        public static IEnumerable<Type> ExpandTypeInheritance(IEnumerable<Type> types)
        {
            HashSet<Type> buffer = new HashSet<Type>(types);
            foreach (var type in types)
            {
                if (type.IsInterface)
                {
                    buffer.UnionWith(ExpandTypeInheritance(type));
                }
            }

            return buffer;
        }

        /// <summary>
        /// Expands the interface inheritance tree for the input interface type.
        /// </summary>
        /// <param name="input">The input interface type.</param>
        /// <returns>Each of the interface types up the inheritance tree.</returns>
        public static IEnumerable<Type> ExpandTypeInheritance(Type input)
        {
            if (input.IsInterface)
            {
                yield return input;
            }

            foreach (var type in input.GetInterfaces())
            {
                foreach (var subType in ExpandTypeInheritance(type))
                {
                    yield return subType;
                }
            }
        }

        /// <summary>
        /// Finds the type within the loaded assemblies whose name matches the specified
        /// typeName. The most efficient input for this method (besides <c>null</c>) is the
        /// AssemblyQualified type name.
        /// </summary>
        /// <param name="typeName">Name of the type.</param>
        /// <returns>The type whose name matches the specified type, or null if one could not be found.</returns>
        public static Type FindType(string typeName)
        {
            if (!string.IsNullOrEmpty(typeName))
            {
                Type ret = Type.GetType(typeName);
                if (ret == null)
                {
                    string[] parts = typeName.Split(',');
                    if (parts.Length > 1)
                    {
                        typeName = parts[0];
                        string asmName = parts[1];
                        if (!string.IsNullOrEmpty(asmName))
                        {
                            var asms = AppDomain.CurrentDomain.GetAssemblies().Where(a => a.GetName().Name == asmName);
                            foreach (var asm in asms)
                            {
                                ret = asm.GetType(typeName, false, false);
                                if (ret != null)
                                {
                                    break;
                                }
                            }
                        }
                    }

                    if (ret == null)
                    {
                        var asms = AppDomain.CurrentDomain.GetAssemblies();
                        for (int i = 0; i < asms.Length; i++)
                        {
                            try
                            {
                                var cAsm = asms[i];
                                ret = cAsm.GetType(typeName, false, false);
                                if (ret != null)
                                {
                                    break;
                                }
                            }
                            catch (TypeLoadException)
                            {
                            }
                        }
                    }
                }

                return ret;
            }

            return null;
        }

        /// <summary>
        /// Casts the specified source delegate to the new type if the signature is compatible.
        /// </summary>
        /// <typeparam name="TDelegate">The type to cast the delegate to.</typeparam>
        /// <param name="source">The new type of delegate to be returned.</param>
        /// <returns>A new delegate which points to the same method and target as the specified source delegate.</returns>
        /// <exception cref="System.InvalidCastException">Throws an exception if the specified type is incompatible.</exception>
        /// <exception cref="System.ArgumentException"></exception>
        /// <exception cref="System.MissingMethodException"></exception>
        public static TDelegate CastDelegate<TDelegate>(Delegate source)
        {
            return (TDelegate)(CastDelegate(typeof(TDelegate), source));
        }

        /// <summary>
        /// Casts the specified source delegate to the new type if the signature is compatible.
        /// </summary>
        /// <param name="newType">The type to cast the delegate to.</param>
        /// <param name="source">The new type of delegate to be returned.</param>
        /// <returns>A new delegate which points to the same method and target as the specified source delegate.</returns>
        /// <exception cref="System.InvalidCastException">Throws an exception if the specified type is incompatible.</exception>
        /// <exception cref="System.ArgumentException"></exception>
        /// <exception cref="System.MissingMethodException"></exception>
        public static object CastDelegate(Type newType, Delegate source)
        {
            if (!typeof(Delegate).IsAssignableFrom(newType))
            {
                throw new InvalidCastException("newType must be inherited from type: System.Delegate");
            }

            return Delegate.CreateDelegate(newType, source.Target, source.Method, true);
        }

        /// <summary>
        /// Casts the specified source delegate to the new type if the signature is compatible.
        /// </summary>
        /// <typeparam name="TDelegate">The type to cast the delegate to.</typeparam>
        /// <param name="source">The new type of delegate to be returned.</param>
        /// <returns>A new delegate which points to the same method and target as the specified source delegate.</returns>
        /// <exception cref="System.InvalidCastException">Throws an exception if the specified type is incompatible.</exception>
        /// <exception cref="System.ArgumentException"></exception>
        /// <exception cref="System.MissingMethodException"></exception>
        public static TDelegate TryCastDelegate<TDelegate>(Delegate source)
        {
            return (TDelegate)(TryCastDelegate(typeof(TDelegate), source));
        }

        /// <summary>
        /// Casts the specified source delegate to the new type if the signature is compatible.
        /// </summary>
        /// <param name="newType">The type to cast the delegate to.</param>
        /// <param name="source">The new type of delegate to be returned.</param>
        /// <returns>A new delegate which points to the same method and target as the specified source delegate.</returns>
        /// <exception cref="System.InvalidCastException">Throws an exception if the specified type is incompatible.</exception>
        /// <exception cref="System.ArgumentException"></exception>
        /// <exception cref="System.MissingMethodException"></exception>
        public static object TryCastDelegate(Type newType, Delegate source)
        {
            if (!typeof(Delegate).IsAssignableFrom(newType))
            {
                throw new InvalidCastException("newType must be inherited from type: System.Delegate");
            }

            return Delegate.CreateDelegate(newType, source.Target, source.Method, false);
        }

        /// <summary>
        /// Exports the dynamic assembly to the directory where the CKLib.Utilities assembly lies.
        /// </summary>
        /// <remarks>Only performs this application in DEBUG mode.</remarks>
        [Conditional("DEBUG")]
        public static void ExportCommunicationModule(ref string location)
        {
            m_assemblyBuilder.Save("DynamicTypes.dll");
            var asm = typeof(FactoryHelpers).Assembly;
            string directory = Path.GetDirectoryName(asm.Location);
            location = Path.Combine(directory, "DynamicTypes.dll");
        }

        #region Extensions
        /// <summary>
        /// Gets all public methods all the way up the inheritance tree.
        /// </summary>
        /// <param name="onType">The type to get the methods for.</param>
        /// <returns>An enumeration of methods available on the specified type.</returns>
        public static IEnumerable<MethodInfo> GetAllMethods(this Type onType)
        {
            if (onType != null)
            {
                MethodInfo[] myTypes = onType.GetMethods();
                for (int i = 0; i < myTypes.Length; i++)
                {
                    yield return myTypes[i];
                }

                if (onType.IsInterface)
                {
                    Type[] interfaces = onType.GetInterfaces();
                    for (int i = 0; i < interfaces.Length; i++)
                    {
                        foreach (var method in interfaces[i].GetAllMethods())
                        {
                            yield return method;
                        }
                    }
                }
                else if (onType.BaseType != null)
                {
                    foreach (var method in onType.BaseType.GetAllMethods())
                    {
                        yield return method;
                    }
                }
            }
        }

        /// <summary>
        /// Gets an enumeration of events defined on the specified type all the way up the inheritance tree.
        /// </summary>
        /// <param name="onType">The type to get the events for.</param>
        /// <returns>An enumeration of all events available on the specified type.</returns>
        public static IEnumerable<EventInfo> GetAllEvents(this Type onType)
        {
            if (onType != null)
            {
                EventInfo[] myTypes = onType.GetEvents();
                for (int i = 0; i < myTypes.Length; i++)
                {
                    yield return myTypes[i];
                }

                if (onType.IsInterface)
                {
                    Type[] interfaces = onType.GetInterfaces();
                    for (int i = 0; i < interfaces.Length; i++)
                    {
                        foreach (var method in interfaces[i].GetAllEvents())
                        {
                            yield return method;
                        }
                    }
                }
                else if (onType.BaseType != null)
                {
                    foreach (var method in onType.BaseType.GetAllEvents())
                    {
                        yield return method;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the event whose name matches the specified name, and searches up the inheritance tree
        /// until it is found.
        /// </summary>
        /// <param name="onType">The type to search for the event.</param>
        /// <param name="name">The name of the event to search for.</param>
        /// <returns>The event or null.</returns>
        public static EventInfo GetEventRecursive(this Type onType, string name)
        {
            if (onType != null)
            {
                EventInfo info = onType.GetEvent(name);
                if (info != null)
                {
                    return info;
                }

                else if (onType.IsInterface)
                {
                    Type[] interfaces = onType.GetInterfaces();
                    for (int i = 0; i < interfaces.Length; i++)
                    {
                        info = GetEventRecursive(interfaces[i], name);
                        if (info != null)
                        {
                            return info;
                        }
                    }
                }
                else
                {
                    return GetEventRecursive(onType.BaseType, name);
                }
            }

            return null;
        }
        #endregion

        /// <summary>
        /// Gets the Type of EventArgs an EventInfo's Eventhandler is expecting
        /// based on the signiture of the <c>Invoke</c> method of said EventHandler.
        /// </summary>
        /// <param name="eventInfo">The EventInfo to get the EventArg type from.</param>
        /// <returns>The Type or null.</returns>
        internal static Type GetEventArgsType(EventInfo eventInfo)
        {
            Type tOut = null;
            Type[] baseTypes = eventInfo.EventHandlerType.GetGenericArguments();
            if (baseTypes.Length > 0)
            {
                tOut = baseTypes[0];
            }
            else
            {
                var method = eventInfo.EventHandlerType.GetMethod("Invoke");
                var parameters = method.GetParameters();
                if (parameters != null && parameters.Length > 1)
                {
                    var eventInfoParam = parameters[1].ParameterType;
                    tOut = eventInfoParam;
                }
            }

            return tOut;
        }

        /// <summary>
        /// Defines an assembly which will be used for the proxy creation.
        /// </summary>
        /// <returns>The assembly which is used for proxy creation.</returns>
        private static AssemblyBuilder GetAssemblyBuilder()
        {
            if (m_assemblyBuilder == null)
            {
                AssemblyName name = new AssemblyName("DynamicTypeAssembly");
                var asm = typeof(FactoryHelpers).Assembly;
                string directory = Path.GetDirectoryName(asm.Location);
                m_assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(name, AssemblyBuilderAccess.RunAndSave, directory);
            }

            return m_assemblyBuilder;
        }

        /// <summary>
        /// Defines a module which will be used for proxy creation.
        /// </summary>
        /// <returns>The module which will be used for proxy creation.</returns>
        private static ModuleBuilder GetModuleBuilder()
        {
            if (m_moduleBuider == null)
            {
                var asmBuilder = GetAssemblyBuilder();
                m_moduleBuider = asmBuilder.DefineDynamicModule("DynamicTypeModule", "DynamicTypes.dll", true);
            }

            return m_moduleBuider;
        }

        /// <summary>
        /// Gets a type builder for a Type whose name will be the specifed typeName, and that inherits from
        /// the specifed baseType and implements the specified interfaces.
        /// </summary>
        /// <param name="typeName">Name of the type to create.</param>
        /// <param name="baseType">The Type that the new Type should inherit from.</param>
        /// <param name="interfaces">The interfaces that the type should implement.</param>
        /// <returns>A new TypeBuilder or null.</returns>
        internal static TypeBuilder GetTypeBuilder(string typeName, Type baseType, Type[] interfaces)
        {
            var modBuilder = GetModuleBuilder();
            try
            {
                var typeBuilder = modBuilder.DefineType(typeName, TypeAttributes.Public | TypeAttributes.Class, baseType, interfaces);
                return typeBuilder;
            }
            catch (ArgumentException)
            {
                // Either a type with that name already exists or a name was not specified.
                return null;
            }
        }

        /// <summary>
        /// Defines a new property on the specified TypeBuilder.
        /// </summary>
        /// <param name="typeBuilder">The type builder to define the property on.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="propertyType">Return type of the property.</param>
        /// <returns>A PropertyBuilder object or null.</returns>
        internal static PropertyBuilder GetPropertyBuilder(TypeBuilder typeBuilder, string propertyName, Type propertyType)
        {
            if (typeBuilder != null)
            {
                var builder = typeBuilder.DefineProperty(propertyName, PropertyAttributes.HasDefault, propertyType, null);
                return builder;
            }

            return null;
        }

        /// <summary>
        /// Defines a special method for a property or event.
        /// </summary>
        /// <param name="typeBuilder">The type builder to define the method on.</param>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="returnType">Return type of the method.</param>
        /// <param name="parameterTypes">The parameter types to require when calling the method.</param>
        /// <returns>A MethodBuilder object or null.</returns>
        internal static MethodBuilder GetPropertyMethodBuilder(TypeBuilder typeBuilder, string methodName, Type returnType, Type[] parameterTypes)
        {
            if (typeBuilder != null)
            {
                var builder = typeBuilder.DefineMethod(methodName, SpecialNameMethodImplementation, returnType, parameterTypes);
                return builder;
            }

            return null;
        }

        /// <summary>
        /// Defines a special method for a property or event.
        /// </summary>
        /// <param name="typeBuilder">The type builder to define the method on.</param>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="returnType">Return type of the method.</param>
        /// <param name="parameterTypes">The parameter types to require when calling the method.</param>
        /// <returns>A MethodBuilder object or null.</returns>
        internal static MethodBuilder GetPropertyMethodBuilder(TypeBuilder typeBuilder, PropertyBuilder builder, bool getter)
        {
            if (getter)
            {
                return GetPropertyMethodBuilder(typeBuilder, "get_" + builder.Name, builder.PropertyType, Type.EmptyTypes);
            }
            else
            {
                return GetPropertyMethodBuilder(typeBuilder, "set_" + builder.Name, null, new Type[] { builder.PropertyType });
            }
        }

        /// <summary>
        /// Defines an event on the specified TypeBuilder.
        /// </summary>
        /// <param name="typeBuilder">The type builder.</param>
        /// <param name="eventName">Name of the event.</param>
        /// <param name="eventDelegateType">Type of the callback which will handle the event.</param>
        /// <returns>An EventBuilder object or null.</returns>
        internal static EventBuilder GetEventBuilder(TypeBuilder typeBuilder, string eventName, Type eventDelegateType)
        {
            if (typeBuilder != null)
            {
                var builder = typeBuilder.DefineEvent(eventName, EventAttributes.None, eventDelegateType);
                return builder;
            }

            return null;
        }

        /// <summary>
        /// Defines a method on the specified TypeBuilder.
        /// </summary>
        /// <param name="typeBuilder">The type builder.</param>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="returnType">Return type for the method.</param>
        /// <param name="parameterTypes">The parameter types to require when calling the method.</param>
        /// <returns>A MethodBuilder object or null.</returns>
        internal static MethodBuilder GetMethodBuilder(TypeBuilder typeBuilder, string methodName, Type returnType, Type[] parameterTypes)
        {
            if (typeBuilder != null)
            {
                var builder = typeBuilder.DefineMethod(methodName, ImplicitImplementation, returnType, parameterTypes);
                return builder;
            }

            return null;
        }

        internal static DynamicMethod DefineDynamicMethod(string methodName, Type returnType, Type[] parameterTypes)
        {
            return new DynamicMethod(methodName,
                MethodAttributes.Public|MethodAttributes.Static,
                CallingConventions.Standard, 
                returnType, parameterTypes, GetModuleBuilder(), true);
        }
        #endregion
    }
}
