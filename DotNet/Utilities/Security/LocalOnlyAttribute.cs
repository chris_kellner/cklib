#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL CHRISTOPHER KELLNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.Security
{
    #region Using List
    using System;
    using System.Reflection;
    using CKLib.Utilities.Connection;
    using CKLib.Utilities.DataStructures;
    #endregion
    /// <summary>
    /// This attribute is used to tag certain features as being only available for the localhost connection.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class|AttributeTargets.Method)]
    public class LocalOnlyAttribute : Attribute
    {
        #region Variables
        /// <summary>
        /// A cache of all requests that have been made for security checks.
        /// </summary>
        private static SynchronizedDictionary<object, bool> m_AccessDictionary;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes static members of the LocalOnlyAttribute class.
        /// </summary>
        static LocalOnlyAttribute()
        {
            m_AccessDictionary = new SynchronizedDictionary<object, bool>();
        }

        /// <summary>
        /// Initializes a new instance of the LocalOnlyAttribute.
        /// </summary>
        public LocalOnlyAttribute()
            : base()
        {
            Console.WriteLine(this.TypeId);
        }
        #endregion
        #region Methods
        /// <summary>
        /// Gets a value indicating whether the specified connection has access rights to the specified type t.
        /// </summary>
        /// <param name="t">The type to check the connection's access rights for.</param>
        /// <param name="messenger">The messenger used to get the credentials of the connection.</param>
        /// <param name="connectionId">The connection to check the access rights of.</param>
        /// <returns>True if the connection has rights for the type, false otherwise.</returns>
        public static bool CheckAccess(Type t, IMessenger messenger, Guid connectionId)
        {
            if (messenger != null && connectionId != Guid.Empty)
            {
                bool localOnly = m_AccessDictionary.GetOrAdd(t, (p) =>
                {
                    var attrs = t.GetCustomAttributes(typeof(LocalOnlyAttribute), true);
                    return attrs != null && attrs.Length > 0;
                });

                return !localOnly || messenger.IsConnectionLocal(connectionId);
            }

            return false;
        }

        /// <summary>
        /// Gets a value indicating whether the specified connection has access rights to the specified method, and its declaring type.
        /// </summary>
        /// <param name="method">The method to check the connection's access rights for.</param>
        /// <param name="messenger">The messenger used to get the credentials of the connection.</param>
        /// <param name="connectionId">The connection to check the access rights of.</param>
        /// <returns>True if the connection has rights for the type, false otherwise.</returns>
        public static bool CheckAccess(MethodInfo method, IMessenger messenger, Guid connectionId)
        {
            if (messenger != null && connectionId != Guid.Empty)
            {
                bool localOnly = m_AccessDictionary.GetOrAdd(method, (p) =>
                {
                    var attrs = method.GetCustomAttributes(typeof(LocalOnlyAttribute), true);
                    return attrs != null && attrs.Length > 0;
                });

                return (!localOnly || messenger.IsConnectionLocal(connectionId))
                    && CheckAccess(method.DeclaringType, messenger, connectionId);
            }

            return false;
        }
        #endregion
    }

}
