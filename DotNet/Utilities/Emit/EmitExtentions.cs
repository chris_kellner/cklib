﻿using CKLib.Utilities.Emit.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

namespace CKLib.Utilities.Emit
{
    public static class EmitExtentions
    {
        public static ILBuilder GetILBuilder(this MethodBuilder method, Type[] parameterTypes)
        {
            return new ILBuilder(new MethodDefinition(method, parameterTypes));
        }

        public static ILBuilder GetILBuilder(this DynamicMethod method)
        {
            return new ILBuilder(new MethodDefinition(method));
        }

        public static ILBuilder GetILBuilder(this ConstructorBuilder method, Type[] parameterTypes)
        {
            return new ILBuilder(new MethodDefinition(method, parameterTypes));
        }

        public static ICallParameter[] GetCallParameters(this MethodInfo currentMethod)
        {
            ICallParameter[] parameters = new ICallParameter[currentMethod.GetParameters().Length];
            for (int i = 0; i < parameters.Length; i++)
            {
                int idx = i;
                if (!currentMethod.IsStatic)
                {
                    idx++;
                }
                parameters[i] = new ArgumentCallParameter() { Index = idx };
            }

            return parameters;
        }

        public static ICallParameter[] GetCallParameters(this MethodDefinition currentMethod)
        {
            ICallParameter[] parameters = new ICallParameter[currentMethod.GetParameterTypes().Length];
            for (int i = 0; i < parameters.Length; i++)
            {
                int idx = i;
                if (!currentMethod.Method.IsStatic)
                {
                    idx++;
                }
                parameters[i] = new ArgumentCallParameter(currentMethod, idx);
            }

            return parameters;
        }

        public static ICallParameter[] GetCallParameters(this MethodDefinition currentMethod, EventHandler<LoadEventArgs> parameterLoadedHandler)
        {
            ICallParameter[] parameters = currentMethod.GetCallParameters();
            if (parameterLoadedHandler != null)
            {
                for (int i = 0; i < parameters.Length; i++)
                {
                    parameters[i].Loaded += parameterLoadedHandler;
                }
            }

            return parameters;
        }

        public static ICallParameter WithCast(this ICallParameter self, ILBuilder builder, Type asType)
        {
            self.Loaded += (s, e) =>
            {
                builder.Cast(self.ParameterType, asType);
            };
            return self;
        }
    }
}
