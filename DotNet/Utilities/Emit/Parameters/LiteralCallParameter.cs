﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

namespace CKLib.Utilities.Emit.Parameters
{
    public class LiteralCallParameter : ICallParameter
    {
        #region Constants
        static readonly OpCode[] i4_codes = { OpCodes.Ldc_I4_0, OpCodes.Ldc_I4_1, OpCodes.Ldc_I4_2, OpCodes.Ldc_I4_3, OpCodes.Ldc_I4_4,
                                OpCodes.Ldc_I4_5, OpCodes.Ldc_I4_6, OpCodes.Ldc_I4_7, OpCodes.Ldc_I4_8 };
        #endregion

        public LiteralCallParameter()
        {
        }

        public LiteralCallParameter(object value)
        {
            Value = value;
        }

        public CallParameterType CallParameterType
        {
            get
            {
                return CallParameterType.Literal;
            }
        }

        public Type ParameterType
        {
            get
            {
                if (Value == null)
                {
                    return typeof(void);
                }

                return Value.GetType();
            }
        }

        public object Value
        {
            get;
            set;
        }

        public void Load(ILGenerator ilGen)
        {
            if (Value == null)
            {
                ilGen.Emit(OpCodes.Ldnull);
            }
            else
            {
                Type typ = Value.GetType();
                TypeCode code = Type.GetTypeCode(typ);
                switch (code)
                {
                    case TypeCode.Empty:
                    case TypeCode.DBNull:
                        ilGen.Emit(OpCodes.Ldnull);
                        break;
                    case TypeCode.Boolean:
                        ilGen.Emit((bool)Value ? OpCodes.Ldc_I4_1 : OpCodes.Ldc_I4_0);
                        break;
                    case TypeCode.Char:
                    case TypeCode.SByte:
                    case TypeCode.Byte:
                    case TypeCode.Int16:
                    case TypeCode.UInt16:
                    case TypeCode.Int32:
                    case TypeCode.UInt32:
                        LoadI4(ilGen);
                        break;
                    case TypeCode.Int64:
                    case TypeCode.UInt64:
                        ilGen.Emit(OpCodes.Ldc_I8, Convert.ToInt64(Value));
                        break;
                    case TypeCode.Single:
                        ilGen.Emit(OpCodes.Ldc_R4, (float)Value);
                        break;
                    case TypeCode.Double:
                        ilGen.Emit(OpCodes.Ldc_R8, (double)Value);
                        break;
                    case TypeCode.String:
                        ilGen.Emit(OpCodes.Ldstr, (string)Value);
                        break;
                    case TypeCode.Decimal:
                    case TypeCode.DateTime:
                    case TypeCode.Object:
                        if (typeof(Type).IsAssignableFrom(typ))
                        {
                            MethodInfo getTypeInfo = typeof(Type).GetMethod("GetTypeFromHandle");
                            ilGen.Emit(OpCodes.Ldtoken, typ);
                            ilGen.Emit(OpCodes.Call, getTypeInfo);
                            break;
                        }
                        else if (typ.IsEnum)
                        {
                            Type underlyingType = Enum.GetUnderlyingType(typ);
                            object underlyingValue = Convert.ChangeType(Value, underlyingType);
                            new LiteralCallParameter(underlyingValue).Load(ilGen);
                            break;
                        }

                        throw new InvalidOperationException(
                            string.Format("Invalid literal specified: {0}, of type: {1}", Value, Value.GetType()));
                    default:
                        throw new InvalidOperationException(
                            string.Format("Invalid literal specified: {0}, of type: {1}", Value, Value.GetType()));
                }
            }

            OnLoaded(ilGen);
        }

        private void LoadI4(ILGenerator ilGen)
        {
            int i4 = Convert.ToInt32(Value);
            if (i4 == -1)
            {
                ilGen.Emit(OpCodes.Ldc_I4_M1);
            }
            else if (i4 >= 0 && i4 < i4_codes.Length)
            {
                ilGen.Emit(i4_codes[i4]);
            }
            else
            {
                ilGen.Emit(OpCodes.Ldc_I4, i4);
            }
        }

        protected virtual void OnLoaded(ILGenerator ilGen)
        {
            EventHandler<LoadEventArgs> h = Loaded;
            if (h != null)
            {
                h(this, new LoadEventArgs(ilGen));
            }
        }

        public event EventHandler<LoadEventArgs> Loaded;
    }
}
