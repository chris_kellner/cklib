﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

namespace CKLib.Utilities.Emit.Parameters
{
    public class MethodCallParameter : ITargetedCallParameter
    {
        public ICallParameter Target { get; set; }
        public MethodInfo Method { get; set; }
        public ICallParameter[] Parameters { get; set; }

        public MethodCallParameter()
        {
        }

        public MethodCallParameter(Type type, string name, params ICallParameter[] parameters)
        {
            Method = type.GetMethod(
                name,
                BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static,
                null,
                parameters.Select(p => p.ParameterType).ToArray(),
                null);
            Parameters = parameters;
        }

        public MethodCallParameter(ICallParameter target, string name, params ICallParameter[] parameters)
        {
            Target = target;
            Method = target.ParameterType.GetMethod(
                name,
                BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static,
                null,
                parameters.Select(p => p.ParameterType).ToArray(),
                null);
            Parameters = parameters;
        }

        /// <summary>
        /// Indicates whether or not the return value of the call should be removed from the stack automatically.
        /// </summary>
        public bool AutoPopResult { get; set; }

        public CallParameterType CallParameterType
        {
            get { return CallParameterType.MethodResult; }
        }

        public Type ParameterType { get { return Method.ReturnType; } }

        public void Load(System.Reflection.Emit.ILGenerator ilGen)
        {
            if (Target != null)
            {
                Target.Load(ilGen);
            }

            if (Parameters != null)
            {
                for (int i = 0; i < Parameters.Length; i++)
                {
                    Parameters[i].Load(ilGen);
                }
            }

            if (Method.IsStatic)
            {
                ilGen.Emit(OpCodes.Call, Method);
            }
            else
            {
                ilGen.Emit(OpCodes.Callvirt, Method);
            }

            if (AutoPopResult && Method.ReturnType != typeof(void))
            {
                ilGen.Emit(OpCodes.Pop);
            }

            OnLoaded(ilGen);
        }

        protected virtual void OnLoaded(ILGenerator ilGen)
        {
            EventHandler<LoadEventArgs> h = Loaded;
            if (h != null)
            {
                h(this, new LoadEventArgs(ilGen));
            }
        }

        public event EventHandler<LoadEventArgs> Loaded;
    }
}
